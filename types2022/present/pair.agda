{-# OPTIONS --rewriting --confluence #-}
open import Function
open import Data.Bool
open import Data.Nat
open import Data.Fin
open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}


-- ℕ × ℕ
p : (b : Bool) → if b then ℕ else ℕ
p true = 5
p false = 3

-- Fin : ℕ → Set
-- Σ ℕ Fin
postulate
  a : ℕ

dp : (b : Bool) → if b then ℕ else Fin a

postulate
  thm : a ≡ dp true
  {-# REWRITE thm #-}

dp true = 5
dp false = # 3
