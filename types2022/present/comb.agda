{-# OPTIONS --type-in-type #-}

Pi : (A : Set) → (A → Set) → Set
Pi A B = (a : A) → B a

K- : ∀ {A B : Set} → A → B → A
K- x y = x

S- : ∀ {A B C : Set} → (A → B → C) 
   → (A → B) → A → C
S- f g x = f x (g x)

C- :  ∀ {A B C : Set} → (A → B → C) → (B → A → C)
C- f x y = f y x

Arr : Set → Set → Set
Arr A B = Pi A (K- B)



K : ∀ {A : Set}{B : A → Set}
  → Pi A (λ a → Pi (B a) (K- A))
K x y = x

















 





K′ : ∀ {A : Set}{B : A → Set}
   → Pi A (S- (K- (C- Arr A)) B)
K′ x y = x

