---
title: "The Münchhausen method and combinatory type theory"
author: "Thorsten Altenkirch, Ambrus Kaposi, <u>Artjoms Šinkarovs</u>, Tamás Végh"
date: "23 June 2022"

transition: "linear"
theme: "white"
center: "false"
margin: 0.1
width: 960
height: 720
---



# Introduction

#

<center>
<img  src="img/Dore-munchausen-illustration.jpg"/ >
</center>

#

<center>
<img src="img/munchausen-pull.png" />
</center>

# Type Theory

. . . 

<div style="font-size:300%">
  \begin{equation}
  U : U
  \end{equation}
</div>

. . . 

<div style="font-size:300%">
  \begin{equation}
  U : 1 + F\ U
  \end{equation}
</div>


# Products As Functions

<div style="font-size:140%">
  \begin{equation}
  A \times B \cong (b : \text{Bool})
  \to \text{if}\ b\ \text{then}\ A\ \text{else}\ B
  \end{equation}
</div>

. . . 

<div style="font-size:140%">
  \begin{equation}
  \Sigma\ A\ B \cong (b : \text{Bool})
  \to \text{if}\ b\ \text{then}\ A\ \text{else}\ B\ ?
  \end{equation}
</div>

#

<div style="font-size:190%">
  \begin{align}
  f & : (b : \text{Bool}) \\
    & \to \text{if}\ b\ \text{then}\ A\ \text{else}\ B\ (f\ true)
  \end{align}
</div>


# Demo

# Summary of the Method

* Algebraic definitions (objects + equations)
* Declarations for objects that cannot be defined right now
* Bootstraping with equations


# Type Theory without Types

<!--
```
{-# OPTIONS --type-in-type --rewriting #-}
module _ where

open import Relation.Binary.PropositionalEquality hiding ([_])
{-# BUILTIN REWRITE _≡_ #-}
module Ex where
```
-->

```

  postulate
    Con : Set
    Ty  : Con → Set
    Tm  : (Γ : Con) → Ty Γ → Set
    U   : ∀ {Γ} → Ty Γ
    TmU : ∀ {Γ} → Ty Γ ≡ Tm Γ U
    {-# REWRITE TmU #-}

```

## 

<!--
```
module Ex′ where
```
-->

```
  data Con : Set
  postulate Ty : Con → Set
  data Con where
    ∙   : Con
    _▹_ : (Γ : Con) → Ty Γ → Con

  data Tm : (Γ : Con) → Ty Γ → Set
  U′ : ∀ {Γ} → Ty Γ

  postulate TmU : ∀ {Γ} → Ty Γ ≡ Tm Γ U′
            {-# REWRITE TmU #-}

  data Tm where
    U : ∀ {Γ} → Ty Γ
    Π : ∀ {Γ} → (a : Ty Γ) → Ty (Γ ▹ a) → Ty Γ
    lam : ∀ {Γ a b} → Tm (Γ ▹ a) b → Tm Γ (Π a b)
    -- ...
  U′ = U


```

<!--
# Type Theory without Contexts

module Ex₁ where
  postulate
    Con : Set
    Sub : Con → Con → Set
    _∘_   : ∀{Γ Δ Σ} → Sub Δ Σ → Sub Γ Δ → Sub Γ Σ
    Ty  : Con → Set
    Tm  : (Γ : Con) → Ty Γ → Set
    ∙ : Con
    _▹_  : (Γ : Con) → Ty Γ → Con
    ⊤ : ∀ {Γ} → Ty Γ
    tt : ∀ {Γ} → Tm Γ ⊤
    Σ  : ∀ {Γ} → (a : Ty Γ) → Ty (Γ ▹ a) → Ty Γ

    Con≡ : Con ≡ Ty ∙
    {-# REWRITE Con≡ #-}

    _[_]T : ∀ {Δ Γ} → Ty Γ → Sub Δ Γ → Ty Δ
    _[_]t : ∀ {Δ Γ A} → Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)

    π : ∀ {Γ a} → Sub (Γ ▹ a) Γ

    π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Sub Γ (Δ ▹ A)) → Tm Γ (A [ π ∘ σ ]T)

    ε : ∀ {Γ} → Sub Γ ∙
    q : ∀ {Γ a} → Tm (Γ ▹ a) (a [ π ]T)

    Sub≡ : ∀ {Δ Γ} → Sub Δ Γ ≡ Tm Δ (Γ [ ε ]T)
    {-# REWRITE Sub≡ #-}

    _,_ : ∀ {Γ a b} → Tm Γ a → Tm (Γ ▹ a) b → Tm Γ (Σ a b)

    ▹≡ : ∀ {Δ a} → Δ ▹ a ≡ Σ Δ (a [ ? ]T) 

    --∙≡ : ∙ ≡ ⊤
    --{-# REWRITE ∙≡ #-}
-->

# Type Theory without Contexts

* A model of dependently typed system:
<!--
```
module DTT where
  postulate
```
-->

```
    Ty : Set
    Tm : Ty → Set
    U : Ty
```

* Π-type, dependent S and K combinators.



## Dependent Combinators
<!--
```
module CTT where
```
-->
```
  Pi : (A : Set) → (A → Set) → Set
  Pi A B = (a : A) → B a

  K : ∀ {A : Set}{B : A → Set}
    → (a : A) → B a → A
  K x y = x

  S : ∀ {A : Set}{B : A → Set}
        {C : (a : A) → B a → Set}
    → ((a : A) → (b : B a) → C a b)
    → (f : (a : A) → B a)
    → ((a : A) → C a (f a))
  S f g x = f x (g x)

```


<!--
```
module Ex₃ where
  infixr 5 _⇒_
  infixl 10 _$_
```
-->

## First Attempt

```
  postulate
    Ty  : Set
    Tm  : Ty → Set
    U   : Ty
    TmU : Ty ≡ Tm U
    {-# REWRITE TmU #-}
    _⇒_ : Ty → Ty → Ty
    _$_ : ∀ {A}{B} → Tm (A ⇒ B) → Tm A → Tm B
    |⇒| : Tm (U ⇒ U ⇒ U)
    ∅⇒  : ∀ {A B} → A ⇒ B ≡ |⇒| $ A $ B
```

## First Problem

When adding implicit variables, we get:
```
  -- ∅⇒ : {A B} → A ⇒ B ≡ |⇒| $ A $ B

  -- A ⇒ B ≡ |⇒| $ A $ B
  --       ≡ _$_ {U} {U} (|⇒| $ A) B
  --       ≡ _$_ {U} {U} (_$_ {U} {U ⇒ U} |⇒| A) B

```

## Family

<!--
```
module Ex₄ where
  infixr 5 _⇒_
  infixl 10 _$_
  infixl 10 _$f_
  
```
-->
```
  postulate
    Ty : Set
    Tm : Ty → Set

  variable X Y Z : Ty

  postulate
    U : Ty

    Tm-U : Tm U ≡ Ty
    {-# REWRITE Tm-U #-}

    Fam : Ty → Ty   -- (X → U)
    _$f_ : Tm (Fam X) → Tm X → Ty
```

## Family (2)

```
    Pi : (X : Ty) → Tm (Fam (Fam X))
    _$_ : {X : Ty}{Y : Tm (Fam X)}
        → Tm (Pi X $f Y) → (a : Tm X) → Tm (Y $f a)

    Kf : (Y : Ty) → Tm (Fam X)  -- Kf Y ≡ λ a → Y
    Kf$ : ∀ {a : Tm X} → _$f_ {X} (Kf Y) a ≡ Y
    {-# REWRITE Kf$ #-}

  _⇒_ : (X Y : Ty) → Ty
  X ⇒ Y = Pi X $f (Kf Y)

```

## Family (3)

```
  postulate -- Dependent K

   Yx⇒Z : ∀ X (Y : Tm (Fam X)) → Ty → Tm (Fam X)
   Yx⇒Z$ : ∀ X Y Z {x : Tm X} 
         → Yx⇒Z X Y Z $f x ≡ Y $f x ⇒ Z
   {-# REWRITE Yx⇒Z$ #-}

   Kd : {Y : Tm (Fam X)} → Tm (Pi X $f Yx⇒Z X Y X)
   Kd$ : ∀ {Y : Tm (Fam X)}{x : Tm X}
           {y : Tm (Y $f x)}
       → Kd {X = X}{Y = Y} $ x $ y ≡ x
   {-# REWRITE Kd$ #-}

```

## Family (4)

<!--
```
  postulate -- Dependent S helpers
    Yx⇒U : ∀ X (Y : Tm (Fam X)) → Tm (Fam X)
  
    Yx⇒Zx : ∀ X (Y : Tm (Fam X)) 
           → (Z : Tm (Pi X $f Yx⇒U X Y)) → Tm (Fam X)
  
    -- (X : Set) (Y : X → Set) (Z : (x : X) (Y x) → Set)
    -- (g : (x : X) → Y x) → X → Set
    -- λ x → Z x (g x)
    Zx[gx] : ∀ X (Y : Tm (Fam X)) 
              (Z : Tm (Pi X $f Yx⇒U X Y))
          → (g : Tm (Pi X $f Y)) → Tm (Fam X)
  
    X⇒Zx[gx] : ∀ X (Y : Tm (Fam X)) 
             (Z : Tm (Pi X $f Yx⇒U X Y))
           → Tm (Fam (Pi X $f Y))

```
-->
```
    Yx⇒U$ : ∀ X Y {x : Tm X} 
         → Yx⇒U X Y $f x ≡ Fam (Y $f x)
    {-# REWRITE Yx⇒U$ #-}

    Yx⇒Zx$ : ∀ X Y Z {x : Tm X} 
          → Yx⇒Zx X Y Z $f x ≡ Pi (Y $f x) $f (Z $ x)
    {-# REWRITE Yx⇒Zx$ #-}

    Zx[gx]$ : ∀ X Y Z g {x} 
           → Zx[gx] X Y Z g $f x ≡ Z $ x $f (g $ x)
    {-# REWRITE Zx[gx]$ #-}

    X⇒Zx[gx]$ : ∀ X Y Z g 
              → X⇒Zx[gx] X Y Z $f g 
                ≡ Pi X $f (Zx[gx] X Y Z g)
    {-# REWRITE X⇒Zx[gx]$ #-}
```

## Family (5)

```
  postulate
   -- Dependent S
   Sd : {Y : Tm (Fam X)}{Z : Tm (Pi X $f Yx⇒U X Y)} 
      → Tm (Pi X $f Yx⇒Zx X Y Z 
            ⇒ (Pi (Pi X $f Y) $f (X⇒Zx[gx] X Y Z)))
   Sd$ : {Y : Tm (Fam X)}{Z : Tm (Pi X $f Yx⇒U X Y)}
       → {f : Tm (Pi X $f Yx⇒Zx X Y Z) }
       → {g : Tm (Pi X $f Y)}
       → {x : Tm X}
       → Sd $ f $ g $ x ≡ f $ x $ (g $ x)
   {-# REWRITE Sd$ #-}

```


## Remaining Bits

```xxx
Π : UΠU((S((SKΠ)(SΠ(KKU))))(KKU))
K : ΠU((S((SKΠ)(SΠ(KKU))))((S((SKS)((SKK)Π)))
       ((S(K(S((SKS)(SKΠ)))))((SKK)((SKK)K)))))
S : ΠU((S((SKΠ)(SΠ(KKU))))((S((SKS)((S(K(SKΠ)))
       ((S((SKS)((SKK)Π)))(K((S((SKS)(SKΠ)))
       (K(KKU))))))))((S((SKS)((S(K(SKS)))
       ((S(K(S(K(SKΠ)))))((S((SKS)((SKK)((SKS)
       ((SKK)Π)))))(K((SKS)(SKΠ))))))))
       ((S(K(S(K(SKK)))))((S((SKS)((S(K(SKS)))
       ((S(K(SKK)))((S(K(SKΠ)))Π)))))((SKK)
       ((S((SKS)((SKK)((SKS)((SKK)Π)))))KS)))))))
```


# Summary

* Münchhausen method
* Works on simple examples
* Fails in some cases when using rewrite rules
* Good progress with Combinatory TT
* Algebraic definition of simply-typed SKI↔Λ (side project)


# Thanks
