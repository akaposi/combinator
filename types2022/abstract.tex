\documentclass{easychair}
%\documentclass[EPiC]{easychair}
%\documentclass[EPiCempty]{easychair}
%\documentclass[debug]{easychair}
%\documentclass[verbose]{easychair}
%\documentclass[notimes]{easychair}
%\documentclass[withtimes]{easychair}
%\documentclass[a4paper]{easychair}
%\documentclass[letterpaper]{easychair}

\usepackage{doc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{hyperref}
\usepackage{stackengine} % for \RaU

% use this if you have a long article and want to create an index
% \usepackage{makeidx}

% In order to save space or manage large tables or figures in a
% landcape-like text, you can use the rotating and pdflscape
% packages. Uncomment the desired from the below.
%
% \usepackage{rotating}
% \usepackage{pdflscape}

% Some of our commands for this guide.
%

%\makeindex

%% Front Matter
%%
% Regular title as in the article class.
%
\title{The Münchhausen method and combinatory type theory}

% Authors are joined by \and. Their affiliations are given by \inst, which indexes
% into the list defined using \institute
%
\author{
  Thorsten Altenkirch\inst{1}
  \and
  Ambrus Kaposi\inst{2}\thanks{Ambrus Kaposi was supported by the ``Application Domain Specific Highly Reliable IT Solutions''
    project which has been implemented with support from the
    National Research, Development and Innovation Fund of Hungary,
    financed under the Thematic Excellence Programme TKP2020-NKA-06
    (National Challenges Subprogramme) funding scheme, and by Bolyai Scholarship BO/00659/19/3.}
  \and
  Artjoms {\v{S}}inkarovs\inst{3}\thanks{%
    This work is supported by the Engineering and Physical Sciences Research Council
    through the grant EP/N028201/1.}
  \and
  Tamás Végh\inst{2}
}

% Institutes for affiliations are also joined by \and,
\institute{
  School of Computer Science, University of Nottingham, UK\\
  \email{psztxa@nottingham.ac.uk}
  \and
  E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest, Hungary\\
  \email{\{akaposi,vetuaat\}@inf.elte.hu}
  \and
  Heriot-Watt University, Scotland, UK \\
  \email{a.sinkarovs@hw.ac.uk}
 }

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair

\authorrunning{Altenkirch, Kaposi, {\v{S}}inkarovs, Végh}

% \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{The Münchhausen method and combinatory type theory}

\begin{document}

\maketitle

\begin{abstract}
  In one of his tall tales, Baron Münchhausen pulled himself out of a
  swamp by his own hair. Inspired by this, we present a technique to
  justify ``very dependent types'': terms with types that include the
  term itself. The Münchhausen method is an informal way to make this
  precise. We don't have to resort to untyped preterms or typing
  relations, the technique works in a completely algebraic setting
  (such as categories with families). We present the method through a
  series of examples.
\end{abstract}

% The table of contents below is added for your convenience. Please do not use
% the table of contents if you are preparing your paper for publication in the
% EPiC Series or Kalpa Publications series

% \setcounter{tocdepth}{2}
% {\small
% \tableofcontents}

%\section{To mention}
%
%Processing in EasyChair - number of pages.
%
%Examples of how EasyChair processes papers. Caveats (replacement of EC
%class, errors).

\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
\newcommand{\Bool}{\mathsf{Bool}}
\newcommand{\true}{\mathsf{true}}
\newcommand{\Type}{\mathsf{Type}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\RaK}{\Rightarrow_{\mathsf{K}}}
% \newcommand{\RaU}{\Rightarrow\hspace{-0.5em}\mathsf{U}}
\newcommand{\RaU}{\ensurestackMath{\stackengine{0pt}{\Ra}{\hspace{1.3em}\U}{O}{c}{F}{F}{L}}}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Sub}{\mathsf{Sub}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\U}{\mathsf{U}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\p}{\mathsf{p}}
\newcommand{\q}{\mathsf{q}}
\newcommand{\app}{\mathbin{\$}}
\renewcommand{\S}{\mathsf{S}}
\newcommand{\K}{\mathsf{K}}
\newcommand{\Kf}{\mathsf{K}_{\mathsf{f}}}

%------------------------------------------------------------------------------

\subsection*{A dependent version of ``products as functions from Bool''}

There is a well known type isomorphism
\[
A \times B \,\,\cong\,\, (b:\Bool) \ra \mathsf{if}\, b \,\mathsf{then}\, A \,\mathsf{else}\, B.
\]
Can we turn the nondependent product into a $\Sigma$ type? Given
$A:\Type$, $B:A\ra\Type$, we want something like
\[
\Sigma\,A\,B \,\,\cong\,\, (b:\Bool)\ra \mathsf{if}\, b \,\mathsf{then}\, A \,\mathsf{else}\, (B\,\square),
\]
but we don't know what to put in the placeholder $\square$. It should
be the output of the function when the input is $b=\true$. Once the
function is given a name, we can refer to it:
\[
f : (b:\Bool)\ra \mathsf{if}\, b \,\mathsf{then}\, A \,\mathsf{else}\, (B\,(f\,\true))
\]
This is sometimes called a ``very dependent type''
\cite{Hickey96formalobjects}: the term $f$ appears in its own type. It
is possible to make sense of such a type using preterms and typing
relations \cite{DBLP:journals/jfp/Barendregt91}, but we can also
present it algebraically as follows.
\begin{alignat*}{10}
  & a_0 && : A \\
  & f   && : (b : \Bool)\ra \mathsf{if}\, b \,\mathsf{then}\, A \,\mathsf{else}\, (B\,a_0) \\
  & \cancel{a_0} && : a_0 = f\,\true
\end{alignat*}
We first declare $a_0$ as the extra component that the type of $f$
will depend on. Then we can declare $f$ itself with the help of $a_0$. Finally, knowing about
$f$, we can equate $a_0$ away: after learning about $\cancel{a_0}$, we
know that the type of $f$ is $(b : \Bool)\ra \mathsf{if}\, b
\,\mathsf{then}\, A \,\mathsf{else}\, (B\,(f\,\true))$. We only needed
$a_0$ for bootstrapping the type of $f$.

%------------------------------------------------------------------------------

\subsection*{Type theory without types}

A well-known example of the same technique is equating the sort of
types away. We declare the sorts and operations of type theory as
follows.
\begin{alignat*}{10}
  & \Con         && : \Type \\
  & \Ty          && : \Con\ra\Type \\
  & \Tm          && : (\Gamma:\Con)\ra\Ty\,\Gamma\ra\Type \\
  & \U           && : \Ty\,\Gamma \\
  & \cancel{\Ty} && : \Ty\,\Gamma = \Tm\,\Gamma\,\U
\end{alignat*}
We have to introduce types, but once we have $\cancel{\Ty}$, we know
that types are just terms of type $\U$. Note that such a theory would
be inconsistent through Russell's paradox, but it is easy to fix this by
stratification (adding natural number indices to $\Ty$ and $\U$, see
\cite{DBLP:conf/csl/Kovacs22}). Thus Münchhausen provides an algebraic
way to define universes {\`a} la Russell, that is, a type theory
without types: we use types only for bootstrapping.

%------------------------------------------------------------------------------

\subsection*{Type theory without contexts}

Just as we equated types away, we can do the same with contexts and
substitutions. We start with the signature for categories with
families (CwF \cite{DBLP:journals/corr/abs-1904-00827}) with the four
sorts $\Con:\Type$, $\Sub:\Con\ra\Con\ra\Type$, $\Ty:\Con\ra\Type$,
$\Tm:(\Gamma:\Con)\ra\Ty\,\Gamma\ra\Type$, empty context
$\diamond:\Con$, context extension $\blank\rhd\blank :
(\Gamma:\Con)\ra\Ty\,\Gamma\ra\Con$, and we have $\top$ and $\Sigma$
types. Then we add equations such as $\Con = \Ty\,\diamond$,
$\Sub\,\Delta\,\Gamma = \Tm\,\Delta\,(\Gamma[\epsilon])$,
$\sigma\circ\delta = \sigma[\delta]$, $\Gamma\rhd A =
\Sigma\,\Gamma\,(A[\mathsf{q}])$. In the end we have e.g.\ $\Tm :
(\Gamma:\Ty\,\top)\ra\Ty\,\Gamma\ra\Type$ and $\Sigma :
(A:\Ty\,\Gamma)\ra\Ty\,(\Sigma\,\Gamma\,(A[\q]))\ra\Ty\,\Gamma$.

The language with all these equations can be justified: any CwF with
$\top$ and $\Sigma$ can be turned into an equivalent CwF which
satisfies all these equations. There is an analogous model
construction for type theory with types, and the two model
constructions can be combined.

%------------------------------------------------------------------------------

\subsection*{Towards type theory without contexts for real}

Simply typed combinator calculus is a language where contexts are not
even present for bootstrapping \cite{DBLP:books/cu/HindleyS86}. There
are no variables, function space is built-in and the combinators $\S$
and $\K$ are used to define functions. Due to the technical challenges
which come with not being able to use variables \cite{conor},
combinator calculus was never extended to dependent types. We are in
the process of defining a combinatory (dependent) type theory using
Münchhausen's technique.

We first introduce types $\Ty:\Type$, terms indexed by types
$\Tm:\Ty\ra\Type$, a universe $\U:\Ty$ with the equation $\Ty =
\Tm\,\U$, then we introduce families $\blank\RaU : \Ty\ra\Ty$ with
instantiation $\blank\app\blank : \Tm\,(A \RaU)\ra\Tm\,A\ra\Ty$. Now
we are in the position of declaring dependent function space $\Pi :
(A:\Ty)\ra\Tm\,((A\RaU)\RaU)$ and application $\blank\cdot\blank :
\Tm\,(\Pi\,A \app B)\ra(a:\Tm\,A)\ra\Tm\,(B\app a)$. We introduce
constant families $\Kf : \Ty\ra\Tm\,(B\RaU)$ with the equation $\Kf\,A
\app b = A$ which allows us to express the non-dependent function type
$A\Ra B := \Pi\,A\app (\Kf\,B)$. Using a helper combinator
$\blank\RaK\blank : \Tm\,(A\RaU)\ra\Ty\ra\Tm\,(A\RaU)$ with equation
$(B\RaK C) \app a = B \app a \Ra C$, we can can express the dependent
version of the $\K$ combinator $\K : \Tm\,(\Pi\,A \app B\RaK A)$ with
equation $\K \cdot a \cdot b = a$. We can do the same for the
dependently typed $\S$ combinator.

Our goal is to show that the syntax of combinatory type theory is
equivalent to the syntax of CwF with $\top$, $\Sigma$, $\Pi$ and
universes.

\label{sect:bib}
\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{references}

%------------------------------------------------------------------------------

\end{document}
