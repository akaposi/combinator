{-# OPTIONS --cubical #-}
open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Transport
open import Cubical.Data.Empty
open import Cubical.Data.Sigma

open import Syntax.Ty

module Syntax.C.Norm where

open import Syntax.C T'
open TY T'

data Nf : Ty → Set where
  K₀ : Nf (τ ⇒ σ ⇒ τ)
  K₁ : Nf τ → Nf (σ ⇒ τ)
  S₀ : Nf ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  S₁ : Nf (τ ⇒ σ ⇒ δ) → Nf ((τ ⇒ σ) ⇒ τ ⇒ δ)
  S₂ : Nf (τ ⇒ σ ⇒ δ) → Nf (τ ⇒ σ) → Nf (τ ⇒ δ)
  XlamKβ  : _≡_ {A = Nf ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
               (S₂ (K₁ S₀) (S₁ (K₁ K₀)))
               K₀
  lamSβ  : _≡_ {A = Nf ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
               (S₂ (K₁ (S₁ (K₁ S₀))) (S₂ (K₁ S₀) (S₁ (K₁ S₀))))
               (S₂ (S₂ (K₁ S₀) (S₂ (K₁ K₀) (S₂ (K₁ S₀) (S₂ (K₁ (S₁ (K₁ S₀))) S₀)))) (K₁ S₀))
  lamwk$ : _≡_ {A = Nf ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
               (S₁ (K₁ K₀))
               (S₂ (S₂ (K₁ S₀) (S₂ (K₁ K₀) (S₂ (K₁ S₀) K₀))) (K₁ K₀))
  η      : _≡_ {A = Nf ((τ ⇒ σ) ⇒ τ ⇒ σ)}
               (S₂ (S₂ (K₁ S₀) K₀) (K₁ (S₂ K₀ (K₀ {σ = δ}))))
               (S₂ K₀ (K₀ {σ = δ}))
  NfSet  : isSet (Nf τ)


⌜_⌝ : Nf τ → Tm τ
⌜ K₀ ⌝ = K
⌜ K₁ n ⌝ = K $ ⌜ n ⌝
⌜ S₀ ⌝ = S
⌜ S₁ n ⌝ = S $ ⌜ n ⌝
⌜ S₂ m n ⌝ = S $ ⌜ m ⌝ $ ⌜ n ⌝
⌜ XlamKβ i ⌝ = lamKβ i
⌜ lamSβ i ⌝ = lamSβ i
⌜ lamwk$ i ⌝ = lamwk$ i
⌜ η {δ = δ} i ⌝ = η {δ = δ} i
⌜ NfSet m n x y i j ⌝ = TmSet ⌜ m ⌝ ⌜ n ⌝ (cong ⌜_⌝ x) (cong ⌜_⌝ y) i j

P : (τ : Ty) → Tm τ → Set
q : (τ : Ty) → ∀{t} → P τ t → Nf τ
q= : (τ : Ty) → ∀{t}(pt : P τ t) → ⌜ q τ pt ⌝ ≡ t
P base t = ⊥
P (τ ⇒' σ) t = ({u : Tm τ} → P τ u → P σ (t $ u)) × Σ (Nf (τ ⇒ σ)) λ n → ⌜ n ⌝ ≡ t
q (τ ⇒' σ) (_ , (n , _)) = n
q= (τ ⇒' σ) (_ , (_ , n=)) = n=

-- fund : ∀{τ}(t : Tm τ) → P τ t
-- fund (K {τ = τ}) = (λ {u} pu → (λ {v} pv → transport⁻ (λ i → P τ (Kβ {x = u}{v} i)) pu) , (K₁ (q τ pu) , cong (K $_) (q= τ pu))) , (K₀ , refl)
-- fund (S {τ = τ}{δ = δ}) = (λ {u} pu → (λ {v} pv → (λ {w} pw → transport⁻ (λ i → P δ (Sβ {x = u}{v}{w} i)) (fst (fst pu pw) (fst pv pw))) , (S₂ (q _ pu) (q _ pv) , cong₂ (λ x y → S $ x $ y) (q= _ pu) (q= _ pv))) , (S₁ (q _ pu) , cong (S $_) (q= _ pu))) , (S₀ , refl)
-- fund (t $ u) = fst (fund t) (fund u) 
-- fund (Kβ {τ = τ}{x = u}{v} i) = toPathP {A = λ i → P τ (Kβ i)} (transportTransport⁻ (λ i → P τ (Kβ {x = u}{v} i)) (fund u)) i
-- fund (Sβ {τ = τ}{σ}{δ}{x = u}{v}{w} i) = toPathP {A = λ i → P δ (Sβ i)} (transportTransport⁻ (λ i → P δ (Sβ {x = u}{v}{w} i)) (fst (fst (fund u) (fund w)) (fst (fund v) (fund w)))) i
-- fund (lamKβ {ι = ι}{τ}{σ} i) = (λ { {u} (f , (n , p)) → (λ { {u′} (f′ , (n′ , p′)) → (λ { {u′′} t → 
--                                   let rr = subst ? lamKβ in ? }) , ? }) , ? }) , (XlamKβ i , refl) -- (λ {u} pu → (λ {v} pv → (λ {w} pw → {!!}) , {!!}) , ({!snd (snd pu) i!} , {!!})) , (lamKβ i , refl)
-- fund (lamSβ i) = {!!} , (lamSβ i , refl)
-- fund (lamwk$ i) = {!!} , (lamwk$ i , refl)
-- fund (η i) = {!!} , (η i , refl)
-- fund (TmSet u v x y i j) = {!!}



-- Let's try simpler version without the equality (PP == fst P)
PP : (τ : Ty) → Tm τ → Set
PP base t = ⊥
PP (τ ⇒' σ) t = ∀ {u : Tm τ} → PP τ u → PP σ (t $ u)

lamKβ′ : (f : Tm (ι ⇒ τ)) (g : Tm (ι ⇒ σ)) (x : Tm ι)
      → _≡_ { A = Tm τ } 
            (S $ (K $ S) $ (S $ (K $ K)) $ f $ g $ x)
            (K $ f $ g $ x)
lamKβ′ f g x = cong (λ t → t $ g $ x) Sβ 
            ∙ cong (λ t → t $ (S $ (K $ K) $ f) $ g $ x) Kβ
            ∙ Sβ
            ∙ cong (_$ (g $ x)) Sβ
            ∙ cong (λ t → t $ (f $ x) $ (g $ x)) Kβ
            ∙ Kβ
            ∙ sym (cong (_$ x) Kβ) 

PPlamKβ′ : (f : Tm (ι ⇒ τ)) (g : Tm (ι ⇒ σ)) (x : Tm ι)
         → PP _ (S $ (K $ S) $ (S $ (K $ K)) $ f $ g $ x)
           ≡ PP _ (K $ f $ g $ x)
PPlamKβ′ f g x = cong (PP _) (lamKβ′ f g x)



f : ∀ {τ} (t : Tm τ) → PP τ t
f (K {τ = τ}) {u} = λ pu {v} pv → transport⁻ (λ i → PP τ (Kβ {x = u}{v} i)) pu
f (S {τ = τ}{δ = δ}) {u} pu {v} pv {w} pw = transport⁻ (λ i → PP δ (Sβ {x = u}{v}{w} i)) (pu {w} pw {v $ w} (pv {w} pw))

f (t $ t₁) = (f t) {t₁} (f t₁)
f (Kβ {τ = τ}{σ}{x = u}{v} i) = 
    let 
      r = (transportTransport⁻ (λ i → PP τ (Kβ {x = u}{v} i)) (f u)) 
      p = toPathP {A = λ i → PP τ (Kβ {x = u}{v} i)} r
    in p i
f (Sβ {τ = τ}{σ}{δ}{x = u}{v}{w} i) = 
    let 
      t = (f u) {w} (f w) {v $ w} ((f v) {w} (f w))
      r = transportTransport⁻ (λ i → PP δ (Sβ {x = u}{v}{w} i)) t 
      p = toPathP {A = λ i → PP δ (Sβ {x = u}{v}{w} i)} r
    in p i
f (lamKβ {ι = ι}{τ}{σ} i) {tg} g {th} h {tt} t = ?
f (lamSβ i) = ?
f (lamwk$ i) = ?
f (η i) = ?
f (TmSet t t₁ x y i i₁) = ?

