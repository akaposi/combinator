{-# OPTIONS --cubical #-}
open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Transport
open import Cubical.Foundations.HLevels
open import Cubical.Data.Empty
open import Cubical.Data.Sigma

open import Syntax.Ty

module Syntax.C.LogPred where

open import Syntax.C T'
open import Syntax.C.Eval
open TY T'

-- interpretation into the predicate displayed model over the standard interpretation

PT : (A : Ty) → ⟦ A ⟧T → Set
PT (A ⇒' B) f = {a : ⟦ A ⟧T} → PT A a → PT B (f a)

isSetImplicitΠ : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'} → ((x : A) → isSet (B x)) → isSet ({x : A} → B x)
isSetImplicitΠ h f g F G i j {z} = h z (f {z}) (g {z}) (λ k → F k {z}) (λ k → G k {z}) i j

PTset : (A : Ty) → (a : ⟦ A ⟧T) → isSet (PT A a)
PTset (A ⇒' B) f = isSetImplicitΠ λ a → isSetΠ λ pa → PTset B (f a)

Pt : ∀{A}(t : Tm A) → PT A ⟦ t ⟧t
Pt K = λ x y → x
Pt S = λ f g x → f x (g x)
Pt (t $ u) = Pt t (Pt u)
Pt (Kβ {x = u} i) = Pt u
Pt (Sβ {x = u}{v}{w} i) = Pt u (Pt w) (Pt v (Pt w))
Pt (lamKβ i) = λ f g x → f x
Pt (lamSβ i) = λ f g h x → f x (h x) (g x (h x))
Pt (lamwk$ i) = λ f x y → f x
Pt (η i) = λ f x → f x
Pt (TmSet {A} a b c d i j) = {!PTset A ⟦ a ⟧t!} -- this should be easy
