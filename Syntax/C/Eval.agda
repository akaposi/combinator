{-# OPTIONS --cubical #-}
open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Transport
open import Cubical.Foundations.HLevels
open import Cubical.Data.Empty
open import Cubical.Data.Sigma

open import Syntax.Ty

module Syntax.C.Eval where

open import Syntax.C T'
open TY T'

-- evaluation into the standard model

⟦_⟧T : Ty → Set
⟦ base ⟧T = ⊥
⟦ A ⇒' B ⟧T = ⟦ A ⟧T → ⟦ B ⟧T

⟦_⟧Tset : (A : Ty) → isSet ⟦ A ⟧T
⟦ base ⟧Tset = isProp→isSet isProp⊥
⟦ A ⇒' B ⟧Tset = isSetΠ (λ _ → ⟦ B ⟧Tset)

⟦_⟧t : ∀{A} → Tm A → ⟦ A ⟧T
⟦ K ⟧t = λ x y → x
⟦ S ⟧t = λ f g x → f x (g x)
⟦ t $ u ⟧t = ⟦ t ⟧t ⟦ u ⟧t
⟦ Kβ {x = u} i ⟧t = ⟦ u ⟧t
⟦ Sβ {x = u}{v}{w} i ⟧t = ⟦ u ⟧t ⟦ w ⟧t (⟦ v ⟧t ⟦ w ⟧t)
⟦ lamKβ i ⟧t = λ f g x → f x
⟦ lamSβ i ⟧t = λ f g h x → f x (h x) (g x (h x))
⟦ lamwk$ i ⟧t = λ f x y → f x
⟦ η i ⟧t = λ f x → f x
⟦ TmSet {A} a b c d i j ⟧t = ⟦ A ⟧Tset ⟦ a ⟧t ⟦ b ⟧t (cong ⟦_⟧t c) (cong ⟦_⟧t d) i j
