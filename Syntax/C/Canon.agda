{-# OPTIONS --cubical #-}
open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Transport
open import Cubical.Foundations.HLevels
open import Cubical.Data.Empty
open import Cubical.Data.Sigma

open import Syntax.Ty

module Syntax.C.Canon where

open import Syntax.C T'
open import Syntax.C.Eval
open TY T'

-- canonicity (sconing, gluing over the global(?) section functor from the syntax to the standard model)

PT : (A : Ty) → Tm A → Set
PT base t = ⊥
PT (A ⇒' B) t = {u : Tm A} → PT A u → PT B (t $ u)

Pt : ∀{A}(t : Tm A) → PT A t
Pt (K {τ = τ}) {u} pu {v} pv = transport⁻ (λ i → PT τ (Kβ {x = u}{v} i)) pu
Pt (S {δ = δ}) {t} pt {u} pu {v} pv = transport⁻ (λ i → PT δ (Sβ {x = t}{u}{v} i)) (pt {v} pv {u $ v} (pu {v} pv))
Pt (t $ u) = Pt t {u} (Pt u)
Pt (Kβ {τ = τ}{x = u}{v} i) = toPathP {A = λ i → PT τ (Kβ {x = u}{v} i)} (transportTransport⁻ (λ i → PT τ (Kβ {x = u}{v} i)) (Pt u)) i
Pt (Sβ {τ = τ}{σ}{δ}{x = u}{v}{w} i) = toPathP {A = λ i → PT δ (Sβ {x = u}{v}{w} i)} (transportTransport⁻ (λ i → PT δ (Sβ {x = u}{v}{w} i)) (Pt u {w} (Pt w) {v $ w} (Pt v {w} (Pt w)))) i
Pt (lamKβ i) {t} pt {u} pu {v} pv = {!Pt S (Pt K {S} (Pt S)) (Pt S (Pt K {K} (Pt K))) pt pu pv!} -- this is horrible transport hell
Pt (lamSβ i) {t} pt {u} pu {v} pv {w} pw = {!!} -- pt {w} pw {v $ w} (pv {w} pw) {{!u $ w $ (v $ w)!}} (pu {w} pw {v $ w} (pv {w} pw))
Pt (lamwk$ i) = {!!} -- {t} pt {u} pu {v} pv = pt {u} pu
Pt (η i) = {!!} -- λ f x → f x
Pt (TmSet {A} a b c d i j) = {!PTset A ⟦ a ⟧t!} -- this should be easy


-- goal : PT τ (lamKβ i $ t $ u $ v)
-- i0  Pt (S $ (K $ S) $ (S $ (K $ K))) pt pu pv =
--     Pt S (Pt K (Pt S)) (Pt S (Pt K (Pt K))) pt pu pv =
-- i1  Pt K pt pu pv

