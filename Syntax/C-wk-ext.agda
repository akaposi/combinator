{-# OPTIONS --cubical --safe #-}
open import Cubical.Core.Primitives 
open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

module Syntax.C-wk-ext (T : TY) where

open import Syntax.Common T

infixl 6 _$_

data Tm : Con → Ty → Type where
  q : Tm (Γ ▹ τ) τ
  wk : Tm Γ σ → Tm (Γ ▹ τ) σ

  K : Tm Γ (τ ⇒ σ ⇒ τ)
  S : Tm Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_ : Tm Γ (τ ⇒ σ) → Tm Γ τ → Tm Γ σ
  Kβ : {x : Tm Γ τ}{y : Tm Γ σ} → K $ x $ y ≡ x
  Sβ : {x : Tm Γ (τ ⇒ σ ⇒ δ)}{y : Tm Γ (τ ⇒ σ)}{z : Tm Γ τ}
     → S $ x $ y $ z ≡ x $ z $ (y $ z)

  wk$ : {x : Tm Γ (τ ⇒ σ)}{y : Tm Γ τ} → wk {τ = δ} (x $ y) ≡ wk x $ wk y
  wkK : wk {Γ = Γ}{τ = δ} (K {τ = τ}{σ = σ}) ≡ K
  wkS : ∀ {γ} → wk {Γ = Γ}{τ = γ} (S {τ = τ}{σ = σ}{δ = δ}) ≡ S

  ext : {x y : Tm Γ (τ ⇒ σ)} → wk x $ q ≡ wk y $ q → x ≡ y

  TmSet : isSet (Tm Γ τ)

SKK : Tm Γ (τ ⇒ τ)
SKK {τ = τ} = S $ K $ K {σ = τ}

-- we derive the four extensionality equations: no idea here, we just use ext multiple times and applying the computation rules
lamKβ  : _≡_ {A = Tm ε ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
             (S $ (K $ S) $ (S $ (K $ K)))
             K
lamKβ {ι = ι}{τ = τ}{σ = σ} = ext (ext
  (cong (_$ q)
    (wk$ ∙ cong (λ x → wk {τ = ι ⇒ σ} x $ wk q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK)))) ∙
  cong (λ x → x $ wk q $ q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK))) ∙
  cong (_$ q) Sβ ∙
  cong (λ x → x $ (S $ (K $ K) $ wk q) $ q) Kβ ∙
  ext
    (cong (_$ q) (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk (wk q)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK))))) ∙
    Sβ ∙
    cong (_$ (wk q $ q)) Sβ ∙
    cong (λ x → x $ (wk (wk q) $ q) $ (wk q $ q)) Kβ ∙
    Kβ) ∙
  sym Kβ ∙
  cong (_$ q) (cong (_$ wk q) (sym wkK) ∙ sym wk$) ∙
  cong (λ x → wk x $ q) (cong (_$ q) (sym wkK))))

lamSβ  : _≡_ {A = Tm ε ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
             (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
             (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))
lamSβ = ext
  (cong (_$ q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)))) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)))) ∙
  Sβ ∙
  cong (_$ (S $ (K $ S) $ (S $ (K $ S)) $ q)) Kβ ∙
  ext
    (cong (_$ q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS))))) ∙
    Sβ ∙
    cong (_$ (S $ (K $ S) $ (S $ (K $ S)) $ wk q $ q)) Kβ ∙
    cong (S $_) (cong (_$ q) (Sβ ∙ cong (_$ (S $ (K $ S) $ wk q)) Kβ)) ∙
    ext
      (cong (_$ q) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk (wk q)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)))))) ∙
      ext
        (cong (_$ q) (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk (wk q)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk (wk (wk q))) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS))))))) ∙
        Sβ ∙
        cong (_$ (wk q $ q)) (Sβ ∙ cong (_$ (wk (wk q) $ q)) (Sβ ∙ cong (_$ (wk (wk (wk q)) $ q)) Kβ)) ∙
        Sβ ∙
        cong (wk (wk (wk q)) $ q $ (wk q $ q) $_) (sym Sβ) ∙
        cong (_$ (S $ wk (wk q) $ wk q $ q)) (sym Sβ) ∙
        sym Sβ ∙
        cong (_$ q) (sym (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong (_$ wk (wk (wk q))) wkS))) (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong (_$ wk (wk q)) wkS))))) ∙
      sym (Sβ ∙ cong (_$ (S $ wk q $ q)) (Sβ ∙ cong (_$ (S $ wk (wk q) $ q)) Kβ)) ∙
      cong (_$ q) (sym (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong (_$ wk (wk q)) wkS))) (wk$ ∙ cong (_$ wk q) wkS)))) ∙
    cong₂ (λ x y → S $ x $ y) (cong (_$ (S $ wk q)) (sym Kβ) ∙ sym Sβ) (cong (_$ q) (sym Kβ)) ∙
    cong (λ x → x $ (S $ (K $ (S $ (K $ S))) $ S $ wk q) $ (K $ S $ wk q $ q)) (sym Kβ) ∙
    cong (_$ (K $ S $ wk q $ q)) (sym (Kβ ∙ Sβ)) ∙
    cong (λ x → x $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S) $ wk q) $ q $ (K $ S $ wk q $ q)) (sym Kβ) ∙
    cong (λ x → x $ q $ (K $ S $ wk q $ q)) (sym Sβ) ∙
    sym Sβ ∙
    cong (_$ q) (sym (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ (cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)))) wkS)))))) (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ wkK wkS))))) ∙
  cong (λ x → x $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)) $ q) $ (K $ S $ q)) (sym Kβ) ∙
  cong (_$ (K $ S $ q)) (sym Sβ) ∙
  sym Sβ ∙
  sym (cong (_$ q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)))) wkS))))) (wk$ ∙ cong₂ _$_ wkK wkS))))

lamwk$ : _≡_ {A = Tm ε ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
             (S $ (K $ K))
             (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))
lamwk$ {τ = τ}{σ = σ}{ι = ι} = ext
  (cong (_$ q) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK)) ∙
  ext
    (cong (_$ q) (wk$ ∙ cong (_$ wk q) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK))) ∙
    Sβ ∙
    cong (_$ (wk q $ q)) Kβ ∙
    ext
      (cong (_$ q) (wk$ ∙ cong₂ _$_ wkK wk$) ∙
      Kβ ∙
      cong (wk (wk q) $_) (sym Kβ ∙ cong (_$ q) (cong (_$ wk q) (sym wkK) ∙ sym wk$)) ∙
      cong (_$ (wk (K $ q) $ q)) (sym Kβ) ∙
      sym Sβ ∙
      cong (_$ q) (sym (wk$ ∙ cong (_$ wk (K $ q)) (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk (wk q)) wkK))))) ∙
    cong (_$ (K $ q)) (sym Kβ) ∙
    sym Sβ ∙
    cong (_$ q) (sym (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk q) wkK)))) wkK))) ∙
  cong (S $ (K $ (S $ (K $ q))) $_) (sym Kβ) ∙
  cong (λ x → S $ x $ (K $ K $ q)) (sym (Sβ ∙ cong (_$ (S $ (K $ S) $ K $ q)) Kβ ∙ cong (K {σ = τ} $_) (Sβ ∙ cong (_$ (K $ q)) Kβ))) ∙
  cong (_$ (K $ K $ q)) (sym (Sβ ∙ cong (_$ (S $ (K $ K) $ (S $ (K $ S) $ K) $ q)) Kβ)) ∙
  sym Sβ ∙
  cong (_$ q) (sym (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkK)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) wkK)))) (wk$ ∙ cong₂ _$_ wkK wkK))))

η      : _≡_ {A = Tm ε ((τ ⇒ σ) ⇒ τ ⇒ σ)}
             (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = δ})))
             (S $ K $ K {σ = δ})
η {τ = τ}{σ = σ}{δ = δ} = ext
  (cong (_$ q) (wk$ ∙
  cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong₂ _$_ wkK wkS)) wkK)) (wk$ ∙ cong₂ _$_ wkK (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS wkK) wkK))) ∙
  Sβ ∙
  cong (_$ (K $ (S $ K $ K {σ = δ}) $ q)) (Sβ ∙ cong (_$ (K $ q)) Kβ) ∙
  cong (λ x → S $ (K $ q) $ x) Kβ ∙
  ext
    (cong (_$ q) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS (wk$ ∙ cong (_$ wk q) wkK)) (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS wkK) wkK)) ∙
    Sβ ∙
    cong (_$ (S $ K $ K {σ = δ} $ q)) Kβ ∙
    cong (wk q $_) (Sβ ∙ Kβ)) ∙
  sym (Sβ ∙ Kβ) ∙
  cong (_$ q) (sym (wk$ ∙ cong₂ _$_ (wk$ ∙ cong₂ _$_ wkS wkK) wkK)))

{-
lam : Tm (Γ ▹ τ) σ → Tm Γ (τ ⇒ σ)


lam {τ = τ} q = SKK
lam (wk x) = K $ x
lam K = K $ K
lam S = K $ S
lam (x $ x₁) = S $ lam x $ lam x₁
lam (Kβ {x = x}{y = y} i) =
  ext
    (wk (S $ (S $ (K $ K) $ lam x) $ lam y) $ q  ≡⟨ cong (_$ q) wk$ ⟩
     wk (S $ (S $ (K $ K) $ lam x)) $ wk (lam y) $ q  ≡⟨ cong (λ t → t $ wk (lam y) $ q) wk$ ⟩
     wk S $ wk (S $ (K $ K) $ lam x) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → t $ wk (S $ (K $ K) $ lam x) $ wk (lam y) $ q) wkS ⟩
     S $ wk (S $ (K $ K) $ lam x) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ t $ wk (lam y) $ q) wk$ ⟩
     S $ (wk (S $ (K $ K)) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (lam x)) $ wk (lam y) $ q) wk$ ⟩
     S $ (wk S $ wk (K $ K) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (K $ K) $ wk (lam x)) $ wk (lam y) $ q) wkS  ⟩
     S $ (S $ wk (K $ K) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (S $ t $ wk (lam x)) $ wk (lam y) $ q) wk$ ⟩
     S $ (S $ (wk K $ wk K) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (S $ (t $ wk K) $ wk (lam x)) $ wk (lam y) $ q) wkK ⟩
     S $ (S $ (K $ wk K) $ wk (lam x)) $ wk (lam y) $ q
        ≡⟨ cong (λ t → S $ (S $ (K $ t) $ wk (lam x)) $ wk (lam y) $ q) wkK ⟩
     S $ (S $ (K $ K) $ wk (lam x)) $ wk (lam y) $ q  ≡⟨ Sβ ⟩
     S $ (K $ K) $ wk (lam x) $ q $ (wk (lam y) $ q) ≡⟨ cong (_$ (wk (lam y) $ q)) Sβ ⟩
     K $ K $ q $ (wk (lam x) $ q) $ (wk (lam y) $ q) 
        ≡⟨ cong (λ t → t $ (wk (lam x) $ q) $ (wk (lam y) $ q)) Kβ ⟩
     K $ (wk (lam x) $ q) $ (wk (lam y) $ q) ≡⟨ Kβ ⟩
     (wk (lam x) $ q)
    ∎) i

lam (Sβ {x = x}{y = y}{z = z} i) =
  ext
    (wk (S $ (S $ (S $ (K $ S) $ lam x) $ lam y) $ lam z) $ q ≡⟨ cong (_$ q) wk$ ⟩
     wk (S $ (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → t $ (wk (lam z)) $ q) wk$ ⟩
     wk S $ (wk (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → t $ (wk (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q ) wkS ⟩
     S $ (wk (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ t $ (wk (lam z)) $ q) wk$ ⟩
     S $ (wk (S $ (S $ (K $ S) $ lam x)) $ (wk (lam y))) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ (t $ (wk (lam y))) $ (wk (lam z)) $ q ) wk$ ⟩
     S $ (wk S $ wk (S $ (K $ S) $ lam x) $ (wk (lam y))) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ (t $ wk (S $ (K $ S) $ lam x) $ (wk (lam y))) $ (wk (lam z)) $ q) wkS ⟩
     S $ (S $ wk (S $ (K $ S) $ lam x) $ (wk (lam y))) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ (S $ t $ (wk (lam y))) $ (wk (lam z)) $ q ) wk$ ⟩
     S $ (S $ (wk (S $ (K $ S)) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q 
        ≡⟨ cong (λ t → S $ (S $ (t $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q ) wk$ ⟩
     S $ (S $ (wk S $ wk (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (t $ wk (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q) wkS ⟩
     S $ (S $ (S $ wk (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (S $ t $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q) wk$ ⟩
     S $ (S $ (S $ (wk K $ wk S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (S $ (t $ wk S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q ) wkK ⟩
     S $ (S $ (S $ (K $ wk S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (S $ (K $ t) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q ) wkS ⟩
     S $ (S $ (S $ (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  ≡⟨ Sβ ⟩
     S $ (S $ (K $ S) $ wk (lam x)) $ wk (lam y) $ q $ (wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (wk (lam z) $ q)) Sβ ⟩
     S $ (K $ S) $ wk (lam x) $ q $ (wk (lam y) $ q) $ (wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (wk (lam y) $ q) $ (wk (lam z) $ q)) Sβ ⟩
     K $ S $ q $ (wk (lam x) $ q) $ (wk (lam y) $ q) $ (wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (wk (lam x) $ q) $ (wk (lam y) $ q) $ (wk (lam z) $ q)) Kβ ⟩
     S $ (wk (lam x) $ q) $ (wk (lam y) $ q) $ (wk (lam z) $ q)  ≡⟨ Sβ ⟩
     (wk (lam x) $ q) $ (wk (lam z) $ q) $ ((wk (lam y) $ q) $ (wk (lam z) $ q))
        ≡⟨ cong (λ t → (wk (lam x) $ q) $ (wk (lam z) $ q) $ t) (sym Sβ) ⟩
     (wk (lam x) $ q) $ (wk (lam z) $ q) $ (S $ wk (lam y) $ wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (S $ wk (lam y) $ wk (lam z) $ q)) (sym Sβ) ⟩
     (S $ wk (lam x) $ wk (lam z) $ q) $ (S $ wk (lam y) $ wk (lam z) $ q)  ≡⟨ sym Sβ ⟩
     S $ (S $ wk (lam x) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (lam x) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q) (sym wkS) ⟩
     S $ (wk S $ wk (lam x) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x)) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ t $ (S $ wk (lam y) $ wk (lam z)) $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (wk (S $ (lam x) $ lam z)) $ (t $ wk (lam y) $ wk (lam z)) $ q) (sym wkS) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (wk S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (wk (S $ (lam x) $ lam z)) $ (t $ wk (lam z)) $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (wk (S $ (lam x) $ lam z)) $ t $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q  
        ≡⟨ cong (λ t → t $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q) (sym wkS) ⟩
     wk S $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q  
        ≡⟨ cong (λ t → t $ (wk (S $ lam y $ lam z)) $ q) (sym wk$) ⟩
     wk (S $ (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q  
        ≡⟨ cong (_$ q) (sym wk$) ⟩
     wk ((S $ (S $ (lam x) $ lam z)) $ ((S $ lam y $ lam z))) $ q
     ∎) i

lam (wk$ {x = x}{y = y} i) =
  ext
    (wk (K $ (x $ y)) $ q ≡⟨ cong (_$ q) wk$ ⟩
     wk K $ wk (x $ y) $ q ≡⟨ cong (λ t → t $ wk (x $ y) $ q) wkK ⟩
     K $ wk (x $ y) $ q ≡⟨ Kβ ⟩
     wk (x $ y) ≡⟨ wk$ ⟩
     wk x $ wk y ≡⟨ cong (λ t → t $ wk y) (sym Kβ) ⟩
     K $ wk x $ q $ wk y ≡⟨ cong (λ t → K $ wk x $ q $ t) (sym Kβ) ⟩
     K $ wk x $ q $ (K $ wk y $ q) ≡⟨ sym Sβ ⟩
     S $ (K $ wk x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → t $ (K $ wk x) $ (K $ wk y) $ q) (sym wkS) ⟩
     wk S $ (K $ wk x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → wk S $ (t $ wk x) $ (K $ wk y) $ q) (sym wkK) ⟩
     wk S $ (wk K $ wk x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → wk S $ t $ (K $ wk y) $ q) (sym wk$) ⟩
     wk S $ wk (K $ x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → t $ (K $ wk y) $ q ) (sym wk$) ⟩
     wk (S $ (K $ x)) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ x)) $ (t $ wk y) $ q ) (sym wkK) ⟩
     wk (S $ (K $ x)) $ (wk K $ wk y) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ x)) $ t $ q) (sym wk$)⟩
     wk (S $ (K $ x)) $ wk (K $ y) $ q 
        ≡⟨ cong (_$ q) (sym wk$) ⟩
     wk (S $ (K $ x) $ (K $ y)) $ q
     ∎
     ) i
     
lam (wkK i) = K $ K
lam (wkS i) = K $ S
lam {τ = τ}{σ} (ext {x = x}{y = y} p i) = ext {x = lam x}{lam y}
  (wk (lam x) $ q
     ≡⟨ {!!} ⟩
   wk (lam y) $ q
   ∎)
  i

{-
?                 : wk (lam x) $ q ≡ wk (lam y) $ q

lam ()

wk x $ q
                                    ≡⟨ ? ⟩
wk x $ (I $ q)
                                    ≡⟨ ? ⟩
K $ wk x $ q $ (I $ q)
                                    ≡⟨ ? ⟩
S $ (K $ wk x) $ I $ q
                                    ≡⟨ ? ⟩
wk (S $ (K $ x) $ I) $ q
                                    ≡(λ i → wk (lam (p i)) $ q)
wk (S $ (K $ y) $ I) $ q
                                    ≡⟨ ? ⟩
S $ (K $ wk y) $ I $ q
                                    ≡⟨ ? ⟩
wk y $ q




ext : {x y : Tm Γ (τ ⇒ σ)} → wk x $ q ≡ wk y $ q → x ≡ y

lam x = lam y


-}


  
   {-
ext (
    wk (lam x) $ q
      ≡⟨ {!p!} ⟩
    wk (lam y) $ q
    ∎) i


   (wk (lam x) $ q
      ≡⟨ cong (wk (lam x) $_) (sym Kβ) ⟩
    wk (lam x) $ (K $ q $ (K $ q))
      ≡⟨ cong (wk (lam x) $_) (sym Sβ) ⟩
    wk (lam x) $ (S $ K $ K $ q)
      ≡⟨ cong (_$ (SKK $ q)) (sym Kβ) ⟩
    K $ wk (lam x) $ q $ (S $ K $ K $ q)
      ≡⟨ sym Sβ ⟩
    S $ (K $ wk (lam x)) $ (S $ K $ K) $ q 
      ≡⟨ cong (λ t → t $ (K $ wk (lam x)) $ SKK $ q) (sym wkS) ⟩
    wk S $ (K $ wk (lam x)) $ (S $ K $ K) $ q 
      ≡⟨ cong (λ t → wk S $ (t $ wk (lam x)) $ SKK $ q) (sym wkK)⟩
    wk S $ (wk K $ wk (lam x)) $ (S $ K $ K) $ q 
      ≡⟨ cong (λ t → wk S $ t $ SKK $ q) (sym wk$) ⟩
    wk S $ wk (K $ lam x) $ (S $ K $ K) $ q 
      ≡⟨ cong (λ t → t $ (S $ K $ K {_}{_}{τ}) $ q) (sym wk$) ⟩
    wk (S $ (K $ lam x)) $ (S $ K $ K) $ q
      ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (t $ K $ K) $ q ) (sym wkS) ⟩
    wk (S $ (K $ lam x)) $ (wk S $ K $ K) $ q 
      ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (wk S $ t $ K) $ q) (sym wkK) ⟩
    wk (S $ (K $ lam x)) $ (wk S $ wk K $ K) $ q 
      ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (t $ K) $ q) (sym wk$)⟩
    wk (S $ (K $ lam x)) $ (wk (S $ K) $ K) $ q 
      ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (wk (S $ K) $ t) $ q) (sym wkK)⟩
    wk (S $ (K $ lam x)) $ (wk (S $ K) $ wk K) $ q 
      ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ t $ q) (sym wk$)⟩
    wk (S $ (K $ lam x)) $ wk (S $ K $ K) $ q 
      ≡⟨ cong (_$ q) (sym wk$) ⟩
    wk (S $ (K $ lam x) $ (S $ K $ K)) $ q
      ≡⟨ cong (λ t → wk (S $ (K $ lam t) $ (S $ K $ K)) $ q) (ext p) ⟩
    wk (S $ (K $ lam y) $ (S $ K $ K)) $ q 
    
      ≡⟨ cong (_$ q) wk$ ⟩
    wk (S $ (K $ lam y)) $ wk (S $ K $ K) $ q 
      ≡⟨ cong (λ t → t $ wk (S $ K $ K) $ q) wk$ ⟩
    wk S $ wk (K $ lam y) $ wk (S $ K $ K) $ q 
      ≡⟨ cong (λ t → t $ wk (K $ lam y) $ wk (S $ K $ K) $ q ) wkS ⟩
    S $ wk (K $ lam y) $ wk (S $ K $ K) $ q ≡⟨ Sβ ⟩
    wk (K $ lam y) $ q $ (wk (S $ K $ K) $ q) 
      ≡⟨ cong (λ t → t $ q $ (wk (S $ K $ K) $ q)) wk$ ⟩
    wk K $ wk (lam y) $ q $ (wk (S $ K $ K) $ q) 
      ≡⟨ cong (λ t → t $ wk (lam y) $ q $ (wk (S $ K $ K) $ q)) wkK ⟩
    K $ wk (lam y) $ q $ (wk (S $ K $ K) $ q) 
      ≡⟨ cong (λ t → t $ (wk (S $ K $ K) $ q)) Kβ ⟩
    wk (lam y) $ (wk (S $ K $ K) $ q) 
      ≡⟨ cong (λ t → wk (lam y) $ (t $ q)) wk$ ⟩
    wk (lam y) $ (wk (S $ K) $ wk K $ q) 
      ≡⟨ cong (λ t → wk (lam y) $ (t $ wk K $ q) ) wk$ ⟩
    wk (lam y) $ (wk S $ wk K $ wk K $ q) 
      ≡⟨ cong (λ t → wk (lam y) $ (t $ wk K $ wk K $ q)) wkS ⟩
    wk (lam y) $ (S $ wk K $ wk K $ q) 
      ≡⟨ cong (λ t → wk (lam y) $ (S $ t $ wk K $ q)) wkK ⟩
    wk (lam y) $ (S $ K $ wk K $ q) 
      ≡⟨ cong (wk (lam y) $_) Sβ ⟩
    wk (lam y) $ (K $ q $ (wk K $ q)) 
      ≡⟨ cong (wk (lam y) $_) Kβ ⟩
    wk (lam y) $ q
    ∎) i
-}
lam (TmSet a b c d i j) = TmSet (lam a) (lam b) (cong lam c) (cong lam d) i j
-}
