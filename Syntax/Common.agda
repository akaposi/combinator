{-# OPTIONS --cubical --safe #-}

open import Syntax.Ty

module Syntax.Common (T : TY) where

open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)

open TY T public

infixr 5 _⇒*_
infixl 5 _▹_

data Con : Set where
  ε : Con
  _▹_ : Con → Ty → Con

-- variable
--   A B C D W X Y Z : Ty
-- 
-- variable
--   Γ Γ₀ Γ₁ Δ Θ : Con

variable
  Γ Δ Ξ Ψ : Con

_⇒*_ : Con → Ty → Ty
ε       ⇒* τ = τ
(Γ ▹ τ) ⇒* σ = Γ ⇒* τ ⇒ σ
