{-# OPTIONS --cubical --safe #-}

open import Syntax.Ty

-- combinatory calculus w/ q and wk
module Syntax.C-wk (T : TY) where

open import Syntax.Common T
open import Syntax.C-wk.Base T public
open import Syntax.C-wk.Lift-ctx T public
open import Syntax.C-wk.Lam T public
open import Syntax.C-wk.LamProp T public
open import Syntax.C-wk.Sub T public
open import Syntax.C-wk.SubProp T public
open import Syntax.C-wk.Wk T public
open import Syntax.C-wk.Eta T public
