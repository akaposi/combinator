{-# OPTIONS --cubical --safe #-}
open import Cubical.Core.Primitives 
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Path

open import Syntax.Ty

module Syntax.C-wk-ext.Induction (T : TY) where

open import Syntax.Common T
open import Syntax.C-wk-ext T

record DispModel : Type₁ where
  field
    Tmᴰ     : ∀ Γ τ → Tm Γ τ → Type
    TmᴰProp : ∀{Γ τ t} → isProp (Tmᴰ Γ τ t)
    qᴰ      : Tmᴰ (Γ ▹ τ) τ q
    wkᴰ     : ∀{t} → Tmᴰ Γ σ t → Tmᴰ (Γ ▹ τ) σ (wk t)
    Kᴰ      : Tmᴰ Γ (τ ⇒ σ ⇒ τ) K
    Sᴰ      : Tmᴰ Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ) S
    _$ᴰ_    : ∀{t} → Tmᴰ Γ (τ ⇒ σ) t → ∀{u} → Tmᴰ Γ τ u → Tmᴰ Γ σ (t $ u)

module _ (D : DispModel) where
  open DispModel D

  ind : (t : Tm Γ τ) → Tmᴰ Γ τ t
  ind q = qᴰ
  ind (wk t) = wkᴰ (ind t)
  ind K = Kᴰ
  ind S = Sᴰ
  ind (t $ u) = ind t $ᴰ ind u
  ind (Kβ {Γ}{τ}{σ}{x}{y} i)       = toPathP {A = λ i → Tmᴰ _ _ (Kβ {Γ}{τ}{σ}{x}{y} i)}       (TmᴰProp (transport (λ i → Tmᴰ _ _ (Kβ {Γ}{τ}{σ}{x}{y} i)) ((Kᴰ $ᴰ ind x) $ᴰ ind y)) (ind x)) i
  ind (Sβ {Γ}{τ}{σ}{δ}{x}{y}{z} i) = toPathP {A = λ i → Tmᴰ _ _ (Sβ {Γ}{τ}{σ}{δ}{x}{y}{z} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ (Sβ {Γ}{τ}{σ}{δ}{x}{y}{z} i)) (((Sᴰ $ᴰ ind x) $ᴰ ind y) $ᴰ ind z)) ((ind x $ᴰ ind z) $ᴰ (ind y $ᴰ ind z))) i
  ind (wk$ {Γ}{τ}{σ}{δ}{x}{y} i)   = toPathP {A = λ i → Tmᴰ _ _ (wk$ {Γ}{τ}{σ}{δ}{x}{y} i)}   (TmᴰProp (transport (λ i → Tmᴰ _ _ (wk$ {Γ}{τ}{σ}{δ}{x}{y} i)) (wkᴰ (ind x $ᴰ ind y))) (wkᴰ (ind x) $ᴰ wkᴰ (ind y))) i
  ind (wkK {Γ}{τ}{σ}{δ} i)         = toPathP {A = λ i → Tmᴰ _ _ (wkK {Γ}{τ}{σ}{δ} i)}         (TmᴰProp (transport (λ i → Tmᴰ _ _ (wkK {Γ}{τ}{σ}{δ} i)) (wkᴰ Kᴰ)) Kᴰ) i
  ind (wkS {Γ}{τ}{σ}{δ}{γ} i)      = toPathP {A = λ i → Tmᴰ _ _ (wkS {Γ}{τ}{σ}{δ}{γ} i)}      (TmᴰProp (transport (λ i → Tmᴰ _ _ (wkS {Γ}{τ}{σ}{δ}{γ} i)) (wkᴰ Sᴰ)) Sᴰ) i
  ind (ext {Γ}{τ}{σ}{x}{y} e i)    = toPathP {A = λ i → Tmᴰ _ _ (ext {Γ}{τ}{σ}{x}{y} e i)}    (TmᴰProp (transport (λ i → Tmᴰ _ _ (ext {Γ}{τ}{σ}{x}{y} e i)) (ind x)) (ind y)) i
  ind (TmSet {Γ}{τ} t u x y i j)   = isProp→SquareP {_}{λ i j → Tmᴰ Γ τ (TmSet t u x y i j)}(λ i j → TmᴰProp) (λ _ → ind t) (λ _ → ind u) (λ j → ind (x j)) (λ j → ind (y j)) i j 
