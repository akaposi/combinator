{-# OPTIONS --cubical --safe #-}

module Syntax.Ty where

open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)

record TY : Set₁ where
  field
    Ty  : Set
    _⇒_ : Ty → Ty → Ty
  infixr 5 _⇒_ 

  variable
    τ σ δ ι : Ty

data Ty' : Set where
  base : Ty'
  _⇒'_ : Ty' → Ty' → Ty'

T' : TY
T' = record { Ty = Ty' ; _⇒_ = _⇒'_ }
