{-# OPTIONS --cubical --safe #-}
open import Cubical.Core.Primitives hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Path

open import Syntax.Ty

module Syntax.L.Induction (T : TY) where

open import Syntax.Common T
open import Syntax.L.Base T

record DispModel : Type₁ where
  field
    Tmᴰ      : ∀ Γ τ → Tm Γ τ → Type
    TmᴰProp  : ∀{Γ τ t} → isProp (Tmᴰ Γ τ t)
    Subᴰ     : ∀ Γ Δ → Sub Γ Δ → Type
    SubᴰProp : ∀{Γ Δ ψ} → isProp (Subᴰ Γ Δ ψ)
    idᴰ      : Subᴰ Γ Γ id
    _∘ᴰ_     : ∀{ψ} → Subᴰ Ξ Δ ψ → ∀{ϕ} → Subᴰ Γ Ξ ϕ → Subᴰ Γ Δ (ψ ∘ ϕ)
    _,sᴰ_    : ∀{ψ} → Subᴰ Γ Δ ψ → ∀{t} → Tmᴰ Γ τ t → Subᴰ Γ (Δ ▹ τ) (ψ ,s t)
    π₁ᴰ      : Subᴰ (Γ ▹ τ) Γ π₁
    qᴰ       : Tmᴰ (Γ ▹ τ) τ q
    lamᴰ     : ∀{t} → Tmᴰ (Γ ▹ τ) σ t → Tmᴰ Γ (τ ⇒ σ) (lam t)
    appᴰ     : ∀{t} → Tmᴰ Γ (τ ⇒ σ) t → Tmᴰ (Γ ▹ τ) σ (app t)
    _[_]ᴰ    : ∀{t} → Tmᴰ Γ τ t → ∀{ϕ} → Subᴰ Δ Γ ϕ → Tmᴰ Δ τ (t [ ϕ ])

module _ (D : DispModel) where
  open DispModel D
  
  indSub : (ϕ : Sub Γ Δ) → Subᴰ Γ Δ ϕ
  indTm : (t : Tm Γ τ) → Tmᴰ Γ τ t
  
  indSub id = idᴰ
  indSub (ϕ ∘ ψ) = indSub ϕ ∘ᴰ indSub ψ
  indSub (ϕ ,s x) = indSub ϕ ,sᴰ indTm x
  indSub π₁ = π₁ᴰ
  indSub (ass {Γ}{Δ}{Ξ}{Θ}{ϕ}{ψ}{ξ} i) = toPathP {A = λ i → Subᴰ Θ Δ (ass {Γ}{Δ}{Ξ}{Θ}{ϕ}{ψ}{ξ} i)} (SubᴰProp (transport (λ i → Subᴰ Θ Δ (ass {Γ}{Δ}{Ξ}{Θ}{ϕ}{ψ}{ξ} i)) (indSub ϕ ∘ᴰ (indSub ψ ∘ᴰ indSub ξ))) ((indSub ϕ ∘ᴰ indSub ψ) ∘ᴰ indSub ξ)) i
  indSub (idl {Γ}{Δ}{ϕ} i) = toPathP {A = λ i → Subᴰ _ _ (idl {Γ}{Δ}{ϕ} i)} (SubᴰProp (transport (λ i → Subᴰ _ _ (idl {Γ}{Δ}{ϕ} i)) (idᴰ ∘ᴰ indSub ϕ)) (indSub ϕ)) i
  indSub (idr {Γ}{Δ}{ϕ} i) = toPathP {A = λ i → Subᴰ _ _ (idr {Γ}{Δ}{ϕ} i)} (SubᴰProp (transport (λ i → Subᴰ _ _ (idr {Γ}{Δ}{ϕ} i)) (indSub ϕ ∘ᴰ idᴰ)) (indSub ϕ)) i
  indSub (▹βs {Γ}{Δ}{τ}{ϕ}{t} i) = toPathP {A = λ i → Subᴰ _ _ (▹βs {Γ}{Δ}{τ}{ϕ}{t} i)} (SubᴰProp (transport (λ i → Subᴰ _ _ (▹βs {Γ}{Δ}{τ}{ϕ}{t} i)) (π₁ᴰ ∘ᴰ (indSub ϕ ,sᴰ indTm t))) (indSub ϕ)) i
  indSub (▹η {Γ}{Δ}{τ}{ϕ} i) = toPathP {A = λ i → Subᴰ _ _ (▹η {Γ}{Δ}{τ}{ϕ} i)} (SubᴰProp (transport (λ i → Subᴰ _ _ (▹η {Γ}{Δ}{τ}{ϕ} i)) ((π₁ᴰ ∘ᴰ indSub ϕ) ,sᴰ (qᴰ [ indSub ϕ ]ᴰ))) (indSub ϕ)) i
  indSub (SubSet {Γ}{Δ} ϕ ψ x y i j) = isProp→SquareP {_}{λ i j → Subᴰ Γ Δ (SubSet ϕ ψ x y i j)}(λ i j → SubᴰProp) (λ _ → indSub ϕ) (λ _ → indSub ψ) (λ j → indSub (x j)) (λ j → indSub (y j)) i j

  indTm v₀ = qᴰ
  indTm (lam t) = lamᴰ (indTm t)
  indTm (app t) = appᴰ (indTm t)
  indTm (t [ ϕ ]) = indTm t [ indSub ϕ ]ᴰ
  indTm ([∘] {Ξ}{Γ}{Δ}{τ}{ψ}{ϕ}{t} i) = toPathP {A = λ i → Tmᴰ _ _ ([∘] {Ξ}{Γ}{Δ}{τ}{ψ}{ϕ}{t} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ ([∘] {Ξ}{Γ}{Δ}{τ}{ψ}{ϕ}{t} i)) (indTm t [ indSub ψ ∘ᴰ indSub ϕ ]ᴰ)) ((indTm t [ indSub ψ ]ᴰ) [ indSub ϕ ]ᴰ)) i
  indTm ([id] {Γ}{τ}{t} i) = toPathP {A = λ i → Tmᴰ _ _ ([id] {Γ}{τ}{t} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ ([id] {Γ}{τ}{t} i)) (indTm t [ idᴰ ]ᴰ)) (indTm t)) i
  indTm (lamβ {Γ}{τ}{σ}{t} i) = toPathP {A = λ i → Tmᴰ _ _ (lamβ {Γ}{τ}{σ}{t} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ (lamβ {Γ}{τ}{σ}{t} i)) (appᴰ (lamᴰ (indTm t)))) (indTm t)) i
  indTm (lamη {Γ}{τ}{σ}{t} i) = toPathP {A = λ i → Tmᴰ _ _ (lamη {Γ}{τ}{σ}{t} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ (lamη {Γ}{τ}{σ}{t} i)) (lamᴰ (appᴰ (indTm t)))) (indTm t)) i
  indTm (app-sub {Γ}{Δ}{σ}{τ} ϕ t i) = toPathP {A = λ i → Tmᴰ _ _ (app-sub {Γ}{Δ}{σ}{τ} ϕ t i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ (app-sub {Γ}{Δ}{σ}{τ} ϕ t i)) (appᴰ (indTm t [ indSub ϕ ]ᴰ))) (appᴰ (indTm t) [ (indSub ϕ ∘ᴰ π₁ᴰ) ,sᴰ qᴰ ]ᴰ)) i
  indTm (▹βt {Γ}{Δ}{τ}{ϕ}{t} i) = toPathP {A = λ i → Tmᴰ _ _ (▹βt {Γ}{Δ}{τ}{ϕ}{t} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ (▹βt {Γ}{Δ}{τ}{ϕ}{t} i)) (qᴰ [ indSub ϕ ,sᴰ indTm t ]ᴰ)) (indTm t)) i
  indTm (lam[] {Γ}{τ}{σ}{Δ}{t}{ϕ} i) = toPathP {A = λ i → Tmᴰ _ _ (lam[] {Γ}{τ}{σ}{Δ}{t}{ϕ} i)} (TmᴰProp (transport (λ i → Tmᴰ _ _ (lam[] {Γ}{τ}{σ}{Δ}{t}{ϕ} i)) (lamᴰ (indTm t) [ indSub ϕ ]ᴰ)) (lamᴰ (indTm t [ (indSub ϕ ∘ᴰ π₁ᴰ) ,sᴰ qᴰ ]ᴰ))) i
  indTm (TmSet {Γ}{τ} t u x y i j) = isProp→SquareP {_}{λ i j → Tmᴰ Γ τ (TmSet t u x y i j)}(λ i j → TmᴰProp) (λ _ → indTm t) (λ _ → indTm u) (λ j → indTm (x j)) (λ j → indTm (y j)) i j 
