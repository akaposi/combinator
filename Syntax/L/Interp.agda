{-# OPTIONS --cubical --safe #-}
open import Agda.Primitive
open import Cubical.Core.Primitives hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Foundations.HLevels

module Syntax.L.Interp where

open import Syntax.Ty

open import Syntax.Common T'
open import Syntax.L.Base T'

-- Evaluation/Interpetation of L in hSet 0

⟦_⟧t : Ty' → hSet lzero  -- TODO: rewrite Set to HSet
⟦ base ⟧t = ℕ , isSetℕ
⟦ t ⇒' t₁ ⟧t = (fst ⟦ t ⟧t → fst ⟦ t₁ ⟧t) , isSetΠ (λ x → snd (⟦ t₁ ⟧t))

⟦_⟧e : Con → hSet lzero -- TODO: rewrite Set to HSet
⟦ ε ⟧e = Unit , isSetUnit
⟦ Γ ▹ x ⟧e = fst ⟦ Γ ⟧e × fst ⟦ x ⟧t , isSetΣ (snd (⟦ Γ ⟧e)) (λ _ → snd (⟦ x ⟧t))

⟦_⟧ : Tm Γ τ → fst ⟦ Γ ⟧e → fst ⟦ τ ⟧t
⟦_⟧s : Sub Γ Δ → fst ⟦ Γ ⟧e → fst ⟦ Δ ⟧e



⟦ id ⟧s x = x
⟦ s ∘ s₁ ⟧s = ⟦ s ⟧s F.∘ ⟦ s₁ ⟧s
⟦ s ,s x ⟧s ρ = ⟦ s ⟧s ρ , ⟦ x ⟧ ρ
⟦ π₁ ⟧s = fst
⟦ ass {γ = γ}{φ = φ}{ψ = ψ} i ⟧s ρ = F.∘-assoc ⟦ γ ⟧s ⟦ φ ⟧s ⟦ ψ ⟧s i ρ
⟦ idl {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ idr {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ ▹βs {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ ▹η  {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ SubSet {Δ = Δ} a b c d i j ⟧s ρ = 
    (snd (⟦ Δ ⟧e)) (⟦ a ⟧s ρ) (⟦ b ⟧s ρ) (λ i → ⟦ c i ⟧s ρ) (λ i → ⟦ d i ⟧s ρ) i j


⟦ v₀ ⟧ ρ = snd ρ
⟦ lam t ⟧ ρ x = ⟦ t ⟧ (ρ , x)
⟦ app t ⟧ ρ = ⟦ t ⟧ (fst ρ) (snd ρ)
⟦ t [ s ] ⟧ ρ = ⟦ t ⟧ (⟦ s ⟧s ρ)
⟦ [∘] {γ = γ}{φ = φ}{t = t} i ⟧ ρ = ⟦ t ⟧ (⟦ γ ⟧s (⟦ φ ⟧s ρ))
⟦ [id] {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ lamβ {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ lamη {t = t} i ⟧ ρ x = ⟦ t ⟧ ρ x
⟦ ▹βt {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ app-sub γ f i ⟧ ρ = ⟦ f ⟧ (⟦ γ ⟧s (fst ρ)) (snd ρ)
⟦ lam[] {t = t}{γ = γ} i ⟧ ρ x = ⟦ t ⟧ (⟦ γ ⟧s ρ , x)
⟦ TmSet {τ = τ} a b c d i j ⟧ ρ =
   (snd (⟦ τ ⟧t)) (⟦ a ⟧ ρ) (⟦ b ⟧ ρ) (λ i → ⟦ c i ⟧ ρ) (λ i → ⟦ d i ⟧ ρ) i j 
