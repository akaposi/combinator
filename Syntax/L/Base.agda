{-# OPTIONS --cubical --safe #-}
open import Agda.Primitive
open import Cubical.Core.Primitives  hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Foundations.HLevels

open import Syntax.Ty

-- Lambda calculus w/ q and wk
module Syntax.L.Base (T : TY) where

open import Syntax.Common T

infixl 15 _^
infixl 11 _∘_
infixl 10 _$_
infixl 5 _,s_

data Tm : Con → Ty → Type
data Sub : Con → Con → Type

-- Forward declarations of the "constructors"
q′    : Tm (Γ ▹ τ) τ
_[_]′ : Tm Γ τ → Sub Δ Γ → Tm Δ τ

data Sub where
  --εs   : Sub Γ ε
  id   : Sub Γ Γ
  _∘_  : Sub Ξ Δ → Sub Γ Ξ → Sub Γ Δ
  _,s_ : Sub Γ Δ → Tm Γ τ → Sub Γ (Δ ▹ τ)
  π₁   : Sub (Γ ▹ τ) Γ

  -- Equalities
  --εη  : (γ : Sub Γ ε) → γ ≡ εs
  ass : ∀ {γ : Sub Ξ Δ}{φ : Sub Ψ Ξ}{ψ : Sub Γ Ψ}
      → γ ∘ (φ ∘ ψ) ≡ (γ ∘ φ) ∘ ψ
  idl : {γ : Sub Γ Δ} → id ∘ γ ≡ γ
  idr : {γ : Sub Γ Δ} → γ ∘ id ≡ γ

  ▹βs : {γ : Sub Γ Δ}{t : Tm Γ τ} → π₁ ∘ (γ ,s t) ≡ γ
  ▹η : {γ : Sub Γ (Δ ▹ τ)} → ((π₁ ∘ γ) ,s (q′ [ γ ]′)) ≡ γ

  SubSet : isSet (Sub Γ Δ)

_^ : Sub Γ Δ → Sub (Γ ▹ τ) (Δ ▹ τ)
_^ γ = (γ ∘ π₁) ,s q′

data Tm where
  --var  : Var Γ τ → Tm Γ τ
  q    : Tm (Γ ▹ τ) τ
  lam  : Tm (Γ ▹ τ) σ → Tm Γ (τ ⇒ σ)
  app  : Tm Γ (τ ⇒ σ) → Tm (Γ ▹ τ) σ

  _[_] : Tm Γ τ → Sub Δ Γ → Tm Δ τ

  -- Equalities
  [∘] : {γ : Sub Ξ Γ}{φ : Sub Δ Ξ} → {t : Tm Γ τ} 
      → t [ γ ∘ φ ] ≡ t [ γ ] [ φ ]
  [id] : ∀ {t : Tm Γ τ} → t [ id ] ≡ t
  lamβ : ∀ {t : Tm (Γ ▹ τ) σ} → app (lam t) ≡ t

  lamη : ∀ {t : Tm Γ (τ ⇒ σ)} → lam (app t) ≡ t

  -- We add this rule for the cases when lamη is false.
  -- Otherwise we can define this via lamη, 
  app-sub : (γ : Sub Δ Γ) (f : Tm Γ (σ ⇒ τ))
          → app (f [ γ ]) ≡ app f [ γ ^ ]

  ▹βt : {γ : Sub Γ Δ}{t : Tm Γ τ} → q [ γ ,s t ] ≡ t
  lam[] : ∀ {t : Tm (Γ ▹ τ) σ}{γ : Sub Δ Γ} 
        → (lam t) [ γ ] ≡ lam (t [ γ ^ ])

  TmSet : isSet (Tm Γ τ)

q′ = q
_[_]′ = _[_]


-- Do we need to axiomatise the empty subsitution?
εs : Sub Γ ε
εs {ε} = id
εs {Γ ▹ x} = εs {Γ} ∘ π₁

_$_ : Tm Γ (τ ⇒ σ) → Tm Γ τ → Tm Γ σ
f $ x = (app f) [ id ,s x ]

,s∘  : ∀ {γ : Sub Γ Δ}{φ : Sub Ξ Γ}{t : Tm Γ τ} 
     → (γ ,s t) ∘ φ ≡ (γ ∘ φ) ,s t [ φ ]
,s∘ {γ = γ}{φ}{t} = 
   (γ ,s t) ∘ φ ≡⟨ sym ▹η ⟩
   (π₁ ∘ ( (γ ,s t) ∘ φ)) ,s (q [ (γ ,s t) ∘ φ ]) ≡⟨ cong₂ _,s_ ass [∘] ⟩
   ((π₁ ∘ (γ ,s t)) ∘ φ) ,s (q [ γ ,s t ] [ φ ])  ≡⟨ cong₂ _,s_ (cong (_∘ φ) ▹βs) (cong (_[ φ ]) ▹βt) ⟩
   (γ ∘ φ) ,s (t [ φ ]) 
   ∎

lift∘,s : ∀ (x : Tm Γ τ) (γ : Sub Γ Δ) → (γ ^ ) ∘ (id ,s x) ≡ γ ,s x
lift∘,s x γ =
  (γ ∘ π₁ ,s q) ∘ (id ,s x) ≡⟨ ,s∘ ⟩
  ((γ ∘ π₁) ∘ (id ,s x)) ,s (q [ id ,s x ]) ≡⟨ cong₂ _,s_ refl ▹βt ⟩
  ((γ ∘ π₁) ∘ (id ,s x)) ,s x ≡⟨ cong (_,s x) (sym ass) ⟩
  (γ ∘ (π₁ ∘ (id ,s x))) ,s x ≡⟨ cong (_,s x) (cong (γ ∘_) ▹βs) ⟩
  (γ ∘ id) ,s x ≡⟨ cong (_,s x) idr ⟩
  γ ,s x
  ∎


-- This is to show that when we have η-rule,
-- we can *define* app-sub.  No need to postulate it in the
-- definition.
app-sub′ : (γ : Sub Δ Γ) (f : Tm Γ (σ ⇒ τ))
        → app (f [ γ ]) ≡ app f [ γ ^ ]
app-sub′ γ f = 
    app (f [ γ ])              ≡⟨ cong (λ x → app (x [ γ ])) (sym lamη) ⟩
    app (lam (app f) [ γ ])    ≡⟨ cong app lam[] ⟩
    app (lam (app f [ γ ^ ] )) ≡⟨ lamβ ⟩
    (app f [ γ ^ ] ) 
    ∎

sub-$ : (γ : Sub Δ Γ) (f : Tm Γ (σ ⇒ τ)) (x : Tm Γ σ)
      → (f $ x)[ γ ] ≡ (f [ γ ]) $ (x [ γ ])
sub-$ γ f x = 
    app f [ id ,s x ] [ γ ]             ≡⟨ sym [∘] ⟩ 
    app f [ (id ,s x) ∘ γ ]             ≡⟨ cong ((app f) [_]) ,s∘ ⟩ 
    app f [ (id ∘ γ) ,s (x [ γ ]) ]     ≡⟨ cong (λ t → app f [ t ,s (x [ γ ]) ]) idl ⟩
    app f [ γ ,s (x [ γ ]) ]            ≡⟨ cong (app f [_]) (sym (lift∘,s _ _))  ⟩ 
    app f [ (γ ^) ∘ (id ,s x [ γ ]) ]   ≡⟨ [∘]  ⟩ 
    app f [ γ ^  ] [ id ,s x [ γ ] ]    ≡⟨ cong (_[ id ,s x [ γ ] ]) (sym (app-sub _ _))  ⟩ 
    app (f [ γ ]) [ id ,s x [ γ ] ]
    ∎

lam-$ : (x : Tm Γ τ) (t : Tm (Γ ▹ τ) σ) → lam t $ x ≡ t [ id ,s x ]
lam-$ x t =
   app (lam t) [ id ,s x ] ≡⟨ cong (_[ id ,s x ]) lamβ ⟩
   t [ id ,s x ]
   ∎

eta : {x : Tm Γ (τ ⇒ σ)} → lam (x [ π₁ ] $ q) ≡ x
eta {Γ}{τ}{σ}{x} = cong lam (cong (_[ id ,s q ]) (app-sub _ _) 
                                    ∙ sym [∘]
                                    ∙ cong (app x [_]) (lift∘,s _ _ ∙ cong (_,s q) (sym idl)))
                          ∙ sym lam[] 
                          ∙ [id]
                          ∙ lamη

ext : {x y : Tm Γ (τ ⇒ σ)} 
    → x [ π₁ ] $ q ≡ y [ π₁ ] $ q → x ≡ y
ext {x} {y} eq = sym (eta) ∙ cong lam eq ∙ eta

pattern v₀ = q
pattern v₁ = v₀ [ π₁ ]
pattern v₂ = v₁ [ π₁ ]
pattern v₃ = v₂ [ π₁ ]


