{-# OPTIONS --cubical --safe #-}
open import Agda.Primitive
open import Cubical.Core.Primitives hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Foundations.HLevels

open import Syntax.Ty

-- Lambda calculus w/ q and wk
module Syntax.L.Comb (T : TY) where

open import Syntax.Common T
open import Syntax.L.Base T

-- id
LI : Tm Γ (τ ⇒ τ)
LI = lam v₀
-- 
-- -- k
LK : Tm Γ (τ ⇒ σ ⇒ τ)
LK = lam (lam v₁)

--test₂′ : Tm Γ (τ ⇒ σ ⇒ δ ⇒ τ)
--test₂′ = lam (lam (lam v₂))

---- λ f x → f x
--test₃ : Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)
--test₃ = lam (lam (v₁ $ v₀))

-- s
LS : Tm Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ (τ ⇒ δ))
LS = lam (lam (lam (v₂ $ v₀ $ (v₁ $ v₀))))

-- k x y = x
--LKβ : (x : Tm Γ τ) (y : Tm Γ σ) → LK $ x $ y ≡ x
--LKβ x y = 
--    (app (app (lam (lam v₁)) [ id ,s x ]) [ id ,s y ]) ≡⟨ cong (λ t → app (t [ id ,s x ]) [ id ,s y ]) lamβ  ⟩
--    (app (lam v₁ [ id ,s x ]) [ id ,s y ]) ≡⟨ cong (λ t → app t [ id ,s y ]) lam[] ⟩
--    (app (lam (v₁ [ (id ,s x)^ ])) [ id ,s y ]) ≡⟨ cong (λ t → t [ id ,s y ]) lamβ ⟩
--    (v₁ [ (id ,s x)^ ]) [ id ,s y ] ≡⟨ refl ⟩
--    q [ π₁ ] [ (id ,s x)^ ] [ id ,s y ] ≡⟨ cong (_[ id ,s y ]) (sym [∘]) ⟩
--    q [ π₁ ∘ ((id ,s x)^) ] [ id ,s y ] ≡⟨ cong (λ t → (q [ t ] [ id ,s y ])) ▹βs ⟩
--    q [ (id ,s x) ∘ π₁ ] [ id ,s y ] ≡⟨ sym [∘] ⟩
--    q [ ((id ,s x) ∘ π₁) ∘ (id ,s y) ] ≡⟨ cong (q [_]) (sym ass) ⟩
--    q [ (id ,s x) ∘ (π₁ ∘ (id ,s y)) ] ≡⟨ cong (λ t → q [ (id ,s x) ∘ t ]) ▹βs  ⟩
--    q [ (id ,s x) ∘ id ] ≡⟨ cong (q [_]) idr ⟩
--    q [ (id ,s x) ] ≡⟨ ▹βt ⟩
--    x
--    ∎

LKβ : (x : Tm Γ τ) (y : Tm Γ σ) → LK $ x $ y ≡ x
LKβ x y = 
    lam (lam v₁) $ x $ y     ≡⟨ cong (_$ y) (lam-$ _ _) ⟩
    ((lam v₁) [ id ,s x ]) $ y ≡⟨ cong (_$ y) lam[] ⟩
    lam (v₁ [ (id ,s x)^ ]) $ y ≡⟨ lam-$ _ _ ⟩
    q [ π₁ ] [ (id ,s x)^ ] [ id ,s y ] ≡⟨ cong _[ id ,s y ] (sym [∘]) ⟩
    q [ π₁ ∘ ((id ,s x)^) ] [ id ,s y ] ≡⟨ sym [∘] ⟩
    q [ (π₁ ∘ ((id ,s x)^)) ∘ (id ,s y) ] ≡⟨ cong (q [_]) (sym ass) ⟩
    q [ π₁ ∘ (((id ,s x)^) ∘ (id ,s y)) ] ≡⟨ cong (λ t → q [ π₁ ∘ t ]) (lift∘,s _ _) ⟩
    q [ π₁ ∘ ((id ,s x) ,s y) ] ≡⟨ cong (q [_]) ▹βs ⟩
    q [ id ,s x ] ≡⟨ ▹βt ⟩
    x
    ∎

LSβ : (x : Tm Γ (τ ⇒ σ ⇒ δ)) (y : Tm Γ (τ ⇒ σ)) (z : Tm Γ τ) 
       → LS $ x $ y $ z ≡ x $ z $ (y $ z)
LSβ {τ = τ} x y z =
   lam (lam (lam (v₂ $ v₀ $ (v₁ $ v₀)))) $ x $ y $ z ≡⟨ cong (λ t → t $ y $ z) (lam-$ _ _) ⟩
   lam (lam (v₂ $ v₀ $ (v₁ $ v₀))) [ id ,s x ] $ y $ z ≡⟨ cong (λ t → t $ y $ z) lam[] ⟩
   lam (lam (v₂ $ v₀ $ (v₁ $ v₀))[ (id ,s x)^ ]) $ y $ z ≡⟨ cong (λ t → (lam t) $ y $ z) lam[] ⟩
   lam (lam ((v₂ $ v₀ $ (v₁ $ v₀)) [ (id ,s x) ^ ^ ])) $ y $ z ≡⟨ cong (λ t → (lam (lam t)) $ y $ z) (sub-$ _ _ _) ⟩ 
   lam (lam
         (((v₂ $ v₀) [ (id ,s x) ^ ^ ]) $ ((v₁ $ v₀) [ (id ,s x) ^ ^ ])))
        $ y
        $ z ≡⟨ cong₂ (λ t q → (lam (lam (t $ q))) $ y $ z) (sub-$ _ _ _) (sub-$ _ _ _) ⟩
   lam (lam ((v₂ [ (id ,s x) ^ ^ ]) $ (v₀ [ (id ,s x) ^ ^ ]) $
            ((v₁ [ (id ,s x) ^ ^ ]) $ (v₀ [ (id ,s x) ^ ^ ]))))
   $ y $ z ≡⟨ cong (λ t → lam (lam (t $ (v₀ [ (id ,s x) ^ ^ ]) $
                                   ((v₁ [ (id ,s x) ^ ^ ]) $ (v₀ [ (id ,s x) ^ ^ ]))))
                          $ y $ z) 
                   (sym [∘] ∙ (cong (v₁ [_]) ▹βs) ∙ (cong (v₁ [_]) ,s∘) 
                    ∙ sym [∘] ∙ (cong (v₀ [_]) ▹βs) 
                    ∙ (cong (λ t → v₀ [ t ∘ π₁ {τ = τ} ]) ,s∘) ∙ cong (v₀ [_]) ,s∘ ∙ ▹βt )
            ⟩
   lam (lam (((x [ π₁ ]) [ π₁ ]) $ (v₀ [ (id ,s x) ^ ^ ]) $
            ((v₁ [ (id ,s x) ^ ^ ]) $ (v₀ [ (id ,s x) ^ ^ ]))))
        $ y $ z ≡⟨ cong (λ t → lam (lam (((x [ π₁ ]) [ π₁ ]) $ t $
                ((v₁ [ (id ,s x) ^ ^ ]) $ (v₀ [ (id ,s x) ^ ^ ]))))
                $ y $ z) 
               ▹βt ⟩
   lam (lam (((x [ π₁ ]) [ π₁ ]) $ v₀ $
            ((v₁ [ (id ,s x) ^ ^ ]) $ (v₀ [ (id ,s x) ^ ^ ]))))
        $ y $ z ≡⟨ cong (λ t → lam (lam (((x [ π₁ ]) [ π₁ ]) $ v₀ $
                        (t $ (v₀ [ (id ,s x) ^ ^ ]))))
                    $ y $ z)
        (sym [∘] ∙ cong (v₀ [_]) (▹βs ∙ ,s∘) ∙ ▹βt ) ⟩
   lam (lam (((x [ π₁ ]) [ π₁ ]) $ v₀ $ (v₁ $ (v₀ [ (id ,s x) ^ ^ ]))))
   $ y $ z ≡⟨ cong (λ t → lam (lam (((x [ π₁ ]) [ π₁ ]) 
                          $ v₀ $ (v₁ $ t)))
                          $ y $ z)
              ▹βt ⟩
   lam (lam (((x [ π₁ ]) [ π₁ ]) $ v₀ $ (v₁ $ v₀))) $ y $ z 
   ≡⟨ cong (λ t → t $ z) (lam-$ _ _) ⟩
   (lam (((x [ π₁ ]) [ π₁ ]) $ v₀ $ (v₁ $ v₀))) [ id ,s y ] $ z 
   ≡⟨ cong (_$ z) lam[] ⟩
   lam ((((x [ π₁ ]) [ π₁ ]) $ v₀ $ (v₁ $ v₀)) [ (id ,s y) ^ ]) $ z
   ≡⟨ cong (λ t → lam t $ z) (sub-$ _ _ _) ⟩
   lam (((((x [ π₁ ]) [ π₁ ]) $ v₀) [ (id ,s y) ^ ]) $
         ((v₁ $ v₀) [ (id ,s y) ^ ])) $ z 
   ≡⟨ cong₂ (λ t p → lam (t $ p) $ z) (sub-$ _ _ _) (sub-$ _ _ _) ⟩
   lam ((((x [ π₁ ]) [ π₁ ]) [ (id ,s y) ^ ]) $ (v₀ [ (id ,s y) ^ ]) $
         ((v₁ [ (id ,s y) ^ ]) $ (v₀ [ (id ,s y) ^ ])))
        $ z 
   ≡⟨ cong (λ t → lam (t $ (v₀ [ (id ,s y) ^ ]) $
                      ((v₁ [ (id ,s y) ^ ]) $ (v₀ [ (id ,s y) ^ ]))) $ z)
      (sym [∘] ∙ cong (λ t → x [ π₁ ] [ t ]) (▹βs ∙ ,s∘) ∙ sym [∘] ∙ cong (x [_]) (▹βs ∙ idl)) ⟩
   lam ((x [ π₁ ]) $ (v₀ [ (id ,s y) ^ ]) $
         ((v₁ [ (id ,s y) ^ ]) $ (v₀ [ (id ,s y) ^ ]))) $ z 
   ≡⟨ cong (λ t → lam ((x [ π₁ ]) $ t $
                      ((v₁ [ (id ,s y) ^ ]) $ (v₀ [ (id ,s y) ^ ]))) $ z)
      ▹βt ⟩
   lam ((x [ π₁ ]) $ v₀ $ ((v₁ [ (id ,s y) ^ ]) $ (v₀ [ (id ,s y) ^ ]))) $ z
   ≡⟨ cong (λ t → lam ((x [ π₁ ]) $ v₀ $ (t $ (v₀ [ (id ,s y) ^ ]))) $ z)
      (sym [∘] ∙ cong (v₀ [_]) (▹βs ∙ ,s∘) ∙  ▹βt) ⟩
   lam ((x [ π₁ ]) $ v₀ $ ((y [ π₁ ]) $ (v₀ [ (id ,s y) ^ ]))) $ z 
   ≡⟨ cong (λ t → lam ((x [ π₁ ]) $ v₀ $ ((y [ π₁ ]) $ t)) $ z)
      ▹βt ⟩
   lam ((x [ π₁ ]) $ v₀ $ ((y [ π₁ ]) $ v₀)) $ z 
   ≡⟨ lam-$ _ _ ⟩
   ((x [ π₁ ]) $ v₀ $ ((y [ π₁ ]) $ v₀)) [ id ,s z ]
   ≡⟨ sub-$ _ _ _ ⟩
   (((x [ π₁ ]) $ v₀) [ id ,s z ]) $ (((y [ π₁ ]) $ v₀) [ id ,s z ]) 
   ≡⟨ cong₂ _$_ 
      (sub-$ _ _ _) (sub-$ _ _ _) ⟩
   ((x [ π₁ ]) [ id ,s z ]) $ (v₀ [ id ,s z ]) $
        (((y [ π₁ ]) [ id ,s z ]) $ (v₀ [ id ,s z ])) 
   ≡⟨ cong (λ t → t $ (((y [ π₁ ]) [ id ,s z ]) $ (v₀ [ id ,s z ])))
      (cong₂ _$_ (sym [∘] ∙ cong (x [_]) (▹βs) ∙ [id]) ▹βt) ⟩
   x $ z $ (((y [ π₁ ]) [ id ,s z ]) $ (v₀ [ id ,s z ]))
   ≡⟨ cong (λ t → x $ z $ t)
      (cong₂ _$_ (sym [∘] ∙ cong (y [_]) ▹βs ∙ [id]) ▹βt) ⟩
   x $ z $ (y $ z)
   ∎

--eqKβ : _≡_ {A = Tm ε ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
--           (LS $ (LK $ LS) $ (LS $ (LK $ LK)))
--           LK


x[π]q : ∀ {x : Tm Γ (τ ⇒ σ)} → x [ π₁ ] $ v₀ ≡ app x
x[π]q {x = x} = cong (_[ id ,s v₀ ]) (app-sub π₁ x) 
              ∙ sym [∘] 
              ∙ cong (app x [_]) (lift∘,s _ _ ∙ cong₂ _,s_ (sym idr) (sym [id]) ∙ ▹η) 
              ∙ [id]



k[π₁] : LK {τ = τ}{σ} [ π₁ {Γ = Γ}{τ = δ} ] ≡ LK
k[π₁] = lam[] 
      ∙ cong lam lam[] 
      ∙ cong (λ x → lam (lam x)) (sym [∘])
      ∙ cong (λ x → lam (lam (v₀ [ x ]))) ▹βs 
      ∙ cong (λ x → lam (lam (v₀ [ x ]))) ,s∘
      ∙ cong (λ x → lam (lam (x))) ▹βt

s[π₁] : ∀ {γ} → LS {τ = τ}{σ}{δ} [ π₁ {Γ = Γ}{τ = γ} ] ≡ LS
s[π₁] {τ = τ} = lam[]
      ∙ cong lam lam[]
      ∙ cong (λ t → lam (lam t)) 
             (lam[] 
              ∙ cong lam (sub-$ _ _ _ 
                          ∙ cong₂ _$_ 
                                  (sub-$ _ _ _ ∙ cong₂ _$_ (sym [∘] ∙ cong (v₁ [_]) (▹βs ∙ ,s∘) 
                                                            ∙ sym [∘] ∙ cong (v₀ [_]) (▹βs ∙ cong (_∘ π₁ {τ = τ}) (,s∘))
                                                            ∙ cong (v₀ [_]) ,s∘ ∙ ▹βt) ▹βt) 
                                  (sub-$ _ _ _ ∙ cong₂ _$_ (sym [∘] ∙ cong (v₀ [_]) (▹βs ∙ ,s∘) ∙ ▹βt) ▹βt))) 


