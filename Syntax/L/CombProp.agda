{-# OPTIONS --cubical --safe #-}
open import Agda.Primitive
open import Cubical.Core.Primitives hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Foundations.HLevels

open import Syntax.Ty

module Syntax.L.CombProp (T : TY) where

open import Syntax.Common T
open import Syntax.L.Base T
open import Syntax.L.Comb T

π, : ∀ {x : Tm Γ τ}{γ : Sub Δ Γ}{y} → (x [ π₁ {τ = σ} ])[ γ ,s y ] ≡ (x [ γ ])
π, {x = x} = sym [∘] ∙ cong (x [_]) (▹βs)


^ππ : ∀ {x : Tm Δ δ}{γ : Sub Δ Γ} → (((γ ,s x) ∘ π₁ {τ = σ}) ∘ π₁ {τ = τ}) ≡ γ ∘ π₁ ∘ π₁ ,s x [ π₁ ] [ π₁ ]
^ππ {τ = τ} = cong (_∘ π₁ {τ = τ}) ,s∘ ∙ ,s∘

v₀-ign : {γ : Sub Δ Γ} → _[_] {Γ = Γ ▹ τ} v₀ (γ ^) ≡ v₀
v₀-ign = ▹βt

v₁-ign : {γ : Sub Δ Γ} → _[_] {Γ = Γ ▹ τ ▹ σ} v₁ (γ ^ ^) ≡ v₁
v₁-ign = π, ∙ cong (v₀ [_]) ,s∘ ∙ ▹βt

v₂-ign : {γ : Sub Δ Γ} → _[_] {Γ = Γ ▹ τ ▹ σ ▹ δ} v₂ (γ ^ ^ ^) ≡ v₂
v₂-ign = π, ∙ cong (v₁ [_]) ,s∘ ∙ π, ∙ cong (v₀ [_]) ^ππ ∙ ▹βt 


v₁-app : ∀ {x : Tm Γ τ} → _[_] {Γ = Γ ▹ τ ▹ σ} v₁ ((id ,s x) ^) ≡ x [ π₁ ]
v₁-app {x = x} = π, ∙ cong (v₀ [_]) ,s∘ ∙ ▹βt


v₂-app : ∀ {x : Tm Γ τ} → _[_] {Γ = Γ ▹ τ ▹ σ ▹ δ} v₂ ((id ,s x) ^ ^) ≡ x [ π₁ ] [ π₁ ]
v₂-app {δ = δ}{x = x} = π, ∙ cong (v₁ [_]) ,s∘ ∙ π, ∙ cong (v₀ [_]) ^ππ ∙ ▹βt



Kx : {x : Tm Γ τ}
   → LK {τ = τ}{σ} $ x ≡ lam (x [ π₁ ])
Kx {x = x} = lam-$ _ _
             ∙ lam[]
             ∙ cong lam (π, ∙ cong (v₀ [_]) ,s∘ ∙ ▹βt)


Sx : {x : Tm Γ (τ ⇒ σ ⇒ δ)}
   → LS $ x ≡ lam (lam (x [ π₁ ] [ π₁ ] $ v₀ $ (v₁ $ v₀))) 
Sx = lam-$ _ _
     ∙ lam[]
     ∙ cong lam 
         (lam[]
          ∙ cong lam
              (sub-$ _ _ _ 
               ∙ cong₂ _$_ 
                   (sub-$ _ _ _ ∙ cong₂ _$_ v₂-app v₀-ign) 
                   (sub-$ _ _ _ ∙ cong₂ _$_ v₁-ign v₀-ign)))

Sxy : {x : Tm Γ (τ ⇒ σ ⇒ δ)}{y : Tm Γ (τ ⇒ σ)} 
    → LS $ x $ y ≡ lam (x [ π₁ ] $ v₀ $ (y [ π₁ ] $ v₀))
Sxy {x = x}{y} = cong (_$ y) Sx
                 ∙ lam-$ _ _
                 ∙ lam[]
                 ∙ cong lam
                     (sub-$ _ _ _ 
                      ∙ cong₂ _$_ 
                          (sub-$ _ _ _ ∙ cong₂ _$_ (π, ∙ cong (x [ π₁ ] [_]) ,s∘ ∙ π, ∙ cong (x [_]) idl) v₀-ign)
                          (sub-$ _ _ _ ∙ cong₂ _$_ v₁-app v₀-ign))


skk≡i : _≡_ {A = Tm Γ (τ ⇒ τ)} 
        (LS $ LK $ LK {σ = σ})
        (lam v₀)
skk≡i = Sxy ∙ cong lam (cong₂ _$_ (cong (_$ q) k[π₁]) refl ∙ LKβ _ _)



lam-cong : ∀ {x y : Tm (Γ ▹ τ) σ} → lam x ≡ lam y → x ≡ y
lam-cong {x = x}{y} p 
           = x ≡⟨ sym lamβ ⟩
             app (lam x) ≡⟨ cong app p ⟩
             app (lam y) ≡⟨ lamβ ⟩
             y
             ∎



{-
-- Testign things

K : ∀ {A B : Set}
  → A → B → A
K x y = x

S : ∀ {A B C : Set} → (A → B → C) → (A → B) → A → C
S x y z = x z (y z)

--boo : ∀ {A B : Set} → A → B → A
boo : {A B C : Set} → (A → B) → (A → C) → (A → B) 
boo x y = (S (K S) (S (K K))) x y

bar : {A B C : Set} → boo {A} {B} {C} ≡ K
bar = refl

lamwk$′ : ∀ {A B C : Set} →
         _≡_ {A = ((A → B) → A → (C → B))}
         (S  (K  K))
         (S  (S  (K  S)  (S  (K  K)  (S  (K  S)  K)))  (K  K))
lamwk$′ = refl


lamη′ : ∀ {A B} →
        _≡_ {A = (A → B) → (A → B)}
            (S  (S  (K  S)  K)  (K  (S  K  K)))
            (S  K  K)
lamη′ = refl

lamwk$₁′ = (S (K K))
lamwk$₂′ = (S  (S  (K  S)  (S  (K  K)  (S  (K  S)  K)))  (K  K))


lams₁ = (S  (K (S  (K  S)))  (S  (K  S)  (S  (K  S))))
lams₂ = (S  (S  (K  S)  (S  (K  K)  (S  (K  S)  (S  (K  (S  (K  S)))  S))))  (K  S))


--test = S  (S  (K  S)  (S  (K  K)  (S  (K  S)  K)))  (K  K) (S K K)

-- xxx =  S 
--          (S  (K  S) 
--           (S  (S  (K  S)  (S  (K  K)  (K  S))) 
--            (S 
--             (S  (K  S) 
--              (S  (S  (K  S)  (S  (K  K)  (K  S))) 
--               (S  (S  (K  S)  (S  (K  K)  (K  K))) 
--                (S  (K  K)  (K  K)))))
--              (S  (K  K)  (S  K  K )))))
--           (S  (S  (K  S)  (K  K))  (K  K ))

{-


(λ x y z → x z (y z)) $ (λ x y → x) $ (λ x y → x)
(λ z → (λ x y → x) z ((λ x y → x) z))
(λ z → (λ x y → x) z ((λ x y → x) z))
(λ z → z)

(S $ (K $ S) $ (S $ (K $ K))) ≡  K

(S $ (K $ S) $ (S $ (K $ K)))
λ x → (K S) x (S (K K) x)
λ x → S (S (K K) x)
λ x → S (λ t → (K K) t (x t))
λ x → S (λ t → K (x t))
λ x g h → (λ t → K (x t)) h (g h)
λ x g h → (K (x h)) (g h)
λ x g h → x h


-}
-}


-- This kit computes really annoying proofs when weakening
-- expressions built of combinators
infixl 5 _$$_
-- data Stable  : (t : (∀ {Γ} → Tm Γ τ)) → Set where
--   sk : Stable (LK {τ = τ}{σ})
--   ss : Stable (LS {τ = τ}{σ}{δ})
--   _$$_ : ∀ {f : (∀ {Γ} → Tm Γ (τ ⇒ σ))}{x : ∀ {Γ} → Tm Γ τ}
--        → Stable f → Stable x → Stable (f $ x)
-- 
-- pf : ∀ {t : ∀ {Γ} → Tm Γ τ} → Stable t → t [ π₁ {Γ}{δ} ] ≡ t
-- pf sk = k[π₁]
-- pf ss = s[π₁]
-- pf (s $$ s₁) = sub-$ _ _ _ ∙ cong₂ _$_ (pf s) (pf s₁)


data _∈_ : Ty → Con → Set where
  vz : τ ∈ (Γ ▹ τ)
  vs : τ ∈ Γ → τ ∈ (Γ ▹ σ)

∈-tm : τ ∈ Γ → Tm Γ τ
∈-tm vz = q
∈-tm (vs v) = ∈-tm v [ π₁ ]

data SVar : Tm Γ τ → Set where
  var : (v : τ ∈ Γ) → SVar (∈-tm v)
  sk : SVar {Γ} (LK {τ = τ}{σ})
  ss : SVar {Γ} (LS {τ = τ}{σ}{δ})
  _$$_ : ∀ {f x}
       → SVar {Γ} f → SVar {Γ}{τ} x → SVar {Γ}{σ} (f $ x)

ppf : ∀ {t} → SVar t → Σ (Tm (Γ ▹ σ) τ) (λ r → t [ π₁ ] ≡ r) 
ppf (var v) = ∈-tm (vs v) , refl
ppf sk = LK , k[π₁]
ppf ss = LS , s[π₁]
ppf (t $$ t₁) = 
    let (r  , p)  = ppf t
        (r₁ , p₁) = ppf t₁
    in (r $ r₁) , (sub-$ _ _ _ ∙ cong₂ _$_ p p₁)

ppf^ : ∀ {t} → SVar t → Σ (Tm (Γ ▹ σ ▹ δ) τ) (λ r → t [ π₁ ^ ] ≡ r) 
ppf^ (var vz) = q , ▹βt
ppf^ (var (vs v)) = ∈-tm (vs (vs v)) , (π, ∙ [∘])
ppf^ sk = LK , (lam[] ∙ cong lam (lam[] ∙ cong lam v₁-ign))
ppf^ ss = LS , (lam[] 
                ∙ cong lam 
                    (lam[]
                     ∙ cong lam 
                         (lam[]
                          ∙ cong lam 
                              (sub-$ _ _ _
                               ∙ cong₂ _$_ 
                                   (sub-$ _ _ _ ∙ cong₂ _$_ v₂-ign ▹βt)
                                   (sub-$ _ _ _ ∙ cong₂ _$_ v₁-ign ▹βt)))))
ppf^ (t $$ t₁) = let
   (r  , p)  = ppf^ t
   (r₁ , p₁) = ppf^ t₁
   in (r $ r₁) , (sub-$ _ _ _ ∙ cong₂ _$_ p p₁)

-- ppfx : ∀ {t} → SVar t → {γ : Sub Δ Γ} → Σ (Tm (Δ ▹ δ) τ) (λ r → t [ γ ^ ] ≡ r) 
-- ppfx (var vz) = q , ▹βt
-- ppfx (var (vs v)) {γ} = ∈-tm v [ γ ] [ π₁ ] , (π, ∙ [∘])
-- ppfx sk = LK , (lam[] ∙ cong lam (lam[] ∙ cong lam v₁-ign))
-- ppfx ss = LS , (lam[] 
--                 ∙ cong lam 
--                     (lam[]
--                      ∙ cong lam 
--                          (lam[]
--                           ∙ cong lam 
--                               (sub-$ _ _ _
--                                ∙ cong₂ _$_ 
--                                    (sub-$ _ _ _ ∙ cong₂ _$_ v₂-ign ▹βt)
--                                    (sub-$ _ _ _ ∙ cong₂ _$_ v₁-ign ▹βt)))))
-- 
-- ppfx (t $$ t₁) = let
--    (r  , p)  = ppfx t
--    (r₁ , p₁) = ppfx t₁
--    in (r $ r₁) , (sub-$ _ _ _ ∙ cong₂ _$_ p p₁)


-- ppf$ : ∀ {t : Tm Γ (τ ⇒ σ)} → SVar t → {x : Tm Γ τ} → Σ (Tm Γ σ) (λ r → t $ x ≡ r) 
-- ppf$ (var vz) {x} = q $ x , refl
-- ppf$ (var (vs v)) {x} = _ , (cong (_[ id ,s x ]) (app-sub _ _) ∙ sym [∘] ∙ cong (app (∈-tm v) [_]) (lift∘,s _ _))
-- ppf$ sk {x} = lam (x [ π₁ ]) , Kx
-- ppf$ ss {x} = _ , Sx {x = x} 
-- ppf$ (_$$_ {f = f}{x} t t₁) {y} = let
--    (r  , p)  = ppf$ t {x = x}
--    in r $ y , cong (_$ y) p

ppf$ : ∀ {t : Tm (Γ ▹ τ) σ} → SVar t → {x : Tm Γ τ} → Σ (Tm Γ σ) (λ r → t [ id ,s x ] ≡ r) 
ppf$ (var vz) {x} = x , ▹βt
ppf$ (var (vs v)) {x} =  _ , (π, ∙ [id])
ppf$ sk {x} = LK , (lam[] ∙ cong lam (lam[] ∙ cong lam v₁-ign))
ppf$ ss {x} = LS , (lam[] 
                ∙ cong lam 
                    (lam[]
                     ∙ cong lam 
                         (lam[]
                          ∙ cong lam 
                              (sub-$ _ _ _
                               ∙ cong₂ _$_ 
                                   (sub-$ _ _ _ ∙ cong₂ _$_ v₂-ign ▹βt)
                                   (sub-$ _ _ _ ∙ cong₂ _$_ v₁-ign ▹βt)))))

ppf$ (t $$ t₁) {x} = let
   (r  , p)  = ppf$ t
   (r₁ , p₁) = ppf$ t₁
   in (r $ r₁) , (sub-$ _ _ _ ∙ cong₂ _$_ p p₁)

KK : _≡_ {A = Tm Γ (ι ⇒ τ ⇒ σ ⇒ τ)}
     (LK $ LK)
     (lam LK)
KK = Kx ∙ cong lam (snd (ppf sk))

KS : _≡_ {A = Tm Γ ((ι ⇒ (τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ))}
     (LK $ LS)
     (lam LS)
KS = Kx ∙ cong lam (snd (ppf ss))

K[γ,x] : {γ : Sub Δ Γ}{x : Tm Δ ι} → LK [ γ ,s x ] ≡ LK {τ = τ}{σ = σ}
K[γ,x] = lam[] ∙ cong lam (lam[] ∙ cong lam v₁-ign)

S[γ,x] : {γ : Sub Δ Γ}{x : Tm Δ ι} → LS [ γ ,s x ] ≡ LS {τ = τ}{σ}{δ}
S[γ,x] = lam[] 
         ∙ cong lam
             (lam[]
              ∙ cong lam 
                  (lam[]
                   ∙ cong lam 
                       (sub-$ _ _ _
                        ∙ cong₂ _$_ 
                            (sub-$ _ _ _ ∙ cong₂ _$_ v₂-ign v₀-ign)
                            (sub-$ _ _ _ ∙ cong₂ _$_ v₁-ign v₀-ign))))



S$lamx$lamy : ∀ {x : Tm (Γ ▹ τ) (ι ⇒ σ)}{y : Tm (Γ ▹ τ) ι}
    → _≡_ {A = Tm Γ (τ ⇒ σ)}
      (LS $ lam x $ lam y)
      (lam (x $ y))
S$lamx$lamy {x = x}{y} = Sxy 
              ∙ cong lam (cong₂ _$_ 
                  (cong (_$ v₀) (lam[])
                   ∙ (lam-$ _ _ ∙ sym [∘]
                      ∙ cong (x [_]) (lift∘,s _ _ ∙ cong₂ _,s_ (sym idr) (sym [id]) ∙ ▹η) ∙ [id]))
                  (cong (_$ v₀) (lam[])
                   ∙ (lam-$ _ _ ∙ sym [∘]
                      ∙ cong (y [_]) (lift∘,s _ _ ∙ cong₂ _,s_ (sym idr) (sym [id]) ∙ ▹η) ∙ [id])))

lamKβ : _≡_ {A = Tm ε ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
            (LS $ (LK $ LS) $ (LS $ (LK $ LK)))
            LK
lamKβ = ext (
          cong (_$ q) (snd (ppf ((ss $$ (sk $$ ss) $$ (ss $$ (sk $$ sk))))))
          ∙ LSβ _ _ _
          ∙ cong (_$ (LS $ (LK $ LK) $ q)) (LKβ _ _)
          ∙ ext (
              cong (_$ q) (snd (ppf (ss $$ (ss $$ (sk $$ sk) $$ var vz))))
              ∙ ext (
                 cong (_$ q) (snd (ppf (ss $$ (ss $$ (sk $$ sk) $$ var (vs vz)) $$ var vz)))
                 ∙ LSβ _ _ _
                 ∙ cong (_$ (v₁ $ v₀)) (LSβ _ _ _)
                 ∙ cong (λ t → t $ (v₂ $ v₀) $ (v₁ $ v₀)) (LKβ _ _)
                 ∙ LKβ _ _
                 )
              ∙ sym (x[π]q ∙ cong app (x[π]q ∙ lamβ) ∙ lamβ)
            )
          )
lamwk$₂ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
          (LS $ (LS $ (LK $ LS) $ (LS $ (LK $ LK) $ (LS $ (LK $ LS) $ LK))) $ (LK $ LK))
          (lam (lam (lam (v₂ $ v₁))))
lamwk$₂ {τ = τ}{σ = σ}{ι = ι} = ext (
             cong (_$ q) 
                  (snd (ppf ((ss $$ (ss $$ (sk $$ ss) $$ (ss $$ (sk $$ sk) $$ (ss $$ (sk $$ ss) $$ sk))) $$ (sk $$ sk)))))
             ∙ LSβ _ _ _
             ∙ cong₂ _$_ 
                 (LSβ _ _ _
                  ∙ cong (_$ (LS $ (LK $ LK) $ (LS $ (LK $ LS) $ LK) $ q)) (LKβ _ _)
                  ∙ cong (LS $_) (LSβ _ _ _ 
                                  ∙ cong (_$ (LS $ (LK $ LS) $ LK $ q)) (LKβ _ _)
                                  ∙ cong (LK {τ = (ι ⇒ τ) ⇒ ι ⇒ σ}{τ}  $_)
                                         (LSβ _ _ _ ∙ cong (_$ (LK $ q)) (LKβ _ _)))) 
                 (LKβ _ _)
             ∙ ext (cong (_$ q) 
                        (snd (ppf ((ss $$ (sk $$ (ss $$ (sk $$ var vz))) $$ sk) ))) 
                      ∙ LSβ _ _ _
                      ∙ cong (_$ (LK $ q)) (LKβ _ _)
                      ∙ ext (
                            cong (_$ q) (snd (ppf ((ss $$ (sk $$ var (vs vz)) $$ (sk $$ var vz)))))
                            ∙ LSβ _ _ _
                            ∙ cong (_$ (LK $ v₁ $ v₀)) (LKβ _ _)
                            ∙ cong (v₂ $_) (LKβ _ _) 
                            ∙ (sym (x[π]q
                                    ∙ cong app (x[π]q ∙ (cong app (x[π]q ∙ lamβ) ∙ lamβ))
                                    ∙ lamβ))
                        )
               )
        )


lamwk$₁ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
          (LS $ (LK $ LK))
          (lam (lam (lam (v₂ $ v₁))))
lamwk$₁ = ext (
              cong (_$ q) (snd (ppf (ss $$ (sk $$ sk))))
              ∙ ext (
                  cong (_$ q) (snd (ppf (ss $$ (sk $$ sk) $$ var vz)))
                  ∙ LSβ _ _ _
                  ∙ cong (_$ (v₁ $ v₀)) (LKβ _ _)
                  ∙ ext (
                      cong (_$ q) (sub-$ _ _ _ ∙ cong₂ _$_ k[π₁] (sub-$ _ _ _))
                      ∙ LKβ _ _
                      ∙ sym (x[π]q ∙ cong app (x[π]q ∙ cong app (x[π]q ∙ lamβ) ∙ lamβ) ∙ lamβ))
                )
            )

lamwk$ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
             (LS $ (LK $ LK))
             (LS $ (LS $ (LK $ LS) $ (LS $ (LK $ LK) $ (LS $ (LK $ LS) $ LK))) $ (LK $ LK))
lamwk$ = lamwk$₁ ∙ sym lamwk$₂


lamη₁ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)}
            (LS $ (LS $ (LK $ LS) $ LK) $ (LK $ (LS $ LK $ LK {σ = δ})))
            (lam v₀)
lamη₁ {τ = τ}{σ = σ}{δ = δ} = ext (
            cong (_$ q) (snd (ppf (ss $$ (ss $$ (sk $$ ss) $$ sk) $$ (sk $$ (ss $$ sk $$ sk)))))
            ∙ LSβ _ _ _
            ∙ cong (_$ (LK $ (LS $ LK $ LK {σ = δ}) $ q)) (LSβ _ _ _)
            ∙ cong (λ t → t $ (LK $ q) $ (LK $ (LS $ LK $ LK {σ = δ}) $ q)) (LKβ _ _)
            ∙ ext (
                cong (_$ q) (snd (ppf (ss $$ (sk $$ var vz) $$ (sk $$ (ss $$ sk $$ sk) $$ var vz))))
                ∙ LSβ _ _ _
                ∙ cong (λ t → t $ (LK $ (LS $ LK $ LK {σ = δ}) $ v₁ $ v₀)) (LKβ _ _)
                ∙ cong (v₁ $_) (cong (_$ q) (LKβ _ _ ∙ skk≡i) ∙ lam-$ _ _ ∙ ▹βt)
                ∙ sym (x[π]q ∙ cong app (x[π]q ∙ lamβ) ∙ sym x[π]q)
              )
          )

comb-lamη : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)}
       (LS $ (LS $ (LK $ LS) $ LK) $ (LK $ (LS $ LK $ LK {σ = δ})))
       (LS $ LK $ LK {σ = δ})
comb-lamη = lamη₁ ∙ sym skk≡i

-- XXX this proof can be shortened by using some of the ppf tricks as in the second part
-- This is a proof without ext!
lamSβ₁ : _≡_ {A = Tm Γ ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
             (LS $ (LK $(LS $ (LK $ LS))) $ (LS $ (LK $ LS) $ (LS $ (LK $ LS))))
             (lam (lam (lam (lam (v₃ $ v₀ $ (v₁ $ v₀) $ (v₂ $ v₀ $ (v₁ $ v₀)))))))
lamSβ₁ = Sxy 
           ∙ cong lam 
               (cong₂ _$_ 
                  (cong (_$ q) (snd (ppf (sk $$ (ss $$ (sk $$ ss)))))
                   ∙ LKβ _ _)
                  (cong (_$ q) (snd (ppf (ss $$ (sk $$ ss) $$ (ss $$ (sk $$ ss)))))
                   ∙ LSβ _ _ _ ∙ cong (_$ (LS $ (LK $ LS) $ q)) (LKβ _ _))
                ∙ Sxy
                ∙ cong lam
                    (cong₂ _$_ 
                       (cong (_$ q) (snd (ppf (sk $$ ss))) ∙ LKβ _ _)
                       (cong (_$ q) (snd (ppf (ss $$ (ss $$ (sk $$ ss) $$ var vz))))
                        ∙ Sxy
                        ∙ cong lam 
                            (cong₂ _$_ 
                               (cong (_$ q) (snd (ppf (ss $$ (sk $$ ss) $$ var (vs vz))))
                                ∙ LSβ _ _ _
                                ∙ cong (_$ (v₂ $ v₀)) (LKβ _ _)
                                )
                               refl
                             ∙ Sxy
                             ∙ cong lam 
                                 (cong₂ _$_ (cong (_$ q) (snd (ppf (var (vs (vs vz)) $$ var vz))))
                                 (cong (_$ q) (snd (ppf (var (vs vz) $$ var vz)))))
                             )
                        )
                      ∙ Sx
                      ∙ cong 
                          (λ t → lam (lam t)) 
                          (cong (_$ (v₁ $ v₀)) 
                             (x[π]q
                              ∙ cong app 
                                  (lam[]
                                   ∙ cong lam 
                                       (lam[]
                                        ∙ cong lam 
                                            (sub-$ _ _ _
                                             ∙ cong₂ _$_ 
                                                 (sub-$ _ _ _
                                                  ∙ cong₂ _$_ 
                                                      (sub-$ _ _ _ ∙ cong₂ _$_ (π, ∙ cong (v₂ [_]) ,s∘ ∙ π, ∙ [∘]) v₁-ign) v₀-ign)
                                                      (sub-$ _ _ _ 
                                                       ∙ cong₂ _$_ 
                                                           (sub-$ _ _ _ 
                                                            ∙ cong₂ _$_ 
                                                                (π, ∙ cong (v₁ [_]) ,s∘ ∙ π, ∙ [∘]) 
                                                                (π, ∙ cong (v₀ [_]) ,s∘ ∙ ▹βt) ) ▹βt)))
                                   )
                              ∙ lamβ)
                           ∙ lam-$ _ _
                           ∙ sub-$ _ _ _ 
                           ∙ cong₂ _$_
                               (sub-$ _ _ _ 
                                ∙ cong₂ _$_
                                    (sub-$ _ _ _ ∙ cong₂ _$_ (π, ∙ [id] ∙ [∘]) (π, ∙ [id]))
                                    ▹βt)
                               (sub-$ _ _ _ 
                                ∙ cong₂ _$_
                                    (sub-$ _ _ _
                                     ∙ cong₂ _$_ 
                                         (π, ∙ [id] ∙ [∘])
                                         (π, ∙ [id]))
                                    ▹βt)
                           )
                      )
                )



lamSβ₂ : _≡_ {A = Tm Γ ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
             (LS $ (LS $ (LK $ LS) $ (LS $ (LK $ LK) $ (LS $ (LK $ LS) $ (LS $ (LK $ (LS $ (LK $ LS))) $ LS)))) $ (LK $ LS))
             (lam (lam (lam (lam (v₃ $ v₀ $ (v₁ $ v₀) $ (v₂ $ v₀ $ (v₁ $ v₀)))))))
lamSβ₂ = Sxy
         ∙ cong lam (cong₂ _$_
                       (cong (_$ q) (snd (ppf ((ss $$ (sk $$ ss) $$ (ss $$ (sk $$ sk) $$
                                    (ss $$ (sk $$ ss) $$ (ss $$ (sk $$ (ss $$ (sk $$ ss))) $$ ss)))))))
                        ∙ LSβ _ _ _) 
                       (cong (_$ q) (snd (ppf (sk $$ ss))) ∙ LKβ _ _))
         ∙ cong lam 
             (cong₂ _$_ 
                (cong₂ _$_ (LKβ _ _) 
                (cong₂ _$_ 
                   (cong₂ _$_ refl 
                     (Sxy
                      ∙ cong lam (cong₂ _$_ 
                                    (cong (_$ q) (snd (ppf (sk $$ ss))) ∙ LKβ _ _) 
                                    (cong (_$ q) (snd (ppf (ss $$ (sk $$ (ss $$ (sk $$ ss))) $$ ss))) ∙ LSβ _ _ _))
                      ∙ cong (λ t → lam (LS $ t)) (cong (_$ (LS $ q)) (LKβ _ _) {-∙ ? -})
                      --∙ ?
                      ))
                   refl)) refl)
         ∙ cong lam
             (Sxy
              ∙ cong lam (cong₂ _$_ 
                            (cong₂ _$_ 
                               (sub-$ _ _ _ 
                                ∙ cong (_$ v₁)
                                    (sub-$ _ _ _
                                     ∙ cong₂ _$_ 
                                         (snd (ppf (ss $$ (sk $$ sk))))
                                         (lam[] ∙ cong lam (snd (ppf^ (ss $$ (ss $$ (sk $$ ss) $$ (ss $$ var vz))))))))
                               refl) 
                            (cong (_$ q) s[π₁])))
         ∙ cong (λ t → lam (lam (t $ v₀ $ (LS $ v₀))))
             (LSβ _ _ _ ∙ cong (_$ (lam (LS $ (LS $ (LK $ LS) $ (LS $ q))) $ (q [ π₁ ]))) (LKβ _ _))
         ∙ cong (λ t → lam (lam (t $ (LS $ v₀))))
             (LKβ _ _ ∙ lam-$ _ _ ∙ snd (ppf$ (ss $$ (ss $$ (sk $$ ss) $$ (ss $$ var vz)))))
         ∙ cong (λ t → lam (lam t))
             (Sxy ∙ cong lam (cong₂ _$_ 
                                (cong (_$ q) (snd (ppf (ss $$ (sk $$ ss) $$ (ss $$ var (vs vz)))))
                                 ∙ LSβ _ _ _ ∙ cong (_$ (LS $ ((q [ π₁ ]) [ π₁ ]) $ q)) (LKβ _ _))
                                (cong (_$ q) (snd (ppf (ss $$ var vz)))))
                                 )
         ∙ cong (λ t → lam (lam (lam t)))
             (Sxy ∙ cong lam (cong₂ _$_ 
                                (cong (_$ q) (snd (ppf (ss $$ var (vs (vs vz)) $$ var vz))))
                                (cong (_$ q) (snd (ppf (ss $$ var (vs vz) $$ var vz))))))
         ∙ cong (λ t → lam (lam (lam (lam t))))
             (cong₂ _$_ (LSβ _ _ _) (LSβ _ _ _))

lamSβ : _≡_ {A = Tm Γ ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
             (LS $ (LK $(LS $ (LK $ LS))) $ (LS $ (LK $ LS) $ (LS $ (LK $ LS))))
             (LS $ (LS $ (LK $ LS) $ (LS $ (LK $ LK) $ (LS $ (LK $ LS) $ (LS $ (LK $ (LS $ (LK $ LS))) $ LS)))) $ (LK $ LS))
lamSβ = lamSβ₁ ∙ sym lamSβ₂
