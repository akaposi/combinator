{-# OPTIONS --cubical --safe #-}
open import Agda.Primitive
open import Cubical.Core.Primitives hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Foundations.HLevels

open import Syntax.Ty

-- combinatory calculus w/ q and wk
module Syntax.L (T : TY) where

open import Syntax.Common T

open import Syntax.L.Base T public
open import Syntax.L.Comb T public
open import Syntax.L.CombProp T public
open import Syntax.L.MoreCombProp T public
import Syntax.L.Interp
