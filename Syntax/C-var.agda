{-# OPTIONS --cubical --safe #-}
open import Cubical.Core.Primitives 
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels

open import Syntax.Ty

-- combinatory calculus w/ variable
module Syntax.C-var (T : TY) where

open import Syntax.Common T

infixl 6 _$_

data Var : Con → Ty → Type where
  vz  : Var  (Γ ▹ τ) τ
  vs  : Var Γ τ → Var  (Γ ▹ σ) τ 

data Tm : Con → Ty → Type where
  var    : Var Γ τ → Tm Γ τ
  K      : Tm Γ (τ ⇒ σ ⇒ τ)
  S      : Tm Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_    : Tm Γ (τ ⇒ σ) → Tm Γ τ → Tm Γ σ
  Kβ     : {x : Tm Γ τ}{y : Tm Γ σ} → K $ x $ y ≡ x
  Sβ     : {x : Tm Γ (τ ⇒ σ ⇒ δ)}{y : Tm Γ (τ ⇒ σ)}{z : Tm Γ τ}
           → S $ x $ y $ z ≡ x $ z $ (y $ z)

  lamKβ  : _≡_ {A = Tm Γ ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
              (S $ (K $ S) $ (S $ (K $ K)))
              K
  lamSβ  : _≡_ {A = Tm Γ ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
              (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
              (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))
  lamwk$ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
              (S $ (K $ K))
              (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))

  η      : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)}
          (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = δ})))
          (S $ K $ K {σ = δ})

  TmSet  : isSet (Tm Γ τ)

wk : Tm Γ σ → Tm (Γ ▹ τ) σ
wk (var x)              = var (vs x)
wk K                    = K
wk S                    = S
wk (e $ e₁)             = wk e $ wk e₁
wk (Kβ {x = x}{y} i)    = Kβ {x = wk x}{wk y} i
wk (Sβ {x = x}{y}{z} i) = Sβ {x = wk x}{wk y}{wk z} i
wk (TmSet a b c d i j)  =
   isSet→isSet' TmSet 
     (λ j → wk (c j))
     (λ j → wk (d j))
     (λ _ → wk a)
     (λ _ → wk b)
     i j

wk (lamKβ i)    = lamKβ i
wk (lamSβ i)    = lamSβ i
wk (lamwk$ i)   = lamwk$ i
wk (η{δ = δ} i) = η{δ = δ} i
