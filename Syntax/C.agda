{-# OPTIONS --cubical --safe #-}
open import Cubical.Core.Primitives
open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

-- combinatory calculus w/o variables
module Syntax.C (T : TY) where

open import Syntax.Common T

infixl 6 _$_

data Tm : Ty → Type where
  K      : Tm (τ ⇒ σ ⇒ τ)
  S      : Tm ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_    : Tm (τ ⇒ σ) → Tm τ → Tm σ
  Kβ     : {x : Tm τ}{y : Tm σ} → K $ x $ y ≡ x
  Sβ     : {x : Tm (τ ⇒ σ ⇒ δ)}{y : Tm (τ ⇒ σ)}{z : Tm τ}
     → S $ x $ y $ z ≡ x $ z $ (y $ z)

  lamKβ  : _≡_ {A = Tm ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
               (S $ (K $ S) $ (S $ (K $ K)))
               K

  lamSβ  : _≡_ {A = Tm ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
               (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
               (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))

  lamwk$ : _≡_ {A = Tm ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
               (S $ (K $ K))
               (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))

  -- this is the point free version of (S$K$u)$I = u
  η      : _≡_ {A = Tm ((τ ⇒ σ) ⇒ τ ⇒ σ)}
               (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = δ})))
               (S $ K $ K {σ = δ})

  TmSet  : isSet (Tm τ)
