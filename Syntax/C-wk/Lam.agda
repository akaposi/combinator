{-# OPTIONS --cubical --safe #-}
open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

module Syntax.C-wk.Lam (T : TY) where

open import Syntax.Common T
open import Syntax.C-wk.Base T
open import Syntax.C-wk.Lift-ctx T

lam : Tm (Γ ▹ τ) σ → Tm Γ (τ ⇒ σ)
lam {τ = τ} q                 = S $ K $ K {σ = τ}
lam (wk x)                    = K $ x
lam K                         = K $ K
lam S                         = K $ S
lam (x $ x₁)                  = S $ lam x $ lam x₁
lam {Γ = Γ} (Kβ {x = x}{y} i) = lamKβ-≡ {x = lam x}{lam y} i
lam (Sβ {x = x}{y}{z} i)      = lamSβ-≡ {x = lam x}{lam y}{lam z} i
lam (wk$ {x = x}{y} i)        = lamwk$-≡ {x = x}{y} i
lam (wkK i)                   = K $ K
lam (wkS i)                   = K $ S
lam (TmSet a b c d i j)       = TmSet (lam a) (lam b) (cong lam c) (cong lam d) i j
