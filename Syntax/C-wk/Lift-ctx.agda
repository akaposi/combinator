{-# OPTIONS --cubical --safe #-}
open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

module Syntax.C-wk.Lift-ctx (T : TY) where

open import Syntax.Common T
open import Syntax.C-wk.Base T

-- wk' : Con → Tm ε τ → Tm Γ τ 
lift-ctx : Tm ε τ → Tm Γ τ 
lift-ctx {Γ = ε} t = t
lift-ctx {Γ = Γ ▹ x} t = wk (lift-ctx {Γ = Γ} t)

-- wk' Γ K ≡ K
lift-ctx-K : lift-ctx {Γ = Γ} (K {τ = τ}{σ}) ≡ K
lift-ctx-K {Γ = ε} = refl
lift-ctx-K {Γ = Γ ▹ x} = cong wk lift-ctx-K ∙ wkK

-- wk' Γ S ≡ S
lift-ctx-S : lift-ctx {Γ = Γ} (S {τ = τ}{σ}{δ}) ≡ S
lift-ctx-S {Γ = ε} = refl
lift-ctx-S {Γ = Γ ▹ x} = cong wk lift-ctx-S ∙ wkS

-- wk' Γ (t $ u) ≡ wk' Γ t $ wk' Γ u
lift-ctx-$ : ∀ {t : Tm _ (τ ⇒ σ)}{u} 
           → (lift-ctx {Γ = Γ} (t $ u)) ≡ lift-ctx t $ lift-ctx u
lift-ctx-$ {Γ = ε} {t} {u} = refl
lift-ctx-$ {Γ = Γ ▹ x} {t} {u} = cong wk lift-ctx-$ ∙ wk$

lift-$₁ : ∀ {t p : Tm Γ (τ ⇒ σ)}{a₁} → t ≡ p → t $ a₁ ≡ p $ a₁
lift-$₁ {a₁ = a₁} eq = cong (_$ a₁) eq

lift-$₂ : ∀ {t p : Tm Γ (τ ⇒ σ ⇒ δ)}{a₁ a₂} 
        → t ≡ p → t $ a₁ $ a₂ ≡ p $ a₁ $ a₂
lift-$₂ {a₁ = a₁}{a₂} eq = lift-$₁ (lift-$₁ eq)


lift-$₃ : ∀ {γ}{t p : Tm Γ (τ ⇒ σ ⇒ δ ⇒ γ)}{a₁ a₂ a₃} 
        → t ≡ p → t $ a₁ $ a₂ $ a₃ ≡ p $ a₁ $ a₂ $ a₃
lift-$₃ eq = lift-$₁ (lift-$₂ eq)

lift-$₄ : ∀ {γ α}{t p : Tm Γ (τ ⇒ σ ⇒ δ ⇒ γ ⇒ α)}{a₁ a₂ a₃ a₄} 
        → t ≡ p → t $ a₁ $ a₂ $ a₃ $ a₄ ≡ p $ a₁ $ a₂ $ a₃ $ a₄
lift-$₄ eq = lift-$₂ (lift-$₂ eq)

-- lamKβ-open
lamKβ'-≡ : _≡_ {A = Tm Γ ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
            (S $ (K $ S) $ (S $ (K $ K)))
            K
lamKβ'-≡ = sym (
           (lift-ctx-$
            ∙ cong₂ _$_
                   (lift-ctx-$
                    ∙ cong₂ _$_ refl lift-ctx-$)
                   (lift-ctx-$
                    ∙ refl))
            ∙ cong₂ _$_
                   (cong₂ _$_
                          lift-ctx-S
                          (cong₂ _$_
                                 lift-ctx-K
                                 lift-ctx-S))
                    (cong₂ _$_
                           lift-ctx-S
                           (lift-ctx-$
                            ∙ cong₂ _$_
                                    lift-ctx-K
                                    lift-ctx-K))
           )
           ∙ cong lift-ctx lamKβ
           ∙ lift-ctx-K

-- lamKβ-open-pointful
lamKβ-≡ : ∀ {x : Tm Γ (ι ⇒ τ)}{y : Tm Γ (ι ⇒ σ)}
        → S $ (S $ (K $ K) $ x) $ y ≡ x    
lamKβ-≡ {x = x}{y} =
  cong (λ z → z $ (S $ (K $ K) $ x) $ y) (sym Kβ) ∙
  cong (_$ y) (sym Sβ) ∙
  cong (λ z → z $ x $ y) lamKβ'-≡ ∙
  Kβ

abstract
  lamSβ-≡ : ∀ {x : Tm Γ (ι ⇒ τ ⇒ σ ⇒ δ)}{y : Tm Γ (ι ⇒ τ ⇒ σ)}{z : Tm Γ (ι ⇒ τ)}
          → S $ (S $ (S $ (K $ S) $ x) $ y) $ z
            ≡ S $ (S $ x $ z) $ (S $ y $ z)
  lamSβ-≡ {x = x}{y}{z} =
        sym (
          lift-$₃ lift-ctx-$
          ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
          ∙ lift-$₂ Sβ
          ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₃ Kβ
          ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
          ∙ lift-$₁ Sβ
          ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₂ Kβ
          ∙ lift-$₂ lift-ctx-S
          ∙ cong (λ t → S $ t $ z)
                 (
                    lift-$₂ lift-ctx-$
                    ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
                    ∙ lift-$₁ Sβ
                    ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
                    ∙ lift-$₂ Kβ
                    ∙ lift-$₂ lift-ctx-S
                    ∙ cong (λ t → S $ t $ y) 
                           (
                             lift-$₁ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
                             ∙ cong (λ t → S $ t $ x) 
                                    (lift-ctx-$ ∙ cong₂ _$_ lift-ctx-K lift-ctx-S)
                           )
                 )
        ) 
        ∙ cong (λ t → t $ x $ y $ z) (cong lift-ctx lamSβ)
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-$ ∙ lift-$₂ lift-ctx-S)
        ∙ lift-$₃ Sβ
        ∙ lift-$₃ (lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K))
        ∙ lift-$₄ Kβ
        ∙ lift-$₄ lift-ctx-S
        ∙ lift-$₁ Sβ
        ∙ lift-$₄ lift-ctx-$
        ∙ lift-$₃ (lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S))
        ∙ lift-$₃ Sβ
        ∙ lift-$₃ (lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K))
        ∙ lift-$₄ Kβ
        ∙ lift-$₄ lift-ctx-K
        ∙ lift-$₂ Kβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ lift-ctx-S
        ∙ Sβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₁ Sβ
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₂ Kβ
        ∙ lift-$₂ lift-ctx-S
        ∙ cong₂ (λ x y → S $ x $ y)
                (lift-$₂ lift-ctx-S)
                (lift-$₃ (lift-ctx-$ 
                          ∙ lift-$₁ lift-ctx-K)
                          ∙ lift-$₂ Kβ
                          ∙ lift-$₂ lift-ctx-S)

lamSβ'-≡ : _≡_ {A = Tm Γ ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
              (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
              (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))
lamSβ'-≡ = sym (
             lift-ctx-$
             ∙ cong₂ _$_
                     (lift-ctx-$
                       ∙ cong₂ _$_
                               lift-ctx-S
                               (lift-ctx-$
                                ∙ cong₂ _$_
                                        lift-ctx-K
                                        (lift-ctx-$
                                         ∙ cong₂ _$_
                                                 lift-ctx-S
                                                 (lift-ctx-$
                                                  ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))))
                     (lift-ctx-$
                      ∙ cong₂ _$_
                              (lift-ctx-$
                               ∙ cong₂ _$_
                                       lift-ctx-S
                                       (lift-ctx-$
                                        ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))
                              (lift-ctx-$
                               ∙ cong₂ _$_
                                       lift-ctx-S
                                       (lift-ctx-$
                                        ∙ cong₂ _$_ lift-ctx-K lift-ctx-S)))
           )
           ∙ cong lift-ctx lamSβ
           ∙ lift-ctx-$
           ∙ cong₂ _$_
                   (lift-ctx-$
                    ∙ cong₂ _$_
                            lift-ctx-S
                            (lift-ctx-$
                             ∙ cong₂ _$_
                                   (lift-ctx-$
                                    ∙ cong₂ _$_
                                            lift-ctx-S
                                            (lift-ctx-$
                                             ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))
                                   (lift-ctx-$
                                    ∙ cong₂ _$_
                                             (lift-ctx-$
                                              ∙ cong₂ _$_
                                                   lift-ctx-S
                                                   (lift-ctx-$
                                                    ∙ cong₂ _$_ lift-ctx-K lift-ctx-K))
                                             (lift-ctx-$
                                              ∙ cong₂ _$_
                                                       (lift-ctx-$
                                                        ∙ cong₂ _$_
                                                                lift-ctx-S
                                                                (lift-ctx-$
                                                                 ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))
                                                       (lift-ctx-$
                                                        ∙ cong₂ _$_
                                                                (lift-ctx-$
                                                                 ∙ cong₂ _$_
                                                                         lift-ctx-S
                                                                         (lift-ctx-$
                                                                          ∙ cong₂ _$_
                                                                                  lift-ctx-K
                                                                                  (lift-ctx-$
                                                                                   ∙ cong₂ _$_
                                                                                           lift-ctx-S
                                                                                           (lift-ctx-$
                                                                                            ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))))
                                                                 lift-ctx-S)))))
                   (lift-ctx-$
                    ∙ cong₂ _$_ lift-ctx-K lift-ctx-S)

lamwk$-≡ : ∀ {x : Tm Γ (τ ⇒ σ)}{y : Tm Γ τ}
         → K {σ = ι} $ (x $ y) ≡ S $ (K $ x) $ (K $ y)
lamwk$-≡ {ι = ι}{x = x}{y} =
        sym (
          lift-$₂ lift-ctx-$
          ∙ lift-$₃ lift-ctx-S
          ∙ Sβ
          ∙ lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₁ Kβ
          ∙ lift-$₁ lift-ctx-K
        )
        ∙ cong (λ t → t $ x $ y) (cong lift-ctx (lamwk$ {ι = ι}))
        ∙ lift-$₂ lift-ctx-$
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₁ Sβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ lift-ctx-S
        ∙ Sβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ lift-ctx-K
        ∙ lift-$₁ Kβ
        ∙ lift-$₂ lift-ctx-$
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₁ Sβ
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₂ Kβ
        ∙ lift-$₂ lift-ctx-S
        ∙ cong₂ (λ x y → S $ x $ y)
                (
                  lift-$₁ lift-ctx-K
                )
                (
                  lift-$₂ lift-ctx-$
                  ∙ lift-$₃ lift-ctx-K
                  ∙ lift-$₁ Kβ
                  ∙ lift-$₁ lift-ctx-K
                )

lamwk$'-≡ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
               (S $ (K $ K))
               (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))
lamwk$'-≡ = sym (
           lift-ctx-$
           ∙ cong₂ _$_
                   lift-ctx-S
                   (lift-ctx-$
                    ∙ cong₂ _$_ lift-ctx-K lift-ctx-K)
          )
          ∙ cong lift-ctx lamwk$
          ∙ lift-ctx-$
          ∙ cong₂ _$_
                  (lift-ctx-$
                   ∙ cong₂ _$_
                           lift-ctx-S
                           (lift-ctx-$
                            ∙ cong₂ _$_
                                    (lift-ctx-$
                                     ∙ cong₂ _$_
                                             lift-ctx-S
                                             (lift-ctx-$
                                              ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))
                                    (lift-ctx-$
                                     ∙ cong₂ _$_
                                             (lift-ctx-$
                                              ∙ cong₂ _$_
                                                      lift-ctx-S
                                                      (lift-ctx-$
                                                       ∙ cong₂ _$_ lift-ctx-K lift-ctx-K))
                                             (lift-ctx-$
                                              ∙ cong₂ _$_
                                                      (lift-ctx-$
                                                       ∙ cong₂ _$_
                                                               lift-ctx-S
                                                               (lift-ctx-$
                                                                ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))
                                                      lift-ctx-K))))
                  (lift-ctx-$
                   ∙ cong₂ _$_ lift-ctx-K lift-ctx-K)

η-≡ : _≡_ {A = Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)}
          (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = δ})))
           (S $ K $ K {σ = δ})
η-≡ = sym (
                 lift-ctx-$
                 ∙ cong₂ _$_
                         (lift-ctx-$
                          ∙ cong₂ _$_
                                  lift-ctx-S
                                  (lift-ctx-$
                                   ∙ cong₂ _$_
                                           (lift-ctx-$
                                            ∙ cong₂ _$_
                                                    lift-ctx-S
                                                    (lift-ctx-$
                                                     ∙ cong₂ _$_
                                                             lift-ctx-K lift-ctx-S))
                                           lift-ctx-K))
                         (lift-ctx-$
                          ∙ cong₂ _$_
                                  lift-ctx-K
                                  (lift-ctx-$
                                   ∙ cong₂ _$_
                                           (lift-ctx-$
                                            ∙ cong₂ _$_
                                                    lift-ctx-S lift-ctx-K)
                                           lift-ctx-K))
                    )
                ∙ cong lift-ctx η
                ∙ lift-ctx-$
                ∙ cong₂ _$_
                        (lift-ctx-$
                         ∙ cong₂ _$_
                                 lift-ctx-S lift-ctx-K)
                        lift-ctx-K

