{-# OPTIONS --cubical --safe #-}
open import Cubical.Core.Primitives 
open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

-- combinatory calculus w/ q and wk
module Syntax.C-wk.Base (T : TY) where

open import Syntax.Common T

infixl 6 _$_

data Tm : Con → Ty → Type where
  q      : Tm (Γ ▹ τ) τ
  wk     : Tm Γ σ → Tm (Γ ▹ τ) σ 
  K      : Tm Γ (τ ⇒ σ ⇒ τ)
  S      : Tm Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_    : Tm Γ (τ ⇒ σ) → Tm Γ τ → Tm Γ σ
  Kβ     : {x : Tm Γ τ}{y : Tm Γ σ} → K $ x $ y ≡ x
  Sβ     : {x : Tm Γ (τ ⇒ σ ⇒ δ)}{y : Tm Γ (τ ⇒ σ)}{z : Tm Γ τ}
         → S $ x $ y $ z ≡ x $ z $ (y $ z) 
  wk$    : {x : Tm Γ (τ ⇒ σ)}{y : Tm Γ τ} → wk {τ = δ} (x $ y) ≡ wk x $ wk y
  wkK    : wk {Γ = Γ}{τ = δ} (K {τ = τ}{σ = σ}) ≡ K
  wkS    : ∀ {γ} → wk {Γ = Γ}{τ = γ} (S {τ = τ}{σ = σ}{δ = δ}) ≡ S

  -- Previously extensionality was defined as follows:
  -- ext : {x y : Tm Γ (τ ⇒ σ)} → wk x $ q ≡ wk y $ q → x ≡ y
  --
  -- instead of ext, we need:
  -- 1. lamKβ : lam (K $ x $ y) ≡ lam x
  -- unfold lam
  -- 2. lamKβ : S $ (S $ (K $ K) $ lam x) $ lam y ≡ lam x
  -- remove lam
  -- 3. lamKβ : S $ (S $ (K $ K) $ u) $ v ≡ u
  -- make it closed
  -- 4. lamKβ : lam (lam (S $ (S $ (K $ K) $ wk vz) $ vz)) ≡ K
  lamKβ  : _≡_ {A = Tm ε ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
               (S $ (K $ S) $ (S $ (K $ K)))
               K

  lamSβ  : _≡_ {A = Tm ε ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
               (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
               (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))

  lamwk$ : _≡_ {A = Tm ε ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
               (S $ (K $ K))
               (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))

  η      : _≡_ {A = Tm ε ((τ ⇒ σ) ⇒ τ ⇒ σ)}
               (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = δ})))
               (S $ K $ K {σ = δ})

  TmSet : isSet (Tm Γ τ)

