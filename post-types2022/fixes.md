Detailed Description of the Changes
===================================

* Improved the introduction, explaining that we are presenting
  the method; Agda is simply a vehicle to implement the method,
  and that within Agda we have four different ways of encoding
  the proposed method.

* We addressed (a) of reviewer (1) by adding a short 
  background section where we explain the key Agda constructions
  used in the paper.

* We addressed (b) of reviewer (1) by first listing all the variants
  of the Munchhausen method in the introduction, and then
  referring to them in later sections.

* We addressed (c) of reviewer (1) by adjusting the text.

* We fixed the typos found by the reviewer (2).
  - we didn't fix (:225) as it seems that "intrinsically-typed" can
    be found in literature, for example: https://dl.acm.org/doi/10.1145/3158104

* We adjusted the prose in the beginning of Section 6, and we
  explicitly mentioned that we use rewrite rules and postulates
  to implement the Munchausen method.

* We adjusted the conclusions eliminating the feeling that
  the paper is Agda-specific.

