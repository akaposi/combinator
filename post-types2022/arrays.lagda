\begin{code}[hide]
{-# OPTIONS --rewriting --confluence #-} -- -v tc:50 #-}
open import Relation.Binary.PropositionalEquality
open import Function
{-# BUILTIN REWRITE _≡_ #-}
module _ where
\end{code}

\section{\label{sec:arrays}Multi-dimensional Arrays}

The next example we consider is a type for multi-dimensional
arrays that are commonly found in array languages such as
APL~\cite{APL}.  Arrays can be thought of as $n$-dimensional
rectangles, where the size of the rectangle is given
by the \emph{shape}, which is a vector of natural numbers
describing extents along each dimension.
Array languages follow the slogan ``everything is
an array'', treating natural numbers and
shape vectors as arrays.  Natural numbers
are 0-dimensional arrays, \eg{} their shape is the empty vector.
Shape vectors are 1-dimensional arrays, \eg{} their shapes are
1-element vectors.

The problem with capturing this construction with inductive
types is the following circularity.  Array
types depend on shapes, but the shapes are arrays.
That is, the index of the type is the very type that we are
defining.

\subsection{Unshaped arrays} 
One way to define the array type inductively is to avoid
the shape argument entirely.  This construction is proposed
by Jenkins~\cite{Jenkins88}:
\begin{AgdaSuppressSpace}
\begin{code}
module Unshaped where
  data Ar : Set where
    z        : Ar             -- Natural numbers with zero (z)
    s        : Ar → Ar        --   and successor (s)
    []       : Ar             -- Cons lists with empty list ([])
    _∷_      : Ar → Ar → Ar   --   and cons operation (_∷_).
    reshape  : Ar → Ar → Ar   -- Multi-dimensional array constructor.
\end{code}
\begin{code}[hide]
  infixr 5 _∷_
\end{code}
\end{AgdaSuppressSpace}


With these definitions we get a closed universe of arrays of
natural numbers.  On the positive side, we obtained the uniformity
of arrays as in APL --- if a function expects an array, it is
possible to pass a number or a vector without any casting.
\begin{code}
  0ₐ 1ₐ 2ₐ 3ₐ : Ar        -- Natural numbres
  0ₐ = z; 1ₐ = s 0ₐ; 2ₐ = s 1ₐ; 3ₐ = s 2ₐ   
 
  v₂ v₄ mat₂₂ : Ar
  v₂ = 2ₐ ∷ 2ₐ ∷ []       -- Vector [2,2]
  v₄ = 1ₐ ∷ 0ₐ            -- Flattened identity matrix [[1,0],[0,1]]
     ∷ 0ₐ ∷ 1ₐ ∷ []
  mat₂₂ = reshape v₂ v₄   -- Identity matrix of shape [2,2]
\end{code}
On the negative side, our array type does not enforce
any shape invariants.  That is, we can produce non-rectangular
arrays such as:
\begin{code}
  weird₁ = reshape (2ₐ ∷ 2ₐ ∷ []) (1ₐ ∷ 2ₐ ∷ 3ₐ ∷ [])
  weird₂ = (3ₐ ∷ []) ∷ weird₁
\end{code}
While it might be possible to define the meaning for such
cases, normally they are considered type errors.  We could
also try restricting these constructions with refinement types,
but we are interested in intrinsically-typed solution instead.


% Secondly, in the
% encoding we can distinguish a natural number constructed with
% \AC{s} and \AC{z} and the one that is constructed with \AC{reshape}:
% \begin{code}
%   _ : 1ₐ ≢ reshape [] (1ₐ ∷ [])
%   _ = λ ()
% \end{code}

\subsection{Inductive-inductive} Intrinsically-typed
array universe can be defined using inductive-inductive types,
following the ideas from~\cite{2020-msfp}.
We define arrays and shapes mutually.
\begin{AgdaAlign}
\begin{code}
module Univ where
  data Sh : Set
  data Ar : Sh → Set
  data Sh where
    scal  : Sh
    vec   : Ar scal → Sh
    mda   : ∀ {s} → Ar (vec s) → Sh
\end{code}
The shapes form the following hierarchy: scalars (\eg{}
natural numbers) have a unit shape; vector shapes are parametrised
by scalars; multi-dimensional shapes are parametrised by vectors.

\begin{code}
  Nat : Set
  Nat = Ar scal

  Vec : Nat → Set
  Vec n = Ar (vec n)

  prod : ∀ {n} → Vec n → Nat
\end{code}
We define names \AD{Nat} and \AD{Vec} which are synonyms for
arrays of the corresponding shape.  We also make a forward declaration
of the \AB{prod} function that computes the product of the given
vector.  Now we are ready to define the array universe as follows:
\begin{AgdaSuppressSpace}
\begin{code}[hide]
  infixr 10 _∷_
\end{code}
\begin{code}
  data Ar where
    z        : Nat
    s        : Nat → Nat
    []       : Vec z
    _∷_      : ∀ {n} → Nat → Vec n → Vec (s n)
    reshape  : ∀ {n} → (s : Vec n) → Vec (prod s) → Ar (mda s)
\end{code}
\end{AgdaSuppressSpace}
We use exactly the same constructors as before, except
vectors are indexed by their length, and multi-dimensional arrays
are indexed by shape vectors.  Also, the reshape constructor has
a coherence condition saying that the number of elements 
(\AB{prod} \AB{s}) in the vector we are reshaping matches
the new shape \AB{s}.

\begin{code}[hide]
  _+ₙ_ : Nat → Nat → Nat
  z   +ₙ b = b
  s a +ₙ b = s (a +ₙ b)

  _*ₙ_ : Nat → Nat → Nat
  z   *ₙ b = z
  s a *ₙ b = b +ₙ (a *ₙ b)

  open import Agda.Builtin.FromNat
  open import Agda.Builtin.Nat renaming (Nat to ℕ)
  open import Data.Unit
  instance
    number : Number Nat
    number = record { Constraint = λ _ → ⊤ 
                    ; fromNat = fromNat′ }
     where
       fromNat′ : _
       fromNat′ zero = z
       fromNat′ (suc n) = s (fromNat′ n)
\end{code}

We complete the definition of \AB{prod},
expressing it as a fold with multiplication \AF{\_*ₙ\_} (defined as
usual, not shown here).
\begin{code}
  0ₐ 1ₐ 2ₐ 3ₐ : Nat
  0ₐ = z; 1ₐ = s z; 2ₐ = s 1ₐ; 3ₐ = s 2ₐ; 4ₐ = s 3ₐ

  prod [] = 1ₐ
  prod (x ∷ xs) = x *ₙ prod xs
\end{code}

Vector and matrix examples can be expressed as follows.
\begin{code}
  v₂ : Vec 2ₐ
  v₂ = 2ₐ ∷ 2ₐ ∷ []

  v₄ : Vec 4ₐ
  v₄ = 1ₐ ∷ 0ₐ
     ∷ 0ₐ ∷ 1ₐ ∷ []

  mat₂₂ : Ar (mda v₂)
  mat₂₂ = reshape v₂ v₄
\end{code}
While the numbers, vectors and arrays are elements
of the same universe, we did not achieve the desired
array uniformity.  The problem is that we maintain the
distinction between arrays and shapes, even though
morally they are the same thing.  For example, the
type of \AB{mat₂₂} is \AD{Ar} (\AC{mda} \AB{v₂}),
not \AD{Ar} \AB{v₂}.  Also, the expression \AC{reshape}
\AC{[]} (\AB{1ₐ} \AC{∷} \AC{[]})) cannot be typed as
\AC{Nat}, even though it is an array of the empty shape.
\end{AgdaAlign}

\subsection{M\"unchhausen universe} In order to resolve the lack
of uniformity, we use
the M\"unchhausen method (the variant with forward declarations).
Our goal is to equate \AD{Ar} and \AD{Sh}.
Therefore, we forward-declare \AD{Sh} as a placeholder to bootstrap
the array type. After that we close the cycle by defining
\AD{Sh} to be \AD{Ar} with a certain index.

We start with forward-declaring types \AD{N} (natural numbers)
and shapes \AD{Sh} that are indexed by natural numbers.
Both of these types are placeholders that we will eliminate
later.
\begin{AgdaAlign}
\begin{code}
module UniformUniv where
  N : Set
  Sh : N → Set
\end{code}
The array type is a concrete definition, whereas its parameter
is a placeholder type.
\begin{code}
  data Ar : ∀ {n} → Sh n → Set
\end{code}
We make forward declarations of \AD{N} and \AD{Sh} constructors
that are needed to fill-in the indices of the array type.
Note that \AD{N} constructors are used to fill-in the indices
of \AD{Sh} constructors.
\begin{code}
  z′ : N
  s′ : N → N
  []′ : Sh z′
  _∷′_ : ∀ {n} → N → Sh n → Sh (s′ n)
\end{code}
We define names \AD{Nat} and \AD{Vec} for 0-dimensional and
1-dimensional arrays correspondingly.  We also make a forward
declaration of \AB{prod} as before, except we use placeholder
types.
\begin{code}
  Nat : Set
  Nat = Ar []′

  Vec : N → Set
  Vec n = Ar (n ∷′ []′)

  prod : ∀ {n} → Sh n → N
\end{code}
Now we can define an array universe, exactly as before.
\begin{AgdaSuppressSpace}
\begin{code}[hide]
  infixr 10 _∷_
\end{code}
\begin{code}
  data Ar where
    z        : Nat
    s        : Nat → Nat
    []       : Vec z′
    _∷_      : ∀ {n} → Nat → Vec n → Vec (s′ n)
    reshape  : ∀ {n} → (s : Sh n) → Vec (prod s) → Ar s
\end{code}
\end{AgdaSuppressSpace}
Finally, we eliminate the placeholder types by equating
\AD{N} and \AD{Sh} with 0-dimensional and 1-dimensional
arrays correspondingly.  After we do this, we define
the placeholder constructors to be those defined in \AD{Ar}.
\begin{code}
  N     = Ar []′
  Sh n  = Ar (n ∷′ []′)
  z′ = z; s′ = s; []′ = []; _∷′_ = _∷_
\end{code}
This closes the cycle and turns \AD{Ar} into a very dependent
type that is witnessed by the following Agda expression:
\begin{code}
  _ : ∀ {n : Ar []} → (s : Ar (n ∷ [])) → Set
  _ = Ar
\end{code}
\begin{code}[hide]
  postulate
    nat-elim : (P : Nat → Set) → P z → (∀ n → P n → P (s n)) → ∀ n → P n
    nat-elim-z : ∀ {P pz ps} → nat-elim P pz ps z ≡ pz
    nat-elim-s : ∀ {P pz ps n} 
               → nat-elim P pz ps (s n) ≡ ps n (nat-elim P pz ps n)
    {-# REWRITE nat-elim-z nat-elim-s #-}

    sh-elim : (P : (n : Nat) → Sh n → Set) 
             → P _ []
             → (∀ n x xs → P n xs → P (s n) (x ∷ xs))
             → ∀ n (v : Sh n) → P n v
    sh-elim-[] : ∀ {P pz ps} → sh-elim P pz ps _ [] ≡ pz
    sh-elim-∷ : ∀ {n}{P pz ps x}{xs : Sh n} 
               → sh-elim P pz ps _ (x ∷ xs) 
                 ≡ ps _ x xs (sh-elim P pz ps _ xs)
    {-# REWRITE sh-elim-[] sh-elim-∷ #-}

  plus : Nat → Nat → Nat
  plus a = nat-elim _ (λ b → b) (λ _ f b → s (f b)) a

  _*ₙ_ : Nat → Nat → Nat
  _*ₙ_ = nat-elim _ (λ b → z) (λ _ f b → plus b (f b))

\end{code}
As expected, our examples are definable, and 1-dimensional
arrays can be immediately used as array shapes.
\begin{code}
  0ₐ 1ₐ 2ₐ 3ₐ 4ₐ : Nat
  0ₐ = z; 1ₐ = s 0ₐ; 2ₐ = s 1ₐ; 3ₐ = s 2ₐ; 4ₐ = s 3ₐ

  v₂ : Ar (2ₐ ∷ [])
  v₂ = 2ₐ ∷ 2ₐ ∷ []

  v₄ : Ar (4ₐ ∷ [])
  v₄ = 1ₐ ∷ 0ₐ ∷ 0ₐ ∷ 1ₐ ∷ []
\end{code}
One technical drawback that we ran into is that with such
a cyclic type, Agda loops when attempting to define pattern-matching
functions.  The loop happens when solving the unification problem ---
it has to check that two arrays type match.  As array types are indexed,
Agda has to unify the indices, which triggers unifying the type
of the indices, and so on.  As a workaround, we can define
eliminators via rewrite rules, which makes it possible to
define \AB{prod}.  The rest works as expected.
\begin{code}
  prod {n} xs = sh-elim _ 1ₐ (λ n x xs r → x *ₙ r) n xs

  mat₂₂ : Ar v₂
  mat₂₂ = reshape v₂ v₄

  scal-test : Nat
  scal-test = reshape [] (1ₐ ∷ [])
\end{code}
\begin{code}[hide]

  -- We can make this work by adding an equation
  --    prod (x ∷ []) ≡ x
  --
  -- postulate
  --   scal : ∀ x → reshape [] (x ∷ []) ≡ x
  --   vec : ∀ n xs → reshape (n ∷ []) xs ≡ ?

  -- But it kills the pattern matching :(
  -- f : Nat → Nat
  -- f z = z
  -- f (s x) = s x
\end{code}
\end{AgdaAlign}




\begin{code}[hide]
module Tpd where

  infixr 5 _∷_
  postulate
    N : Set
    z : N
    s : N → N

    Sh : N → Set
    []′  : Sh z
    _∷′_ : ∀ {n} → N → Sh n → Sh (s n)

    prod : ∀ {n} → Sh n → N

    A : ∀ {n} → Sh n → Set
    [] : A (z ∷′ []′)
    _∷_ : ∀ {n} → N → A (n ∷′ []′) → A (s n ∷′ []′)
    reshape : ∀ {n} → (s : Sh n) → A (prod s ∷′ []′) → A s

    -- Closure equations
    sh-ar : ∀ {n} → Sh n ≡ A (n ∷′ []′)
    {-# REWRITE sh-ar #-}

    eq-[] : []′ ≡ []
    eq-∷ : ∀ {n} → (_∷′_ {n = n}) ≡ _∷_
    {-# REWRITE eq-[] eq-∷ #-}

    n-ar : N ≡ A []
    {-# REWRITE n-ar #-}

    -- Evaluation
    _+_ : N → N → N
    add-z : ∀ {n} → z + n ≡ n
    add-s : ∀ {m n} → (s m) + n ≡ s (m + n)
    {-# REWRITE add-z add-s #-}

    _*_ : N → N → N
    mul-z : ∀ {n} → z * n ≡ z
    mul-s : ∀ {m n} → (s m) * n ≡ n + (m * n)
    {-# REWRITE mul-z mul-s #-}

    prod-[] : prod [] ≡ s z
    prod-sg : ∀ {x} → prod (x ∷ []) ≡ x
    prod-∷ : ∀ {n}{s : Sh n}{x y} → prod (x ∷ y ∷ s) ≡ x * (prod (y ∷ s))
    {-# REWRITE prod-[] prod-sg prod-∷ #-}

  postulate
    scal : ∀ x → reshape [] (x ∷ []) ≡ x
    vec : ∀ n xs → reshape (n ∷ []) xs ≡ xs
    {-# REWRITE scal vec #-}

  0ₐ = z
  1ₐ = s 0ₐ
  2ₐ = s 1ₐ
  3ₐ = s 2ₐ
  4ₐ = s 3ₐ

  v₂ : A (2ₐ ∷ [])
  v₂ = 2ₐ ∷ 2ₐ ∷ []

  v₄ : A (4ₐ ∷ [])
  v₄ = 1ₐ ∷ 0ₐ ∷ 0ₐ ∷ 1ₐ ∷ []

  mat₂ₓ₂ : A v₂
  mat₂ₓ₂ = reshape v₂ v₄

  test : 1ₐ ≡ reshape [] (1ₐ ∷ [])
  test = refl
\end{code}

