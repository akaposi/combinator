\begin{code}[hide]
{-# OPTIONS --rewriting #-}
open import Data.Nat
open import Data.Product
open import Relation.Binary.PropositionalEquality
--open import Function
{-# BUILTIN REWRITE _≡_ #-}

postulate
  ⋯ : ∀ {ℓ}{A : Set ℓ} → A

module _ where
\end{code}


\section{\label{sec:combtt}Combinatory Type Theory}

In our final example, we also present a (dependnet) type theory without
contexts.  Instead of eliminating contexts with equations as we did in
the previous section, we avoid introducing them in the encoding.  This
raises the question: if there are no contexts, how do we talk about
well-scoped variables? As a matter of fact, we do not talk about
variables at all.

It is well known that for simply-typed systems, combinator
calculus~\cite{DBLP:books/cu/HindleyS86} gives a contextless
presentation of the type system.  There are no variables,
function space is built-in, and the combinators $\S$ and $\K$
are used to define functions.

Combinator calculus for dependently-typed systems is a much
more challenging~\cite{conor} task, and it was never defined.
Unsurprisingly, contextless dependently-typed theory is an
example of a very dependent type, and we use
Münchhausen method to define it.  Specifically, we
use postulates and rewrite rules to encode very dependent types.
While there might be a solution with forward declarations, we
chose rewrite rules for the sake of simplicity of the presentation.

\subsection*{First Attempt}
In our first attempt we started defining a non-dependent function
type, hoping to internalise it in the universe and define $\Pi$
types afterwards.
Concretely, we define types \AD{Ty}, terms \AD{Tm} indexed by types,
the universe \AD{U}, and eliminate the \AD{Ty} type.
After that, we define the arrow type \AD{\_⇒\_}, applications,
the arrow type within the universe \AD{|⇒|} and the equation
that turns the arrow into the internal arrow.
\begin{AgdaAlign}
\begin{AgdaSuppressSpace}
\begin{code}
module FirstAttempt where
  postulate
    Ty  : Set                        -- Types
    Tm  : Ty → Set                   -- Terms
    U   : Ty                         -- Universe
    TmU : Ty ≡ Tm U                  -- Russell-ification
    {-# REWRITE TmU #-}
\end{code}
\begin{code}[hide]
  infixr 5 _⇒_
  infixl 10 _$_
  variable  X Y : Ty
  postulate
\end{code}
\begin{code}
    _⇒_ : Ty → Ty → Ty               -- Non-dependent (external) arrow type
    _$_ : Tm (X ⇒ Y) → Tm X → Tm Y   -- Applications
    |⇒| : Tm (U ⇒ U ⇒ U)             -- Internal arrow type
    ∅⇒  : X ⇒ Y ≡ |⇒| $ X $ Y        -- Internalising arrow
\end{code}
\end{AgdaSuppressSpace}
\end{AgdaAlign}
While this looks promising, after rewriting \AD{∅⇒} we run
into the following problem.  Consider the sequence of rewrites
that is happening for the type \AD{Tm} (\AB{X} \AD{⇒} \AB{Y})
which is the type of the first argument of the application \AD{\_\$\_}.
\begin{code}
  _₀ : Tm (X ⇒ Y)                       -- Expands to
  _₁ : Tm (|⇒| $ X $ Y)                 -- Show hidden arguments
  _₂ : Tm ((_$_ {U}{U ⇒ U} |⇒| X) $ Y)  -- Arrow in (U ⇒ U) again!
\end{code}
\begin{code}[hide]
  _₀ = ⋯; _₁ = ⋯; _₂ = ⋯
\end{code}
As Agda applies all the rewrite rules before type checking,
we end-up in the infinite rewrite loop.  There does not seem to
be an easy fix.

\subsection*{Second Attempt}

Now we start with $\Pi$ types straight away and use them to
define dependent combinators.  The notion of types, terms
and the universe is the same as before.
\begin{AgdaAlign}
\begin{AgdaSuppressSpace}
\begin{code}
module SecondAttempt where
  postulate
    Ty : Set
    Tm : Ty → Set
\end{code}
\begin{code}[hide]
  infixr 5 _⇒_
  infixl 10 _$_
  infixl 10 _$f_
  variable X Y Z : Ty
  postulate
\end{code}
\begin{code}
    U : Ty
    Tm-U : Tm U ≡ Ty
    {-# REWRITE Tm-U #-}
\end{code}
\end{AgdaSuppressSpace}

We introduce the notion of a \AD{U}-valued family and
the application operation for it.  Using family we can
immediately define $\Pi$ types and applications to the
terms of $\Pi$ types.
\begin{code}
    Fam   : Ty → Ty                      -- Fam X ≈ (X ⇒ U)
    _$f_  : Tm (Fam X) → Tm X → Ty       -- Apply (x : X) to (t : Fam X)

    Pi    : (X : Ty) → Tm (Fam (Fam X))  -- X → ((X ⇒ U) ⇒ U)
    _$_   : {X : Ty}{Y : Tm (Fam X)}
          → Tm (Pi X $f Y) → (a : Tm X) → Tm (Y $f a)
\end{code}
Consider defining a non-dependent function type for
(\AB{X} \AB{Y} : \AD{Ty}) using the \AD{Pi} type.
We can immediately apply \AB{X} to \AD{Pi}, but
then we need to turn \AB{Y} into a constant \AB{X}-family
in order to complete the definition
(\AD{Pi} \AB{X} \AD{\$f} $\square$).
To achieve this we introduce the \AD{Kf} combinator that
turns a type into a constant family.  Its beta rule is
the same as of the standard K combinator.  Using \AD{Kf}
we can complete the definition of non-dependent arrow.
\begin{code}
    Kf    : (Y : Ty) → Tm (Fam X)          -- Kf Y ≈ λ a → Y
    Kf$   : ∀ {a : Tm X} → _$f_ {X} (Kf Y) a ≡ Y
    {-# REWRITE Kf$ #-}

  _⇒_ : (X Y : Ty) → Ty
  X ⇒ Y = Pi X $f (Kf Y)
\end{code}

Let us remind ourselves, the type of dependent K combinator:
\begin{code}
  [K] : (X : Set)(Y : X → Set) → (x : X) (y : Y x) → X
  [K] X Y x y = x
\end{code}
Translation into our formalism requires expressing
(\AB{x} : \AB{X})(\AB{y} : \AB{Y} \AB{x}) $\to$ \AB{X}
as a \AD{Pi} type.  More precisely, how do we express
(\AB{Y} \AB{x} $\to$ \AB{X}) as an \AB{X}-family?
We do this by introducing a helper combinator with
the corresponding beta rule.  After that, defining
dependent K and its beta rule becomes straight-forward.
\begin{code}
  postulate -- Dependent K
    Yx⇒Z : ∀ X (Y : Tm (Fam X)) → (Z : Ty) → Tm (Fam X)
    Yx⇒Z$ : ∀ X Y Z {x : Tm X} → Yx⇒Z X Y Z $f x ≡ Y $f x ⇒ Z
    {-# REWRITE Yx⇒Z$ #-}

    Kd : {Y : Tm (Fam X)} → Tm (Pi X $f Yx⇒Z X Y X)
    Kd$ : ∀ {Y : Tm (Fam X)}{x : Tm X}{y : Tm (Y $f x)}
        → Kd {X = X}{Y = Y} $ x $ y ≡ x
    {-# REWRITE Kd$ #-}
\end{code}

Similarly to dependent K, we start with reminding ourselves the
type of the dependent S combinator.  We will use the same strategy
of defining extra combinators to construct parts of the type
signature.
\begin{code}
  [S] : (X : Set)(Y : X → Set)
        (Z : (x : X) → Y x → Set)            -- λ (x : X) → Yx⇒U x
      → (f : (x : X) → (y : Y x) → Z x y)    -- λ (x : X) → Π[Yx][Zx] x
      → (g : (x : X) → Y x)
      → ((x : X) → Z x (g x))                -- λ (g : ΠXY) → ΠX[Zx[gx]] g
  [S] X Y Z f g x = f x (g x)
\end{code}
We annotate the combinators we introduced at the corresponding
positions of the [S] type.  With these definitions, we can define dependent
S and its beta rule as follows.

\begin{code}
  postulate -- Dependent S
    Yx⇒U : ∀ X (Y : Tm (Fam X)) → Tm (Fam X)
    Yx⇒U$ : ∀ X Y {x : Tm X} → Yx⇒U X Y $f x ≡ Fam (Y $f x)
    {-# REWRITE Yx⇒U$ #-}

    Π[Yx][Zx]  : ∀ X (Y : Tm (Fam X)) → (Z : Tm (Pi X $f Yx⇒U X Y)) → Tm (Fam X)
    Π[Yx][Zx]$ : ∀ X Y Z {x : Tm X} → Π[Yx][Zx] X Y Z $f x ≡ Pi (Y $f x) $f (Z $ x)
    {-# REWRITE Π[Yx][Zx]$ #-}

    Zx[gx] : ∀ X (Y : Tm (Fam X)) (Z : Tm (Pi X $f Yx⇒U X Y))
           → Tm (Pi X $f Y) → Tm (Fam X)
    Zx[gx]$ : ∀ X Y Z g {x} → Zx[gx] X Y Z g $f x ≡ Z $ x $f (g $ x)
    {-# REWRITE Zx[gx]$ #-}

    ΠX[Zx[gx]] : ∀ X (Y : Tm (Fam X)) (Z : Tm (Pi X $f Yx⇒U X Y)) 
               → Tm (Fam (Pi X $f Y))
    ΠX[Zx[gx]]$ : ∀ X Y Z g → ΠX[Zx[gx]] X Y Z $f g ≡ Pi X $f (Zx[gx] X Y Z g)
    {-# REWRITE ΠX[Zx[gx]]$ #-}

    Sd : {Y : Tm (Fam X)}{Z : Tm (Pi X $f Yx⇒U X Y)} 
       → Tm (Pi X $f Π[Yx][Zx] X Y Z 
             ⇒ (Pi (Pi X $f Y) $f (ΠX[Zx[gx]] X Y Z)))
    Sd$ : {Y : Tm (Fam X)}{Z : Tm (Pi X $f Yx⇒U X Y)}
        → {f : Tm (Pi X $f Π[Yx][Zx] X Y Z) }
        → {g : Tm (Pi X $f Y)}
        → {x : Tm X}
        → Sd $ f $ g $ x ≡ f $ x $ (g $ x)
    {-# REWRITE Sd$ #-}
\end{code}

Finally, with a few more rewrite rules, we can define non-dependent
S and K combinators as special cases of their dependent versions.
\begin{AgdaSuppressSpace}
\begin{code}[hide]
  postulate -- Simply-typed K defined via Kd
    KYx⇒Z$ : ∀ X Y Z → Yx⇒Z X (Kf Y) Z ≡ Kf (Y ⇒ Z)
    {-# REWRITE KYx⇒Z$ #-}
\end{code}
\begin{code}
  K : Tm (X ⇒ Y ⇒ X)
  K {X}{Y} = Kd {X}{Kf Y}
\end{code}
\begin{code}[hide]
  postulate -- Simply-typed S defined via Sd
    -- Eta rules for constant families
    KYx⇒U$ : ∀ X Y → Yx⇒U X (Kf Y) ≡ Kf (Fam Y)
    {-# REWRITE KYx⇒U$ #-}

    KYx⇒Zx$ : ∀ X (Y : Ty) (Z : Tm (Fam Y)) 
             → Π[Yx][Zx] X (Kf Y) (K $ Z) ≡ Kf (Pi Y $f Z)
    {-# REWRITE KYx⇒Zx$ #-}

    KXSisch$ : ∀ X Y Z → ΠX[Zx[gx]] X (Kf Y) (K $ (Kf Z)) ≡ Kf (Pi X $f (Kf Z))
    {-# REWRITE KXSisch$ #-}
\end{code}
\begin{code}

  S : Tm ((X ⇒ Y ⇒ Z) ⇒ (X ⇒ Y) ⇒ X ⇒ Z)
  S {X}{Y}{Z} = Sd {X}{Kf Y}{K $ (Kf Z)}

\end{code}
\end{AgdaSuppressSpace}
\end{AgdaAlign}

We made a good progress with defining combinatory type theory.
However, current combinators are not yet powerful enough to
internalise \AD{Pi} and \AD{Fam}.  The problem is that in \AD{Pi},
\AD{Kd} and \AD{Sd} type parameters \AB{X}, \AB{Y} and \AB{Z} are
quantified externally.  We need to define the version of these
combinators that internalises this quantification within \AD{U}.
There is no conceptual problem in doing so, but the resulting
terms become incredibly large and inconvenient to work with.
Specifically, the one for the dependent S combinator.  It is
not clear whether there is a more elegant way of doing this.


