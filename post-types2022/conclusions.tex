\section{Conclusions}

This paper demonstrates a technique to justify and make
practical use of very dependent types.  Our method is
based on the observation that the ``cycle'' of a very
dependent type can be ``cut'' by introducing placeholder
types, defining the data and then eliminating placeholders
by means of equations.

When we try to apply the proposed technique within the
actual theorem provers such as Agda, we have a few choices
on how to implement this.  First, we can pack together
placeholders, data and explicit equalities, \eg{} as we
do in \AD{DPair} type in Section~\ref{sec:sequences}.  This
is a straight-forward implementation of the M\"unchhausen
technique.  However, dealing with explicit propositional
equalities as parts of data often brings us to the
situation called ``transport hell''.  For example, the
isomorphism proof about \AD{DPair}s is an instance of that.
Alternatively, for the objects of very dependent types,
we can turn propositional equalities into definitional
ones. On paper, extensional type theory achieves this, and
in special cases we can use shallow embedding (as in the formalisation of Sections \ref{sec:russell} and \ref{sec:uncat}). In Agda, there are two ways to do this: forward
declarations and rewrite rules.  Forward declarations
are demonstrated when declaring \AD{pair} in 
Section~\ref{sec:sequences}, \AD{Ar} universe in
Section~\ref{sec:arrays} and \AD{Tm} in
Section~\ref{sec:russell}.  While this is a very
convenient feature of Agda, it is considered\footnote{
See the following Agda issue
\url{https://github.com/agda/agda/issues/1556}
that discusses forward declarations and very dependent
types.}
not very well understood by many Agda developers.
Also, as we have seen with \AD{Ar} example, currently
it leads to loops in the typechecker, which is clearly
a bug.

Rewrite rules~\cite{cockx:LIPIcs:2020:13066} make it possible to turn arbitrary
propositional equalities into definitional ones, but this
feature of Agda is considered unsafe. However, it is clearly a
localized implementation of extensional type theory which is
conservative over intensional type theory with extensionality
principles (as available in Cubical Agda). We expect that the
conservativity result~\cite{hofmann} extends to our setting and hence
the use of rewriting rules is only a cosmetic and labour saving tool
to avoid \emph{transport hell}.
% At the same time,
% very often it is the only way to avoid transport hell.
% For example, if we have \AB{n} \AF{+} \AC{0} as an index
% of some type, it is not definitionally equal to \AB{n}.
We use rewrite rules in Section~\ref{sec:combtt}.
Currently, the interplay between the
rewrite rules and the typechecker is not always satisfying.
For example, our first attempt in Section~\ref{sec:combtt}
ends up in an infinite rewrite, as all the rules have to
fire before the typechecker.  We believe that more
interleaved approach to rewriting could make our example
to typecheck.

The examples show that very dependent types can be used in a fully
algebraic setting, \ie{} without referring to untyped preterms as
in~\cite{Hickey96formalobjects}. The essential ingredient are forward
declarations, \ie{} we introduce the type of an object but only define
it later while already using it in the types of other objects ---
see~\cite{alti:pisigma-new} for a formal definition of this concept.
This is also the idea in inductive-inductive definitions, where constructors
may depend on previous constructors~\cite{alti:catind2,forsberg2013inductive}.

Clearly, Agda provides us with a mechanism to play around with these
concepts but it is not yet clear what exactly the theory behind these
constructions is. In this sense, our paper raises questions instead of
answering them. We believe that this is a valuable contribution to the
subject. 


% The proposed M\"unchhausen technique is a very useful
% discipline to work with very dependent types in a fully
% algebraic setting.  While it is unclear whether all
% very dependent functions from~\cite{Hickey96formalobjects}
% can be handled by this technique, we conjecture that
% very dependent functions with inductive domains (those
% that can be defined using \AK{data} in non-cubical Agda)
% can be represented in a dependent type theory with
% induction-recursion.
% The reason for this is that all the inductive types
% are well-founded by definition, and induction-recursion
% makes it possible to build a type that ``wraps'' all constructors
% and accumulates dependencies on ``smaller'' types.

% This approach does not seem  work in general in the absence of Uniqueness of
% Identity Proofs (UIP). A potential counterexample are semi-simplicial
% types (\eg{} see~\cite{nicolai:thesis}) which can be viewed as a
% very dependent type but does not seem to have a representation in
% type theory without UIP. We can represent semi-simplicial type in a
% 2-level type theory~\cite{DBLP:journals/corr/AltenkirchCK16} hence it would be interesting to
% investigate very dependent types in this setting.

% % However, it is known~\cite{nicolai:thesis} that this does
% % not hold for higher-inductive types.  For example, there seem
% % to be no way to define semi-simplicial types in a standard
% % cubical Agda.

% A number of questions are still to be answered before
% we can claim full support of very dependent types in
% theorem provers.  For example,
% how to resolve the unification problems in the presence of
% very dependent types?  What is the best strategy to deal
% with equalities and placeholders?  What would it take
% to interleave rewrite rules and typechecking?
% Despite these questions, we believe that M\"unchausen technique is a solid
% step forward towards understanding what do very dependent
% types actually mean.


