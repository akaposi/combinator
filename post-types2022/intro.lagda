\begin{code}[hide]
open import Data.Bool
open import Data.Product
open import Relation.Binary.PropositionalEquality
open import Function renaming (_↔_ to _≊_)

postulate
  ⋯ : ∀ {ℓ}{A : Set ℓ} → A
\end{code}

\section{\label{sec:intro}Introduction}

When we want to understand how powerful the given type system is,
we identify objects that the given type is allowed to depend
on.  For instance,
in simply-typed systems types are built from a fixed set
of ground types and operations.  In System F we introduce type
variables and binders making it possible to define new operations that
compute types.  In dependently-typed systems we are allowed
to compute types from terms.

At the same time, we rarely explore dependencies within
a typing relation.  For example, consider the case when
the type is allowed to depend on the term that it is typing:
\begin{equation*}
   x : F x
\end{equation*}
Such a situation is often referred to as \emph{very dependent
type}~\cite{Hickey96formalobjects}.  The immediate two questions
arise: (i) does this ever occur in practice? (ii) how do you support
this within a type system?

Let us consider an example where such a type occurs very
naturally.  There is a well-known type isomorphism, saying
that pairs can be represented as functions from boolean:
\[
A \times B \,\,\cong\,\,
(b:\Bool) \ra \mathsf{if}\, b \,\mathsf{then}\, A \,
                                \mathsf{else}\, B.
\]
Consider now upgrading the isomorphism to dependent product
on the left hand side. Given
$A:\Type$, $B:A\ra\Type$, we want something like
\[
\Sigma\,A\,B \,\,\cong\,\,
(b:\Bool)\ra \mathsf{if}\, b \,\mathsf{then}\, A \,
                               \mathsf{else}\, (B\,\square),
\]
but what do you put in the placeholder $\square$? It should
be the output of the function when the input is $b=\true$.
Once the function is given a name, we can refer to it:
\[
f : (b:\Bool)\ra \mathsf{if}\, b \,\mathsf{then}\, A \,
                                   \mathsf{else}\, (B\,(f\,\true))
\]

Supporting such definitions in a type system can be tricky.
Hickey gives~\cite{Hickey96formalobjects} a type system with very dependent functions
using pre-terms and typing
relations~\cite{DBLP:journals/jfp/Barendregt91}.
However, it turns out that many very dependent types can be
understood algebraically and even encoded in proof assistants.

Many practical examples are easier to understand
when very dependent types are present.  One familiar example
is the use of type universes in proof assistants such as Coq
or Agda.  Both systems use Russell universes, and if we ignore
the universe levels, \AD{Set} is of type \AD{Set} in Agda, and
\AD{Type} is of type \AD{Type} in Coq. This is clearly
the case of very dependent types.

Our main observation is that algebraic presentation requires
cutting the cycle of a given very dependent type.  This is
achieved by introducing a temporary placeholder type and
a number of equations that eliminate the placeholders.
The proposed scheme can be summarised as follows.  For
a very dependent type (\AB{x} : \AD{F} \AB{x}) find:
\begin{AgdaSuppressSpace}
\begin{code}[hide]
postulate
\end{code}
\begin{code}
  A : Set; G : A → Set; α : {a : A} → G a → A
\end{code}
\end{AgdaSuppressSpace}
Such that \AD{F} can be decomposed in \AD{G} \AF{∘} \AF{α}.
In this case, a very dependent type can be expressed as
the following triplet:
\begin{code}
  a   : A        -- Placeholder
  x   : G a      -- The data
  eq  : a ≡ α x  -- Closing the cycle
\end{code}


This approach works if these equations are propositional,
but it forces a lot of transport along the newly
introduced equations (this situation is commonly referred  to
as transport hell).  In Agda we can turn these propositional
equations into definitional ones by means of rewrite rules
or forward declarations.

%\paragraph{Contribution}

The main contribution of this paper lies in applying the M\"{u}nchhausen
method to five practical examples. Our setting is Martin-L\"{o}f type
theory extended with function extensionality, UIP (uniqueness of
identity proofs) and forward declarations. From~\cite{hofmann} we
know that such a formulation without forward declarations is
conservative with respect to its intensional version.  This means that,
in principle, all the presented types that do not use forward
declarations can be given in intensional type theory, but in a
much more verbose way.  We conjecture that the same holds for the
type theory with forward declarations as well, but as these are not
very well understood, we would not claim this.

We use Agda to demonstrate concrete implementation of our examples, but
there is nothing Agda-specific in the method itself. In Agda,
the M\"{u}nchausen method can be realised in four different ways:
\begin{enumerate}
\item Identity types and explicit equations (Section~\ref{sec:sequences});
\item Forward declarations (Sections~\ref{sec:sequences},
\ref{sec:arrays} and~\ref{sec:russell});
\item Shallow embedding as described in~\cite{shallow} (Sections~\ref{sec:uncat} and~\ref{sec:uncat});
\item Postulates and rewrite rules (Section~\ref{sec:combtt})
\end{enumerate}


While it is not yet clear whether all very dependent types as
defined in~\cite{Hickey96formalobjects} can be
handled by the proposed method, we believe that the examples that
we provide give a first step towards answering this question.

This paper is an Agda script, therefore all the examples in the paper
have been typechecked.

The content of this paper was presented at the TYPES'22 conference in
Nantes~\cite{munchhausen-talk}.

% \todo[inline]{Should we make a conjecture of sorts?  For example,
% induction-recursion is sufficient to define all Hickey's
% very dependent functions?}


