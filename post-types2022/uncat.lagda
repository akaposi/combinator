\begin{code}[hide]
{-# OPTIONS --rewriting #-}
open import Data.Nat
--open import Data.Product
open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}
\end{code}


\section{Type Theory without Contexts}
\label{sec:uncat}

Having Russell universes could be called ``type theory without types''
as types are just special terms. Type theory without contexts is when
contexts are just types without free variables.

When defining type theory as an algebraic theory, the final goal is to
describe the rules for types and terms. Contexts and substitutions
(the category structure) are only there as supporting
infrastructure. However, when enough structure is added to types and
terms, we don't need this supporting infrastructure anymore and we can
get rid of it using the Münchhausen technique. We will still have
explicit substitutions, but we use terms instead of context
morphisms. The resulting theory with very dependent types includes the
following sorts and operations. Note that some of these operations are not only
very dependently typed, but the typing is very mutual: for example,
the type of \AD{Ty} includes \AD{⊤} which is only listed later.
\begin{alignat*}{10}
& \AD{Ty} && {}\mathop{\AD{:}}{} && \AD{Ty ⊤ → Set} \\
& \AD{Tm} && {}\mathop{\AD{:}}{} && \AD{(Γ : Ty ⊤) → Ty Γ → Set} \\
& \AD{\_[\_]T} && {}\mathop{\AD{:}}{} && \AD{Ty Γ → Tm Δ (Γ [ tt ]T) → Ty Δ} \\
& \AD{\_[\_]t} && {}\mathop{\AD{:}}{} && \AD{Tm Γ A → (σ : Tm Δ (Γ [ tt ]T)) → Tm Δ (A [ σ ]T)} \\
& \AD{id} && {}\mathop{\AD{:}}{} && \AD{Tm Γ (Γ [ tt ]T)} \\
& \AD{⊤} && {}\mathop{\AD{:}}{} && \AD{Ty Γ} \\
& \AD{tt} && {}\mathop{\AD{:}}{} && \AD{Tm Γ ⊤} \\
& \AD{Σ} && {}\mathop{\AD{:}}{} && \AD{(A : Ty Γ) → Ty (Σ Γ (A [ snd id ]T)) → Ty Γ} \\
& \AD{\_,\_} && {}\mathop{\AD{:}}{} && \AD{(a : Tm Γ A) → Tm Γ (B [ id , a ]T) → Tm Γ (Σ A B)} \\
& \AD{fst} && {}\mathop{\AD{:}}{} && \AD{Tm Γ (Σ A B) → Tm Γ A} \\
& \AD{snd} && {}\mathop{\AD{:}}{} && \AD{(w : Tm Γ (Σ A B)) → Tm Γ (B [ id , fst w ]T)}
\end{alignat*}
It is difficult to derive the above in Agda using forward declarations
or rewrite rules, but working on paper (in extensional type theory)
this is possible.  A \emph{model of type theory without contexts} is given by
a CwF with \AD{⊤} and \AD{Σ} types\footnote{List of notations: the
category is denoted \AD{Con}, \AD{Sub}, \AD{\_∘\_}, \AD{id}, the empty
context (terminal object) \AD{◇}, the empty substitution \AD{ε}, types
are \AD{Ty Γ} with the substitution operation \AD{\_[\_]T}, terms
\AD{Tm Γ A} with \AD{\_[\_]t}, context extension \AD{\_▹\_},
substitution extension \AD{\_,\_} and projections \AD{p : Sub (Γ ▹ A)
Γ}, \AD{q : Tm (Γ ▹ A) (A [ p ]T)}. The type former \AD{⊤ : Ty Γ}
comes with constructor \AD{tt} and $\eta$ law. \AD{Σ}'s
constructor is denoted \AD{\_,\_}, the destructors are \AD{fst} and
\AD{snd} and we have both $\beta$ laws and an $\eta$ law.} where the following equations
hold.
\begin{alignat*}{10}
& \AD{Con} & {}\mathop{\AD{=}}{} & \AD{Ty ◇} \\
& \AD{Sub Δ Γ} & {}\mathop{\AD{=}}{} & \AD{Tm Δ (Γ [ ε ]T)} \\
& \AD{σ ∘ ν} & {}\mathop{\AD{=}}{} & \AD{σ [ ν ]t} \\
& \AD{◇} & {}\mathop{\AD{=}}{} & \AD{⊤} \\
& \AD{ε} & {}\mathop{\AD{=}}{} & \AD{tt} \\
& \AD{Γ ▹ A} & {}\mathop{\AD{=}}{} & \AD{Σ Γ (A [ q ]T)} \\
& \AD{σ , t} & {}\mathop{\AD{=}}{} & \AD{σ , t} \\
& \AD{p} & {}\mathop{\AD{=}}{} & \AD{fst id} \\
& \AD{q} & {}\mathop{\AD{=}}{} & \AD{snd id}
\end{alignat*}
Note that the well-typedness of the second equation depends on the first equation, as
\AD{Γ : Con} has to be viewed as \AD{Γ : Ty ◇} and then we can substitute it with the empty
substitution to obtain \AD{Γ [ ε ]T : Ty Δ}. Just as substitutions are special terms, composition of
substitutions is a special case of substitution of terms, the empty context is \AD{⊤ : Ty ◇},
context extension is a \AD{Σ} type where \AD{A : Ty Γ}, but \AD{Σ} requires a \AD{Ty (◇ ▹ Γ)}, so
we need a \AD{Sub (◇ ▹ Γ) Γ = Tm (◇ ▹ Γ) (Γ [ ε ]T) = Tm (◇ ▹ Γ) (Γ [ p ]T)}, which is given by
\AD{q \{◇\}\{Γ\}}.

We can check that in a model of type theory without contexts, the very
dependent types listed above are all valid.

As for Russell models, we have a model construction which replaces any
CwF with \AD{⊤} and \AD{Σ} with a model without contexts. We cannot
directly use the equations of model without contexts above for the
model construction. \Eg{} if we said that \AD{Con' := Ty ◇} and \AD{◇'
:= ⊤} and \AD{Ty' Γ := Ty (◇ ▹ Γ)} then we would have \AD{Con' = Ty ◇
≠ Ty (◇ ▹ ⊤) = Ty' ◇'}. Instead we define \AD{Con' := Ty (◇ ▹ ⊤)},
\AD{◇' := ⊤} and \AD{Ty' Γ := Ty (◇ ▹ Γ [ ε , tt ]T)}.  Now we have
\AD{Con' = Ty (◇ ▹ ⊤) = Ty (◇ ▹ ⊤ [ ε , tt ]T) = Ty (◇ ▹ ◇' [ ε , tt
]T) = Ty' ◇'}. We refer to Appendix \ref{sec:uncatmodel} for the
definition of the rest of the components of the output model and also
for a proof that if the input model has $\Pi$ types then so does the
output model. We formalised this model construction using shallow
embedding \cite{shallow}, the formalisation is part of the
source code of the current paper\footnote{See \url{https://bitbucket.org/akaposi/combinator/src/master/post-types2022/uncat.lagda}}.

\begin{code}[hide]
module _ where

module S where
  infixl 5 _▹_
  infixl 7 _[_]T
  infixl 5 _,_ _,Σ_
  infixr 6 _∘_
  infixl 8 _[_]t

  private
    record 𝟙 : Set where
    record Sigma (A : Set)(B : A → Set) : Set where
      constructor pair
      field proj₁ : A
      field proj₂ : B proj₁
    open Sigma

  -- the shallow embedding of the syntax
  {-# NO_UNIVERSE_CHECK #-}
  record Con : Set where
    constructor mk
    field un : Set
  open Con
  variable Γ Δ Θ Ξ : Con
  record Sub (Δ Γ : Con) : Set where
    constructor mk
    field un : un Δ → un Γ
  open Sub
  variable σ ν ω : Sub Δ Γ
  _∘_    : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  σ ∘ ν  = mk λ θ → un σ (un ν θ)
  ass    : (σ ∘ ν) ∘ ω ≡ σ ∘ (ν ∘ ω)
  ass    = refl
  id     : Sub Γ Γ
  id     = mk λ γ → γ
  idl    : id ∘ σ ≡ σ
  idl    = refl
  idr    : σ ∘ id ≡ σ
  idr    = refl
  ◇      : Con
  ◇      = mk 𝟙
  ε      : Sub Γ ◇
  ε      = _
  ◇η     : σ ≡ ε
  ◇η     = refl
  {-# NO_UNIVERSE_CHECK #-}
  record Ty (Γ : Con) : Set where
    constructor mk
    field un : un Γ → Set
  open Ty
  variable A B C : Ty Γ
  _[_]T  : Ty Γ → Sub Δ Γ → Ty Δ
  A [ σ ]T = mk λ δ → un A (un σ δ)
  [∘]T   : A [ σ ]T [ ν ]T ≡ A [ σ ∘ ν ]T
  [∘]T   = refl
  [id]T  : A [ id ]T ≡ A
  [id]T  = refl
  record Tm (Γ : Con)(A : Ty Γ) : Set where
    constructor mk
    field un : (γ : un Γ) → un A γ
  open Tm
  variable t u v : Tm Γ A
  _[_]t  : Tm Γ A → (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T)
  t [ σ ]t = mk λ δ → un t (un σ δ)
  [∘]t   : t [ σ ∘ ν ]t ≡ t [ σ ]t [ ν ]t
  [∘]t   = refl
  [id]t  : {t : Tm Γ A} → t [ id ]t ≡ t
  [id]t  = refl
  _▹_    : (Γ : Con) → Ty Γ → Con
  Γ ▹ A  = mk (Sigma (un Γ) (un A))
  _,_    : (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T) → Sub Δ (Γ ▹ A)
  σ , t  = mk λ δ → pair (un σ δ) (un t δ)
  p      : Sub (Γ ▹ A) Γ
  p      = mk proj₁
  q      : Tm (Γ ▹ A) (A [ p ]T)
  q      = mk proj₂
  ▹β₁    : p {Γ}{A} ∘ (σ , t) ≡ σ
  ▹β₁    = refl
  ▹β₂    : q [ σ , t ]t ≡ t
  ▹β₂    = refl
  ▹η     : (p ∘ σ , q [ σ ]t) ≡ σ
  ▹η     = refl
  ,∘     : {σ : Sub Δ Γ}{t : Tm Δ (A [ σ ]T)}{ν : Sub Θ Δ} →  (_,_ {A = A} σ t) ∘ ν ≡ (σ ∘ ν , t [ ν ]t)
  ,∘     = refl
  ⊤      : Ty Γ
  ⊤      = mk λ _ → 𝟙
  ⊤[]    : ⊤ [ σ ]T ≡ ⊤
  ⊤[]    = refl
  tt     : Tm Γ ⊤
  tt     = _
  tt[]   : tt {Γ} [ σ ]t ≡ tt {Δ}
  tt[]   = refl
  ⊤η     : t ≡ tt
  ⊤η     = refl
  Σ      : (A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  Σ A B  = mk (λ γ → Sigma (un A γ) λ a → un B (pair γ a))
  Σ[]    : {σ : Sub Δ Γ} → Σ A B [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ∘ p , q ]T)
  Σ[]    = refl
  _,Σ_   : (t : Tm Γ A) → Tm Γ (B [ id , t ]T) → Tm Γ (Σ A B)
  t ,Σ u = mk λ γ → pair (un t γ) (un u γ)
  fst    : Tm Γ (Σ A B) → Tm Γ A
  fst t  = mk λ γ → proj₁ (un t γ)
  snd    : (t : Tm Γ (Σ A B)) → Tm Γ (B [ id , fst t ]T)
  snd t  = mk λ γ → proj₂ (un t γ)
  Σβ₁    : fst {A = A} (u ,Σ v) ≡ u
  Σβ₁    = refl
  Σβ₂    : snd (u ,Σ v) ≡ v
  Σβ₂    = refl
  Ση     : (fst t ,Σ snd t) ≡ t
  Ση     = refl
  ,[]    : _[_]t {Δ = Δ} (_,Σ_ {Γ = Γ}{A = A}{B = B} u v) σ ≡ _,Σ_ {A = A [ σ ]T}{B = B [ σ ∘ p , q ]T} (u [ σ ]t) (v [ σ ]t)
  ,[]    = refl
  fst[]  : fst t [ σ ]t ≡ fst (t [ σ ]t)
  fst[]  = refl
  snd[]  : snd t [ σ ]t ≡ snd (t [ σ ]t)
  snd[]  = refl
  Π      : (A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  Π A B  = mk (λ γ → (a : un A γ) → un B (pair γ a))
  Π[]    : {σ : Sub Δ Γ} → Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ∘ p , q ]T)
  Π[]    = refl
  lam    : Tm (Γ ▹ A) B → Tm Γ (Π A B)
  lam t  = mk λ γ a → un t (pair γ a)
  app    : Tm Γ (Π A B) → Tm (Γ ▹ A) B
  app t  = mk λ γa → un t (proj₁ γa) (proj₂ γa)
  Πβ     : app {A = A}{B = B} (lam t) ≡ t
  Πβ     = refl
  Πη     : lam {Γ = Γ}{A = A}{B = B} (app t) ≡ t
  Πη     = refl
  lam[]  : {σ : Sub Δ Γ} → lam {Γ = Γ}{A = A}{B = B} t [ σ ]t ≡ lam (t [ σ ∘ p , q ]t)
  lam[]  = refl
  U      : Ty Γ
  U      = mk λ _ → Con
  U[]    : U [ σ ]T ≡ U
  U[]    = refl
  El     : Tm Γ U → Ty Γ
  El t   = mk λ γ → un (un t γ)
  El[]   : El t [ σ ]T ≡ El (t [ σ ]t)
  El[]   = refl
  c      : Ty Γ → Tm Γ U
  c A    = mk λ γ → mk (un A γ)
  c[]    : c A [ σ ]t ≡ c (A [ σ ]T)
  c[]    = refl
  Uβ     : El (c A) ≡ A
  Uβ     = refl
  Uη     : c (El t) ≡ t
  Uη     = refl

-- the model without contexts defined using the model S

infixl 5 _▹_
infixl 7 _[_]T
infixl 5 _,_ _,Σ_
infixr 6 _∘_
infixl 8 _[_]t

Con      : Set
Con      = S.Ty (S.◇ S.▹ S.⊤)
variable Γ Δ Θ Ξ : Con
Sub      : Con → Con → Set
Sub Δ Γ  = S.Tm (S.◇ S.▹ Δ S.[ S.ε S., S.tt ]T) (Γ S.[ S.ε S., S.tt ]T)
variable σ ν ω : Sub Δ Γ
_∘_      : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
σ ∘ ν    = σ S.[ S.ε S., ν ]t
ass      : (σ ∘ ν) ∘ ω ≡ σ ∘ (ν ∘ ω)
ass      = refl
id       : Sub Γ Γ
id       = S.q
idl      : id ∘ σ ≡ σ
idl      = refl
idr      : σ ∘ id ≡ σ
idr      = refl
◇        : Con
◇        = S.⊤
ε        : Sub Γ ◇
ε        = S.tt
◇η       : σ ≡ ε
◇η       = refl
Ty       : Con → Set
Ty Γ     = S.Ty (S.◇ S.▹ Γ S.[ S.ε S., S.tt ]T)
variable A B C : Ty Γ
_[_]T    : Ty Γ → Sub Δ Γ → Ty Δ
A [ σ ]T = A S.[ S.ε S., σ ]T
[∘]T     : A [ σ ]T [ ν ]T ≡ A [ σ ∘ ν ]T
[∘]T     = refl
[id]T    : A [ id ]T ≡ A
[id]T    = refl
Tm       : (Γ : Con) → Ty Γ → Set
Tm Γ A   = S.Tm (S.◇ S.▹ Γ S.[ S.ε S., S.tt ]T) A
variable t u v : Tm Γ A
_[_]t    : Tm Γ A → (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T)
t [ σ ]t = t S.[ S.ε S., σ ]t
[∘]t     : t [ σ ∘ ν ]t ≡ t [ σ ]t [ ν ]t
[∘]t     = refl
[id]t    : {t : Tm Γ A} → t [ id ]t ≡ t
[id]t    = refl
_▹_      : (Γ : Con) → Ty Γ → Con
Γ ▹ A    = S.Σ Γ (A S.[ S.ε S., S.q ]T)
_,_      : (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T) → Sub Δ (Γ ▹ A)
σ , t    = σ S.,Σ t
p        : Sub (Γ ▹ A) Γ
p        = S.fst S.q
q        : Tm (Γ ▹ A) (A [ p ]T)
q        = S.snd S.q
▹β₁      : p {Γ}{A} ∘ (σ , t) ≡ σ
▹β₁      = refl
▹β₂      : q [ σ , t ]t ≡ t
▹β₂      = refl
▹η       : (p ∘ σ , q [ σ ]t) ≡ σ
▹η       = refl
,∘       : {σ : Sub Δ Γ}{t : Tm Δ (A [ σ ]T)}{ν : Sub Θ Δ} →  (_,_ {A = A} σ t) ∘ ν ≡ (σ ∘ ν , t [ ν ]t)
,∘       = refl
⊤        : Ty Γ
⊤        = S.⊤
⊤[]      : ⊤ [ σ ]T ≡ ⊤
⊤[]      = refl
tt       : Tm Γ ⊤
tt       = S.tt
tt[]     : tt {Γ} [ σ ]t ≡ tt {Δ}
tt[]     = refl
⊤η       : t ≡ tt
⊤η       = refl
Σ        : (A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
Σ A B    = S.Σ A (B S.[ S.ε S., (S.q S.[ S.p ]t S.,Σ S.q) ]T)
Σ[]      : {σ : Sub Δ Γ} → Σ A B [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ∘ p , q ]T)
Σ[]      = refl
_,Σ_     : (t : Tm Γ A) → Tm Γ (B [ id , t ]T) → Tm Γ (Σ A B)
u ,Σ v   = u S.,Σ v
fst      : Tm Γ (Σ A B) → Tm Γ A
fst t    = S.fst t
snd      : (t : Tm Γ (Σ A B)) → Tm Γ (B [ id , fst t ]T)
snd t    = S.snd t
Σβ₁      : fst {A = A} (u ,Σ v) ≡ u
Σβ₁      = refl
Σβ₂      : snd (u ,Σ v) ≡ v
Σβ₂      = refl
Ση       : (fst t ,Σ snd t) ≡ t
Ση       = refl
,[]      : _[_]t {Δ = Δ} (_,Σ_ {Γ = Γ}{A = A}{B = B} u v) σ ≡ _,Σ_ {A = A [ σ ]T}{B = B [ σ ∘ p , q ]T} (u [ σ ]t) (v [ σ ]t)
,[]      = refl
fst[]    : fst t [ σ ]t ≡ fst (t [ σ ]t)
fst[]    = refl
snd[]    : snd t [ σ ]t ≡ snd (t [ σ ]t)
snd[]    = refl
Π        : (A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
Π A B    = S.Π A (B S.[ S.ε S., (S.q S.[ S.p ]t S.,Σ S.q) ]T)
Π[]      : {σ : Sub Δ Γ} → Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ∘ p , q ]T)
Π[]      = refl
lam      : Tm (Γ ▹ A) B → Tm Γ (Π A B)
lam t    = S.lam (t S.[ S.ε S., (S.q S.[ S.p ]t S.,Σ S.q) ]t)
app      : Tm Γ (Π A B) → Tm (Γ ▹ A) B
app t    = S.app t S.[ S.ε S., S.fst S.q S., S.snd S.q ]t
Πβ       : app {Γ = Γ}{A = A}{B = B} (lam t) ≡ t
Πβ       = refl
Πη       : lam {Γ = Γ}{A = A}{B = B} (app t) ≡ t
Πη       = refl
lam[]    : {σ : Sub Δ Γ} → lam {Γ = Γ}{A = A}{B = B} t [ σ ]t ≡ lam (t [ σ ∘ p , q ]t)
lam[]    = refl
U        : Ty Γ
U        = S.U
U[]      : U [ σ ]T ≡ U
U[]      = refl
El       : Tm Γ U → Ty Γ
El t     = S.El t
El[]     : El t [ σ ]T ≡ El (t [ σ ]t)
El[]     = refl
c        : Ty Γ → Tm Γ U
c A      = S.c A
c[]      : c A [ σ ]t ≡ c (A [ σ ]T)
c[]      = refl
Uβ       : El (c A) ≡ A
Uβ       = refl
Uη       : c (El t) ≡ t
Uη       = refl

-- the equalities of a model without contexts

Con∅     : Con ≡ Ty ◇
Con∅     = refl
Sub∅     : Sub Δ Γ ≡ Tm Δ (Γ [ ε ]T)
Sub∅     = refl
∘∅       : σ ∘ ν ≡ σ [ ν ]t
∘∅       = refl
◇∅       : ◇ ≡ ⊤
◇∅       = refl
ε∅       : ε {Γ = Γ} ≡ tt
ε∅       = refl
▹∅       : Γ ▹ A ≡ Σ Γ (A [ q ]T)
▹∅       = refl
,∅       : σ , t ≡ σ ,Σ t
,∅       = refl
p∅       : p {A = A} ≡ fst id
p∅       = refl
q∅       : q {A = A} ≡ snd id
q∅       = refl
\end{code}
