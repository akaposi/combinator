\begin{code}[hide]
{-# OPTIONS --rewriting #-}
open import Data.Nat
open import Relation.Binary.PropositionalEquality hiding ([_])

module _ where

module RussellExample where
\end{code}


\section{\label{sec:russell}Russell Universes}

In pure type systems \cite{DBLP:journals/jfp/Barendregt91} there is no separate
sort for terms and types, there are only terms and those terms which appear on the
right hand side of the colon in the typing relation are called types. Using
well-typed terms, this would lead to the following very dependent type for the sort of terms: \AD{Tm : (Γ : Con) → Tm Γ U → Set}. That is, terms depend on a context
and a term of type \AD{U}. Using the Münchhausen method (its variant
with forward declarations), we can make sense of this. We temporarily introduce types and the type \AD{U′} for the universe, then
after declaring the sort of terms we can say that actually types are just terms of type \AD{U′}, then we can add the actual \AD{U} operator for terms and close the loop by saying that \AD{U′} is the same as \AD{U}. Using forward declarations, part of
the syntax of type theory is given as follows.
\begin{code}
  data Con : Set
  Ty : Con → Set                   -- forward declaration
  data Con where
    ∙    : Con
    _▹_  : (Γ : Con) → Ty Γ → Con
  U′ : ∀ {Γ} → Ty Γ                -- forward declaration
  data Tm : (Γ : Con) → Ty Γ → Set
  Ty Γ = Tm Γ U′
  data Tm where
    U    : ∀ {Γ} → Ty Γ
    Π    : ∀ {Γ} → (A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    lam  : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (Π A B)
  U′ = U
\end{code}
Note that such a theory is inconsistent through Russell's paradox, but it is easy to fix this by stratification (adding natural number indices to $\Ty$ and $\U$, see e.g.\ \cite{DBLP:conf/csl/Kovacs22}).
%
More precisely, we say that a stratified category with families (CwF \cite{DBLP:journals/corr/abs-1904-00827}) with a type former \AD{U : (i : ℕ) → Ty Γ (i+1)} satisfying \AD{U i [ σ ]T = U i} is Russell if the equations \AD{Ty Γ i = Tm Γ (U i)} and \AD{A [ σ ]T = A [ σ ]t} hold (where \AD{\_[\_]T} and \AD{\_[\_]t} are the substitution operations for types and terms, respectively).

Any CwF with a hierarchy of Tarski universes can be equipped with a Russell family structure supporting the same type formers as the Tarski universe. A hierarchy of Tarski universes is given by the universe types \AD{U i : Ty Γ (i+1)}, their decoding \AD{El : Tm Γ (U i) → Ty Γ i}, a code for each universe \AD{u i : Tm Γ (U (i+1))} such that their decoding is the actual universe \AD{El (u i) = U i}. We further have the evident substitution rules and additional operations expressing that \AD{U} is closed under certain type formers. The Russell family structure then is defined by \AD{Tyᴿ Γ i := Tm Γ (U i)} and \AD{Tmᴿ Γ A := Tm Γ (El A)}, both substitution operations are \AD{\_[\_]t}, context extension is \AD{Γ ▹ᴿ A := Γ ▹ El A}. The Russell universe is defined as \AD{Uᴿ i := u i}, thus we obtain the Russell sort equation by \AD{Tyᴿ Γ i = Tm Γ (U i) = Tm Γ (El (u i)) = Tmᴿ Γ (Uᴿ i)}. We formalised this model construction using the shallow embedding trick of \cite{shallow}, the formalisation is part of the source code of the current paper\footnote{See \url{https://bitbucket.org/akaposi/combinator/src/master/post-types2022/russel.lagda}}.

\begin{code}[hide]
module S where
  infixl 5 _▹_
  infixl 7 _[_]T
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]t

  private
    record 𝟙 : Set where
    record Sigma (A : Set)(B : A → Set) : Set where
      constructor pair
      field proj₁ : A
      field proj₂ : B proj₁
    open Sigma

  -- the shallow embedding of the syntax
  {-# NO_UNIVERSE_CHECK #-}
  record Con : Set where
    constructor mk
    field un : Set
  open Con
  variable Γ Δ Θ Ξ : Con
  record Sub (Δ Γ : Con) : Set where
    constructor mk
    field un : un Δ → un Γ
  open Sub
  variable σ ν ω : Sub Δ Γ
  _∘_    : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  σ ∘ ν  = mk λ θ → un σ (un ν θ)
  ass    : (σ ∘ ν) ∘ ω ≡ σ ∘ (ν ∘ ω)
  ass    = refl
  id     : Sub Γ Γ
  id     = mk λ γ → γ
  idl    : id ∘ σ ≡ σ
  idl    = refl
  idr    : σ ∘ id ≡ σ
  idr    = refl
  ◇      : Con
  ◇      = mk 𝟙
  ε      : Sub Γ ◇
  ε      = _
  ◇η     : σ ≡ ε
  ◇η     = refl
  {-# NO_UNIVERSE_CHECK #-}
  record Ty (Γ : Con)(n : ℕ) : Set where
    constructor mk
    field un : un Γ → Set
  open Ty
  variable i j : ℕ
  variable A B C : Ty Γ i
  _[_]T  : Ty Γ i → Sub Δ Γ → Ty Δ i
  A [ σ ]T = mk λ δ → un A (un σ δ)
  [∘]T   : A [ σ ]T [ ν ]T ≡ A [ σ ∘ ν ]T
  [∘]T   = refl
  [id]T  : A [ id ]T ≡ A
  [id]T  = refl
  record Tm (Γ : Con)(A : Ty Γ i) : Set where
    constructor mk
    field un : (γ : un Γ) → un A γ
  open Tm
  variable t u v : Tm Γ A
  _[_]t  : Tm Γ A → (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T)
  t [ σ ]t = mk λ δ → un t (un σ δ)
  [∘]t   : t [ σ ∘ ν ]t ≡ t [ σ ]t [ ν ]t
  [∘]t   = refl
  [id]t  : {t : Tm Γ A} → t [ id ]t ≡ t
  [id]t  = refl
  _▹_    : (Γ : Con) → Ty Γ i → Con
  Γ ▹ A  = mk (Sigma (un Γ) (un A))
  _,_    : (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T) → Sub Δ (Γ ▹ A)
  σ , t  = mk λ δ → pair (un σ δ) (un t δ)
  p      : Sub (Γ ▹ A) Γ
  p      = mk proj₁
  q      : Tm (Γ ▹ A) (A [ p {A = A} ]T)
  q      = mk proj₂
  ▹β₁    : p {Γ = Γ}{A = A} ∘ (σ , t) ≡ σ
  ▹β₁    = refl
  ▹β₂    : q [ σ , t ]t ≡ t
  ▹β₂    = refl
  ▹η     : _,_ {A = A} (p ∘ σ) (q [ σ ]t) ≡ σ
  ▹η     = refl
  ,∘     : {σ : Sub Δ Γ}{t : Tm Δ (A [ σ ]T)}{ν : Sub Θ Δ} →  (_,_ {A = A} σ t) ∘ ν ≡ (σ ∘ ν , t [ ν ]t)
  ,∘     = refl
  U      : (i : ℕ) → Ty Γ (suc i)
  U i    = mk λ _ → Con
  U[]    : U i [ σ ]T ≡ U i
  U[]    = refl
  El     : Tm Γ (U i) → Ty Γ i
  El t   = mk λ γ → un (un t γ)
  El[]   : El t [ σ ]T ≡ El (t [ σ ]t)
  El[]   = refl
  u'     : (i : ℕ) → Tm Γ (U (suc i))
  u' i   = mk λ _ → mk Con
  Elu    : El {Γ = Γ} (u' i) ≡ U i
  Elu    = refl

infixl 5 _▹_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t

Con      : Set
Con      = S.Con
variable Γ Δ Θ Ξ : Con
Sub      : Con → Con → Set
Sub      = S.Sub
variable σ ν ω : Sub Δ Γ
_∘_      : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
_∘_      = S._∘_
ass      : (σ ∘ ν) ∘ ω ≡ σ ∘ (ν ∘ ω)
ass      = refl
id       : Sub Γ Γ
id       = S.id
idl      : id ∘ σ ≡ σ
idl      = refl
idr      : σ ∘ id ≡ σ
idr      = refl
◇        : Con
◇        = S.◇
ε        : Sub Γ ◇
ε        = S.ε
◇η       : σ ≡ ε
◇η       = refl
Ty       : Con → ℕ → Set
Ty Γ i   = S.Tm Γ (S.U i)
variable i j : ℕ
variable A B C : Ty Γ i
_[_]T    : Ty Γ i → Sub Δ Γ → Ty Δ i
A [ σ ]T = A S.[ σ ]t
[∘]T     : A [ σ ]T [ ν ]T ≡ A [ σ ∘ ν ]T
[∘]T     = refl
[id]T    : A [ id ]T ≡ A
[id]T    = refl
Tm       : (Γ : Con) → Ty Γ i → Set
Tm Γ A   = S.Tm Γ (S.El A)
variable t u v : Tm Γ A
_[_]t    : Tm Γ A → (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T)
t [ σ ]t = t S.[ σ ]t
[∘]t     : t [ σ ∘ ν ]t ≡ t [ σ ]t [ ν ]t
[∘]t     = refl
[id]t    : {t : Tm Γ A} → t [ id ]t ≡ t
[id]t    = refl
_▹_      : (Γ : Con) → Ty Γ i → Con
Γ ▹ A    = Γ S.▹ S.El A
_,_      : (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T) → Sub Δ (Γ ▹ A)
σ , t    = σ S., t
p        : Sub (Γ ▹ A) Γ
p {A = A} = S.p {A = S.El A}
q        : Tm (Γ ▹ A) (A [ p {A = A} ]T)
q        = S.q
▹β₁      : p {Γ = Γ}{A = A} ∘ (σ , t) ≡ σ
▹β₁      = refl
▹β₂      : q [ σ , t ]t ≡ t
▹β₂      = refl
▹η       : {σ : Sub Δ (Γ ▹ A)} → _,_ {A = A} (p {A = A} ∘ σ) (q [ σ ]t) ≡ σ
▹η       = refl
,∘       : {σ : Sub Δ Γ}{t : Tm Δ (A [ σ ]T)}{ν : Sub Θ Δ} →  (_,_ {A = A} σ t) ∘ ν ≡ (σ ∘ ν , t [ ν ]t)
,∘       = refl
U        : (i : ℕ) → Ty Γ (suc i)
U i      = S.u' i
U[]      : U i [ σ ]T ≡ U i
U[]      = refl
El       : Tm Γ (U i) → Ty Γ i
El t     = t
El[]     : El t [ σ ]T ≡ El (t [ σ ]t)
El[]     = refl
u'       : (i : ℕ) → Tm Γ (U (suc i))
u' i     = S.u' i
Elu      : El {Γ = Γ} (u' i) ≡ U i
Elu      = refl

-- the Russell equations in the output model
Ty∅      : Ty Γ i ≡ Tm Γ (U i)
Ty∅      = refl
[]∅      : A [ σ ]T ≡ A [ σ ]t
[]∅      = refl
\end{code}
