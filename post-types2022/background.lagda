\begin{code}[hide]
{-# OPTIONS --rewriting #-}
module _ where

open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}

module Basics where
\end{code}
\section{Background\label{sec:background}}
In this section we give a brief introduction to Agda,
which is an implementation of Martin-L{\"o}f's dependent type
theory~\cite{Martin-Lof-1972} extended with a number of
constructions such as inductive data types, records, modules,
\etc{}  We make a brief overview of the features that are
used in this paper.  For the in-depth introduction please
refer to Agda's user manual~\cite{agda-2-6-3}.

\subsection{Datatypes} Datatypes are defined as follows:
\begin{code}
  data ℕ : Set where
    zero  : ℕ
    suc   : ℕ → ℕ

  data Fin : ℕ → Set where
    zero  : {n : ℕ} → Fin (suc n)
    suc   : {n : ℕ} → Fin n → Fin (suc n)
\end{code}
The type \AD{ℕ} of unary natural numbers is a datatype with two
constructors: \AC{zero} and \AC{suc}.  The type of \AD{ℕ} is \AF{Set} which
is Agda's builtin type of small types.

The type \AD{Fin} is indexed by \AB{ℕ} and it also has two constructors
\AC{zero} and \AC{suc}.  The names of the constructors can overlap.  In the
definition of the \AD{Fin} constructors we used implicit argument
syntax\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/implicit-arguments.html}%
       {agda.readthedocs.io/en/v2.6.3/language/implicit-arguments.html}%
} to define the variable \AB{n}.
When using constructors of \AD{Fin}, we can leave out specifying these
arguments relying on Agda's automatic inference. These can be also passed
explicitly as follows:
\begin{code}[hide]
module XXX where
  open import Data.Nat using (ℕ; zero; suc)
  open import Data.Fin using (Fin; zero; suc)
\end{code}
\begin{code}
  a : Fin 2
  a = zero {n = 1}
\end{code}
Numbers 0, 1, 2, \dots are implicitly mapped into \AD{ℕ} in the
usual way.

\subsection{Records}
Agda makes it possible to define records\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/record-types.html}%
       {agda.readthedocs.io/en/v2.6.3/language/record-types.html}%
}.
They generalise dependent products, making it possible to
name the fields. For example, we can define the type of dependent pairs
using records as follows:
\begin{code}
  record Pair (A : Set) (B : A → Set) : Set where
    constructor _,_
    field
      fst : A
      snd : B fst
\end{code}
The \AD{Pair} record is parametrised by the type \AB{A} and
the family \AB{B} (over \AB{A}).  The type has two fields named
\AF{fst} and \AF{snd} that correspond to first and second projections
of the dependent pair.  Finally, we can give a constructor \AC{\_,\_}
that we can use to construct the values of type \AD{Pair}.  Note
that the constructor uses the mixfix notation\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/mixfix-operators.html}%
       {agda.readthedocs.io/en/v2.6.3/language/mixfix-operators.html}%
}. This means that arguments
replace the underscores, so the comma \AC{,} becomes a binary
operation.  The values can be constructed as follows:
\begin{code}
  b : Pair ℕ Fin
  b = 5 , zero
\end{code}

\subsection{Modules}
Modules\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/module-system.html}%
       {agda.readthedocs.io/en/v2.6.3/language/module-system.html}%
} make it possible to collect the definitions that logically
belong together, giving them a separate namespace.  Modules can
accept parameters.  They abstract variables
for the definitions within the module.  In the paper we only use
modules to group the definitions together and reuse the names of
the definitions.  For example, here we define modules \AM{X} and
\AM{Y}, where \AM{Y} is parametrised with the variable \AB{n},
which is a natural number.
\begin{code}
  module X where
    foo : ℕ
    foo = 5

  module Y (n : ℕ) where
    foo : ℕ
    foo = n
\end{code}


\subsection{Forward Declarations}
Agda makes it possible\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/mutual-recursion.html\#mutual-recursion-forward-declaration}
       {agda.readthedocs.io/en/v2.6.3/language/mutual-recursion.html\#mutual-recursion-forward-declaration}%
} to make a declaration and provide a
definition later.  This is useful when dealing with mutual
definitions.  For example, we can have a mutual definition of
even and odd numbers as the following indexed types:
\begin{code}
  data Even : ℕ → Set 
  data Odd : ℕ → Set 

  data Even where
    zero : Even zero
    suc  : {n : ℕ} → Odd n → Even (suc n)
  data Odd where
    suc  : {n : ℕ} → Even n → Odd (suc n) 
\end{code}
First we defined the signature of both data types;
after that we gave the definitions of their constructors.
By making such a forward declaration, we were able to refer
\AD{Odd} in the definition of the \AC{suc} constructor in \AD{Even}.


\subsection{Postulates}
Agda makes it possible\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/postulates.html}%
       {agda.readthedocs.io/en/v2.6.3/language/postulates.html}%
} to declare objects without ever providing
a definition.  This can be thought of as a typed free variable.
For example, we can postulate that there exists some natural number
\AB{q}:
\begin{code}
  postulate
    q : ℕ
\end{code}

\subsection{Rewrite Rules}
Agda makes it possible to define rewrite rules\footnote{%
  \href{https://agda.readthedocs.io/en/v2.6.3/language/rewriting.html}%
       {agda.readthedocs.io/en/v2.6.3/language/rewriting.html}%
}, which are typically used to turn propositional equations into definitional ones.
However, in combination with postulates, we can also simulate some
reduction behaviour.  For example, we can postulate natural numbers,
eliminator for natural numbers and reduction equalities.  Then we
can use rewrite rules to simulate reduction.
\begin{code}
  postulate
    Nat : Set
    z : Nat
    s : Nat → Nat
    elim : (P : Nat → Set) → P z → ((n : Nat) → P n → P (s n)) 
         → (n : Nat) → P n
    elim-z : ∀ {P pz ps} → elim P pz ps z ≡ pz
    elim-s : ∀ {P pz ps n} → elim P pz ps (s n) ≡ ps n (elim P pz ps n)
    {-# REWRITE elim-z elim-s #-}
\end{code}
We postulate the type for natural numbers \AD{Nat} and its two 
constructors \AF{z} and \AF{s}.  After that, we postulate the type
for the eliminator for natural numbers in the usual way.
Finally we define two $\beta$-like equalities for the eliminator.
By turning these equalities into rewrite rules, we make our
eliminator to reduce in the usual way.







