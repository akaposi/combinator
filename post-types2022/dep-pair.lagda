\begin{code}[hide]
open import Data.Nat
open import Data.Bool
open import Data.String
open import Data.Fin
open import Data.Product
open import Relation.Binary.PropositionalEquality
open import Function

postulate
  ⋯ : ∀ {ℓ}{A : Set ℓ} → A
\end{code}

\section{\label{sec:sequences}Dependent Sequences}

We start with a detailed exploration of the dependent product
isomorphism presented in the introduction.  While this example is
not very practical, it is concise and easy
to understand.

For a fixed pair of types, the encoding of non-dependent pair
can be expressed in Agda as follows:
\begin{code}
pair : (b : Bool) → if b then String else ℕ
pair true   = "Types"   -- first projection
pair false  = 22        -- second projection
\end{code}

Surprisingly, similar presentation of dependent pairs for
a fixed type and a family over it is expressible in Agda
using forward declarations.  For example, for \AD{ℕ} and
\AD{Fin} we have:

\begin{code}
dpair-hlpr : ℕ
dpair : (b : Bool) → if b then ℕ else Fin dpair-hlpr
dpair-hlpr = dpair true

dpair true   = 5        -- first projection of type ℕ
dpair false  = # 3      -- second projection of type Fin 5
\end{code}
According to M\"unchhausen method, we ``cut'' the cyclic
dependency of \AB{dpair} by introducing a placeholder
called \AB{dpair-hlpr}.  Here forward declarations
make it possible to postpone the definition of \AB{dpair-hlpr}.
After that, we define \AB{dpair} and we ``close the cycle''
by giving the value to the placeholder.

% In order to describe \AB{dpair} that has a very dependent type,
% we ``cut'' the cyclic dependency by introducing a forward
% declaration called \AB{dpair-hlpr}.  The purpose of this value
% is to serve as a placeholder that helps to express the
% type of \AB{dpair}.  After that, we close the cycle by assigning
% the first projection of \AB{dpair} to the placeholder.  After
% this is done, we can complete the definition by assigning the
% actual values to \AB{dpair}.


Let us try to abstract this encoding to arbitrary types, and
prove the isomorphism from the introduction.  For non-dependent
pairs, we have:
\begin{AgdaAlign}
\begin{AgdaSuppressSpace}
\begin{code}
module _ (ext : ∀ {a b} → Extensionality a b) where
  Pair : Set → Set → Set
  Pair A B = (b : Bool) → if b then A else B

  Pair≅× : ∀ A B → (A × B) ↔ Pair A B
  Pair≅× A B = mk↔ {f = to}{from} (to∘from , λ _ → refl)
    where
      to : _; from : _; to∘from : _
      to  (a , b)  = λ {true → a; false → b}
      from      f  = f true , f false
      to∘from   f  = ext λ {true → refl; false → refl}
\end{code}
\end{AgdaSuppressSpace}
\end{AgdaAlign}
The \AD{↔} is a type for bijections, and \AF{mk↔} constructs
the bijection from forward and backward functions and a pair of
proofs that they are inverses of each other.
As can be seen, conversion from \AD{Pair} is memoisation.
Correspondingly, conversion into \AD{Pair} is ``unmemoisation''.
These operations are clearly inverses of each other, assuming
functional extensionality.


Encoding of dependent pairs has to mention the
placeholder \AR{h} and the value that this placeholder
gets (\AR{f} \AC{true}) by means of explicit equation \AR{eq}.
\begin{code}
record DPair (A : Set) (B : A → Set) : Set where
  constructor _▹_[_]
  field
    h   : A
    f   : (b : Bool) → if b then A else B h
    eq  : h ≡ f true
\end{code}
Such an encoding corresponds to the first variant of the
M\"unchhausen method, as the equality that closes the cycle
is made explicit.  Note that \AF{eq} corresponds
to the definition of \AB{dpair-hlpr} in the presentation
above.

% Note that we are using a record (a generalised Σ type) to
% give an encoding of a Σ type.  


The isomorphism between dependent pairs and \AD{DPair} requires
a little bit more work, as we are dealing with equations within
the structure.  Assuming functional extensionality \AF{ext} and uniqueness
of identity proofs \AF{uip}, equality of two \AD{DPair}s can be derived
from point-wise pair equality given by \AD{\_≡ᵈ\_}.
\begin{AgdaAlign}
\begin{code}
module _ (ext : ∀ {a b} → Extensionality a b)
         (uip : ∀ {A : Set}{a b : A} → (p q : a ≡ b) → p ≡ q) where

  record _≡ᵈ_ {A}{B} (a b : DPair A B) : Set where
    constructor _&_
    field
      fst  : DPair.h a ≡ DPair.h b
      snd  : DPair.f a false ≡ subst B (sym fst) (DPair.f b false)

  ≡ᵈ⇒≡ : ∀ {A B} {a b : DPair A B} → a ≡ᵈ b → a ≡ b
\end{code}
\begin{code}[hide]
  ≡ᵈ⇒≡ {a = .h ▹ f [ eql ]} {h ▹ g [ eqr ]} (refl & snd)
    = let f≡g : f ≡ g
          f≡g = ext λ {true → trans (sym eql) eqr; false → snd}
      in elim-eqs f≡g _ _
    where
      elim-eqs : ∀ {A}{B : A → Set}{h : A}{f g}
          → f ≡ g → (eql : h ≡ f true) → (eqr : h ≡ g true)
          → (_▹_[_] {A}{B} h f eql) ≡ (h ▹ g [ eqr ])
      elim-eqs refl eql eqr with uip eql eqr
      ... | refl = refl

  sym∘sym : ∀ {A : Set}{a b : A} (p : a ≡ b) → p ≡ sym (sym p)
  sym∘sym refl = refl
\end{code}
With these definitions at hand, the isomorphism between \AD{DPair}s
and \AD{Σ} types is very similar to its non-dependent version:
\begin{AgdaSuppressSpace}
\begin{code}
  Pair≅Σ : ∀ A B → (Σ A B) ↔ DPair A B
  Pair≅Σ A B = mk↔ {f = to}{from} (to∘from , λ _ → refl)
    where
\end{code}
\begin{code}[hide]
      to : _; from : _; to∘from : _
\end{code}
\begin{code}
      to (a , b) = a ▹ (λ {true → a; false → b}) [ refl ]
      from (h ▹ f [ eq ]) = f true , subst B eq (f false)
      to∘from (h ▹ f [ eq ]) = ≡ᵈ⇒≡ (sym eq & cong (λ x → subst B x (f false)) (sym∘sym eq))
\end{code}
\end{AgdaSuppressSpace}
\end{AgdaAlign}

As can be seen, working with explicit equalities is 
tricky.  Switching to more powerful type theories (\eg{}
cubical type theory) would eliminate the necessity to use axioms,
but it would not solve the transport hell problem.
The \AB{pair} example works so nicely, because
we essentially turned the propositional equality into
the definitional one.

\subsection{Infinite Sequences}
In the type theory proposed by Hickey, the only extension to
the standard type theory is addition of very dependent
\emph{functions}.  It is observed that (very) dependent records
can be always presented as very dependent functions
by choosing a domain type that enumerates the fields.
This is essentially what the example with dependent pairs
does --- \AD{Σ} type has two fields that are enumerated
by booleans.

However, dependent functions can do more than that,
as their domain does not have to be finite.  Let us now
consider such an infinite case by defining
non-increasing infinite sequences.  With a little abuse of notation,
we can present those as the following very dependent type.
\begin{code}
  -- ↓-seq : (n : Nat) → if n == 0 then ℕ else Fin (1 + ↓-seq (n - 1))
\end{code}

The same M\"unchhausen technique with forward declarations
can be used to define such a function.
We start by forward declaring \AD{Ty} (expression on
the right hand side of the arrow in the type above) and
its interpretation \AD{I} into natural numbers.
\begin{code}
Ty : ℕ → Set
I : ∀ n → Ty n → ℕ
\end{code}
At the same time we forward declare the actual sequence
that we want to define:
\begin{code}
↓-seq : (n : ℕ) → Ty n
\end{code}

The type of the elements in the sequence is defined inductively
as follows: for zero we have \AD{ℕ}, the successor case gives us
\AD{Fin} of whatever the interpretation of the sequence that
we are defining at predecessor is going to return us.
\begin{code}
Ty 0        = ℕ
Ty (suc n)  = Fin $ suc $ I n (↓-seq n)
\end{code}

The interpretation of the elements at the given sequence is
straight-forward: zero case has a natural number that we return;
elements of \AD{Fin} types are casted into natural numbers.
\begin{code}
I 0       n = n
I (suc n) i = toℕ i
\end{code}

Finally, we define the actual data of our non-increasing
infinite sequence.
\begin{code}
↓-seq 0 =   5
↓-seq 1 = # 3
↓-seq 2 = # 2
↓-seq (suc (suc (suc n))) = # 0
\end{code}

Notice that in this particular case, the types of the elements
in sequence only depend on the previous element.  We can imagine
full induction, where the element can depend on all the previously
defined elements.  In this case induction-recursion becomes
crucially important to generate an $n$-fold dependent type.



\begin{code}[hide]
data Σₙ : ℕ → Set₁
⟦_⟧ : ∀ {n} → Σₙ n → Set

data Σₙ where
  ze : Set → Σₙ 0
  su : ∀ {n} → (σ : Σₙ n) → (⟦ σ ⟧ → Set) → Σₙ (suc n)

⟦ ze t   ⟧  = t
⟦ su σ t ⟧  = Σ ⟦ σ ⟧ t


-- Interp
ty : (n : ℕ) → Σₙ n
last : ∀ {n} → ⟦ ty n ⟧ → ℕ

ty 0 = ze ℕ
ty (suc n) = su (ty n) (Fin ∘ last {n})

last {zero}  a = a
last {suc n} (p , q) = toℕ q

test : ⟦ ty 3 ⟧
test = ((5 , # 3) , # 2) , # 0
\end{code}


\begin{comment}
\subsection{Dependent Product Rule}
\todo[inline]{Here we have the encoding of the 3' example from
applications.txt.  Thanks to Ambrus for explaining.  However,
I am not sure whether we actually need this.}

\begin{code}
record Q (A : Set)(f : A → A) : Set₁ where
  constructor qcon
  field
    fst : A → Set
    snd : (a : A) → fst (f a)

-- Cut the cycle in the following very dependent type:
--    G : (a:A) → Set × proj₁ (G (f a))
-- which is isomorphic to Q.
record W (A : Set)(f : A → A) : Set₁ where
  constructor wcon
  field
    fst : A → Set × Σ Set id
    eq : ∀ a → proj₁ (proj₂ (fst a)) ≡ proj₁ (fst (f a))

-- Isomorphism without the proofs
Q≅W : ∀ {A : Set}{f : A → A} → Q A f ↔ W A f
Q≅W {A}{f} = mk↔ {f = to}{from} ⋯
  where
    to : _
    to (qcon F r) = wcon (λ a → F a , F (f a) , r a) (λ _ → refl)

    from : _
    from (wcon r eq) =
      qcon (proj₁ ∘ r)
           (λ a → subst id (eq a) $ proj₂ (proj₂ (r a)))

\end{code}
\end{comment}

