% easychair.tex,v 3.5 2017/03/15

\documentclass{easychair}
%\documentclass[EPiC]{easychair}
%\documentclass[EPiCempty]{easychair}
%\documentclass[debug]{easychair}
%\documentclass[verbose]{easychair}
%\documentclass[notimes]{easychair}
%\documentclass[withtimes]{easychair}
%\documentclass[a4paper]{easychair}
%\documentclass[letterpaper]{easychair}
%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usepackage{doc}

% use this if you have a long article and want to create an index
% \usepackage{makeidx}

% In order to save space or manage large tables or figures in a
% landcape-like text, you can use the rotating and pdflscape
% packages. Uncomment the desired from the below.
%
% \usepackage{rotating}
% \usepackage{pdflscape}

% Some of our commands for this guide.
%
\newcommand{\easychair}{\textsf{easychair}}
\newcommand{\miktex}{MiK{\TeX}}
\newcommand{\texniccenter}{{\TeX}nicCenter}
\newcommand{\makefile}{\texttt{Makefile}}
\newcommand{\latexeditor}{LEd}

%\makeindex

%% Front Matter
%%
% Regular title as in the article class.
%
\title{Towards dependent combinator calculus}

% Authors are joined by \and. Their affiliations are given by \inst, which indexes
% into the list defined using \institute
%
\author{
Thorsten Altenkirch\inst{1}%\thanks{text}
\and
Ambrus Kaposi\inst{1}\inst{2}%\thanks{text}
}

% Institutes for affiliations are also joined by \and,
\institute{
  University of Nottingham,
  Nottingham, United Kingdom\\
  %\email{thorsten.altenkirch@nottingham.ac.uk}
\and
   Eötvös Loránd University,
   Budapest, Hungary\\
   %\email{akaposi@inf.elte.hu}
 }

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair

\authorrunning{Altenkirch and Kaposi}

% \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{A container model of type theory}

\begin{document}

\maketitle

% \begin{abstract}
% Do we need an abstract?
% \end{abstract}

%------------------------------------------------------------------------------
\section*{Introduction}
\label{sect:introduction}

We have recently given a completely intrinsic presentation of simply
typed combinator calculus with extensionality eqations and shown the
equivalence with simply typed
$\lambda$-calculus. \cite{fscd-submission}. The next step is clear: we
need to do the same thing for dependent types.

In simply typed combinator calculus we have:
\begin{code}
K : {A B : Set} → A → B → A
K a b = a
S : {A B C : Set} → (A → B → C) → (A → B) → A → C
S f g a = f a (g a)
\end{code}
Identity |I = S K K| is derivable and using the well known abstraction algorithm we can
encode all simply typed |λ|-terms. It is straightforward to come up
with depdnently typed versions of the combinators:


%------------------------------------------------------------------------------

\newpage

\label{sect:bib}
\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{references}

%------------------------------------------------------------------------------
\end{document}

