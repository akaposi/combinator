% easychair.tex,v 3.5 2017/03/15

\documentclass{easychair}
%\documentclass[EPiC]{easychair}
%\documentclass[EPiCempty]{easychair}
%\documentclass[debug]{easychair}
%\documentclass[verbose]{easychair}
%\documentclass[notimes]{easychair}
%\documentclass[withtimes]{easychair}
%\documentclass[a4paper]{easychair}
%\documentclass[letterpaper]{easychair}
%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usepackage{doc}

% use this if you have a long article and want to create an index
% \usepackage{makeidx}

% In order to save space or manage large tables or figures in a
% landcape-like text, you can use the rotating and pdflscape
% packages. Uncomment the desired from the below.
%
% \usepackage{rotating}
% \usepackage{pdflscape}

% Some of our commands for this guide.
%
\newcommand{\easychair}{\textsf{easychair}}
\newcommand{\miktex}{MiK{\TeX}}
\newcommand{\texniccenter}{{\TeX}nicCenter}
\newcommand{\makefile}{\texttt{Makefile}}
\newcommand{\latexeditor}{LEd}

%\makeindex

%% Front Matter
%%
% Regular title as in the article class.
%
\title{Towards dependent combinator calculus}
\pagenumbering{gobble}
% Authors are joined by \and. Their affiliations are given by \inst, which indexes
% into the list defined using \institute
%
\author{
  Thorsten Altenkirch\inst{1}
  \and
  Ambrus Kaposi\inst{2}
  \and
  Artjoms {\v{S}}inkarovs\inst{3}\thanks{%
    This work is supported by the Engineering and Physical Sciences Research Council
    through the grant EP/N028201/1.}
  \and
  Tamás Végh\inst{2}
}% Institutes for affiliations are also joined by \and,
\institute{
  School of Computer Science, University of Nottingham, UK\\
  \email{psztxa@@nottingham.ac.uk}
  \and
  E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest, Hungary\\
  \email{\{akaposi,vetuaat\}@@inf.elte.hu}
  \and
  Heriot-Watt University, Scotland, UK \\
  \email{a.sinkarovs@@hw.ac.uk}
 }

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair
\authorrunning{Altenkirch, Kaposi, {\v{S}}inkarovs, Végh}

% \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{Towards dependent combinator calculus}

\begin{document}

\maketitle

% \begin{abstract}
% Do we need an abstract?
% \end{abstract}

%------------------------------------------------------------------------------
% \section*{Introduction}
% \label{sect:introduction}

We have recently given a completely intrinsic presentation of simply
typed combinator calculus with extensionality equations and shown the
equivalence with simply typed
$\lambda$-calculus. \cite{fscd-sub}. The next step is clear: we
need to do the same thing for dependent types. We will present below the main ideas of the
construction using a shallow embedding in Agda.

In simply typed combinator calculus we have:
\begin{spec}
K : {A B : Set} → A → B → A
K a b = a
S : {A B C : Set} → (A → B → C) → (A → B) → A → C
S f g a = f a (g a)
\end{spec}
Identity |I = S K K| is derivable and using the well known abstraction algorithm \cite{DBLP:books/cu/HindleyS86} we can
encode all simply typed |λ|-terms. It is straightforward to come up
with dependently typed versions of the combinators:
\begin{code}
K : {A : Set}{B : A → Set} → (a : A) → B a → A

S : {A : Set}{B : A → Set}{C : (a : A) → B a → Set}
  → ((a : A)(b : B a) → C a b)
  → (g : (a : A) → (B a))
  → (a : A) → C a (g a)
\end{code}
Clearly, the non-dependent version arise by instantiating the
dependent types with constant families. 
However, there is a problem when deriving abstraction. E.g. usually we would say
\begin{spec}
λ x → K = K K 
\end{spec}
However, this is no longer correct, because the variable |x| may occur
in the (hidden) types |A|,|B|. As a consequence we need to make the
type parameters of |K| and |S| explicit by reflecting them into terms
using a universe:
\begin{code}
  U : Set
  El : U → Set
  
  u : U
  Π : (A : U)(B : El A → U) → U
\end{code}
with the equations |El u = U| and |El (Π A B) ≡ ((a : El A) → El (B a))|.

For simplicity we use an inconsistent calculus with  |Type : Type| but
this could be easily stratified by using universe levels. 

We can now redefine the combinators by making the type arguments
explicit using the universe:
\begin{code}
K : {A : U}(B : El A → U) → (a : El A) → El (B a) → El A

S :   {A : U}(B : El A → U)(C : (a : El A) → El (B a) → U)
      → ((a : El A)(b : El (B a)) → El (C a b))
      → (g : (a : El A) → El (B a))
      → (a : El A) → El (C a (g a))
 \end{code}
Now the dependency of the types is a usual term dependency and we can
proceed defining bracket abstraction, e.g. 
\begin{spec}
λ (x : C) → K = K K-Type (K C) K
\end{spec}
where |K-Type| is the term in the universe corresponding to the type
of |K| which can be derived using the combinators (which now include
|u| and |Π|). However, there is at least one point where we have to
appeal to Baron M\"{u}nchhausen \cite{types-22}. How do we derive
non-dependent |K| from dependent |K|?
We would like to say:
\begin{spec}
K' : {A B : U} → El A → El B → El A
K' {A} B = K A (K' u B)
\end{spec}
But here we are using |K'| in the definition of |K'|?! It turns out that we can
get ourselves out of the swamp by using |I| as a primitive combinator:
\begin{code}
I : {A : U} → El A → El A

K' {A} {B} = K (K (K (K (I {u}) u) A) B)
\end{code}
Using |K'| we can derive a non-dependent version of |S| as well. However,
we are still using $\lambda$-calculus for polymorphism which eventually needs to be eliinated too. 

\section*{Summary}
\label{sec:summary}

The idea is that we start with a type theory with |Π|-types (without lambda abstraction) and a
universe and we are going to develop a dependent combinator calculus
in the universe. The main insight is that apart from the dependent
version of |S| and |K| we now also need |u| and |Π|.

We have only started on this work. We need to show that the
abstraction algorithm works in general and has all the desired
properties. Also we need to give a more semantic argument why
the non-dependent version of |K| can be derived from
the dependent one. In the formal version of this construction, we plan
to use the initial categories with families~\cite{DBLP:journals/corr/abs-1904-00827}
with extra structure as our syntax.

We would like to adopt the extensionality axioms to the dependent
case. Furthermore the question is whether we can by further application
of M\"unchausian reasoning completely eliminate the outer level and
avoid variables altogether as in the non-dependent calculus.












%------------------------------------------------------------------------------

\newpage

\label{sect:bib}
\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{references}

%------------------------------------------------------------------------------
\end{document}

