
```
{-# OPTIONS --rewriting  --lossy-unification #-}
module dep-new where

open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}

postulate
  U : Set
  El : U → Set
  
  u : U
  Π : (A : U)(B : El A → U) → U

  eq-u : El u ≡ U
  eq-π : {A : U}{B : El A → U} → El (Π A B) ≡ ((a : El A) → El (B a))

{-# REWRITE eq-u #-}
{-# REWRITE eq-π #-}

{-# INJECTIVE El #-}
{-# INJECTIVE Π #-}
```
We start with I:
```
I : {A : U} → El A → El A
I {A} x = x
```
Unlike in the simply typed case `I` is not directly derivable from the dependent
versions of `S` and `K` hence we add it as a primitive.

Defining K

We introduce dependent K:
```
K : {A : U}(B : El A → U) → (a : El A) → El (B a) → El A
K {A} B a b = a
```
Our goal is to reduce all usages of K to the dependent version:

We need to derive a non-dependent version.
```
KK : {A B : U} → El A → El B → El A
--KK A B = K A (λ x → B)
--KK A B = K A (K u (λ x → A) B)
--KK A B = K A (K u (K u (λ x → u) A) B)
--KK A B = K A (K u (K u (K u (λ x → x) u) A) B)
KK {A} {B} = K (K (K (K (I {u}) u) A) B)
```
Using `KK` we can define a non-dependent version of `Π`
```
infixr 10 _⇒_
_⇒_ : U → U → U
--A ⇒ B = Π A (λ x → B)
A ⇒ B = Π A (KK {u} {A} B) 
{-# INJECTIVE _⇒_ #-}
```
Finally we add the dependent `S`
```
S : {A : U}(B : El A → U)(C : (a : El A) → El (B a) → U)
  → ((a : El A)(b : El (B a)) → El (C a b))
  → (g : (a : El A) → El (B a))
  → (a : El A) → El (C a (g a))
S {A} B C f g a = f a (g a)
```
We now derive the non-dependent version of `S`:
```
SS : {A B C : U} → (El A → El B → El C) → (El A → El B) → El A → El C
-- SS A B C = S A (λ x → B) (λ x y → C)
SS {A} {B} {C} = S {A} (KK {u} {A} B) (KK {B ⇒ u} {A} (KK {u} {B} C))

{-
CC : (A B C : U) → (El B → El C) → (El A → El B) → El A → El C
CC A B C = SS _ _ _ (KK _ _ (SS _ _ _)) (KK _ _)
-- λ f g x → f (g x)
-- S (K S) K
-}
```
Attempt to derive the polymorphic version of K
quickly end in combinator hell
```
KKK : (A B : U) → El A → El B → El A
KKK = λ A → S {u} (KK {u} {u} (A ⇒ u)) (λ B BB → Π A (λ x → BB x ⇒ A))
        {!KK ? u (K A)  --λ B → K A!} (KK {u} {A})
--λ A → S u (KK u u (A ⇒ u)) (λ B BB → Π A (λ x → BB x ⇒ A)) (λ B → K A) (λ B → KK u A B)
-- λ A → S u (KK u u (A ⇒ u)) (λ B BB → Π A (λ x → BB x ⇒ A)) (λ B → K A) (λ B x → B)
-- λ A → S u (λ B → A ⇒ u) (λ B BB → Π A (λ x → BB x ⇒ A)) (λ B → K A) (λ B x → B)
--KKK = λ A B → K A (λ x → B)
```
The translation of the type of K is also laborious
```
KT : U
--Π u (λ A → Π (A ⇒ u) (λ B → Π A (λ a → B a ⇒ A)))
KT = Π u (λ A → Π (A ⇒ u) (SS {A ⇒ u} {A ⇒ u} {u} (KK {(A ⇒ u) ⇒ u} {A ⇒ u} (Π A)) (SS {A ⇒ u} {A ⇒ u} {A ⇒ u} (λ B → SS (SS (KK {u ⇒ u ⇒ u} _⇒_) B)) (KK (KK A) ))))
-- Π u (λ A → Π (A ⇒ u) (SS {A ⇒ u} {A ⇒ u} {u} (KK {(A ⇒ u) ⇒ u} {A ⇒ u} (Π A)) (SS {A ⇒ u} {A ⇒ u} {A ⇒ u} (λ B → SS (SS (KK {u ⇒ u ⇒ u} _⇒_) B)) (KK (KK A) ))))
-- Π u (λ A → Π (A ⇒ u) (SS {A ⇒ u} {A ⇒ u} {u} (KK {(A ⇒ u) ⇒ u} {A ⇒ u} (Π A)) (SS {A ⇒ u} {A ⇒ u} {A ⇒ u} (λ B → SS (SS (KK {u ⇒ u ⇒ u} _⇒_) B)) (λ B → (KK A) ))))
-- Π u (λ A → Π (A ⇒ u) (SS {A ⇒ u} {A ⇒ u} {u} (KK {(A ⇒ u) ⇒ u} {A ⇒ u} (Π A)) (λ B → (SS (SS (KK {u ⇒ u ⇒ u} _⇒_) B) (KK A) ))))
-- Π u (λ A → Π (A ⇒ u) (SS {A ⇒ u} {A ⇒ u} {u} (λ B → Π A) (λ B → (SS (SS (KK {u ⇒ u ⇒ u} _⇒_) B) (KK A) ))))
-- Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS (SS (KK {u ⇒ u ⇒ u} _⇒_) B) (KK A) )))
-- Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS {A} {u} {u} (SS (KK {u ⇒ u ⇒ u} {A} _⇒_) B) (KK A) )))
-- Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS A u u (SS _ _ _ (KK (u ⇒ u ⇒ u) A _⇒_) B) (λ a → A))))
-- Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS A u u (λ a → _⇒_ (B a)) (λ a → A))))
```

```
IT : U
IT = Π u (SS {u} {u} {u} (SS {u} {u} {u ⇒ u} (KK {u ⇒ (u ⇒ u)} _⇒_) I) I)
-- Π u (SS {u} {u} {u} (λ A → A ⇒_) I)
-- Π u (SS {u} {u} {u} (λ A → A ⇒_) (λ A → A))
--Π u (λ A → A ⇒ A)
```



KT : U
--KT = Π u (λ A → Π (A ⇒ u) (λ B → Π A (λ a → B a ⇒ A)))
KT = Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS (λ a → _⇒_ (B a)) (KK A))))
{-
Π u (λ A → Π (A ⇒ u) (λ B → Π A (λ a → B a ⇒ A)))
Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS (λ a → _⇒_ (B a)) (KK A))))
Π u (λ A → Π (A ⇒ u) (λ B → Π A (SS (SS (KK _⇒_) B) (KK A))))
Π u (λ A → Π (A ⇒ u) (SS (KK (Π A)) ((λ B → Π A (SS (SS (KK _⇒_) B) (KK A))))
-}

K' : El KT
K' = {!K!}




Pi : (A : U) → (A → U) → U
Π u (λ A → (A ⇒ U) ⇒ U)
Π u (S (λ A → (A ⇒ U)) (KU u)


