{-# OPTIONS --cubical --safe #-}

-- Typechecked using Agda version 2.6.2.2 and Cubical library version 0.4prec3e097a in Linux on a laptop with 16 GB RAM in less than 10 minutes.

module Readme where

-- formalisation for the paper "Combinatory logic and lambda calculus are equal, algebraically"

-- syntax for different calculi

import Syntax.Common             -- common parts of the syntax (Ty,Con,⇒*)
import Syntax.C                  -- closed combinator calculus with 4 extra equations
import Syntax.C-wk               -- combinator calculus with weakenings and 4 extra equations
import Syntax.L                  -- syntax of simply typed lambda calculus (sCwF with function space)

-- conversion between syntaxes

import Conversion.C=C-wk         -- C.Tm A          = C-wk.Tm ε A
import Conversion.C-wk=L         -- C-wk.Tm Γ A     = L.Tm Γ A
import Conversion.L=L            -- L.Tm ε (Γ ⇒* A) = L.Tm Γ A
import Conversion.C=L            -- C.Tm (Γ ⇒* A)   = L.Tm Γ A     (putting together the above equalities)

-- two more extensions of C not described in the paper (we don't rely on these versions anywhere)

import Syntax.C-var              -- combinator calculus with variables and 4 extra equations
import Syntax.C-wk-ext           -- combinator calculus with weakenings and an extensionality axiom (we don't know how to define lam on this directly)
import Conversion.C=C-var        -- C.Tm A          = C-var.Tm ε A 
import Conversion.C-var=C-wk     -- C-var.Tm Γ A    = C-wk.Tm Γ A
import Conversion.C-wk=C-wk-ext  -- the four equations are equivalent to extensionality

-- example usage

open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Data.Unit
open import Syntax.Ty

module SimplyTyped where
  open import Syntax.Common T'
  import Syntax.L T' as L
  import Syntax.C T' as C
  open import Conversion.C=L T'

  twoL : L.Tm ε (base ⇒ (base ⇒ base) ⇒ base)
  twoL = L.lam (L.lam (L.v₀ L.$ (L.v₀ L.$ L.v₁)))

  twoC twoC' : C.Tm (base ⇒ (base ⇒ base) ⇒ base)
  twoC = g twoL
  twoC' = transport (sym eq) twoL
  -- illustration that transport doesn't compute on indexed types: Ctwo ≠ Ctwo'

  twoC= : let open C in twoC ≡ S $ (S $ (K $ S) $ (S $ (S $ (K $ S) $ (K $ K)) $ (K $ K))) $ (S $ (S $ (K $ S) $ (S $ (S $ (K $ S) $ (K $ K)) $ (K $ K))) $ (S $ (K $ K) $ (S $ K $ K)))
  twoC= = refl

module untyped where
  T : TY
  T = record { Ty = Unit }
  open import Conversion.C=L T
  open import Syntax.Common T
  import Syntax.L T as L
  import Syntax.C T as C

  tL : L.Tm ε _
  tL = lam (q $ q)
    where open L

  tC : C.Tm _
  tC = g tL

  tC= : let open C in tC ≡ S $ (S $ K $ K) $ (S $ K $ K)
  tC= = refl
