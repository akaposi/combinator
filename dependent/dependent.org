* defining lam for sCwF+S,K needs α-normal forms

lam' : Tm Δ B → Sub (Γ,A) Δ → Tm Γ (A⇒B)
lam' (t[σ]) δ := lam' t (σ∘δ)
  t : Tm Θ B
  σ : Sub Δ Θ
  δ : Sub (Γ,A) Δ
  σ ∘ δ : Sub (Γ,A) Θ
lam' vz (σ,t) := lam' t id
   σ:Sub (Γ,A) Δ
   t:Tm  (Γ,A) B
   (σ,t): Sub (Γ,A) (Δ,B)
   vz : Tm (Δ,B) B

lam : Tm (Γ,A) B → Tm Γ (A⇒B)
lam t := lam' t id

α-normal form: you eliminate substitutions, it is still quotiented

CwF with _$_,S,K,Sβ,Kβ

α-normal CwF:


PRECISE: Var, Nf : (Γ:Con)(A: Ty Γ) → Tm Γ A → Set
PRECISE: Var, Nf : (Γ:Con)(A: Ty Γ) → Set
         ⌜_⌝ : Var Γ A → Tm Γ A

Var, Nf : (Γ:Con) → Ty Γ → Set
  vz : Var (Γ,A) (A[p])                       PRECISE: Var (Γ,A) (A[p]) q
  vs : Var Γ A → Var (Γ,B) (A[p])
  var : Var Γ A → Nf Γ A
  -- lam : Nf (Γ,A) B → Nf Γ (Π A B)
  _$_ : Nf Γ (Π A B) → (u:Nf Γ A) → Nf Γ (B[id,u])
  PRECISE _$_ : Nf Γ (Π A B) t → Nf Γ A u → Nf Γ (B[id,u]) (t$u)
  -- β   : 
  -- η   : 
  K  : Tm Γ (Π(x:A).B[x]⇒A)        (Π A (B ⇒ A[p]))     A⇒B := Π A (B[p])
  S  : Tm Γ ((Π(x:A).Π(y:B[x]).C[x,y])⇒Π(f:Π(x:A).B[x]).Π(x:A).C[x,f$x])
  Kβ : K$u$v=u
  Sβ : S$t$u$v = t$v$(u$v)

lamVar : Var (Γ,A) B → Nf Γ (Π A B)
lamVar vz := I = S$K$K
lamVar (suc x) := K$x

lamNf  : Var (Γ,A) B → Nf Γ (Π A B)
lamNf (var x) := lamVar x
lamNf (t$u) := S $ lamNf t $ lamNf u
lamNf K := ??? K$K
      ^ : Nf (Γ,y:C) (Π(x:A[y]).B[y,x]⇒A[y])
           ^ : Nf Γ (Π(y:C).Π(x:A[y]).B[y,x]⇒A[y])
lamNf S := ??? K$S

* lam for CwF+U+El+Π+_·_+S,K
We have CwF +
U : Ty Γ
U[]
El : Tm Γ U ≅ Ty Γ : c                          -- we also need codes for every type! (obviously this has to be stratified to be consistent)
El[]
Π : (A:Ty Γ)→Ty (Γ▹A)→Ty Γ
_⇒_ : Ty Γ → Ty Γ → Ty Γ
A⇒B := Π A (B[p])
Π[]
_·_ : Tm Γ (Π A B) → (u:Tm Γ A) → Tm Γ (B[id,u])
·[]
KT : {Γ:Con} → Ty Γ
KT {Γ} := Π(a:U).Π(b:El a⇒U).Π(x:El a).El(b·x)⇒El a              -- unreadable version: Π U (Π (El q ⇒ U) (Π (El (q[p])) (El (q[p]·q) ⇒ El (q[p²]))))
K : Tm Γ KT
K[] : K {Γ}[σ] = K {Δ}
Kβ : K · a · b · x · y = x
KU : {Γ:Con} → Tm Γ (U ⇒ U)
KUβ : KU · _ = c U
KU∅ : KU = K · c U · (KU (c U))

Now this is how we define lam for K:

lam : Tm (Γ▹A) B → Tm Γ (Π A B)
lam (K {Γ▹A}) := K {Γ} · c (KT {Γ}) · (K · c U · (K · U · KU · c (KT {Γ})) · c A) · K {Γ}
  we need Tm Γ (Π A (KT {Γ▹A})) = Tm Γ (Π A (KT {Γ}[p])) = Tm Γ (A ⇒ KT {Γ})

  K · c U · (K · U · KU · c (KT {Γ})) · c A : Tm Γ (KT {Γ} ⇒ U)
  K · c U · (K · U · KU · c (KT {Γ})) · c A · _ = c A
  K · U · KU · c (KT {Γ}) : Tm Γ (U ⇒ U)
  K · U · KU · c (KT {Γ}) · _ = c (KT {Γ})

TODO: add S, define lam for S

