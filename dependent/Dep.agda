{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Primitives  hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Foundations.HLevels

data Con : Set
data Ty : Con → Set
data Con where
  ε : Con
  _▹_ : (Γ : Con) → Ty Γ → Con

infixl 5 _▹_
infixl 11 _∘_
infixl 10 _$_
infixl 5 _,s_

data Tm : (Γ : Con) → Ty Γ → Type
data Sub : Con → Con → Type


variable
  Γ Δ Ξ Ψ : Con
  A : Ty Γ

_∘′_  : Sub Ξ Δ → Sub Γ Ξ → Sub Γ Δ
id′ : Sub Γ Γ
_[_]ₜ′ : Ty Γ → Sub Δ Γ → Ty Δ
_,s′_ : (δ : Sub Γ Δ) → Tm Γ (A [ δ ]ₜ′) → Sub Γ (Δ ▹ A)
π₁′ : Sub (Γ ▹ A) Γ
q′ : ∀ {δ : Sub Δ Γ} → Tm (Δ ▹ A [ δ ]ₜ′) (A [ δ ∘′ π₁′ ]ₜ′)
data Ty where
  ι : Ty Γ
  Π : (τ : Ty Γ) → Ty (Γ ▹ τ) → Ty Γ
  _[_]ₜ : Ty Γ → Sub Δ Γ → Ty Δ
  [∘] : {γ : Sub Ξ Γ}{φ : Sub Δ Ξ} → {τ : Ty Γ} 
      → τ [ γ ∘′ φ ]ₜ ≡ τ [ γ ]ₜ [ φ ]ₜ
  [id] : {τ : Ty Γ} → τ [ id′ ]ₜ ≡ τ
  Π[] : ∀ {B}{δ : Sub Δ Γ} → ((Π A B)[ δ ]ₜ) ≡ Π (A [ δ ]ₜ′) (B [ (δ ∘′ π₁′) ,s′ q′ ]ₜ)

_[_]ₜ′ = _[_]ₜ

-- Forward declarations of the "constructors"
data Sub where
  --εs   : Sub Γ ε
  id   : Sub Γ Γ
  _∘_  : Sub Ξ Δ → Sub Γ Ξ → Sub Γ Δ
  _,s_ : (δ : Sub Γ Δ) → Tm Γ (A [ δ ]ₜ) → Sub Γ (Δ ▹ A) --(τ [ δ ]ₜ′))
  π₁   : Sub (Γ ▹ A) Γ

  -- Equalities
  --εη  : (γ : Sub Γ ε) → γ ≡ εs
  ass : ∀ {γ : Sub Ξ Δ}{φ : Sub Ψ Ξ}{ψ : Sub Γ Ψ}
      → γ ∘ (φ ∘ ψ) ≡ (γ ∘ φ) ∘ ψ
  idl : {γ : Sub Γ Δ} → id ∘ γ ≡ γ
  idr : {γ : Sub Γ Δ} → γ ∘ id ≡ γ

  ▹βs : {γ : Sub Γ Δ}{t : Tm Γ (A [ γ ]ₜ)} → π₁ ∘ (γ ,s t) ≡ γ
  --▹η : {γ : Sub Γ (Δ ▹ τ)} → ((π₁ ∘ γ) ,s (q′ [ γ ]′)) ≡ γ

  SubSet : isSet (Sub Γ Δ)

▹β₂s : ∀ {γ : Sub Γ Δ}{a : Tm Γ (A [ γ ]ₜ)}{B : Ty (Δ ▹ A)}{b : Tm Γ (B [ γ ,s a ]ₜ)} 
     → ((π₁ ∘ π₁) ∘ (γ ,s a ,s b)) ≡ γ
▹β₂s {γ = γ}{a}{σ}{b} = sym ass ∙ cong (π₁ ∘_) ▹βs ∙ ▹βs

_∘′_ = _∘_
id′ = id
_,s′_ = _,s_

data Tm where
  q : ∀ {δ : Sub Δ Γ} → Tm (Δ ▹ A [ δ ]ₜ) (A [ δ ∘ π₁ ]ₜ)

  _[_] : ∀ {δ : Sub Δ Γ} → Tm Δ (A [ δ ]ₜ) → (γ : Sub Ξ Δ) → Tm Ξ (A [ δ ∘ γ ]ₜ)

  _$_ : ∀ {B}{δ : Sub Δ Γ} → Tm Δ (Π A B [ δ ]ₜ) → (x : Tm Δ (A [ δ ]ₜ)) → Tm Δ (B [ δ ,s x ]ₜ)

  K : ∀ {A : Ty Γ}{B : Ty (Γ ▹ A)}{δ : Sub Δ Γ} → Tm Δ (Π A (Π B (A [ π₁ ∘ π₁ ]ₜ)) [ δ ]ₜ)
  S : ∀ {A : Ty Γ}{B : Ty (Γ ▹ A)}{C : Ty (Γ ▹ A ▹ B)}{δ : Sub Δ Γ} 
    → Tm Δ (Π (Π A (Π B C)) (Π ((Π A B)[ π₁ ]ₜ) (Π (A [ π₁ ∘ π₁ ]ₜ) (C [ (π₁ ∘ π₁ ∘ π₁) ,s 
                                                                         (q) ,s
                                                                         (q [ π₁ ] $ q) ]ₜ))) [ δ ]ₜ)


  -- Equalities 
  -- TODO(artem) add the missing ones  
  [∘] : {δ : Sub Δ Γ}{γ : Sub Ξ Δ}{φ : Sub Ψ Ξ} → {t : Tm Δ (A [ δ ]ₜ)} 
      → t [ γ ∘ φ ] ≡  subst (λ x → Tm Ψ (A [ x ]ₜ)) (sym ass) (t [ γ ] [ φ ])
  [id] : ∀ {δ : Sub Δ Γ}{t : Tm Δ (A [ δ ]ₜ)} → t [ id ] ≡ subst (λ x → Tm Δ ( A [ x ]ₜ)) (sym idr) t

  Kβ : ∀ {B : Ty (Γ ▹ A)}{δ : Sub Δ Γ}{a : Tm Δ (A [ δ ]ₜ)}{b : Tm Δ (B [ δ ,s a ]ₜ)} 
     → K $ a $ b ≡ subst (Tm Δ) (sym (sym [∘] ∙ cong (A [_]ₜ) (sym ass ∙ cong (π₁ ∘_) ▹βs ∙ ▹βs))) a
  Sβ : ∀ {B : Ty (Γ ▹ A)}{C : Ty (Γ ▹ A ▹ B)}{δ : Sub Δ Γ}
         {a : Tm Δ (Π A (Π B C) [ δ ]ₜ)}{b : Tm Δ (Π A B [ π₁ ]ₜ [ δ ,s a ]ₜ)}{c : Tm Δ (A [ π₁ ∘ π₁ ]ₜ [ δ ,s a ,s b ]ₜ)}
     → S $ a $ b $ c ≡ 
       let 
         ac = a $ (subst (Tm Δ) (sym [∘] ∙ cong (A [_]ₜ) (sym ass ∙ cong (π₁ ∘_) ▹βs ∙ ▹βs)) c) 
         bc = (subst (Tm Δ) (sym [∘] ∙ cong (Π A B [_]ₜ) ▹βs) b) 
              $ (subst (Tm Δ) (sym [∘] ∙ cong (A [_]ₜ) (sym ass ∙ cong (π₁ ∘_) ▹βs ∙ ▹βs)) c)
         acbc = ac $ bc
       in ? 
          --subst (Tm Δ) (sym (sym [∘] ∙ (cong (C [_]ₜ) ?) ∙ ?)) acbc

  TmSet : isSet (Tm Γ A)

π₁′ = π₁
q′ = q


-- Ugh, this is the original q with double subsitution...
Q : Tm (Γ ▹ A) (A [ π₁ ]ₜ)
Q = subst (λ x → Tm (_ ▹ x) (x [ π₁ ]ₜ)) [id] ((subst (Tm _) ([∘]) q))


data Var : (Γ : Con)(A : Ty Γ) → Tm Γ A → Set
data Nf : (Γ : Con)(A : Ty Γ) → Tm Γ A → Set

data Var where
  vz : Var (Γ ▹ A [ id ]ₜ) (A [ id ∘ π₁ ]ₜ) q
  vs : ∀ {δ : Sub Δ Γ}{x σ} → (t : Var Δ (A [ δ ]ₜ) x) → Var (Δ ▹ σ) (A [ δ ∘ π₁ ]ₜ) (x [ π₁ ]) 

data Nf where
  var : ∀ {x} → Var Γ A x → Nf Γ A x
  _$_ : ∀ {B}{δ : Sub Δ Γ}{f x} → Nf Δ (Π A B [ δ ]ₜ) f → (Nf Δ (A [ δ ]ₜ) x) → Nf Δ (B [ δ ,s x ]ₜ) (f $ x)
  -- TODO(artem) K, S and probably some equalities are missing.
  --K : ∀ {σ : Ty (Γ ▹ τ)} → Nf Γ (Π τ (Π σ (τ [ π₁ ∘ π₁ ]ₜ))) K

