Applications of the Münchausen technique
----------------------------------------

1. make sense of "_→_ : Set → Set → Set"

   Ty  : Set
   Tm  : Ty → Set
   U   : Ty
   ∅Ty : Ty = Tm U
   _⇒_ : Ty → Ty → Ty
   _$_ : {A}{B} → Tm (A ⇒ B) → Tm A → Tm B
   ar  : Tm (U ⇒ U ⇒ U)
   ∅⇒  : A ⇒ B = ar $ A $ B

   Now we have:
   ar  : Tm (ar $ U $ (ar $ U $ U))

   Note that if we write out the implicit arguments, we still refer to _⇒_, so we don't really get rid of _⇒_:
   ∅⇒  : A ⇒ B = ar ${U}{U⇒U} A ${U}{U} B
   ar  : Tm (U ⇒ U ⇒ U) = Tm (ar ${U}{U⇒U} U ${U}{U} ((ar ${U}{U⇒U} U) ${U}{U} U))


2. universes a la Russell

   Con : Set
   Ty  : Con → Set
   Tm  : (Γ:Con) → Ty Γ → Set
   ∅Ty : Ty Γ = Tm Γ U

   Now we have:
   Tm  : (Γ:Con) → Tm Γ U → Set

   We have a model construction: any model of t.t. with a universe can
   be turned into a Russell-ified version.

3. A dependent version of "A × B ≅ (b:Bool) → if b then A else B"

   We want to replace A × B by Σ A B.

   a₀  : A
   g   : (b:Bool) → if b then A else B a₀
   ∅a₀ : a₀ = g true

   Now we have:
   g : (b:Bool) → if b then A else B (g true)

3'. A very dependent version of "(A → B) × (A → C) ≅  A → B × C"

   (f : A → B) × ((a : A) → C (f a))   ≅  A → Σ B C
   (F : A → Set) × ((a : A) → F a)     ≅  A → (X:Set) × X
   (F : A → Set) × ((a : A) → F (f a)) ≅  (G : (a:A) → Set × proj₁ (G (f a)))
   (F : A → Set) × ((a : A) → F (f a)) ≅  (G : (a:A) → Set × (X:Set) × X) × (∀ a → proj₂ (G a) = proj₁ (G (f a)))

4. Uncategorification: a version of t.t. where we start with contexts
   and substitutions, but we get rid of them by equations ("top-down
   combinatory t.t." -- maybe this shouldn't be called combinatory)

   We want to make sense of the following theory:
   
   Ty   : Ty ⊤ → Set
   Tm   : (Γ:Ty ⊤)→Ty Γ→Set
   _[_] : Ty Γ → Tm Δ (Γ[tt]) → Ty Δ
   _[_] : Tm Γ A → (γ:Tm Δ (Γ[tt])) → Tm Δ (A[γ])
   assT : A[γ[δ]] = A[γ][δ]
   asst : t[γ[δ]] = t[γ][δ]
   id   : Tm Γ (Γ[tt])
   idl  : id[t]=t
   idr  : t[id]=t
   [id] : A[id]=A
   ⊤    : Ty Γ
   tt   : Tm Γ (⊤[tt])
   Σ    : (A:Ty Γ)→Ty (Σ (A[q]))→Ty Γ
   _,_  : (a:Tm Γ A)→Tm Γ (B[id,a])→Tm Γ (Σ A B)
   fst  : Tm Γ (Σ A B) → Tm Γ A
   snd  : (w:Tm Γ (Σ A B)) → Tm Γ (B[id,fst w])
   Σβ₁  : fst (a,b) = a
   Σβ₂  : snd (a,b) = b
   Ση   : (fst w,snd w)=w
   Σ[]  : (Σ A B)[γ]=Σ (A[γ]) (B[γ[fst id],snd id])
   ,[]  : (a,b)[γ]=(a[γ],b[γ])
      
   Note that this is not only circularly typed, but also some types
   (e.g. that of Ty) depend on later operations (e.g. ⊤).

   Note: we could get rid of id (because it is definable using p,q in
   the initial model), but then it is a bit less convenient to specify
   Σ types:
   
     Sub Ω (Γ ▶ A ▶ B) ≅ Sub Ω (Γ ▶ Σ A B)


   We add the following equations to the usual definition of CwF:

   Con     = Ty ∙
   Sub Γ Δ = Tm Γ (Δ[ε])
   σ∘δ     = σ[δ]
   ∙       = ⊤
   ε       = tt
   Γ ▹ A   = Σ Γ (A[q])
   σ,t     = σ ,Σ t
   p       = fst id
   q       = snd id

   Here we also have a model construction. We can also combine it with
   Russell-ification, and obtain the ultimate one-sorted very circular
   type theory.

   Here we should also add an equation like

     "Tm Γ A = Tm ∙ (Π Γ A)"

   and then terms would essentially be only indexed by types and not
   contexts. We also have a (separate) model construction for this,
   and it seems that it cannot be combined with uncategorification or
   Russellification.

5. combinator version of type theory ("bottom-up combinatory t.t.")

   also see http://home.uchicago.edu/~wwtx/variablefree.pdf

   TODO.

   Goal: define dependent version of K, S with their computation rules
   and with equations showing that the previously defined operators
   are all special cases of these.

   It seems that we cannot completely remove _⇒_ by making it a
   special case of ar (see Application 1). This is because of implicit
   arguments of _$_ which contain ⇒:

   ar  : Tm (U ⇒ U ⇒ U)
   ∅⇒  : A⇒B = _$_ {U}{U} (_$_ {U}{U⇒U} ar A) B

   If we replace U⇒U using ∅⇒, it will still contain a U⇒U, and so on.

   Possible strategies:
   
   A. This is not a problem. It is a lie that we can replace ⇒ by ar
      completely, just as it was also a lie that Ty can be replaced
      completely by Tm U (the typing of Tm U needs Ty to start with).

      Formalisation becomes hard: this leads to nontermination. To
      try:

      i)  Flip the equations around. Will this lead to some things not
          typechecking? This way special cases of the more general
          operations become the basic operations. E.g. some instances
          of ar $ A $ B become A⇒B.

      ii) Mark the second implicit argument of _$_ irrelevant with a
          dot. Then Agda will stop unfolding it forever. Does this
          work?

   B. Try to come up with a clever way of avoiding the
      circularity. E.g. define special cases of ar first such as _⇒₁ :
      Ty → Tm (U ⇒ U), or U⇒U : Tm U as a constant, or special case of
      _$_ where the second implicit argument is fixed, etc.

   C. A version of B.: Define Π directly without first defining ⇒. We
      will need _⇒U to express the type of the second argument. Maybe
      we only need one version of S and K then?

   D. Embrace circularity and create a coinductive definition of
      combinator logic. Maybe simple types first. What about the
      coinductive typing of Tm : Tm U → Set?

6. combinator version of system F

7. combinator version of first order logic

   see  http://home.uchicago.edu/~wwtx/Varfreelogic_revised.pdf

   - it might be simpler to define this than full dependent type theory; e.g. there are much fewer equations and the domain of Π is fixed
   
     funar   : ℕ → Set   -- funar n is the set of function symbols with arity ℕ
     relar   : ℕ → Set   -- relar n is the set of relation symbols with arity ℕ

     -- combinator calculus
     Ty      : Set
     Tm      : Ty → Set
     _⇒_     : Ty → Ty → Ty
     _∙_     : Tm (A⇒B) → Tm A → Tm B
     K       : Tm (A⇒B⇒A)                               -- Tait limits this to Tm (A⇒ι⇒A)
     Kβ      : K∙a∙b = a
     S       : Tm ((A⇒B⇒C)⇒(A⇒B)⇒A⇒C)                   -- Tait limits this to Tm ((ι⇒B⇒C)⇒(ι⇒B)⇒ι⇒C)
     Sβ      : S∙f∙g∙a = f∙a∙(g∙a)
     η       : ...                                      -- Tait introduces the η rule

     -- propositional logic
     For     : Ty                                       -- the type of formulas, metavariables: 
     Pf      : Tm For → Ty                              -- the type of proofs
     irr     : (u v : Tm (Pf ϕ)) → u = v                -- proof irrelevance
     ⊃       : Tm (For ⇒ For ⇒ For)                     -- implication
     lam     : Tm ((Pf ϕ ⇒ Pf ψ) ⇒ Pf (⊃∙ϕ∙ψ))
     app     : Tm (Pf (⊃∙ϕ∙ψ) ⇒ Pf ϕ ⇒ Pf ψ)
     ∧       : Tm (For ⇒ For ⇒ For)                     -- conjunction
     ,       : Tm (Pf ϕ ⇒ Pf ψ ⇒ Pf (∧∙ϕ∙ψ))
     fst     : Tm (Pf (∧∙ϕ∙ψ) ⇒ Pf ϕ)
     snd     : Tm (Pf (∧∙ϕ∙ψ) ⇒ Pf ψ)

     -- TODO: add Π type
     Π       : Tm (ι ⇒ For) → Ty                        -- TODO: is this enough? Π ϕs = (t:ι)→Pf(ϕs∙t)
     ⇒∅      : ι⇒Pf ϕ = Π (K∙ϕ)
     Kd      : Tm (Π ϕs ⇒ ι)
     Sd      : TODO

     -- first order logic
     ι       : Ty                                       -- the type of terms (this is one-sorted first order logic)
     _⇒^n_   : Ty → Ty                                  -- a helper definition
     A ⇒^0     B := B
     A ⇒^(1+n) B := A ⇒ (A ⇒^n B)
     fun     : (n:ℕ) → Tm (ι ⇒^(funar n) ι)
     rel     : (n:ℕ) → Tm (ι ⇒^(funar n) For)
     ∀       : Tm ((ι ⇒ For) ⇒ For)
     mk∀     : Tm (Π(t:ι) Pf(ϕs∙t) ⇒ Pf (∀∙ϕs))         -- TODO: here we need a Π type (we only need the domain to be ι)
     un∀     : Tm (Pf (∀∙ϕs) ⇒ Π(t:ι) Pf(ϕs∙t))         -- TODO: Π

   - another version where Pf is not internal but external:

     Ty   : Set
     Tm   : Ty → Set
     _∙_,K,S
     ι    : Ty
     For  : Ty
     Pf   : Tm For → Set
     ∀    : Tm ((ι ⇒ For) ⇒ For)
     _∙∀_ : Pf (∀ ∙ A) → (t : Tm ι) → Pf (A ∙ ι)
     K∀   : ...
     S∀   : ...

   - there is also the infinitary version which is very simple (no need for Munchausen here):
     For : Set
     Pf  : For → Prop
     _⇒_ : For → For → For
     _∙_ : Pf (A⇒B) → Pf A → Pf B
     K   : Pf (A⇒B⇒A)
     S   : Pf ((A⇒B⇒C)⇒(A⇒B)⇒A⇒C)
     _∧_ : For → For → For
     _,_ : Pf A → Pf B → Pf (A∧B)
     fst : Pf (A∧B) → Pf A
     snd : Pf (A∧B) → Pf B
     Tm  : Set
     ∀   : (Tm → For) → For
     mk∀ : ((t:Tm)→Pf(A t)) → Pf (∀ A)
     un∀ : Pf (∀ A) → (t : Tm) → Pf (A t)
