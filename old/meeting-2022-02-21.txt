simply typed:

Con,Ty,Tm

_$_ : Tm Γ (A⇒B) → Tm Γ A → Tm Γ B
S
K

vz : Tm (Γ▹A) A
vs : Tm Γ A → Tm (Γ▹B) A
vs (t$a) = vs t $ vs a
vs K = K
vs S = S

ext : (vs t $ vz = vs u $ vz) → u = t

now we can define lam


t = t' → lam t = lam t'

Kβ : K$a$b=a
Kξ : lam(K$a$b=a) = lam a              I add this
Kξ' : lam(lam(K$a$b)) = lam(lam a)     ???

lam (lam t) = lam (lam t')

η : lam (vs t $ vz) = t
    S $ (lam (vs t)) $ (lam vz)
    S $ (K $ t) $ I = t



dependently typed:

Ty : Con → Set
Tm : (Γ:Con)→Ty Γ→Set
_⇒U : Ty → Ty
_$f_ : Tm (A ⇒U) → Tm A → Ty
U  : Ty Γ
...
Π : (A:Ty Γ)→Tm Γ ((A ⇒U) ⇒U)
A ⇒U = _$_ {}{? ⇒U} (Π A) ?
_$_ : {B : Tm (A ⇒U)} → Tm (Π A $f B) → (a : Tm A) → Tm (B $f a)
no interaction with Con


vs (F $f a) = vs F $f vs a

vs (A ⇒U) = vs A ⇒U
vs U = U
vs (Π A) = Π (vs A)

vs (t $ a) = vs t $ vs a
t : Tm Γ (Π A $f B)

vs : Ty Γ → Ty (Γ▹A)
vz : Tm (Γ▹A) (vs A)
vs : Tm Γ A → Tm (Γ▹B) (vs A)
vs (t$a) = vs t $ vs a
vs K = K
vs S = S

t, u : Tm Γ (Π A $f B)
vs t : Tm (Γ▹A) (Π (vs A) $f (vs B))
vz   : Tm (Γ▹A) (vs A)
ext : (vs t $ vz = vs u $ vz) → u = t


Π' : (A:Ty Γ)→Ty (Γ▹A)→Ty Γ
Π' A B := induction on B

then

lam : Tm (Γ▹A) B → Tm Γ (Π' A B)

TYPES abstract: Munchausen lies about combinatory type theory

- examples of Munchausen (Russell, Σ)

- idea of comb.log.: no variables, only functions. can we do this for dependent types?
  - it becomes technically very complicated. maybe it is a lie.
- explain our strategory towards combinatory t.t. list the rules of Artem
  - we can't simply use the equations as rewrite rules, we need laziness

- statemenet of extensionality (needs at least weakenings)
- maybe definition of Π and lam
