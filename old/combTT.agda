{-# OPTIONS --type-in-type #-}

open import Agda.Builtin.Unit
open import Agda.Builtin.Equality
open import Agda.Builtin.Nat renaming (Nat to ℕ)

module combTT where

-- standard library
_◾_ : ∀{i}{A : Set i}{a b c : A} → a ≡ b → b ≡ c → a ≡ c
refl ◾ refl = refl
transport : ∀{i j}{A : Set i}{x y : A}(P : A → Set j) → x ≡ y → P x → P y
transport _ refl a = a

module shallow where
  infixr 5 _⇒_
  infixl 6 _$_ _∙_

  -- simply typed combinator logic
  Ty  : Set
  Tm  : Ty → Set
  variable
    A B C D : Ty
    t u v w : Tm A
  _⇒_ : Ty → Ty → Ty
  _$_ : Tm (A ⇒ B) → Tm A → Tm B
  K   : Tm (A ⇒ B ⇒ A)
  Kβ  : K $ u $ v ≡ u
  S   : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  Sβ  : S $ u $ v $ w ≡ u $ w $ (v $ w)

  -- its standard model
  Ty  = Set
  Tm  = λ A → A
  _⇒_ = λ A B → A → B
  _$_ = λ t u → t u
  K   = λ x y → x
  Kβ  = refl
  S   = λ x y z → x z (y z)
  Sβ  = refl

  -- some combinators
  I : Tm (A ⇒ A)
  I {A} = S $ K $ K {_}{A}
  Iβ : I $ u ≡ u
  Iβ = refl
  B' : ∀{A B C} → Tm ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  B' = S $ (K $ S) $ K
  B'β : B' $ u $ v $ w ≡ u $ (v $ w)
  B'β = refl
  C' : Tm ((A ⇒ B ⇒ C) ⇒ B ⇒ A ⇒ C)
  C' = S $ (S $ (K $ B') $ S) $ (K $ K)
  C'β : C' $ u $ v $ w ≡ (u $ w) $ v
  C'β = refl
  {- conversion from lambda terms:
  λx.x   := I
  λx.u   := K $ u                 (if x∉u)
  λx.u$x := u
  λx.u$v := S $ (λx.u) $ (λx.v)   (if x∈u,x∈v)
  λx.u$v := B $ u $ (λx.v)        (if x∉u,x∈v)
  λx.u$v := C $ (λx.u) $ v        (if x∈u,x∉v)
  -}

  U   : Ty                                                   -- if we do it properly, we need universe levels: Ty : ℕ → Set, U : (i : ℕ) → Ty (suc i)
  Ty∅ : Ty ≡ Tm U  -- Russell                                -- also here: Ty i = Tm (U i)
  Π   : (A : Ty) → Tm ((A ⇒ U) ⇒ U)
  U   = Set
  Ty∅ = refl
  Π   = λ A B → (x : A) → B x

  -- the first occurrence of very dependent type
  Tm-selftype : Tm U → Set
  Tm-selftype = Tm

  ⇒∅  : ∀{A B} → A ⇒ B ≡ Π A $ (K $ B)
  ⇒∅  = refl
  _∙_ : ∀{A B} → Tm (Π A $ B) → (u : Tm A) → Tm (B $ u)
  _∙_ = λ t u → t u
  $∅  : ∀{A B}{t : Tm (A ⇒ B)}{u : Tm A} → t $ u ≡ t ∙ u
  $∅  = refl

  -- internal variant of _⇒_
  ar : Tm (U ⇒ U ⇒ U) 
  ar = λ A B → A → B
  ⇒∅' : ∀{A B} → A ⇒ B ≡ ar $ A $ B
  ⇒∅' = refl

  ar-selftype : Tm (ar $ U $ (ar $ U $ U))
  ar-selftype = ar

  -- a dependent K
  -- Kdep : (x : A) → B x → A  = Π (x:A) . (B x ⇒ A)
  -- Kdep : Tm (Π A $ (λx.ar$(B$x)$A)) = Tm (Π A $ (C' $ (λx.ar$(B$x)) $ A)) = Tm (Π A $ (C' $ (B' $ ar $ B) $ A))
  Kdep : {A : Ty}{B : Tm (A ⇒ U)} → Tm (Π A $ (C' $ (B' $ ar $ B) $ A))
  Kdep = λ x y → x
  K∅ : ∀{A B} → K {A}{B} ≡ Kdep {A}{K $ B}
  K∅ = refl
  Kdepβ₀ : {A : Ty}{B : Tm (A ⇒ U)}{u : Tm A}{v : Tm (B $ u)} → Kdep {A}{B} ∙ u $ v ≡ u
  Kdepβ₀ = refl
  Kdepβ₁ : {A : Ty}{B : Tm (A ⇒ U)}{u : Tm A}{v : Tm (B $ u)} → Kdep {A}{K $ (B $ u)} $ u $ v ≡ u
  Kdepβ₁ = refl
  Kdepβ₂ : {A : Ty}{B : Tm (A ⇒ U)}{u : Tm A}{v : Tm (B $ u)} → K {A}{B $ u} $ u $ v ≡ u
  Kdepβ₂ = refl

  -- a more general S
  -- Tm (((x:A) ⇒ B x ⇒ C) ⇒ ((x:A) ⇒ B x) ⇒ A ⇒ C)
  Sdep₀ : ∀{A}{B : Tm (A ⇒ U)}{C} → Tm ((Π A $ (C' $ (B' $ ar $ B) $ C)) ⇒ (Π A $ B) ⇒ A ⇒ C)
  Sdep₀ = λ x y z → x z (y z)
  Sdep₀β : {A : Ty}{B : Tm (A ⇒ U)}{C : Ty}{u : Tm (Π A $ (C' $ (B' $ ar $ B) $ C))}{v : Tm (Π A $ B)}{w : Tm A} →
    Sdep₀ $ u $ v $ w ≡ (u ∙ w) $ (v ∙ w)
  Sdep₀β = refl
  S∅₀ : S {A}{B}{C} ≡ Sdep₀
  S∅₀ = refl

  -- another more general S
  -- Tm (((x:A) ⇒ (y:B) ⇒ C x y) ⇒ (f : A ⇒ B) ⇒ (x:A) ⇒ C x (f x))
  -- C : Tm (A⇒B⇒U)
  -- ((x:A) ⇒ (y:B) ⇒ C x y) = Π A $ (λx. Π B $ (C $ x)) = Π A $ (B' $ (Π B) $ C)
  -- ((f : A ⇒ B) ⇒ (x:A) ⇒ C x (f x)) =
  -- Π (A ⇒ B) $ λf . Π A $ λ x . C $ x $ (f $ x) = 
  -- Π (A ⇒ B) $ λf . Π A $ (λ x . C $ x $ (f $ x)) =
  -- Π (A ⇒ B) $ (B' $ Π A $ (λ f . λ x . (C $ x) $ (f $ x))) =
  -- Π (A ⇒ B) $ (B' $ Π A $ (λ f . S $ (λ x . (C $ x)) $ (λ x .f $ x))) =
  -- Π (A ⇒ B) $ (B' $ Π A $ (λ f . S $ C $ f)) =
  -- Π (A ⇒ B) $ (B' $ Π A $ (S $ C))
  Sdep₁ : {C : Tm (A ⇒ B ⇒ U)} → Tm ((Π A $ (B' $ (Π B) $ C)) ⇒ (Π (A ⇒ B) $ (B' $ Π A $ (S $ C))))
  Sdep₁ = λ x y z → x z (y z)
  Sdep₁β : Sdep₀ $ u $ v $ w ≡ (u $ w) $ (v $ w) -- TODO: shouldn't one of the $s be ∙? add the implicit args!
  Sdep₁β = refl
  S∅₁ : S {A}{B}{C} ≡ Sdep₁
  S∅₁ = refl

  -- π : Tm ((A:U)→(A→U)→U)
  -- π : Tm (Π U $ (λA.ar$(ar$A$U)$U))
  --   = Tm (Π U $ (λA.ar$(ar$A$U)$U))
  --   = Tm (Π U $ (C' $ (λA.ar$(ar$A$U)) $ U))
  --   = Tm (Π U $ (C' $ (B' $ ar $ (λA.ar$A$U)) $ U))
  --   = Tm (Π U $ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U))
  π    : Tm (Π U $ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U))
  --                S $ (K $ (C' $ ar $ U)) $ (C' $ ar $ U)
  π    = λ A B → (x : A) → B x
  
  Π∅ : {A : Ty}{B : Tm (A ⇒ U)} → Π A $ B ≡ π ∙ A $ B
  Π∅ = refl

  π-selftype : Tm (π ∙ U ∙ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U))
  π-selftype = π

  -- dependent B'
  -- nondep: Tm ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  -- ((y:B)→C y) → (f:A→B) → (x : A) → C (f x)
  --               (f:A→B) → (x : A) → C (f x) =
  --               Π (A⇒B) $ λ f . Π A $ λ x . C$(f$x) =
  --               Π (A⇒B) $ λ f . Π A $ (B'$C$f) =
  --               Π (A⇒B) $ (B' $ (Π A) $ (B'$C))
  Bdep : {A B : Ty}{C : Tm (B ⇒ U)} → Tm ((Π B $ C) ⇒ (Π (A ⇒ B) $ (B' $ Π A $ (B' $ C))))
  Bdep = λ f g x → f (g x)
  Bdepβ : {A B : Ty}{C : Tm (B ⇒ U)}{u : Tm (Π B $ C)}{v : Tm (A ⇒ B)}{w : Tm A} →
          Bdep $ u ∙ v ∙ w ≡ u ∙ (v $ w)
  Bdepβ = refl
  B∅ : B' {A}{B}{C} ≡ Bdep
  B∅ = refl

  -- dependent S
  -- S    : ∀{A B C : Ty} → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  -- Sdep : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm ((x:A)→B x→U)} →
  --        {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))} →
  --        ((x : A)(y : B x) → C x y)       ⇒ ((f : (x : A) → B x) → (x : A) → C x (f x))
  --        (Π A $ (λ x .π∙(B$x) $ (C ∙ x))) ⇒ (Π (Π A $ B) $ λ f . (x : A) → C x (f x))
  --        (Π A $ (Sdep₀ $ (Bdep$π∙B) $ C)) ⇒ (Π (Π A $ B) $ λ f . π ∙ A $ (λx.C∙x$(f∙x)))
  --                                         ⇒ (Π (Π A $ B) $ λ f . π ∙ A $ (Sdep₀ $ C $ f))
  --                                         ⇒ (Π (Π A $ B) $ (B' $ (π ∙ A) $ (Sdep₀ $ C)))
  Sdep : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))} →
         Tm ((Π A $ (Sdep₀ $ (Bdep $ π ∙ B) $ C)) ⇒ (Π (Π A $ B) $ (B' $ (π ∙ A) $ (Sdep₀ $ C))))
         -- Tm ((Π A $ (Sdep₀ {A}{C' {A}{U}{U} $ (B' {A}{U}{U ⇒ U} $ ar $ B) $ U}{U} $ (Bdep {A}{U}{C' {U}{U}{U} $ (B' {U}{U}{U ⇒ U} $ ar $ (C' {U}{U}{U} $ ar $ U)) $ U} $ π ∙ B) $ C)) ⇒ (Π (Π A $ B) $ (B' {Π A $ B}{A ⇒ U}{U} $ (_∙_ π A) $ (Sdep₀ {A}{B}{U} $ C))))
  Sdep = λ f g x → f x (g x)
  Sdepβ : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))}
          {f : Tm (Π A $ (Sdep₀ $ (Bdep $ π ∙ B) $ C))}{g : Tm (Π A $ B)}{a : Tm A} →
          Sdep $ f ∙ g ∙ a ≡ (f ∙ a) ∙ (g ∙ a)
  Sdepβ = refl
  S∅ : S {A}{B}{C} ≡ Sdep
  S∅ = refl

  -- TODO: give internal versions of K(dep), S(dep)

  -- TODO: can we give a Bdep' {A}{B}{C} which if B = U and C = (C' $ (B' $ ar $ (C' $ ar $ U)) $ U), then
  -- Bdep' $ π = Bdep$π ?

  -- a specific and differently typed version of Bdep:
  Bdep$π  : {A : Ty} → Tm (Π (A ⇒ U) $ (B' $ Π A $ (C' $ (B' $ C' $ (B' $ (B' $ ar) $ (C' $ (B' $ C' $ (B' $ ar)) $ U))) $ U)))
  Bdep$π = Bdep $ π
  Bdep$πβ : {A : Ty}{B : Tm (A ⇒ U)}{a : Tm A} → Bdep$π ∙ B ∙ a ≡ π ∙ (B $ a)
  Bdep$πβ = refl
  -- another version of S (see also combTTdeep.agda):
  Sdep' : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))} →
    Tm ((Π A $ (Sdep₀ {A}{C' {A}{U}{U} $ (B' {A}{U}{U ⇒ U} $ ar $ B) $ U}{U} $ (Bdep$π ∙ B) $ C)) ⇒
        (Π (Π A $ B) $ (B' {Π A $ B}{A ⇒ U}{U} $ (_∙_ π A) $ (Sdep₀ {A}{B}{U} $ C))))
  Sdep' = λ f g x → f x (g x)
  Sdep'β : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))}{f : Tm (Π A $ (Sdep₀ $ (Bdep$π ∙ B) $ C))}{g : Tm (Π A $ B)}{a : Tm A} →
    Sdep' $ f ∙ g ∙ a ≡ (f ∙ a) ∙ (g ∙ a)
  Sdep'β = refl

  -- (Π A $ (C' $ (B' $ ar $ B) $ U)) = (x:A) → B x → U
  -- C : Tm U  |-->   Kdep ∙ (K $ C) : Tm (Π A $ (C' $ (B' $ ar $ B) $ U)) = Tm (Π A $ (λx.B x ⇒ U))
  --                  Kdep {A}{B}    : Tm (Π A $ (C' $ (B' $ ar $ B) $ A)) = Tm (Π A $ (λx.B x ⇒ A))
  -- λx.λy.C    =  K$K$C

  -- double constant function dependent in the middle (it seems that this is not derivable from Kdep) TODO: derive this!
  --                                    A ⇒ (x:B) → C x ⇒ A
  KKdep : {A B : Ty}{C : Tm (B ⇒ U)} → Tm (A ⇒ Π B $ (C' $ (B' $ ar $ C) $ A))
  KKdep = λ x y z → x
  -- λ(x:A).λ(y:B).λ(z:C y).x = 
  -- λ(x:A).λ(y:B). K{A}{C y} $ x =
  --                \___________/
  --                 : C y ⇒ A
  -- λ(x:A).Kdep{C y ⇒ A}{B} ∙ (K{A}{C y} $ x) =
  --        \______________/
  --        : (C y ⇒ A) ⇒ (y:B) → (C y ⇒ A)   <- makes no sense, there is no y
  --        \___________________________________/
  --          : 

  KK : Tm (A ⇒ B ⇒ C ⇒ A)
  KK = B' $ K $ K
  -- K {A}{C} : Tm (A ⇒ C ⇒ A)
  -- λx.λy.(λz.x) =
  -- λx.(λy.(K {A}{C} $ x)) = 
  -- λx.(K {C⇒A}{B} $ (K {A}{C} $ x))
  -- λx.(K $ (K $ x)) =
  -- B' $ K $ K

  -- TODO: do this deeply
  Sdep₀∅ : ∀{A : Ty}{B : Tm (A ⇒ U)}{C : Ty} → Sdep₀ {A}{B}{C} ≡ Sdep {A}{B}{KKdep $ C}
  Sdep₀∅ = refl

module wrappedShallow where

  infixr 5 _⇒_
  infixl 6 _$_ _∙_

  record Wrap (n : ℕ)(A : Set) : Set where
    constructor mk
    field ∣_∣ : A
  open Wrap
  
  Ty          : Set
  Tm          : Ty → Set
  _⇒_         : Ty → Ty → Ty
  _$_         : ∀{A B} → Tm (A ⇒ B) → Tm A → Tm B
  K           : ∀{A B} → Tm (A ⇒ B ⇒ A)
  Kβ          : ∀{A B}{u : Tm A}{v : Tm B} → K $ u $ v ≡ u
  S           : ∀{A B C} → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  Sβ          : ∀{A B C}{u : Tm (A ⇒ B ⇒ C)}{v : Tm (A ⇒ B)}{w : Tm A} → S $ u $ v $ w ≡ u $ w $ (v $ w)
  U           : Ty
  Rus         : Ty ≡ Tm U
  Π           : (A : Ty) → Tm ((A ⇒ U) ⇒ U)
  Ty          = Wrap 0 Set
  Tm A        = Wrap 0 ∣ A ∣
  ∣ A ⇒ B ∣   = Wrap 1 (∣ A ∣ → ∣ B ∣)
  ∣ t $ u ∣   = ∣ ∣ t ∣ ∣ ∣ u ∣ 
  ∣ ∣ K ∣ ∣   = λ x → mk λ _ → x
  Kβ          = refl
  ∣ ∣ S ∣ ∣   = λ x → mk λ y → mk λ z → ∣ ∣ x ∣ z ∣ (∣ y ∣ z)
  Sβ          = refl
  ∣ U ∣       = Set
  Rus         = refl
  ∣ ∣ Π A ∣ ∣ = λ B → Wrap 1 ((x : ∣ A ∣) → ∣ B ∣ x)
  _∙_         : ∀{A B} → Tm (Π A $ B) → (u : Tm A) → Tm (B $ u)
  ⇒Π          : ∀{A B} → A ⇒ B ≡ Π A $ (K $ B)
  $∙          : ∀{A B}{t : Tm (A ⇒ B)}{u : Tm A} → t $ u ≡ t ∙ u
  ar          : Tm (U ⇒ U ⇒ U) 
  ∣ t ∙ u ∣   = ∣ ∣ t ∣ ∣ ∣ u ∣
  ⇒Π          = refl
  $∙          = refl
  ∣ ∣ ar ∣ ∣  = λ A → mk λ B → Wrap 1 (A → B)
  ⇒ar         : ∀{A B} → A ⇒ B ≡ ar $ A $ B
  --                Π U $ λ A → (A ⇒ U) ⇒ U
  π           : Tm (Π U $ (S $ (S $ (K $ ar) $ (S $ ar $ (K $ U))) $ (K $ U)))
  Ππ          : ∀{A B} → Π A $ B ≡ π ∙ A ∙ B
  arπ         : ∀{A B} → ar $ A $ B ≡ π ∙ A ∙ (K ∙ B)
  ⇒ar         = refl
  ∣ ∣ π ∣ ∣   = λ A → mk λ B → Wrap 1 ((x : A) → ∣ B ∣ x)
  Ππ          = refl
  arπ         = refl
  --            ⟦λA.λB.A⇒(B⇒A)⟧ = S⟦λA B.ar A⟧⟦λA B.ar B A⟧ = S(S⟦⟧⟦⟧)()
  aba         : Tm (U ⇒ U ⇒ U)
  aba         = {!!}
  --                        (λ A → π ∙ U ∙ (λ B → A ⇒ (B ⇒ A))) :
  k           : Tm (π ∙ U ∙ (S $ (K $ (π ∙ U)) $ {!!}))
  k           = {!!}

-- deepRewriting moved to combTTdeep.agda file

module deep where

  record STCL : Set where -- simply typed combinator logic
    field
      Ty  : Set
      Tm  : Ty → Set
      _⇒_ : Ty → Ty → Ty
      _$_ : ∀{A B} → Tm (A ⇒ B) → Tm A → Tm B
      K   : ∀{A B} → Tm (A ⇒ B ⇒ A)
      Kβ  : ∀{A B}{u : Tm A}{v : Tm B} → K $ u $ v ≡ u
      S   : ∀{A B C} → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
      Sβ  : ∀{A B C}{u : Tm (A ⇒ B ⇒ C)}{v : Tm (A ⇒ B)}{w : Tm A} → S $ u $ v $ w ≡ u $ w $ (v $ w)
    infixr 5 _⇒_
    infixl 6 _$_

    I : ∀{A} → Tm (A ⇒ A)
    I {A} = S $ K $ K {_}{A}
    Iβ : ∀{A}{u : Tm A} → I $ u ≡ u
    Iβ = Sβ ◾ Kβ
