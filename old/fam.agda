{-# OPTIONS --type-in-type --rewriting --confluence #-}
module _ where

open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}

infixr 5 _⇒_
infixl 10 _$_
infixl 10 _$f_

postulate
  Ty : Set
  Tm : Ty → Set

variable X Y Z : Ty

postulate
  U : Ty

  Tm-U : Tm U ≡ Ty
  {-# REWRITE Tm-U #-}

  Fam : Ty → Ty   -- (X → U)
  _$f_ : Tm (Fam X) → Tm X → Ty

  Kf : (Y : Ty) → Tm (Fam X)  -- Kf Y ≡ λ a → Y
  Kf$ : ∀ {a : Tm X} → _$f_ {X} (Kf Y) a ≡ Y
  {-# REWRITE Kf$ #-}

  Pi : (X : Ty) → Tm (Fam (Fam X))
  _$_ : {X : Ty}{Y : Tm (Fam X)}
      → Tm (Pi X $f Y) → (a : Tm X) → Tm (Y $f a)

_⇒_ : (X Y : Ty) → Ty
X ⇒ Y = Pi X $f (Kf Y)

postulate -- Dependent K

  Yx⇒Z : ∀ X (Y : Tm (Fam X)) → (Z : Ty) → Tm (Fam X)
  Yx⇒Z$ : ∀ X Y Z {x : Tm X} → Yx⇒Z X Y Z $f x ≡ Y $f x ⇒ Z
  {-# REWRITE Yx⇒Z$ #-}

  Kd : {Y : Tm (Fam X)} → Tm (Pi X $f Yx⇒Z X Y X)
  Kd$ : ∀ {Y : Tm (Fam X)}{x : Tm X}{y : Tm (Y $f x)}
      → Kd {X = X}{Y = Y} $ x $ y ≡ x
  {-# REWRITE Kd$ #-}

postulate -- Dependent S

  Yx⇒U : ∀ X (Y : Tm (Fam X)) → Tm (Fam X)
  Yx⇒U$ : ∀ X Y {x : Tm X} → Yx⇒U X Y $f x ≡ Fam (Y $f x)
  {-# REWRITE Yx⇒U$ #-}

  Π[Yx][Zx]  : ∀ X (Y : Tm (Fam X)) → (Z : Tm (Pi X $f Yx⇒U X Y)) → Tm (Fam X)
  Π[Yx][Zx]$ : ∀ X Y Z {x : Tm X} → Π[Yx][Zx] X Y Z $f x ≡ Pi (Y $f x) $f (Z $ x)
  {-# REWRITE Π[Yx][Zx]$ #-}

  Zx[gx] : ∀ X (Y : Tm (Fam X)) (Z : Tm (Pi X $f Yx⇒U X Y))
         → Tm (Pi X $f Y) → Tm (Fam X)
  Zx[gx]$ : ∀ X Y Z g {x} → Zx[gx] X Y Z g $f x ≡ Z $ x $f (g $ x)
  {-# REWRITE Zx[gx]$ #-}

  ΠX[Zx[gx]] : ∀ X (Y : Tm (Fam X)) (Z : Tm (Pi X $f Yx⇒U X Y)) 
             → Tm (Fam (Pi X $f Y))
  ΠX[Zx[gx]]$ : ∀ X Y Z g → ΠX[Zx[gx]] X Y Z $f g ≡ Pi X $f (Zx[gx] X Y Z g)
  {-# REWRITE ΠX[Zx[gx]]$ #-}

  Sd : {Y : Tm (Fam X)}{Z : Tm (Pi X $f Yx⇒U X Y)} 
     → Tm (Pi X $f Π[Yx][Zx] X Y Z 
           ⇒ (Pi (Pi X $f Y) $f (ΠX[Zx[gx]] X Y Z)))
  Sd$ : {Y : Tm (Fam X)}{Z : Tm (Pi X $f Yx⇒U X Y)}
      → {f : Tm (Pi X $f Π[Yx][Zx] X Y Z) }
      → {g : Tm (Pi X $f Y)}
      → {x : Tm X}
      → Sd $ f $ g $ x ≡ f $ x $ (g $ x)
  {-# REWRITE Sd$ #-}


postulate -- Simply-typed K defined via Kd

  KYx⇒Z$ : ∀ X Y Z → Yx⇒Z X (Kf Y) Z ≡ Kf (Y ⇒ Z)
  {-# REWRITE KYx⇒Z$ #-}

K : Tm (X ⇒ Y ⇒ X)
K {X}{Y} = Kd {X}{Kf Y}

postulate -- Simply-typed S defined via Sd

  -- Eta rules for constant families
  KYx⇒U$ : ∀ X Y → Yx⇒U X (Kf Y) ≡ Kf (Fam Y)
  {-# REWRITE KYx⇒U$ #-}

  KYx⇒Zx$ : ∀ X (Y : Ty) (Z : Tm (Fam Y)) 
           → Π[Yx][Zx] X (Kf Y) (K $ Z) ≡ Kf (Pi Y $f Z)
  {-# REWRITE KYx⇒Zx$ #-}

  KXSisch$ : ∀ X Y Z → ΠX[Zx[gx]] X (Kf Y) (K $ (Kf Z)) ≡ Kf (Pi X $f (Kf Z))
  {-# REWRITE KXSisch$ #-}

S : Tm ((X ⇒ Y ⇒ Z) ⇒ (X ⇒ Y) ⇒ X ⇒ Z)
S {X}{Y}{Z} = Sd {X}{Kf Y}{K $ (Kf Z)}

  -- Enabling this rule does not lead to looping, but it kills
  -- the inference, of _$_ arguments.
  -- 
  -- PiU : Pi X $f Kf U ≡ Fam X
  -- {-# REWRITE PiU #-}

