-- Based on Conor's Stack Overflow response:
-- https://stackoverflow.com/questions/11406786/what-is-the-combinatory-logic-equivalent-of-intuitionistic-type-theory
-- with a smarter abstraction algorithm.

open import Data.Maybe
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality hiding ([_])
open import Data.Empty
open import Function using (_$′_)


infixl 10 _$_

data Tm (A : Set) : Set where
  Var : A → Tm A
  Lam : {- Tm A → -} Tm (Maybe A) → Tm A
  _$_ : Tm A → Tm A → Tm A 
  Pi : Tm A → Tm A → Tm A
       -- the second arg of Pi is a function computing U
  U : Tm A

data SKUP : Set where
  S K U P : SKUP

data Unty (A : Set) : Set where
  C : SKUP → Unty A
  _$_ : Unty A → Unty A → Unty A
  V : A → Unty A

bra : ∀ {A} → Unty (Maybe A) -> Unty A

tm : ∀ {A} → Tm A -> Unty A
tm (Var x) = V x
tm (Lam e) = bra (tm e)
tm (e $ e₁) = tm e $ tm e₁
tm (Pi e e₁) = C P $ tm e $ tm e₁
tm U = C U

data freevar (A : Set) : Unty (Maybe A) → Set where
  v : freevar A (V nothing)
  l$ : ∀ {e e₁} → freevar A e → freevar A (e $ e₁)
  r$ : ∀ {e e₁} → freevar A e₁ → freevar A (e $ e₁)

isfreevar : ∀ {A} → (a : Unty (Maybe A)) → Dec (freevar A a)
isfreevar (C x) = no (λ ())
isfreevar (a $ a₁) with isfreevar a | isfreevar a₁
... | yes p | q = yes (l$ p)
... | no p  | yes q = yes (r$ q)
... | no p  | no q = no λ { (l$ x) → p x ; (r$ x) → q x }
isfreevar (V (just x)) = no λ ()
isfreevar (V nothing) = yes v


conv : ∀ {A} → (a : Unty (Maybe A)) → ¬ freevar A a → Unty A
conv (C x) p = C x
conv (a $ a₁) p = conv a (λ z → p (l$ z)) $ conv a₁ λ z → p (r$ z)
conv (V (just x)) p = V x
conv (V nothing) p = ⊥-elim (p v)

bra (V (just x)) = C K $ V x
bra (V nothing) = C S $ C K $ C K
bra (C x) = C K $ C x
bra (x $ x₁) with isfreevar x | isfreevar x₁
bra (x $ x₁) | no p | no q = C K $ (conv x p $ conv x₁ q)
bra (x $ (V nothing)) | no p | yes q = conv x p
bra (x $ x₁) | p | q = C S $ (bra x) $ (bra x₁)


pattern v₀ = Var nothing
pattern v₁ = Var (just nothing)
pattern v₂ = Var (just (just nothing))
pattern v₃ = Var (just (just (just nothing)))
pattern v₄ = Var (just (just (just (just nothing))))

-- Π : (A : Set) (B : A → Set) → Set
pi : Tm ⊥
pi = Pi            {-A-} U 
      $′ Lam $′ Pi {-B-} (Pi v₀ $′ Lam $′ U)
      $′ Lam U

-- K : (A : Set)(B : A → Set) (a : A) (B a) A
k : Tm ⊥
k = Pi            {-A-} U 
     $′ Lam $′ Pi {-B-} (Pi v₀ (Lam U))
     $′ Lam $′ Pi {-a-} v₁
     $′ Lam $′ Pi {-B a-} (v₁ $ v₀)
     $′ Lam $′ v₃

-- S : (A : Set)(B : A → Set)(C : (a : A) (b : B a) Set)
--   → (f : (a : A) → (b : B a) → C a b)
--   → (g : (a : A) → B a)
--   → (a : A) → C a (g a)
s : Tm ⊥
s = Pi            {-A-} U
     $′ Lam $′ Pi {-B-} (Pi v₀ $′ Lam U)
     $′ Lam $′ Pi {-C-} (Pi v₁ $′ Lam $′ Pi (v₁ $ v₀) $′ Lam U)
     $′ Lam $′ Pi {-f-} (Pi v₂ $′ Lam $′ Pi (v₂ $ v₀) $′ Lam (v₂ $ v₁ $ v₀))
     $′ Lam $′ Pi {-g-} (Pi v₃ $′ Lam (v₃ $ v₀))
     $′ Lam $′ Pi {-a-} v₄
     $′ Lam $′ v₃ $ v₀ $ (v₁ $ v₀)

-- Print 
open import Data.String


tos : ∀ {A} → (A → String) → Unty A → String
tos vf (C S) = "S"
tos vf (C K) = "K"
tos vf (C U) = "U"
tos vf (C P) = "Π"
tos vf (V x) = vf x
--tos vf (a@(C x) $ b@(C y)) = tos vf a ++ tos vf b
--tos vf (a@(V x) $ b@(V y)) = tos vf a ++ tos vf b
--tos vf (a@(C x) $ b@(V y)) = tos vf a ++ tos vf b
--tos vf (a@(V x) $ b@(C y)) = tos vf a ++ tos vf b
tos vf (a $ b@(C _)) = tos vf a ++ tos vf b
tos vf (a $ b) = tos vf a ++ "(" ++ tos vf b ++ ")"
-- "((ΠU)((S((S(KΠ))((SΠ)(K(KU)))))((S((S(KS))((S(KK))Π)))((S(K(S((S(KS))(S(KΠ))))))((S(KK))((S(KK))K))))))
-- "Π(U)(S(S(K(Π))(S(Π)(K(K(U)))))(S(S(K(S))(S(K(K))(Π)))(S(K(S(S(K(S))(S(K(Π))))))(S(K(K))(S(K(K))(K))))))"
-- "ΠU(S(S(KΠ)(SΠ(K(KU))))(S(S(KS)(S(KK)Π))(S(K(S(S(KS)(S(KΠ)))))(S(KK)(S(KK)K)))))"




module Tests where
  -- λ x → x
  ex₁ : Tm ⊥
  ex₁ = Lam v₀

  tm₁ = tm ex₁
  _ : tm₁ ≡ C S $ C K $ C K
  _ = refl

  --_ : tos (λ ()) tm₁ ≡ "(SKK)"
  --_ = refl


  -- W combinator λ f x → f x x
  ex₂ : Tm ⊥
  ex₂ = Lam $′ Lam $′ v₁ $ v₀ $ v₀

  tm₂ = tm ex₂
  _ : tm₂ ≡ C S $ C S $ (C K $ (C S $ C K $ C K))
  _ = refl

  --_ : tos (λ ()) tm₂ ≡ "(SS(K(SKK)))"
  --_ = refl


-- Now, the representation of our combinators:

pis = tos (λ ()) (tm pi)
-- ΠU((S((SKΠ)(SΠ(KKU))))(KKU))


ks  = tos (λ ()) (tm k)
-- ΠU((S((SKΠ)(SΠ(KKU))))((S((SKS)((SKK)Π)))((S(K(S((SKS)(SKΠ)))))((SKK)((SKK)K)))))

ss  = tos (λ ()) (tm s)
-- ΠU((S((SKΠ)(SΠ(KKU))))
--    ((S((SKS)((S(K(SKΠ)))((S((SKS)((SKK)Π)))(K((S((SKS)(SKΠ)))(K(KKU))))))))
--     ((S((SKS)((S(K(SKS)))((S(K(S(K(SKΠ)))))((S((SKS)((SKK)((SKS)((SKK)Π)))))(K((SKS)(SKΠ))))))))
--      ((S(K(S(K(SKK)))))((S((SKS)((S(K(SKS)))((S(K(SKK)))((S(K(SKΠ)))Π)))))((SKK)((S((SKS)((SKK)((SKS)((SKK)Π)))))KS)))))))





