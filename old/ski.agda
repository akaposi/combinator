module ski where

infixl 5 _$_ _∙_
infix 3 _≡_

-- stdlib
data _≡_ {A : Set}(a : A) : A → Set where
  refl : a ≡ a

data Tm : Set where
  S K : Tm
  _$_ : Tm → Tm → Tm

variable
  a b c d f g n : Tm

-- normaliser from Chapman's thesis (3.4) https://jmchapman.io/papers/thesis.pdf#page=44
{-# TERMINATING #-}
_∙_ : Tm → Tm → Tm
K           ∙ a = K $ a
(K $ a)     ∙ b = a
S           ∙ a = S $ a
(S $ a)     ∙ b = S $ a $ b
(S $ a $ b) ∙ c = (a ∙ c) ∙ (b ∙ c)

norm : Tm → Tm
norm K       = K
norm S       = S
norm (a $ b) = norm a ∙ norm b


module nonterm where

  I = S $ K $ K

  D : Tm
  D = S $ I $ I

  nonterm1 : {a : Tm} → {!norm (D $ D)!} -- C-c C-n doesn't normalise
  nonterm2 : {a : Tm} → {!D ∙ D!} -- C-c C-n doesn't normalise
  nonterm3 : {a : Tm} → {!norm (K $ a $ (D $ D))!} -- C-c C-n normalises
  nonterm4 : {a : Tm} → {!norm (K $ a) ∙ norm (D $ D)!} -- C-c C-n normalises. Agda is lazy!
  nonterm5 : {a : Tm} → {!K $ a ∙ (D ∙ D)!} -- C-c C-n normalises
  
  KaD : norm (K ∙ a ∙ (D $ D)) ≡ norm a
  KaD = refl

module test where
  -- examples from https://akaposi.github.io/pres_kutatok_ejszakaja.pdf
  
  -- the I combinator
  I : Tm
  I = S $ K $ K 
  testI : norm (I $ a) ≡ norm a
  testI = refl

  -- különböző, de ugyanúgy működő termek
  extensionalityExample : norm (K $ a $ b) ≡ norm (S $ K $ I $ K $ a $ b)
  extensionalityExample = refl

  U : Tm
  U = K $ I
  testU : norm (U $ a $ b) ≡ norm b
  testU = refl

  Mid : Tm
  Mid = K $ K
  testMid : norm (Mid $ a $ b $ c) ≡ norm b
  testMid = refl

  D : Tm
  D = S $ I $ I
  testD : norm (D $ a) ≡ norm (a $ a)
  testD = refl

  -- the B combinator (function composition)
  B : Tm
  B = S $ (K $ S) $ K
  testB : norm (B $ a $ b $ c) ≡ norm (a $ (b $ c))
  testB = refl

  -- the C combinator
  C : Tm
  C = S $ (S $ (K $ B) $ S) $ (K $ K)
  testC : norm (C $ a $ b $ c) ≡ norm (a $ c $ b)
  testC = refl

  F : Tm
  F = S $ (K $ (S $ I)) $ (S $ (K $ K) $ I)
  testF : norm (F $ a $ b) ≡ norm (b $ a)
  testF = refl

  -- zero
  O : Tm
  O = U
  testO : norm (O $ a $ b) ≡ norm b
  testO = refl

  -- successor
  M : Tm
  M = S $ B
  SUC = M
  testM : norm (M $ a $ b $ c) ≡ norm (b $ (a $ b $ c))
  testM = refl

  -- some number
  N1 N2 N3 N4 N5 : Tm
  N1 = M $ O
  N2 = M $ N1
  N3 = M $ N2
  N4 = M $ N3
  N5 = M $ N4

  -- addition
  Ö : Tm
  Ö = B $ S $ (B $ B)
  testÖ : norm (Ö $ a $ b $ c $ d) ≡ norm (a $ c $ (b $ c $ d))
  testÖ = refl
  test0+0=0' : norm (Ö $ O $ O) ≡ norm O -- there is no η! (and we have higher order representations)
  test0+0=0' = {!!}
  test0+0=0 : norm (Ö $ O $ O $ a $ b) ≡ norm (O $ a $ b)
  test0+0=0 = refl
  test1+2=3 : norm (Ö $ N1 $ N2 $ a $ b) ≡ norm (N3 $ a $ b)
  test1+2=3 = refl
  test3+3=5+1 : norm (Ö $ N3 $ N3 $ a $ b) ≡ norm (Ö $ N5 $ N1 $ a $ b)
  test3+3=5+1 = refl
  test1+5=5+1 : norm (Ö $ N1 $ N5 $ a $ b) ≡ norm (Ö $ N5 $ N1 $ a $ b)
  test1+5=5+1 = refl

  -- multiplication = B
  MUL = B
  test2*2=4 : norm (B $ N2 $ N2 $ a $ b) ≡ norm (N4 $ a $ b)
  test2*2=4 = refl
  test3*2=5+1 : norm (B $ N3 $ N2 $ a $ b) ≡ norm (Ö $ N5 $ N1 $ a $ b)
  test3*2=5+1 = refl
  test5*2=5+5 : norm (B $ N5 $ N2 $ a $ b) ≡ norm (Ö $ N5 $ N5 $ a $ b)
  test5*2=5+5 = refl

  -- recursion on natural numbers (order of args: number, suc case, zero case)
  -- rec $ (M $ (M $ (M $ O))) $ a $ b = a $ (a $ (a $ b))
  REC : Tm
  REC = I
  testREC1 : norm (REC $ O $ a $ b) ≡ norm b
  testREC1 = refl
  testREC2 : norm (REC $ (M $ c) $ a $ b) ≡ norm (a $ (REC $ c $ a $ b))
  testREC2 = refl

  -- BC(BC)abcd = C(BCa)bcd = BCacbd = C(ac)bd = acdb
  TOEND1 : Tm
  TOEND1 = B $ C $ (B $ C)
  testTOEND1 : norm (TOEND1 $ a $ b $ c $ d) ≡ norm (a $ c $ d $ b)
  testTOEND1 = refl

  -- B(BC)Cabcd = BC(Ca)bcd = C(Cab)cd = Cabdc = adbc
  TOEND2 : Tm
  TOEND2 = B $ (B $ C) $ C
  testTOEND2 : norm (TOEND2 $ a $ b $ c $ d) ≡ norm (a $ d $ b $ c)
  testTOEND2 = refl

  -- recursion with order of args: suc case, zero case, number
  REC' : Tm
  REC' = TOEND2 $ REC
  testREC'1 : norm (REC' $ a $ b $ O) ≡ norm b
  testREC'1 = refl
  testREC'2 : norm (REC' $ a $ b $ (M $ c)) ≡ norm (a $ (REC' $ a $ b $ c))
  testREC'2 = refl

  -- osszeg tipus

  -- fa = K(fa)g = K(Faf)g = BK(Fa)fg = B(BK)Fafg = INL a f g 
  INL : Tm
  INL = B $ (B $ K) $ F
  testFST : norm (INL $ a $ f $ g) ≡ norm (f $ a)
  testFST = refl

  --  ga = Ufga = C(Uf)ag = BCUfag = C(BCU)afg = INR a f g
  INR : Tm
  INR = C $ (B $ C $ U)
  testSND : norm (INR $ a $ f $ g) ≡ norm (g $ a)
  testSND = refl

  CASE : Tm
  CASE = I
  testCASE1 : norm (CASE $ (INL $ a) $ b $ c) ≡ norm (b $ a)
  testCASE1 = refl
  testCASE2 : norm (CASE $ (INR $ a) $ b $ c) ≡ norm (c $ a)
  testCASE2 = refl

  CASE' : Tm
  CASE' = TOEND2 $ CASE
  testCASE'1 : norm (CASE' $ b $ c $ (INL $ a)) ≡ norm (b $ a)
  testCASE'1 = refl
  testCASE'2 : norm (CASE' $ b $ c $ (INR $ a)) ≡ norm (c $ a)
  testCASE'2 = refl

  -- predecessor
  -- pred = λ n → rec (ι₂ zero) (λ w → case w (λ n → ι₁ (suc n)) (λ _ → ι₁ zero)) n
  PRED : Tm
  PRED = REC' $ (CASE' $ (B $ INL $ SUC) $ (K $ (INL $ O))) $ (INR $ O)
  testPRED1 : norm (PRED $ N5) ≡ norm (INL $ N4)
  testPRED1 = refl
  testPRED2 : norm (PRED $ N2) ≡ norm (INL $ N1)
  testPRED2 = refl
  testPRED3 : norm (PRED $ O) ≡ norm (INR $ O)
  testPRED3 = refl
  testPRED4 : norm (PRED $ (M $ (M $ N5))) ≡ norm (INL $ (M $ N5))
  testPRED4 = refl
  testPRED5 : norm (PRED $ (Ö $ N1 $ N5)) ≡ norm (INL $ N5)
  testPRED5 = refl
  -- testPRED6 : norm (PRED $ (Ö $ N2 $ N5) $ a $ b $ c $ d) ≡ norm (INL $ (Ö $ N1 $ N5) $ a $ b $ c $ d) -- this is an issue: without η these are not equal
  -- testPRED6 = {!!}

  -- this PRED' returns 0 to 0
  -- PRED' = (CASE' $ I $ I) ∘ PRED
  PRED' : Tm
  PRED' = B $ (CASE' $ I $ I) $ PRED
  testPRED'1 : norm (PRED' $ N5) ≡ norm N4
  testPRED'1 = refl
  testPRED'2 : norm (PRED' $ N2) ≡ norm N1
  testPRED'2 = refl
  testPRED'3 : norm (PRED' $ O $ a) ≡ norm (O $ a)
  testPRED'3 = refl
  testPRED'4 : norm (PRED' $ (M $ (M $ N5))) ≡ norm (M $ N5)
  testPRED'4 = refl
  testPRED'6 : norm (PRED' $ (Ö $ N2 $ N5) $ a $ b $ c $ d) ≡ norm ((Ö $ N1 $ N5) $ a $ b $ c $ d)
  testPRED'6 = refl

  -- Csörnyei pred
  P = C $ (C $ (C $ I $ (S $ (B $ C $ (C $ I)) $ (S $ B) $ (C $ I) $ (K $ I))) $ (C $ (C $ I $ (K $ I)) $ (K $ I))) $ K

  testP1 : norm (P $ N5 $ a $ b $ c $ d) ≡ norm (N4 $ a $ b $ c $ d)
  testP1 = {!!}
  testP2 : norm (P $ N1) ≡ norm O
  testP2 = refl
  testP3 : norm (P $ N2 $ a $ b $ c $ d) ≡ norm (N1 $ a $ b $ c $ d)
  testP3 = {!!}

  TRUE = K
  FALSE = K $ I
  ITE = I
  testITE1 : norm (ITE $ TRUE $ a $ b) ≡ norm a
  testITE1 = refl
  testITE2 : norm (ITE $ FALSE $ a $ b) ≡ norm b
  testITE2 = refl
  -- ITE is lazy
  testITE3 : norm (ITE $ TRUE $ a $ (D $ D)) ≡ norm a -- false branch is nonterminating
  testITE3 = refl  

  -- Csörnyei könyvből
  ISZERO = C $ (C $ I $ (TRUE $ FALSE)) $ TRUE
  testISZERO1 : norm (ISZERO $ O) ≡ norm TRUE
  testISZERO2 : norm (ISZERO $ N1) ≡ norm FALSE
  testISZERO3 : norm (ISZERO $ N2) ≡ norm FALSE
  testISZERO4 : norm (ISZERO $ N3) ≡ norm FALSE
  testISZERO1 = refl
  testISZERO2 = refl
  testISZERO3 = refl
  testISZERO4 = refl

  -- FAC' a kovetkezo rekurziv fuggvenyt kodolja el: F(N) := ITE (ISZERO N) 1 (N * F(PRED N))
  -- FAC' $ G $ N                                          = ITE (ISZERO N) 1 (N * G(PRED N))
  FAC' = B $ (S $ (C $ (ITE $ ISZERO) $ N1)) $ (B $ (S $ MUL) $ (C $ B $ PRED'))
  testFAC'1 : norm (FAC' $ g $ n) ≡ norm (ITE $ (ISZERO $ n) $ N1 $ (MUL $ n $ (g $ (PRED' $ n))))
  testFAC'1 = refl

  -- ez a módszer a nyelvek típusrendszere régi jegyzet 36-37. oldalán van leírva ( https://bitbucket.org/akaposi/tipusrendszerek/raw/HEAD/jegyzet.pdf ).
  -- FAC = (λX.FAC' (X X)) (λX.FAC' (X X)) = (B FAC' (λY.Y Y)) (B FAC' (λY.Y Y)) = (B FAC' D) (B FAC' D)
  FAC = D $ (B $ FAC' $ D)

  -- FAC $ O  = D $ (B $ FAC' $ D) $ O  = B $ FAC' $ D $ (B $ FAC' $ D) $ O  = FAC' $ (D $ (B $ FAC' $ D)) $ O  = ITE $ (ISZERO O ) $ N1 $ _ = N1
  -- FAC $ N1 = D $ (B $ FAC' $ D) $ N1 = B $ FAC' $ D $ (B $ FAC' $ D) $ N1 = FAC' $ (D $ (B $ FAC' $ D)) $ N1 = ITE $ (ISZERO N1) $ N1 $ (MUL $ N1 $ (FAC $ O )) = ITE $ (ISZERO 1) $ N1 $ (MUL $ N1 $ N1) = MUL $ N1 $ N1 = N1
  -- FAC $ N2 = D $ (B $ FAC' $ D) $ N2 = B $ FAC' $ D $ (B $ FAC' $ D) $ N2 = FAC' $ (D $ (B $ FAC' $ D)) $ N2 = ITE $ (ISZERO N2) $ N1 $ (MUL $ N2 $ (FAC $ N1)) = ITE $ (ISZERO 1) $ N1 $ (MUL $ N2 $ N1) = MUL $ N2 $ N1 = N2
  -- FAC $ N3 = D $ (B $ FAC' $ D) $ N3 = B $ FAC' $ D $ (B $ FAC' $ D) $ N3 = FAC' $ (D $ (B $ FAC' $ D)) $ N3 = ITE $ (ISZERO N3) $ N1 $ (MUL $ N3 $ (FAC $ N2)) = ITE $ (ISZERO 1) $ N1 $ (MUL $ N3 $ N2) = MUL $ N3 $ N2 = N6
  -- ...

  testFAC0 : norm (FAC $ O $ a $ b) ≡ norm (N1 $ a $ b)
  testFAC0 = refl
  testFAC1 : norm (FAC $ N1 $ a $ b) ≡ norm (N1 $ a $ b)
  testFAC1 = refl
  testFAC2 : norm (FAC $ N2 $ a $ b) ≡ norm (N2 $ a $ b)
  testFAC2 = refl
  testFAC3 : norm (FAC $ N3 $ a $ b) ≡ norm ((Ö $ N3 $ N3) $ a $ b)
  testFAC3 = refl
  testFAC4 : norm (FAC $ N4 $ a $ b) ≡ norm ((MUL $ N4 $ (Ö $ N3 $ N3)) $ a $ b)
  testFAC4 = refl

  -- λx.(f$x) = S$(K$f)$I =? f
  eta : norm (S $ (K $ f) $ I $ a) ≡ norm (f $ a)
  eta = refl

  -- eta rules of Artem
  thm-skk  : norm (S $ (K $ a) $ (K $ b) $ c) ≡ norm (K $ (a $ b) $ c)
  thm-sksk : norm (S $ (K $ (S $ (K $ a) $ b)) $ c $ d) ≡ norm (S $ (K $ a) $ (S $ (K $ b) $ c) $ d)
  thm-skk = refl
  thm-sksk = refl
  -- comparing type of π / ∣Π∣ of Artem and ours
  ππ : norm (S $ (K $ (C $ b $ a)) $ (C $ b $ a) $ c) ≡ norm (C $ (B $ b $ (C $ b $ a)) $ a $ c)
  ππ = refl

module Borisznak where
  I = S $ K $ K
  B = S $ (K $ S) $ K
  C = S $ (S $ (K $ B) $ S) $ (K $ K)
  F = S $ (K $ (S $ I)) $ (S $ (K $ K) $ I)
  O = K $ I
  SUC = S $ B
  REC = B $ (B $ C) $ C $ I
  INL = B $ (B $ K) $ F
  INR = C $ (B $ C $ (K $ I))
  CASE = B $ (B $ C) $ C $ I
  PRED = B $ (CASE $ I $ I) $ (REC $ (CASE $ (B $ INL $ SUC) $ (K $ (INL $ O))) $ (INR $ O))
  N4 = SUC $ SUC $ SUC $ SUC $ O
  N5 = SUC $ N4
  testPRED1 : norm (PRED $ N5 $ a) ≡ norm (N4 $ a)
  testPRED1 = refl

module fixpoint where

  I = S $ K $ K
  SELF = S $ I $ I
  selfApp : norm (SELF $ a) ≡ norm (a $ a)
  selfApp = refl

  -- from https://en-academic.com/dic.nsf/enwiki/11595201
  Y = S $ S $ K $ (S $ (K $ (S $ S $ (S $ (S $ S $ K)))) $ K)
  testY : norm (Y $ f $ a) ≡ norm (f $ (Y $ f $ a))
  testY = {!!} -- infinite loop
