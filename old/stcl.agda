{-# OPTIONS --prop --rewriting #-}

module stcl where

infix 4 _≡_
infix  3 _∎
infixr 2 _≡⟨_⟩_

data _≡_  {i}{A : Set i}(x : A) : A → Prop i where
  refl : x ≡ x

cong : ∀{i j}{A : Set i}{B : Set j}{a₀ a₁ : A} → (f : A → B) →  a₀ ≡ a₁ → f a₀ ≡ f a₁
cong f refl = refl
cong2 : ∀{i j k}{A : Set i}{B : Set j}{C : Set k}{a₀ a₁ : A}{b₀ b₁ : B} → (f : A → B → C) →  a₀ ≡ a₁ → b₀ ≡ b₁  → f a₀ b₀ ≡ f a₁ b₁
cong2 f refl refl = refl
trans : ∀{i}{A : Set i}{a a' a'' : A}(a= : a ≡ a')(a=' : a' ≡ a'') → a ≡ a''
trans refl refl = refl
sym : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a') → a' ≡ a
sym refl = refl
_≡⟨_⟩_ : ∀{ℓ}{A : Set ℓ}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = trans x≡y y≡z
_∎ : ∀{ℓ}{A : Set ℓ}(x : A) → x ≡ x
x ∎ = refl {x = x}


{-# BUILTIN REWRITE _≡_ #-}

data Ty : Set where
  ι : Ty
  _⇒_ : Ty → Ty → Ty
infixr 5 _⇒_
variable
  A B C D : Ty

data Con : Set where
  ◆ : Con
  _▹_ : Con → Ty → Con
variable
  Γ Δ Θ : Con

-- combinatory calculus w/o variables
module ₀ where
  
  data Tm : Ty → Set where
    _$_ : Tm (A ⇒ B) → Tm A → Tm B
    K   : Tm (A ⇒ B ⇒ A)
    S   : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  infixl 6 _$_
  variable
    a b c d : Tm A
  postulate
    Kβ : K $ a $ b ≡ a
    Sβ : S $ c $ b $ a ≡ c $ a $ (b $ a)
  {-# REWRITE Kβ Sβ #-}

module _ where
  open ₀
  cong$₀ : a ≡ b → c ≡ d → a $ c ≡ b $ d
  cong$₀ = cong2 _$_

-- combinatory calculus w/ variable
module ₁ where
  
  data Var : Con → Ty → Set where
    vz : Var (Γ ▹ A) A
    vs : Var Γ A → Var (Γ ▹ B) A

  data Tm : Con → Ty → Set where
    var : Var Γ A → Tm Γ A
    _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    K   : Tm Γ (A ⇒ B ⇒ A) 
    S   : Tm Γ ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)

  infixl 6 _$_
  variable
    a b c d : Tm Γ A
    
  postulate
    Kβ : K $ a $ b ≡ a
    Sβ : S $ c $ b $ a ≡ c $ a $ (b $ a)
  {-# REWRITE Kβ Sβ #-}

module _ where
  open ₁
  cong$₁ : a ≡ b → c ≡ d → a $ c ≡ b $ d
  cong$₁ = cong2 _$_

  congvar₁ : {a a₀ : Var Γ A} → a₀ ≡ a → var a₀  ≡ var a
  congvar₁ = cong ₁.var

-- STCL₀ → STCL₁
fTm : ₀.Tm A → ₁.Tm ◆ A
fTm (x ₀.$ x₁) = fTm x ₁.$ fTm x₁
fTm ₀.K = ₁.K
fTm ₀.S = ₁.S

fTmKβ : {a : ₀.Tm A}{b : ₀.Tm B} → ₁.K ₁.$ fTm a ₁.$ fTm b ≡ fTm a
fTmKβ = refl 

fTmSβ : {c : ₀.Tm (A ⇒ B ⇒ C)}{b : ₀.Tm (A ⇒ B)}{a : ₀.Tm A} → ₁.S ₁.$ fTm c ₁.$ fTm b ₁.$ fTm a ≡ fTm (c ₀.$ a ₀.$ (b ₀.$ a))
fTmSβ = refl

-- STCL₁ → STCL₀
gTm : ₁.Tm ◆ A → ₀.Tm A
gTm (A ₁.$ A₁) = gTm A ₀.$ gTm A₁
gTm ₁.K = ₀.K
gTm ₁.S = ₀.S
gTm (₁.var ())


gTmKβ₁ : {a : ₁.Tm ◆ A}{b : ₁.Tm ◆ B} → ₀.K ₀.$ gTm a ₀.$ gTm b ≡ gTm a
gTmKβ₁ = refl

gTmSβ₁ : {c : ₁.Tm  ◆ (A ⇒ B ⇒ C)}{b : ₁.Tm  ◆ (A ⇒ B)}{a : ₁.Tm  ◆ A} → ₀.S ₀.$ gTm c ₀.$ gTm b ₀.$ gTm a ≡ gTm (c ₁.$ a ₁.$ (b ₁.$ a))
gTmSβ₁ = refl

-- roundtrips STCL₀ ≅ STCL₁

gfTm : (x : ₀.Tm A) → gTm (fTm x) ≡ x
gfTm (x ₀.$ x₁) = cong$₀ (gfTm x) (gfTm x₁) 
gfTm ₀.K = refl
gfTm ₀.S = refl

-- gfTmKβ trivial
-- gfTmSβ trivial

fgTm : (x : ₁.Tm ◆ A) → fTm (gTm x) ≡ x
fgTm (x ₁.$ x₁) = cong$₁ (fgTm x) (fgTm x₁)
fgTm ₁.K = refl
fgTm ₁.S = refl

-- fgTmKβ trivial
-- fgTmSβ trivial

-- define weakening for STCL₁
module _ where
  open ₁

  wk₁ : Tm Γ A → Tm (Γ ▹ B) A
  wk₁ (var x) = var (vs x)
  wk₁ (x $ x₁) = wk₁ x $ wk₁ x₁
  wk₁ K = K
  wk₁ S = S
  
  wk₁Kβ : K $ wk₁ {B = B} a $ wk₁ b ≡ wk₁ a
  wk₁Kβ = refl
  wk₁Sβ : S $ wk₁ {B = B} c $ wk₁ b $ wk₁ a ≡ wk₁ c $ wk₁ a $ (wk₁ b $ wk₁ a)
  wk₁Sβ = refl


-- combinatory calculus w/ variables and single weakening
module ₂ where

  data Tm : Con → Ty → Set where
    _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    K   : Tm Γ (A ⇒ B ⇒ A) 
    S   : Tm Γ ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    q   : Tm (Γ ▹ A) A
    wk  : (B : Ty) → Tm Γ A → Tm (Γ ▹ B) A
  infixl 6 _$_
  variable
    a b c d : Tm Γ A

  postulate
    Kβ : K $ a $ b ≡ a
    Sβ : S $ c $ b $ a ≡ c $ a $ (b $ a)
    wk$ : {a : Tm Γ A}{f : Tm Γ (A ⇒ B)} → wk C (f $ a) ≡ wk C f $ wk C a
    wkK : wk C (K{Γ}{A}{B}) ≡ K
    wkS : wk D (S{Γ}{A}{B}{C}) ≡ S
    {-# REWRITE Kβ Sβ wk$ wkK wkS #-}

  I : Tm Γ (A ⇒ A)
  I {A = A} = S $ K $ K {B = A}

module _ where
  open ₂
  cong$₂ : a ≡ b → c ≡ d → a $ c ≡ b $ d
  cong$₂ x x₂ = cong2 _$_ x x₂

  congwk₂ : a ≡ b →  wk A a  ≡ wk _ b
  congwk₂ x = cong (wk _) x

-- assumption of extensionality, we can define lam for STCL₂
module _ where
  open ₂
  postulate
    ext : {u : Tm Γ (A ⇒ B)}{v : Tm Γ (A ⇒ B)} → wk A u $ q ≡ wk A v $ q → u ≡ v

  lam : Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  lam (x $ x₁) = S $ lam x $ lam x₁
  lam K = K $ K
  lam S = K $ S
  lam (q) = I
  lam (wk _ x) = K $ x
  
  -- lamKβ : {b : Tm (Γ ▹ A) B}{c : Tm (Γ ▹ A) C} → lam (K $ b $ c) ≡ lam b -- if we use rewrite rules, this definition is not proper
  lamKβ : {b : Tm (Γ ▹ A) B}{c : Tm (Γ ▹ A) C} → S $ (S $ (K $ K) $ lam b) $ lam c ≡ lam b
  lamKβ {Γ = Γ}{A = A}{B = B}{C = C}{b = b}{c = c} = ext refl

  lamSβ : S $ (S $ (S $ (K $ S) $ lam a) $ lam b) $ lam c ≡ lam(a $ c $ (b $ c))
  lamSβ = ext refl

  lamwk$ : K $ (c $ a) ≡ lam(wk A c $ wk A a)
  lamwk$ = ext refl
  
  lamwkK :  K {Γ}{B = A} $ K ≡ lam (K {A = B}{C})
  lamwkK = refl

  lamwkS : K {Γ}{B = A} $ S ≡ lam {A = A} (S {A = B}{C}{D})
  lamwkS = refl

  lamext : {u v : Tm (Γ ▹ C) (A ⇒ B)} → lam (wk A u $ q) ≡ lam (wk A v $ q) → lam u ≡ lam v
  lamext e = cong lam (ext (cong (λ z → wk _ z $ q) e))
  {-
   ext (
    wk A (S $ (S $ (K $ K) $ lam b) $ lam c) $ q
                                                    ≡⟨ cong (_$ q) wk$ ⟩
    wk A (S $ (S $ (K $ K) $ lam b)) $ wk A (lam c) $ q
                                                    ≡⟨ cong (λ z → z $ wk A (lam c) $ q) wk$ ⟩
    wk A S $ wk A (S $ (K $ K) $ lam b) $ wk A (lam c) $ q
                                                    ≡⟨ cong (λ z → z $ wk A (S $ (K $ K) $ lam b) $ wk A (lam c) $ q) wkS ⟩
    S $ wk A (S $ (K $ K) $ lam b) $ wk A (lam c) $ q
                                                    ≡⟨ Sβ ⟩
    wk A (S $ (K $ K) $ lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ q $ (wk A (lam c) $ q)) wk$ ⟩
    wk A (S $ (K $ K)) $ wk A (lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ wk A (lam b) $ q $ (wk A (lam c) $ q)) wk$ ⟩
    wk A S $ wk A (K $ K) $ wk A (lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ wk A (K $ K) $ wk A (lam b) $ q $ (wk A (lam c) $ q)) wkS ⟩
    S $ wk A (K $ K) $ wk A (lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (_$ (wk A (lam c) $ q)) Sβ ⟩
    wk A (K $ K) $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) wk$ ⟩
    wk A K $ wk A K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ wk A K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) wkK ⟩
    K $ wk A K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → K $ z  $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) wkK ⟩
    K $ K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) Kβ ⟩
    K $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ Kβ ⟩
    wk A (lam b) $ q
                                                    ∎)
  -}

-- STCL₁ → STCL₂
jVar : ₁.Var Γ A → ₂.Tm Γ A
jVar ₁.vz = ₂.q
jVar (₁.vs x) = ₂.wk _ (jVar x)

jTm :  ₁.Tm Γ A → ₂.Tm Γ A
jTm (x ₁.$ x₁) = jTm x ₂.$ jTm x₁
jTm ₁.K = ₂.K
jTm ₁.S = ₂.S
jTm (₁.var x) = jVar x

jTmKβ : {a : ₁.Tm Γ A}{b : ₁.Tm Γ B} → ₂.K ₂.$ jTm a ₂.$ jTm b ≡ jTm a
jTmKβ = refl

jTmSβ : {c : ₁.Tm  Γ (A ⇒ B ⇒ C)}{b : ₁.Tm Γ (A ⇒ B)}{a : ₁.Tm Γ A} → ₂.S ₂.$ jTm c ₂.$ jTm b ₂.$ jTm a ≡ jTm (c ₁.$ a ₁.$ (b ₁.$ a))
jTmSβ = refl

-- STCL₂ → STCL₁

hTm :  ₂.Tm Γ A → ₁.Tm Γ A
hTm (x ₂.$ x₁) = hTm x ₁.$ hTm x₁
hTm ₂.K = ₁.K
hTm ₂.S = ₁.S
hTm ₂.q = ₁.var ₁.vz
hTm (₂.wk B x) = wk₁ (hTm x)

hTmKβ : {a : ₂.Tm Γ A}{b : ₂.Tm Γ A} → ₁.K ₁.$ hTm a ₁.$ hTm b ≡ hTm a
hTmKβ = refl

hTmSβ : {c : ₂.Tm  Γ (A ⇒ B ⇒ C)}{b : ₂.Tm Γ (A ⇒ B)}{a : ₂.Tm Γ A} → ₁.S ₁.$ hTm c ₁.$ hTm b ₁.$ hTm a ≡ hTm (c ₂.$ a ₂.$ (b ₂.$ a))
hTmSβ = refl

hTmwk$ : {a : ₂.Tm Γ A}{f : ₂.Tm Γ (A ⇒ B)} → wk₁ (hTm f) ₁.$ wk₁ (hTm a) ≡ hTm (₂.wk C f ₂.$ ₂.wk C a)
hTmwk$ = refl

hTmwkK : ₂.wk C (₂.K{Γ}{A}{B}) ≡ ₂.K
hTmwkK = ₂.wkK
hTmwkS : ₂.wk D (₂.S{Γ}{A}{B}{C}) ≡ ₂.S
hTmwkS = ₂.wkS

-- roundtrips STCL₁ ≅ STCL₂

-- hjVar
hjVar : (x : ₁.Var Γ A) → hTm (jVar x) ≡ ₁.var x
hjVar ₁.vz = refl
hjVar (₁.vs x) = cong wk₁ (hjVar x)

hjTm : (x : ₁.Tm Γ A) → hTm (jTm x) ≡ x
hjTm (₁.var x) = hjVar x 
hjTm (x ₁.$ x₁) = cong$₁ (hjTm x) (hjTm x₁)
hjTm ₁.K = refl
hjTm ₁.S = refl

-- hjTmKβ, hjTmSβ trivial

jwk : (x : ₁.Tm Γ A) → jTm (wk₁ x) ≡ ₂.wk B (jTm x)
jwk (₁.var x) = refl
jwk (x ₁.$ x₁) = cong$₂ (jwk x) (jwk x₁)
jwk ₁.K = sym ₂.wkK
jwk ₁.S = sym ₂.wkS

jhTm : (x : ₂.Tm Γ A) → jTm (hTm x) ≡ x
jhTm (x ₂.$ x₁) = cong$₂ (jhTm x) (jhTm x₁)
jhTm ₂.K = refl
jhTm ₂.S = refl
jhTm ₂.q = refl
jhTm (₂.wk B x) = trans (jwk (hTm x)) (cong (₂.wk _) (jhTm x))

-- jhTm Kβ,Sβ,wk$,wkK,wkS trivial

module _ where
  open ₂
  -- OPE - order preserving embedding
{-
sub is the strongest with terms

-- sub
     |
   renaming                           affin (van felejtes nincs duplikalas)
     |                                  |
   weakening                          linear (nem szabad felejteni es duplikalni se)
     |                                  |
   order preserving embedding     -    id 


-}

  data Ope : Con → Con → Set where
    ε : Ope ◆ ◆
    drop : Ope Δ Γ → Ope (Δ ▹ A) Γ
    keep : Ope Δ Γ → Ope (Δ ▹ A) (Γ ▹ A)
  variable
    γ δ : Ope Δ Γ

  -- instantiation
  _[_] : Tm Γ A → Ope Δ Γ → Tm Δ A
  (x $ x₁) [ γ ] = (x [ γ ]) $ (x₁ [ γ ])
  {-
  (x $ x₁) [ ε ] = x $ x₁
  (x $ x₁) [ drop y ] = wk _ (x [ y ] $ x₁ [ y ])
  (x $ x₁) [ keep y ] = (x [ keep y ]) $ (x₁ [ keep y ])
  -}
  K [ γ ] = K
  S [ γ ] = S
  q [ drop γ ] = wk _ (q [ γ ])
  q [ keep γ ] = q
  wk B x [ drop γ ] = wk _ (wk B x [ γ ])
  wk B x [ keep γ ] = wk B (x [ γ ])

  []Kβ : K $ a [ γ ] $ b [ γ ] ≡ a [ γ ]
  []Kβ = refl
  []Sβ : S $ (c [ γ ]) $ (b [ γ ]) $ (a [ γ ]) ≡ (c $ a $ (b $ a)) [ γ ]
  []Sβ = refl
  []wk$ : {a : Tm Γ A}{f : Tm Γ (A ⇒ B)} → (wk C f [ γ ]) $ (wk C a [ γ ]) ≡ (wk C f $ wk C a)[ γ ]
  []wk$ = refl
  []wkK : wk C (K{Γ}{A}{B})[ γ ] ≡ K [ γ ]
  []wkK = refl
  []wkS : wk D (S{Γ}{A}{B}{C})[ γ ] ≡ S [ γ ]
  []wkS = refl


  q[] : q{A = A} [ keep γ ] ≡ q
  q[] = refl
  wk[] : (wk B a)[ keep γ ] ≡ wk _ (a [ γ ])
  wk[] = refl
  [drop] : (a : Tm Γ A){B : Ty} → a [ drop {A = B} γ ] ≡ wk B (a [ γ ])
  [drop] (a $ a₁) = cong$₂ ([drop] a) ([drop] a₁)
  [drop] K = refl
  [drop] S = refl
  [drop] q = refl
  [drop] (wk B a) = refl

  id : Ope Γ Γ
  id {◆} = ε
  id {Γ ▹ A} = keep id


  _∘_ : Ope Δ Γ → Ope Θ Δ → Ope Θ Γ
  γ ∘ ε = γ
  γ ∘ drop δ = drop (γ ∘ δ)
  drop γ ∘ keep δ = drop (γ ∘ δ)
  keep γ ∘ keep δ = keep (γ ∘ δ)

