{-# OPTIONS --type-in-type --rewriting --confluence #-}
module _ where

open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}

open import Function using (_∘_; _∘′_)
                     renaming (_$_ to _$$_)

infixr 5 _⇒₀_
infixl 10 _$_
infixl 10 _$₀_

postulate
  Ty : Set
  Tm : Ty → Set

variable A B C : Ty

postulate
  U : Ty

  Tm-U : Tm U ≡ Ty
  {-# REWRITE Tm-U #-}

  _⇒₀_ : Ty → Ty → Ty
  _$₀_ : Tm (A ⇒₀ B) → Tm A → Tm B

  |⇒| : Tm (U ⇒₀ U ⇒₀ U)
  ⇒≡ : A ⇒₀ B ≡ |⇒| $₀ A $₀ B

  -- Tm⇒≡ : Tm (|⇒| $₀ A $₀ B) ≡ Tm (A ⇒₀ B)
  -- {-# REWRITE Tm⇒≡ #-}

  XTm⇒≡ : (|⇒| $₀ A $₀ B) ≡ (A ⇒₀ B)
  {-# REWRITE XTm⇒≡ #-}

  -- TmΠ⇒≡ : ∀ {B : Tm (A ⇒₀ U)}{a : Tm A}
  --       → Tm (|⇒| $₀ (B $₀ a) $₀ U) ≡ Tm ((B $₀ a) ⇒₀ U)
  -- {-# REWRITE TmΠ⇒≡ #-}
  --XTmΠ⇒≡ : ∀ {B : Tm (A ⇒₀ U)}{a : Tm A}
  --       → |⇒| $₀ (B $₀ a) $₀ U ≡ (B $₀ a) ⇒₀ U
  --{-# REWRITE XTmΠ⇒≡ #-}


-- Simple Combinators
K′ : ∀ {A B : Set} → A → B → A
K′ a b = a

B′ : {A B C : Set} → (B → C) → (A → B) → (A → C)
B′ x y z = x (y z)

S′ : ∀ {A B C : Set} → (A → B → C) → (A → B) → A → C
S′ f g a = f a (g a)

postulate
  K₀ : Tm (A ⇒₀ B ⇒₀ A)
  K₀$ : {a : Tm A}{b : Tm B} → K₀ {A} {B} $₀ a $₀ b ≡ a
  {-# REWRITE K₀$ #-}

  S₀ : Tm ((A ⇒₀ B ⇒₀ C) ⇒₀ (A ⇒₀ B) ⇒₀ A ⇒₀ C)
  S₀$ : {f : Tm (A ⇒₀ B ⇒₀ C)}{g : Tm (A ⇒₀ B)}{a : Tm A}
      → S₀ {A} {B} {C} $₀ f $₀ g $₀ a ≡ f $₀ a $₀ (g $₀ a)

  -- We can implement this via S and K if we want
  C₀ : Tm ((A ⇒₀ B ⇒₀ C) ⇒₀ (B ⇒₀ A ⇒₀ C))
  C₀$ : {f : Tm (A ⇒₀ B ⇒₀ C)}{x : Tm A}{y : Tm B}
      → C₀ $₀ f $₀ y $₀ x ≡ f $₀ x $₀ y
  {-# REWRITE S₀$ #-}
  {-# REWRITE C₀$ #-}

-- Here are two theorems that work in metatheory, and
-- we want to add them as axioms to out object language.
thm-skk : ∀ {A B C : Set}{X : A → B}{Y : A} → S′ {A = C} (K′ X) (K′ Y) ≡ K′ (X Y)
thm-skk = refl

thm-sksk : ∀ {A B C : Set}{BB : A → B}{PP QQ : B → B}
         → (S′ (K′ (S′ (K′ PP) QQ)) BB) ≡ (S′ (K′ PP) (S′ (K′ QQ) BB))
thm-sksk = refl

postulate
  -- One of the eta rules
  SKK : ∀ {A B C : Ty}{X : Tm (A ⇒₀ B)}{Y : Tm A} 
      → S₀ {A = C} $₀ (K₀ $₀ X) $₀ (K₀ $₀ Y) ≡ K₀ $₀ (X $₀ Y)
  {-# REWRITE SKK #-}

  SKKa : ∀ {A B C : Ty}{X : Tm (A ⇒₀ B)}{Y : Tm A}{a : Tm C} 
      → S₀ {A = C} $₀ (K₀ $₀ X) $₀ (K₀ $₀ Y) $₀ a ≡ _$₀_ {B = B} (K₀ {B = C} $₀ (X $₀ Y))  a
  --{-# REWRITE SKKa #-}

  SKSK : {BB : Tm (A ⇒₀ B)}{PP QQ : Tm (B ⇒₀ B)}
       → S₀ $₀ (K₀ $₀ (S₀ $₀ (K₀ $₀ PP) $₀ QQ)) $₀ BB 
       ≡ S₀ $₀ (K₀ $₀ PP) $₀ (S₀ $₀ (K₀ $₀ QQ) $₀ BB)
  {-# REWRITE SKSK #-}

I : {A : Ty} → Tm (A ⇒₀ A)
I {A} = S₀ $₀ K₀ $₀ K₀ {B = A}

postulate

  Π₀ : (A : Ty) → Tm ((A ⇒₀ U) ⇒₀ U)
  ⇒₀≡ : A ⇒₀ B ≡ Π₀ A $₀ (K₀ $₀ B)

  _$_ : {B : Tm (A ⇒₀ U)} 
      → Tm (Π₀ A $₀ B) → (a : Tm A) → Tm (B $₀ a)

  |Π| : Tm (Π₀ U {- A -} 
            $₀ -- (A → U) → U
               (S₀ $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ U)) 
                   $₀ (C₀ $₀ |⇒| $₀ U)))

infixl 10 _$′_
_$′_ : Tm (|⇒| $₀ (|⇒| $₀ A $₀ U) $₀ U)
     → Tm (A ⇒₀ U)
     → Tm U
_$′_ {A} f b rewrite (sym (⇒≡ {A = A}     {B = U})) 
                   | (sym (⇒≡ {A = A ⇒₀ U}{B = U}))
                   = f $₀ b


postulate

  |Π|≡ : {A : Tm U}{B : Tm (A ⇒₀ U)}
       --→ Π₀ A $₀ B ≡ |Π| $ A $′ B
       → Π₀ A $₀ B ≡ |Π| $ A $₀ B

  --|⇒|≡ : |⇒| $₀ A $₀ B ≡ |Π| $ A $′ (K₀ $₀ B)
  |⇒|≡ : |⇒| $₀ A $₀ B ≡ |Π| $ A $₀ (K₀ $₀ B)
  --{-# REWRITE |⇒|≡ #-}

  -- Combine togethere previous equalities (for convenience)
  |⇒|Π≡ : |⇒| $₀ A $₀ B ≡ Π₀ A $₀ (K₀ $₀ B)

  -- Some scary rewrite rules
  R|⇒|Π≡ : ∀ {B : Tm (A ⇒₀ U)}
         → |Π| $ A $₀ B ≡ Π₀ A $₀ B
  {-# REWRITE R|⇒|Π≡ #-}

  FR|⇒|Π≡ : |Π| $ A ≡ Π₀ A
  {-# REWRITE FR|⇒|Π≡ #-}

  -- FIXME this is a hack, refactor through K₀ or K
  KK : Tm (A ⇒₀ Π₀ B $₀ (K₀ $₀ A))
  KK$ : {a : Tm A}{b : Tm B} → KK {A} {B} $₀ a $ b ≡ a
  {-# REWRITE KK$ #-}

  -- Pi A {- a : A -} → (λ a → B a → A)
  K : {B : Tm (A ⇒₀ U)}
    → Tm (Π₀ A 
          $₀ (S₀ {B = U} 
              $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ A)) 
              $₀ B))

  -- Well, S is quite a bit more work, but let's see.
  -- I am trying to proceed with the following
  -- specification given in terms of combinators on
  -- Set.

PI : Set
PI = (A : Set) → (A → Set) → Set -- Pi Set λ A → (A → Set) → Set

Pi : PI --(A : Set) → (B : A → Set) → Set
Pi A B = (a : A) → (B a)

-- Note that C does not depend on A and B
S′′ : ∀ {A C : Set}{B : A → Set} 
    → ((a : A) → B a → C) 
    → ((a : A) → B a) 
    → A → C
S′′ f g a = f a (g a)

-- Note that B does not depend on A
S,, : ∀ {A B : Set}{C : A → B → Set}
    --→ ((a : A) → (b : B) → C a b) 
    → (Pi A (S′ (K′ (Pi B)) C))
    → Pi (A → B) (B′ (Pi A) (S′ C))
    --→ (g : A → B) 
    --→ (a : A) → C a (g a)
S,, f g a = f a (g a)

C′ : ∀ {A B C : Set} → (A → B → C) → (B → A → C)
C′ f x y = f y x

-- Note that C does not depend on B
C′′ : ∀ {A B : Set}{C : A → Set}
    → ((x : A) → (y : B) → C x)
    → (y : B) → (x : A) → C x
C′′ f y x = f x y


Arr : (Set → (Set → Set))
Arr A B = Pi A (K′ B)

K! : {A : Set}{B : A → Set} 
   → (Pi A (S′ (K′ (C′ Arr A)) B))
   --(λ a → Pi (B a) (K′ A))
K! a b = a

Pilike : (F : (A : Set) → (A → Set) → Set)
       → (A : Set)
       → (B : A → Set)
       → (C : (a : A) → (b : B a) → Set)
       → A
       → Set
--Pilike F A B C a = F (B a) (C a)
--Pilike F A B C = Sx (S,, (K! F) B) C
Pilike F A B C = S′′ (S,, (K′ F) B) C
--Pilike F A B C = S′′ (S,, ? (S′ (K′ (C′ Arr Set)) B)) C 


--postulate
Λa→Ba→C : {A : Set} → (B : A → Set) (C : Set) → (A → Set)
Λa→Ba→C B C = S′ (K′ (C′ Arr C)) B

S! : ∀ {A : Set}{B : A → Set}{C : Pi A (Λa→Ba→C B Set)} --{C : (a : A) (b : B a) → Set}
    --→ Pi (Pi A λ a → Pi (B a) (C a))
    --→ Pi (Pi A (Pilike Pi A B C))
    --     (K′ {B = (x : A) (x₁ : B x) → C x x₁} (Pi (Pi A B)
    --             (B′ (Pi A) (S′′ C))))
    → (Pi A (Pilike Pi A B C))
    → (Pi (Pi A B) (B′ (Pi A) (S′′ C)))
S! f g a = f a (g a)

Λa⇒Ba⇒C : (B : Tm (A ⇒₀ U)) (C : Ty) → Tm (A ⇒₀ U)
Λa⇒Ba⇒C B C = S₀ {B = U} $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ C)) $₀ B

-- *Alaternatively* can be written as
Λa⇒Ba⇒C′ : (B : Tm (A ⇒₀ U)) (C : Ty) → Tm (A ⇒₀ U)
Λa⇒Ba⇒C′ B C = S₀ {B = U} $₀ (S₀ $₀ (K₀ $₀ |⇒|) $₀ B) $₀ (K₀ $₀ U)
{-# INLINE Λa⇒Ba⇒C #-}

--infixl 10 _$′′_
--_$′′_ : Tm (|⇒| $₀ A $₀ B)
--     → Tm A
--     → Tm B
--_$′′_ {A}{B} f b rewrite (sym (⇒≡ {A = A}{B = B})) = f $₀ b


postulate
  --B : {A B C : Set} → (B → C) → (A → B) → (A → C)
  --B x y z = x (y z)
  B₀ : Tm ((B ⇒₀ C) ⇒₀ (A ⇒₀ B) ⇒₀ (A ⇒₀ C))
  B₀$ : ∀ {f : Tm (B ⇒₀ C)}{g : Tm (A ⇒₀ B)}{x}
      → B₀ $₀ f $₀ g $₀ x ≡ f $₀ (g $₀ x)
  {-# REWRITE B₀$ #-}

  -- C does not depend on A and B
  -- S′′ : ∀ {A C : Set}{B : A → Set} 
  --     → ((a : A) → B a → C) 
  --     → ((a : A) → B a) 
  --     → A → C
  -- S′′ f g a = f a (g a)
  S₁ : ∀ {B : Tm (A ⇒₀ U)}
     → Tm (Π₀ A $₀ ({- λ a → B a → C-} Λa⇒Ba⇒C B C)
           ⇒₀ Π₀ A $₀ B
           ⇒₀ A ⇒₀ C)
  S₁$ : ∀ {B : Tm (A ⇒₀ U)}{f : Tm (Π₀ A $₀ (Λa⇒Ba⇒C B C))}{g : Tm (Π₀ A $₀ B)}{x}
      --→ S₁ $₀ f $₀ g $₀ x ≡ f $ x $′′ (g $ x)
      → S₁ $₀ f $₀ g $₀ x ≡ f $ x $₀ (g $ x)
  {-# REWRITE S₁$ #-}


  -- B does not depend on A
  -- S,, : ∀ {A B : Set}{C : A → B → Set}
  --     → ((a : A) → (b : B) → C a b) 
  --     → (g : A → B) 
  --     → (a : A) → C a (g a)
  -- ** Alternatively **
  -- S,, : ∀ {A B : Set}{C : A → B → Set}
  --     → (Pi A (S′ (K′ (Pi B)) C))
  --     → Pi (A → B) (B′ (Pi A) (S′ C))
  -- S,, f g a = f a (g a)
  S₂ : ∀ {C : Tm (A ⇒₀ B ⇒₀ U)}
     → Tm (Π₀ A $₀ (S₀ $₀ (K₀ $₀ (Π₀ B)) $₀ C)
           ⇒₀ Π₀ (A ⇒₀ B) $₀ (B₀ $₀ (Π₀ A) $₀ (S₀ $₀ C)))
           --⇒₀ Π₀ (Π₀ A $₀ (K₀ $₀ B)) $₀ (B₀ $₀ (Π₀ A) $₀ ?))
  S₂$ : ∀ {C : Tm (A ⇒₀ B ⇒₀ U)}
          {f : Tm (Π₀ A $₀ (S₀ $₀ (K₀ $₀ (Π₀ B)) $₀ C))}
          {g : Tm (A ⇒₀ B)}{x}
      → S₂ {C = C} $₀ f $ g $ x ≡ f $ x $ (g $₀ x)
  {-# REWRITE S₂$ #-}



castty : {A B : Ty} → A ≡ B → Tm A → Tm B
castty p a = subst Tm p a

Hlpr : (A : Set) → (B : A → Set)
     → Pi A (S′ (K′ (C′ Arr Set)) 
                    (S′ (K′ (C′ Arr Set)) B)) 
--Hlpr A B a Ca = Pi (B a) Ca
--Hlpr A B a = Pi (B a)
Hlpr A B = S,, {C = λ a Ba → (Ba → Set) → Set} (K′ Pi) B

TmHlpr : (A : Tm U) → (B : Tm (A ⇒₀ U))
       → Tm (Π₀ A 
             $₀ (S₀ $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ U)) 
                    $₀ (S₀ $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ U)) 
                           $₀ B)))
TmHlpr A B = let -- pt = Π₀ U {- A -} $₀ -- (A → U) → U
                 --      (S₀ $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ U)) 
                 --       $₀ (C₀ $₀ |⇒| $₀ U))
                 --yy = K {A = pt}{B = K₀ $₀ A} $ |Π| 
                 --yy = KK {A = pt}{B = A} $₀ |Π| 
                 yy = KK $₀ |Π|
                 --zz = castty ((|⇒|Π≡ {A = A} {B = pt})) yy
                 xx = S₂ {C = K₀ $₀ (S₀ $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ U)) $₀ (C₀ $₀ |⇒| $₀ U)) } $₀ yy $ B 
            in xx

TmPlike : (A : Tm U) → (B : Tm (A ⇒₀ U)) → (C : Tm (Π₀ A $₀ Λa⇒Ba⇒C B U))
        → Tm (A ⇒₀ U)
TmPlike A B C = S₁ $₀ TmHlpr A B $₀ C


postulate
  -- S! : ∀ {A : Set}{B : A → Set}{C : (a : A) (b : B a) → Set}
  --     --→ Pi (Pi A λ a → Pi (B a) (C a))
  --     → Pi (Pi A (S′′ (S,, (K′ Pi) B) C))
  --          (K′ (Pi (Pi A B)
  --                  (B′ (Pi A) (S′′ C))))
  -- S! f g a = f a (g a)

  -- XXX This rewrite rule is too specific, but
  --     it makes the S$ rule work.  Refactor it into
  --     something more general.
  S₂K$ : ∀ {B : Tm (A ⇒₀ U)}{a : Tm A}
       → S₂ {C = K₀ $₀ (S₀ $₀ (K₀ $₀ (C₀ $₀ |⇒| $₀ U)) 
                    $₀ (C₀ $₀ |⇒| $₀ U)) }
          $₀ (KK $₀ |Π|) $ B $ a ≡ Π₀ (B $₀ a)
  {-# REWRITE S₂K$ #-}


  S : ∀ {B : Tm (A ⇒₀ U)}{C : Tm (Π₀ A $₀ Λa⇒Ba⇒C B U)}
    → Tm ((Π₀ A $₀ TmPlike A B C)
          ⇒₀ Π₀ (Π₀ A $₀ B) $₀ (B₀ $₀ (Π₀ A) $₀ (S₁ $₀ C)))
  S$ : ∀ {B : Tm (A ⇒₀ U)}{C : Tm (Π₀ A $₀ Λa⇒Ba⇒C B U)}
         {f : Tm (Π₀ A $₀ TmPlike A B C)}{g : Tm (Π₀ A $₀ B)}{a : Tm A}
     → S $₀ f $ g $ a ≡ f $ a $ (g $ a) 

