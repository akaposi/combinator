{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)

infixl 15 _^
infixr 10 _⇒_
infixl 10 _$_
infixl 5 _,s_

data Ty : Type where
  nat : Ty
  _⇒_ : Ty → Ty → Ty

data Con : Type where
  ε   : Con
  _▹_ : Con → Ty → Con

variable
  τ σ δ   : Ty
  Γ Δ Ξ Ψ : Con

-- data _∈_ : Ty → Con → Type where
--   here  : τ ∈ (Γ ▹ τ)
--   there : τ ∈ Γ → τ ∈ (Γ ▹ σ) 

data Tm : Con → Ty → Type
data Sub : Con → Con → Type

-- Forward declarations of the "constructors"
q′    : Tm (Γ ▹ τ) τ
_[_]′ : Tm Γ τ → Sub Δ Γ → Tm Δ τ

data Sub where
  --εs   : Sub Γ ε
  id   : Sub Γ Γ
  _∘_  : Sub Ξ Δ → Sub Γ Ξ → Sub Γ Δ
  _,s_ : Sub Γ Δ → Tm Γ τ → Sub Γ (Δ ▹ τ)
  π₁   : Sub (Γ ▹ τ) Γ

  -- Equalities
  --εη  : (γ : Sub Γ ε) → γ ≡ εs
  ass : ∀ {γ : Sub Ξ Δ}{φ : Sub Ψ Ξ}{ψ : Sub Γ Ψ}
      → γ ∘ (φ ∘ ψ) ≡ (γ ∘ φ) ∘ ψ
  idl : {γ : Sub Γ Δ} → id ∘ γ ≡ γ
  idr : {γ : Sub Γ Δ} → γ ∘ id ≡ γ

  ▹βs : {γ : Sub Γ Δ}{t : Tm Γ τ} → π₁ ∘ (γ ,s t) ≡ γ
  ▹η : {γ : Sub Γ (Δ ▹ τ)} → ((π₁ ∘ γ) ,s (q′ [ γ ]′)) ≡ γ

  -- TODO: add hSet truncation

_^ : Sub Γ Δ → Sub (Γ ▹ τ) (Δ ▹ τ)
_^ γ = (γ ∘ π₁) ,s q′

data Tm where
  --var  : τ ∈ Γ → Tm Γ τ
  q    : Tm (Γ ▹ τ) τ
  lam  : Tm (Γ ▹ τ) σ → Tm Γ (τ ⇒ σ)
  app  : Tm Γ (τ ⇒ σ) → Tm (Γ ▹ τ) σ

  _[_] : Tm Γ τ → Sub Δ Γ → Tm Δ τ

  -- Equalities
  [∘] : {γ : Sub Ξ Γ}{φ : Sub Δ Ξ} → {t : Tm Γ τ} 
      → t [ γ ∘ φ ] ≡ t [ γ ] [ φ ]
  [id] : ∀ {t : Tm Γ τ} → t [ id ] ≡ t
  lamβ : ∀ {t : Tm (Γ ▹ τ) σ} → app (lam t) ≡ t
  lamη : ∀ {t : Tm Γ (τ ⇒ σ)} → lam (app t) ≡ t

  ▹βt : {γ : Sub Γ Δ}{t : Tm Γ τ} → q [ γ ,s t ] ≡ t
  lam[] : ∀ {t : Tm (Γ ▹ τ) σ}{γ : Sub Δ Γ} 
        → (lam t) [ γ ] ≡ lam (t [ γ ^ ])

  -- TODO: add hSet truncation

q′ = q
_[_]′ = _[_]


-- Do we need to axiomatise the empty subsitution?
εs : Sub Γ ε
εs {ε} = id
εs {Γ ▹ x} = εs {Γ} ∘ π₁

_$_ : Tm Γ (τ ⇒ σ) → Tm Γ τ → Tm Γ σ
f $ x = (app f) [ id ,s x ]

,s∘  : ∀ {γ : Sub Γ Δ}{φ : Sub Ξ Γ}{t : Tm Γ τ} 
     → (γ ,s t) ∘ φ ≡ (γ ∘ φ) ,s t [ φ ]
,s∘ {γ = γ}{φ}{t} = 
   (γ ,s t) ∘ φ ≡⟨ sym ▹η ⟩
   (π₁ ∘ ( (γ ,s t) ∘ φ)) ,s (q [ (γ ,s t) ∘ φ ]) ≡⟨ cong₂ _,s_ ass [∘] ⟩
   ((π₁ ∘ (γ ,s t)) ∘ φ) ,s (q [ γ ,s t ] [ φ ])  ≡⟨ cong₂ _,s_ (cong (_∘ φ) ▹βs) (cong (_[ φ ]) ▹βt) ⟩
   (γ ∘ φ) ,s (t [ φ ]) 
   ∎

lift∘,s : ∀ (x : Tm Γ τ) (γ : Sub Γ Δ) → (γ ^ ) ∘ (id ,s x) ≡ γ ,s x
lift∘,s x γ =
  (γ ∘ π₁ ,s q) ∘ (id ,s x) ≡⟨ ,s∘ ⟩
  ((γ ∘ π₁) ∘ (id ,s x)) ,s (q [ id ,s x ]) ≡⟨ cong₂ _,s_ refl ▹βt ⟩
  ((γ ∘ π₁) ∘ (id ,s x)) ,s x ≡⟨ cong (_,s x) (sym ass) ⟩
  (γ ∘ (π₁ ∘ (id ,s x))) ,s x ≡⟨ cong (_,s x) (cong (γ ∘_) ▹βs) ⟩
  (γ ∘ id) ,s x ≡⟨ cong (_,s x) idr ⟩
  γ ,s x
  ∎

app-sub : (γ : Sub Δ Γ) (f : Tm Γ (σ ⇒ τ))
          → app (f [ γ ]) ≡ app f [ γ ^ ]
app-sub γ f = 
    app (f [ γ ])              ≡⟨ cong (λ x → app (x [ γ ])) (sym lamη) ⟩
    app (lam (app f) [ γ ])    ≡⟨ cong app lam[] ⟩
    app (lam (app f [ γ ^ ] )) ≡⟨ lamβ ⟩
    (app f [ γ ^ ] ) 
    ∎

sub-$ : (γ : Sub Δ Γ) (f : Tm Γ (σ ⇒ τ)) (x : Tm Γ σ)
      → (f $ x)[ γ ] ≡ (f [ γ ]) $ (x [ γ ])
sub-$ γ f x = 
    app f [ id ,s x ] [ γ ]             ≡⟨ sym [∘] ⟩ 
    app f [ (id ,s x) ∘ γ ]             ≡⟨ cong ((app f) [_]) ,s∘ ⟩ 
    app f [ (id ∘ γ) ,s (x [ γ ]) ]     ≡⟨ cong (λ t → app f [ t ,s (x [ γ ]) ]) idl ⟩
    app f [ γ ,s (x [ γ ]) ]            ≡⟨ cong (app f [_]) (sym (lift∘,s _ _))  ⟩ 
    app f [ (γ ^) ∘ (id ,s x [ γ ]) ]   ≡⟨ [∘]  ⟩ 
    app f [ γ ^  ] [ id ,s x [ γ ] ]    ≡⟨ cong (_[ id ,s x [ γ ] ]) (sym (app-sub _ _))  ⟩ 
    app (f [ γ ]) [ id ,s x [ γ ] ]
    ∎

lam-$ : (x : Tm Γ τ) (t : Tm (Γ ▹ τ) σ) → lam t $ x ≡ t [ id ,s x ]
lam-$ x t =
   app (lam t) [ id ,s x ] ≡⟨ cong (_[ id ,s x ]) lamβ ⟩
   t [ id ,s x ]
   ∎

pattern v₀ = q
pattern v₁ = v₀ [ π₁ ]
pattern v₂ = v₁ [ π₁ ]
pattern v₃ = v₂ [ π₁ ]

module Tests where

    -- id
    test₁ : Tm Γ (τ ⇒ τ)
    test₁ = lam v₀
    -- 
    -- -- k
    test₂ : Tm Γ (τ ⇒ σ ⇒ τ)
    test₂ = lam (lam v₁)

    test₂′ : Tm Γ (τ ⇒ σ ⇒ δ ⇒ τ)
    test₂′ = lam (lam (lam v₂))

    -- λ f x → f x
    test₃ : Tm Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)
    test₃ = lam (lam (v₁ $ v₀))

    -- s
    test₄ : Tm Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ (τ ⇒ δ))
    test₄ = lam (lam (lam (v₂ $ v₀ $ (v₁ $ v₀))))

    -- k x y = x
    test-k : (x : Tm Γ τ) (y : Tm Γ σ) → test₂ $ x $ y ≡ x
    test-k x y = 
        (app (app (lam (lam v₁)) [ id ,s x ]) [ id ,s y ]) ≡⟨ cong (λ t → app (t [ id ,s x ]) [ id ,s y ]) lamβ  ⟩
        (app (lam v₁ [ id ,s x ]) [ id ,s y ]) ≡⟨ cong (λ t → app t [ id ,s y ]) lam[] ⟩
        (app (lam (v₁ [ (id ,s x)^ ])) [ id ,s y ]) ≡⟨ cong (λ t → t [ id ,s y ]) lamβ ⟩
        (v₁ [ (id ,s x)^ ]) [ id ,s y ] ≡⟨ refl ⟩
        q [ π₁ ] [ (id ,s x)^ ] [ id ,s y ] ≡⟨ cong (_[ id ,s y ]) (sym [∘]) ⟩
        q [ π₁ ∘ ((id ,s x)^) ] [ id ,s y ] ≡⟨ cong (λ t → (q [ t ] [ id ,s y ])) ▹βs ⟩
        q [ (id ,s x) ∘ π₁ ] [ id ,s y ] ≡⟨ sym [∘] ⟩
        q [ ((id ,s x) ∘ π₁) ∘ (id ,s y) ] ≡⟨ cong (q [_]) (sym ass) ⟩
        q [ (id ,s x) ∘ (π₁ ∘ (id ,s y)) ] ≡⟨ cong (λ t → q [ (id ,s x) ∘ t ]) ▹βs  ⟩
        q [ (id ,s x) ∘ id ] ≡⟨ cong (q [_]) idr ⟩
        q [ (id ,s x) ] ≡⟨ ▹βt ⟩
        x
        ∎

    test-k₁ : (x : Tm Γ τ) (y : Tm Γ σ) → test₂ $ x $ y ≡ x
    test-k₁ x y = 
        lam (lam v₁) $ x $ y     ≡⟨ cong (_$ y) (lam-$ _ _) ⟩
        ((lam v₁) [ id ,s x ]) $ y ≡⟨ cong (_$ y) lam[] ⟩
        lam (v₁ [ (id ,s x)^ ]) $ y ≡⟨ lam-$ _ _ ⟩
        q [ π₁ ] [ (id ,s x)^ ] [ id ,s y ] ≡⟨ cong _[ id ,s y ] (sym [∘]) ⟩
        q [ π₁ ∘ ((id ,s x)^) ] [ id ,s y ] ≡⟨ sym [∘] ⟩
        q [ (π₁ ∘ ((id ,s x)^)) ∘ (id ,s y) ] ≡⟨ cong (q [_]) (sym ass) ⟩
        q [ π₁ ∘ (((id ,s x)^) ∘ (id ,s y)) ] ≡⟨ cong (λ t → q [ π₁ ∘ t ]) (lift∘,s _ _) ⟩
        q [ π₁ ∘ ((id ,s x) ,s y) ] ≡⟨ cong (q [_]) ▹βs ⟩
        q [ id ,s x ] ≡⟨ ▹βt ⟩
        x
        ∎


-- Evaluation

⟦_⟧t : Ty → Set -- TODO: rewrite Set to HSet
⟦ nat ⟧t = ℕ
⟦ t ⇒ t₁ ⟧t = ⟦ t ⟧t → ⟦ t₁ ⟧t

⟦_⟧e : Con → Set -- TODO: rewrite Set to HSet
⟦ ε ⟧e = Unit
⟦ Γ ▹ x ⟧e = ⟦ Γ ⟧e × ⟦ x ⟧t

⟦_⟧ : Tm Γ τ → ⟦ Γ ⟧e → ⟦ τ ⟧t
⟦_⟧s : Sub Γ Δ → ⟦ Γ ⟧e → ⟦ Δ ⟧e

⟦ id ⟧s x = x
⟦ s ∘ s₁ ⟧s = ⟦ s ⟧s F.∘ ⟦ s₁ ⟧s
⟦ s ,s x ⟧s ρ = ⟦ s ⟧s ρ , ⟦ x ⟧ ρ
⟦ π₁ ⟧s = fst
⟦ ass {γ = γ}{φ = φ}{ψ = ψ} i ⟧s ρ = F.∘-assoc ⟦ γ ⟧s ⟦ φ ⟧s ⟦ ψ ⟧s i ρ
⟦ idl {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ idr {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ ▹βs {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ
⟦ ▹η  {γ = γ} i ⟧s ρ = ⟦ γ ⟧s ρ

⟦ v₀ ⟧ ρ = snd ρ
⟦ lam t ⟧ ρ x = ⟦ t ⟧ (ρ , x)
⟦ app t ⟧ ρ = ⟦ t ⟧ (fst ρ) (snd ρ)
⟦ t [ s ] ⟧ ρ = ⟦ t ⟧ (⟦ s ⟧s ρ)
⟦ [∘] {γ = γ}{φ = φ}{t = t} i ⟧ ρ = ⟦ t ⟧ (⟦ γ ⟧s (⟦ φ ⟧s ρ))
⟦ [id] {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ lamβ {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ lamη {t = t} i ⟧ ρ x = ⟦ t ⟧ ρ x
⟦ ▹βt {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ lam[] {t = t}{γ = γ} i ⟧ ρ x = ⟦ t ⟧ (⟦ γ ⟧s ρ , x)
