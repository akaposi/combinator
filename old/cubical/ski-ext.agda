{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)

import stlc as L

-- This is what stlc.org calls STCL₁ (combinators with variables).
-- In this file the data structure is called SKV.


infixr 10 _⇒_
infixl 10 _$_

Con = L.Con
Ty = L.Ty
_▹_ = L._▹_
_⇒_ = L._⇒_


variable
  τ σ δ   : Ty
  Γ Δ Ξ Ψ : Con


data SKV : Con → Ty → Type where
  q : SKV (Γ ▹ τ) τ
  wk : SKV Γ σ → SKV (Γ ▹ τ) σ

  K : SKV Γ (τ ⇒ σ ⇒ τ)
  S : SKV Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_ : SKV Γ (τ ⇒ σ) → SKV Γ τ → SKV Γ σ
  Kβ : {x : SKV Γ τ}{y : SKV Γ σ} → K $ x $ y ≡ x
  Sβ : {x : SKV Γ (τ ⇒ σ ⇒ δ)}{y : SKV Γ (τ ⇒ σ)}{z : SKV Γ τ}
     → S $ x $ y $ z ≡ x $ z $ (y $ z)

  wk$ : {x : SKV Γ (τ ⇒ σ)}{y : SKV Γ τ} → wk {τ = δ} (x $ y) ≡ wk x $ wk y
  wkK : wk {Γ = Γ}{τ = δ} (K {τ = τ}{σ = σ}) ≡ K
  wkS : ∀ {γ} → wk {Γ = Γ}{τ = γ} (S {τ = τ}{σ = σ}{δ = δ}) ≡ S

  ext : {x y : SKV Γ (τ ⇒ σ)} → wk x $ q ≡ wk y $ q → x ≡ y
  -- instead of ext, we need:
  -- 1. lamKβ : lam (K $ x $ y) ≡ lam x
  -- unfold lam
  -- 2. lamKβ : S $ (S $ (K $ K) $ lam x) $ lam y ≡ lam x
  -- remove lam
  -- 3. lamKβ : S $ (S $ (K $ K) $ u) $ v ≡ u
  -- make it closed
  -- 4. lamKβ : lam (lam (S $ (S $ (K $ K) $ wk vz) $ vz)) ≡ K
          

-- closed : (u : Tm ∙ A) → u ≡ wk u

lam : SKV (Γ ▹ τ) σ → SKV Γ (τ ⇒ σ)

-- lamClosed : (u v : Tm ∙ A) → u ≡ v → lam u ≡ lam v
-- lamClosed u v e = lam u ≡ lam (wk u)

lam {τ = τ} q = S $ K $ K {σ = τ}
lam (wk x) = K $ x
lam K = K $ K
lam S = K $ S
lam (x $ x₁) = S $ lam x $ lam x₁
lam (Kβ {x = x}{y = y} i) = --?
  ext
    (wk (S $ (S $ (K $ K) $ lam x) $ lam y) $ q  ≡⟨ cong (_$ q) wk$ ⟩
     wk (S $ (S $ (K $ K) $ lam x)) $ wk (lam y) $ q  ≡⟨ cong (λ t → t $ wk (lam y) $ q) wk$ ⟩
     wk S $ wk (S $ (K $ K) $ lam x) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → t $ wk (S $ (K $ K) $ lam x) $ wk (lam y) $ q) wkS ⟩
     S $ wk (S $ (K $ K) $ lam x) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ t $ wk (lam y) $ q) wk$ ⟩
     S $ (wk (S $ (K $ K)) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (lam x)) $ wk (lam y) $ q) wk$ ⟩
     S $ (wk S $ wk (K $ K) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (K $ K) $ wk (lam x)) $ wk (lam y) $ q) wkS  ⟩
     S $ (S $ wk (K $ K) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (S $ t $ wk (lam x)) $ wk (lam y) $ q) wk$ ⟩
     S $ (S $ (wk K $ wk K) $ wk (lam x)) $ wk (lam y) $ q  
        ≡⟨ cong (λ t → S $ (S $ (t $ wk K) $ wk (lam x)) $ wk (lam y) $ q) wkK ⟩
     S $ (S $ (K $ wk K) $ wk (lam x)) $ wk (lam y) $ q
        ≡⟨ cong (λ t → S $ (S $ (K $ t) $ wk (lam x)) $ wk (lam y) $ q) wkK ⟩
     S $ (S $ (K $ K) $ wk (lam x)) $ wk (lam y) $ q  ≡⟨ Sβ ⟩
     S $ (K $ K) $ wk (lam x) $ q $ (wk (lam y) $ q) ≡⟨ cong (_$ (wk (lam y) $ q)) Sβ ⟩
     K $ K $ q $ (wk (lam x) $ q) $ (wk (lam y) $ q) 
        ≡⟨ cong (λ t → t $ (wk (lam x) $ q) $ (wk (lam y) $ q)) Kβ ⟩
     K $ (wk (lam x) $ q) $ (wk (lam y) $ q) ≡⟨ Kβ ⟩
     (wk (lam x) $ q)
    ∎) i
  
lam (Sβ {x = x}{y = y}{z = z} i) =
  ext
    (wk (S $ (S $ (S $ (K $ S) $ lam x) $ lam y) $ lam z) $ q ≡⟨ cong (_$ q) wk$ ⟩
     wk (S $ (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → t $ (wk (lam z)) $ q) wk$ ⟩
     wk S $ (wk (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → t $ (wk (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q ) wkS ⟩
     S $ (wk (S $ (S $ (K $ S) $ lam x) $ lam y)) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ t $ (wk (lam z)) $ q) wk$ ⟩
     S $ (wk (S $ (S $ (K $ S) $ lam x)) $ (wk (lam y))) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ (t $ (wk (lam y))) $ (wk (lam z)) $ q ) wk$ ⟩
     S $ (wk S $ wk (S $ (K $ S) $ lam x) $ (wk (lam y))) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ (t $ wk (S $ (K $ S) $ lam x) $ (wk (lam y))) $ (wk (lam z)) $ q) wkS ⟩
     S $ (S $ wk (S $ (K $ S) $ lam x) $ (wk (lam y))) $ (wk (lam z)) $ q 
        ≡⟨ cong (λ t → S $ (S $ t $ (wk (lam y))) $ (wk (lam z)) $ q ) wk$ ⟩
     S $ (S $ (wk (S $ (K $ S)) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q 
        ≡⟨ cong (λ t → S $ (S $ (t $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q ) wk$ ⟩
     S $ (S $ (wk S $ wk (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (t $ wk (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q) wkS ⟩
     S $ (S $ (S $ wk (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (S $ t $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q) wk$ ⟩
     S $ (S $ (S $ (wk K $ wk S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (S $ (t $ wk S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q ) wkK ⟩
     S $ (S $ (S $ (K $ wk S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  
        ≡⟨ cong (λ t → S $ (S $ (S $ (K $ t) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q ) wkS ⟩
     S $ (S $ (S $ (K $ S) $ wk (lam x)) $ wk (lam y)) $ wk (lam z) $ q  ≡⟨ Sβ ⟩
     S $ (S $ (K $ S) $ wk (lam x)) $ wk (lam y) $ q $ (wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (wk (lam z) $ q)) Sβ ⟩
     S $ (K $ S) $ wk (lam x) $ q $ (wk (lam y) $ q) $ (wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (wk (lam y) $ q) $ (wk (lam z) $ q)) Sβ ⟩
     K $ S $ q $ (wk (lam x) $ q) $ (wk (lam y) $ q) $ (wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (wk (lam x) $ q) $ (wk (lam y) $ q) $ (wk (lam z) $ q)) Kβ ⟩
     S $ (wk (lam x) $ q) $ (wk (lam y) $ q) $ (wk (lam z) $ q)  ≡⟨ Sβ ⟩
     (wk (lam x) $ q) $ (wk (lam z) $ q) $ ((wk (lam y) $ q) $ (wk (lam z) $ q))
        ≡⟨ cong (λ t → (wk (lam x) $ q) $ (wk (lam z) $ q) $ t) (sym Sβ) ⟩
     (wk (lam x) $ q) $ (wk (lam z) $ q) $ (S $ wk (lam y) $ wk (lam z) $ q)  
        ≡⟨ cong (λ t → t $ (S $ wk (lam y) $ wk (lam z) $ q)) (sym Sβ) ⟩
     (S $ wk (lam x) $ wk (lam z) $ q) $ (S $ wk (lam y) $ wk (lam z) $ q)  ≡⟨ sym Sβ ⟩
     S $ (S $ wk (lam x) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (lam x) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q) (sym wkS) ⟩
     S $ (wk S $ wk (lam x) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (t $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x)) $ wk (lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ t $ (S $ wk (lam y) $ wk (lam z)) $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (wk (S $ (lam x) $ lam z)) $ (t $ wk (lam y) $ wk (lam z)) $ q) (sym wkS) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (wk S $ wk (lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (wk (S $ (lam x) $ lam z)) $ (t $ wk (lam z)) $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y) $ wk (lam z)) $ q  
        ≡⟨ cong (λ t → S $ (wk (S $ (lam x) $ lam z)) $ t $ q) (sym wk$) ⟩
     S $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q  
        ≡⟨ cong (λ t → t $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q) (sym wkS) ⟩
     wk S $ (wk (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q  
        ≡⟨ cong (λ t → t $ (wk (S $ lam y $ lam z)) $ q) (sym wk$) ⟩
     wk (S $ (S $ (lam x) $ lam z)) $ (wk (S $ lam y $ lam z)) $ q  
        ≡⟨ cong (_$ q) (sym wk$) ⟩
     wk ((S $ (S $ (lam x) $ lam z)) $ ((S $ lam y $ lam z))) $ q 
     ∎) i
-- i = i0 ⊢ S $ (S $ (S $ (K $ S) $ lam x) $ lam y) $ lam z
-- i = i1 ⊢ S $ (S $ lam x $ lam z) $ (S $ lam y $ lam z)
lam (wk$ {x = x}{y = y} i) = --?
  ext
    (wk (K $ (x $ y)) $ q ≡⟨ cong (_$ q) wk$ ⟩
     wk K $ wk (x $ y) $ q ≡⟨ cong (λ t → t $ wk (x $ y) $ q) wkK ⟩
     K $ wk (x $ y) $ q ≡⟨ Kβ ⟩
     wk (x $ y) ≡⟨ wk$ ⟩
     wk x $ wk y ≡⟨ cong (λ t → t $ wk y) (sym Kβ) ⟩
     K $ wk x $ q $ wk y ≡⟨ cong (λ t → K $ wk x $ q $ t) (sym Kβ) ⟩
     K $ wk x $ q $ (K $ wk y $ q) ≡⟨ sym Sβ ⟩
     S $ (K $ wk x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → t $ (K $ wk x) $ (K $ wk y) $ q) (sym wkS) ⟩
     wk S $ (K $ wk x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → wk S $ (t $ wk x) $ (K $ wk y) $ q) (sym wkK) ⟩
     wk S $ (wk K $ wk x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → wk S $ t $ (K $ wk y) $ q) (sym wk$) ⟩
     wk S $ wk (K $ x) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → t $ (K $ wk y) $ q ) (sym wk$) ⟩
     wk (S $ (K $ x)) $ (K $ wk y) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ x)) $ (t $ wk y) $ q ) (sym wkK) ⟩
     wk (S $ (K $ x)) $ (wk K $ wk y) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ x)) $ t $ q) (sym wk$)⟩
     wk (S $ (K $ x)) $ wk (K $ y) $ q 
        ≡⟨ cong (_$ q) (sym wk$) ⟩
     wk (S $ (K $ x) $ (K $ y)) $ q
     ∎
     ) i
     
--i = i0 ⊢ K $ (x $ y)
--i = i1 ⊢ S $ (K $ x) $ (K $ y)

lam (wkK i) = K $ K
lam (wkS i) = K $ S
lam (ext {x = x}{y = y} p i) = 
   ext 
     (wk (lam x) $ q ≡⟨ cong (wk (lam x) $_) (sym Kβ) ⟩
      wk (lam x) $ (K $ q $ (K $ q)) ≡⟨ cong (wk (lam x) $_) (sym Sβ) ⟩
      wk (lam x) $ (S $ K $ K $ q) ≡⟨ cong (_$ (S $ K $ K $ q)) (sym Kβ) ⟩
      K $ wk (lam x) $ q $ (S $ K $ K $ q) ≡⟨ sym Sβ ⟩
      S $ (K $ wk (lam x)) $ (S $ K $ K) $ q 
        ≡⟨ cong (λ t → t $ (K $ wk (lam x)) $ (S $ K $ K) $ q) (sym wkS) ⟩
      wk S $ (K $ wk (lam x)) $ (S $ K $ K) $ q 
        ≡⟨ cong (λ t → wk S $ (t $ wk (lam x)) $ (S $ K $ K) $ q) (sym wkK)⟩
      wk S $ (wk K $ wk (lam x)) $ (S $ K $ K) $ q 
        ≡⟨ cong (λ t → wk S $ t $ (S $ K $ K) $ q) (sym wk$) ⟩
      wk S $ wk (K $ lam x) $ (S $ K $ K) $ q 
        ≡⟨ cong (λ t → t $ (S $ K $ K) $ q) (sym wk$) ⟩
      wk (S $ (K $ lam x)) $ (S $ K $ K) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (t $ K $ K) $ q ) (sym wkS) ⟩
      wk (S $ (K $ lam x)) $ (wk S $ K $ K) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (wk S $ t $ K) $ q) (sym wkK) ⟩
      wk (S $ (K $ lam x)) $ (wk S $ wk K $ K) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (t $ K) $ q) (sym wk$)⟩
      wk (S $ (K $ lam x)) $ (wk (S $ K) $ K) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ (wk (S $ K) $ t) $ q) (sym wkK)⟩
      wk (S $ (K $ lam x)) $ (wk (S $ K) $ wk K) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ lam x)) $ t $ q) (sym wk$)⟩
      wk (S $ (K $ lam x)) $ wk (S $ K $ K) $ q 
        ≡⟨ cong (_$ q) (sym wk$) ⟩
      wk (S $ (K $ lam x) $ (S $ K $ K)) $ q 
        ≡⟨ cong (λ t → wk (S $ (K $ lam t) $ (S $ K $ K)) $ q) (ext p) ⟩
      wk (S $ (K $ lam y) $ (S $ K $ K)) $ q 
        ≡⟨ cong (_$ q) wk$ ⟩
      wk (S $ (K $ lam y)) $ wk (S $ K $ K) $ q 
        ≡⟨ cong (λ t → t $ wk (S $ K $ K) $ q) wk$ ⟩
      wk S $ wk (K $ lam y) $ wk (S $ K $ K) $ q 
        ≡⟨ cong (λ t → t $ wk (K $ lam y) $ wk (S $ K $ K) $ q ) wkS ⟩
      S $ wk (K $ lam y) $ wk (S $ K $ K) $ q ≡⟨ Sβ ⟩
      wk (K $ lam y) $ q $ (wk (S $ K $ K) $ q) 
        ≡⟨ cong (λ t → t $ q $ (wk (S $ K $ K) $ q)) wk$ ⟩
      wk K $ wk (lam y) $ q $ (wk (S $ K $ K) $ q) 
        ≡⟨ cong (λ t → t $ wk (lam y) $ q $ (wk (S $ K $ K) $ q)) wkK ⟩
      K $ wk (lam y) $ q $ (wk (S $ K $ K) $ q) 
        ≡⟨ cong (λ t → t $ (wk (S $ K $ K) $ q)) Kβ ⟩
      wk (lam y) $ (wk (S $ K $ K) $ q) 
        ≡⟨ cong (λ t → wk (lam y) $ (t $ q)) wk$ ⟩
      wk (lam y) $ (wk (S $ K) $ wk K $ q) 
        ≡⟨ cong (λ t → wk (lam y) $ (t $ wk K $ q) ) wk$ ⟩
      wk (lam y) $ (wk S $ wk K $ wk K $ q) 
        ≡⟨ cong (λ t → wk (lam y) $ (t $ wk K $ wk K $ q)) wkS ⟩
      wk (lam y) $ (S $ wk K $ wk K $ q) 
        ≡⟨ cong (λ t → wk (lam y) $ (S $ t $ wk K $ q)) wkK ⟩
      wk (lam y) $ (S $ K $ wk K $ q) 
        ≡⟨ cong (wk (lam y) $_) Sβ ⟩
      wk (lam y) $ (K $ q $ (wk K $ q)) 
        ≡⟨ cong (wk (lam y) $_) Kβ ⟩
      wk (lam y) $ q
      ∎) i

