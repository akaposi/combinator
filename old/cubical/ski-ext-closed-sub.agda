{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)

open import ski-ext-closed
import stlc as L
--infixr 10 _⇒_
--infixl 10 _$_

Sub : Con → Con → Set
Sub Γ L.ε = Unit
Sub Γ (Δ L.▹ τ) = Sub Γ Δ × SKV Γ τ

wks : Sub Δ Γ → Sub (Δ ▹ τ) Γ
wks {Γ = L.ε}     tt      = tt
wks {Γ = Γ L.▹ x} (s , t) = wks s , wk t

abstract
--module _ where
    lift-lamKβ : _≡_ {A = SKV Γ ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
                 (S $ (K $ S) $ (S $ (K $ K)))
                 K
    lift-lamKβ {Γ = Γ} = 
        let p = lift-ctx-≡ {Γ = Γ} lamKβ 
        in (sym (lift-ctx-$ 
                 ∙ cong₂ _$_ (lift-ctx-$ 
                              ∙ lift-$₁ lift-ctx-S
                              ∙ cong (S $_) (lift-ctx-$ 
                                             ∙ cong₂ _$_ lift-ctx-K lift-ctx-S)) 
                             (lift-ctx-$ 
                              ∙ lift-$₁ lift-ctx-S
                              ∙ cong (S $_) (lift-ctx-$
                                             ∙ cong₂ _$_ lift-ctx-K lift-ctx-K) ))
           ∙ p ∙ lift-ctx-K)

    lift-lamwk$ : _≡_ {A = SKV Γ ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
                  (S $ (K $ K))
                  (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))
    lift-lamwk$ {Γ = Γ} =
        let p = lift-ctx-≡ {Γ = Γ} lamwk$
            in ((sym (lift-ctx-$ 
                      ∙ cong₂ _$_ lift-ctx-S 
                                  (lift-ctx-$ 
                                   ∙ cong₂ _$_ lift-ctx-K 
                                               lift-ctx-K)))
               ∙ p
               ∙ lift-ctx-$
               ∙ cong₂ 
                  _$_ 
                  (lift-ctx-$ 
                   ∙ cong₂ _$_ 
                       lift-ctx-S
                       (lift-ctx-$ 
                        ∙ cong₂ _$_
                          (lift-ctx-$ 
                           ∙ cong₂ _$_ 
                             lift-ctx-S 
                             (lift-ctx-$ 
                              ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))
                          (lift-ctx-$ 
                           ∙ cong₂ _$_
                             (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                lift-ctx-S
                                (lift-ctx-$ ∙ cong₂ _$_ lift-ctx-K lift-ctx-K))
                             (lift-ctx-$ 
                              ∙ cong₂ _$_
                                (lift-ctx-$ 
                                 ∙ cong₂ _$_ 
                                   lift-ctx-S 
                                   (lift-ctx-$ 
                                    ∙ cong₂ _$_ 
                                      lift-ctx-K
                                      lift-ctx-S))
                                lift-ctx-K))
                       ))
                  (lift-ctx-$ ∙ cong₂ _$_ lift-ctx-K lift-ctx-K))


    lift-lamSβ : _≡_ {A = SKV Γ ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
                 (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
                 (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))
    lift-lamSβ {Γ = Γ} =
        let p = lift-ctx-≡ {Γ = Γ} lamSβ
        in ((sym (lift-ctx-$ 
                  ∙ cong₂ _$_ 
                    (lift-ctx-$ 
                     ∙ cong₂ _$_ 
                             lift-ctx-S
                             (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                      lift-ctx-K
                                      (lift-ctx-$ 
                                       ∙ cong₂ _$_ 
                                               lift-ctx-S
                                               (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-S)))) 
                    (lift-ctx-$ 
                     ∙ cong₂ _$_ 
                             (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                      lift-ctx-S
                                      (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-S))
                             (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                      lift-ctx-S
                                      (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-S))))) 
            ∙ p 
            ∙ lift-ctx-$
            ∙ cong₂ _$_
              (lift-ctx-$
               ∙ cong₂ _$_
                 lift-ctx-S
                 (lift-ctx-$
                  ∙ cong₂ _$_
                    (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                      lift-ctx-S
                                      (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-S))
                    (lift-ctx-$
                     ∙ cong₂ _$_
                       (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                      lift-ctx-S
                                      (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-K))
                       (lift-ctx-$
                        ∙ cong₂ _$_
                          (lift-ctx-$ 
                              ∙ cong₂ _$_ 
                                      lift-ctx-S
                                      (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-S))
                          (lift-ctx-$
                           ∙ cong₂ _$_
                             (lift-ctx-$
                              ∙ cong₂ _$_
                                lift-ctx-S
                                (lift-ctx-$
                                 ∙ cong₂ _$_
                                   lift-ctx-K
                                   (lift-ctx-$ 
                                    ∙ cong₂ _$_ 
                                      lift-ctx-S
                                      (lift-ctx-$ 
                                                ∙ cong₂ _$_ 
                                                        lift-ctx-K
                                                        lift-ctx-S))))
                             lift-ctx-S)))))
              (lift-ctx-$ ∙ cong₂ _$_ lift-ctx-K lift-ctx-S))

    lift-lamext : _≡_ {A = SKV Γ ((τ ⇒ σ) ⇒ τ ⇒ σ)}
                  (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = τ})))
                  (S $ K $ K {σ = τ})
    lift-lamext = 
        let p = lift-ctx-≡ lamext 
        in (sym (lift-ctx-$ 
                 ∙ cong₂ _$_ 
                   (lift-ctx-$ 
                    ∙ cong₂ _$_ lift-ctx-S 
                      (lift-ctx-$ 
                       ∙ cong₂ _$_
                         (lift-ctx-$ 
                          ∙ cong₂ _$_ 
                            lift-ctx-S
                            (lift-ctx-$ 
                             ∙ cong₂ _$_ 
                               lift-ctx-K
                               lift-ctx-S))
                         lift-ctx-K))
                   (lift-ctx-$ 
                    ∙ cong₂ _$_
                      lift-ctx-K
                      (lift-ctx-$ 
                       ∙ cong₂ _$_
                         (lift-ctx-$ 
                          ∙ cong₂ _$_ 
                            lift-ctx-S
                            lift-ctx-K)
                         lift-ctx-K))))
           ∙ p
           ∙ lift-ctx-$
           ∙ cong₂ _$_ 
             (lift-ctx-$ ∙ cong₂ _$_ lift-ctx-S lift-ctx-K)
             lift-ctx-K

_[_] : SKV Γ τ → Sub Δ Γ → SKV Δ τ
_[_] q (s , t) = t
_[_] (wk t) (s , _) = t [ s ]
_[_] K s = K
_[_] S s = S
_[_] (t $ t₁) s = t [ s ] $ t₁ [ s ]
_[_] (Kβ {x = x}{y} i) s = Kβ {x = x [ s ]}{y = y [ s ]} i
_[_] (Sβ {x = x}{y}{z} i) s = Sβ {x = x [ s ]}{y = y [ s ]}{z = z [ s ]} i
_[_] (wk$ {x = x}{y} i) (s , _) = x [ s ] $ y [ s ]
_[_] (wkK i) s = K
_[_] (wkS i) s = S
-- Lifting exercises
_[_] (lamKβ i) s = lift-lamKβ i
_[_] (lamSβ i) s = lift-lamSβ i
_[_] (lamwk$ i) s = lift-lamwk$ i
_[_] (lamext i) s = lift-lamext i
_[_] (skvSet a b c d i j) s = skvSet (a [ s ]) (b [ s ]) (cong (_[ s ]) c) (cong (_[ s ]) d) i j

_∘_ : Sub Ψ Δ → Sub Γ Ψ → Sub Γ Δ
_∘_ {Δ = L.ε} tt p = tt
_∘_ {Δ = Δ L.▹ x} (s , t) p = (s ∘ p) , t [ p ]

[∘] : ∀ {t : SKV Γ τ}{s : Sub Ψ Γ}{p : Sub Δ Ψ} 
    → t [ s ∘ p ] ≡ t [ s ] [ p ]
[∘] {t = q} {s} = refl
[∘] {t = wk t} {s , a} = [∘] {t = t}
[∘] {t = K} {s} = refl
[∘] {t = S} {s} = refl
[∘] {t = t $ t₁} {s} = cong₂ _$_ ([∘] {t = t}) ([∘] {t = t₁})
[∘] {t = Kβ {x = x}{y} i} {s}{p} j = isSet→isSet' skvSet eq0- eq1- Kβ Kβ i j
  where
    eq0- : K $ x [ s ∘ p ] $ y [ s ∘ p ] ≡ K $ x [ s ] [ p ] $ y [ s ] [ p ]
    eq0- = cong₂ _$_ (cong (K $_) ([∘] {t = x})) ([∘] {t = y})

    eq1- : x [ s ∘ p ] ≡ x [ s ] [ p ]
    eq1- = [∘] {t = x}

[∘] {t = Sβ {x = x}{y}{z} i} {s}{p} j = isSet→isSet' skvSet eq0- eq1- Sβ Sβ i j
  where
    eq0- : S $ x [ s ∘ p ] $ y [ s ∘ p ] $ z [ s ∘ p ] 
           ≡ S $ x [ s ] [ p ] $ y [ s ] [ p ] $ z [ s ] [ p ]
    eq0- = cong₂ _$_ (cong₂ _$_ (cong (S $_) ([∘] {t = x})) ([∘] {t = y})) ([∘] {t = z})

    eq1- : x [ s ∘ p ] $ z [ s ∘ p ] $ (y [ s ∘ p ] $ z [ s ∘ p ])
         ≡ x [ s ] [ p ] $ z [ s ] [ p ] $ (y [ s ] [ p ] $ z [ s ] [ p ])
    eq1- = cong₂ _$_ (cong₂ _$_ ([∘] {t = x}) ([∘] {t = z})) 
                     (cong₂ _$_ ([∘] {t = y}) ([∘] {t = z}))
[∘] {t = wk$ {x = x}{y} i} {s , _}{p} j = isSet→isSet' skvSet eq0- eq0- refl refl i j
  where
    eq0- : x [ s ∘ p ] $ y [ s ∘ p ] ≡ x [ s ] [ p ] $ y [ s ] [ p ]
    eq0- = cong₂ _$_ ([∘] {t = x}) ([∘] {t = y})

[∘] {t = wkK i} {s} = refl
[∘] {t = wkS i} {s} = refl
[∘] {t = lamKβ i} {s}{p} j = isSet→isSet' skvSet eq0- eq1- lift-lamKβ eq-1 i j
  where
    eq0- : S $ (K $ S) $ (S $ (K $ K)) ≡ S $ (K $ S) $ (S $ (K $ K))
    eq0- = refl

    eq1- : K ≡ K
    eq1- = refl

    eq-1 : S $ (K $ S) $ (S $ (K $ K)) [ p ] ≡ K [ p ]
    eq-1 = cong (λ x → x [ p ]) lift-lamKβ

-- lamKβ i j
[∘] {t = lamSβ i} {s}{p} j = isSet→isSet' skvSet refl refl lift-lamSβ (cong (_[ p ]) lift-lamSβ) i j

[∘] {t = lamwk$ i} {s}{p} j = isSet→isSet' skvSet eq0- refl lift-lamwk$ eq-1 i j
  where
    eq0- : S $ (K $ K) ≡ S $ (K $ K)
    eq0- = refl

    eq-1 : (S $ (K $ K)) [ p ]
           ≡ (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K)) [ p ]
    eq-1 = cong (λ x → x [ p ]) lift-lamwk$

[∘] {t = lamext i} {s}{p} j =
  isSet→isSet' skvSet refl refl lift-lamext (λ i → lift-lamext i [ p ])  i j

[∘] {t = skvSet a b c d i j}{s}{p} k = skvSet
                                          ([∘] {t = a}{s}{p} k)
                                          ([∘] {t = b}{s}{p} k)
                                          (λ i₁ → [∘] {t = c i₁}{s}{p} k)
                                          (λ i₁ → [∘] {t = d i₁}{s}{p} k)
                                          i j


id : Sub Γ Γ
id {Γ = L.ε} = tt
id {Γ = Γ L.▹ x} = wks id , q

-- [wks-id] : ∀ {x : SKV Γ τ} → x [ wks {τ = σ} id ] ≡ wk x
-- [wks-id] {Γ = L.ε} = ?
-- [wks-id] {Γ = Γ L.▹ x} = ?

