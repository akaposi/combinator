{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Bool

open import stlc as L using  (Ty; Con; ε; _▹_; nat; _⇒_)

infixl 10 _$_
infixl 5 _,s_

variable
  τ σ δ   : Ty
  Γ Δ Ξ Ψ : Con
  b b′ : Bool

data Tm : Bool → Con → Ty → Type
data Sub : Bool → Con → Con → Type
Code = Tm false
Quot = Tm true

data Sub where
  id    : Sub b Γ Γ
  _∘_   : Sub b Ξ Δ → Sub b Γ Ξ → Sub b Γ Δ
  _,s_  : Sub b Γ Δ → Tm b Γ τ → Sub b Γ (Δ ▹ τ)
  π₁    : Sub b (Γ ▹ τ) Γ

  ass   : ∀ {γ : Sub b Ξ Δ}{φ : Sub b Ψ Ξ}{ψ : Sub b Γ Ψ}
        → γ ∘ (φ ∘ ψ) ≡ (γ ∘ φ) ∘ ψ
  idl   : {γ : Sub b Γ Δ} → id ∘ γ ≡ γ
  idr   : {γ : Sub b Γ Δ} → γ ∘ id ≡ γ

  ▹βs   : {γ : Sub true Δ Γ}{t : Tm true Δ τ} → π₁ ∘ (γ ,s t) ≡ γ

data Tm where
  q     : Tm b (Γ ▹ τ) τ 
  lam   : (b : Bool) → Code (Γ ▹ τ) σ  → Tm b Γ (τ ⇒ σ)
  _$_   : Tm b Γ (τ ⇒ σ) → Tm b Γ τ → Tm b Γ σ

  -- These two cases collapse, as follows:
  -- _[_] : Tm b Γ τ → Sub b′   Δ Γ → Tm (b ∧ b′) Δ τ
  -- _[_] : Tm b Γ τ → Sub true Δ Γ → Tm true     Δ τ
  -- b b′ b′=1=>1 b∧b′ = b′
  -- 0 0    ?      0
  -- 0 1    1      1
  -- 1 0    ?      0
  -- 1 1    1      1
  _[_]  : Tm b Γ τ → Sub b′ Δ Γ → Tm b′ Δ τ

  [∘]   : {γ : Sub b′ Ξ Γ}{φ : Sub b′ Δ Ξ} → {t : Tm b Γ τ} 
        → t [ γ ∘ φ ] ≡ t [ γ ] [ φ ]
  [id]  : {t : Tm b Γ τ} → t [ id ] ≡ t
  ⇒βt   : {t : Code (Γ ▹ τ) σ} {γ : Sub true Δ Γ}
          {a : Quot Δ τ} → (lam true t)[ γ ] $ a ≡ t [ γ ,s a ]
  $[]   : {t : Tm b Γ (τ ⇒ σ)} {a : Tm b Γ τ} {γ : Sub b′ Δ Γ}
        → (t $ a)[ γ ] ≡ t [ γ ] $ a [ γ ]

  -- Transition between Code and Quot for variables and lam.
  ▹βt   : {γ : Sub b Γ Δ}{t : Tm b Γ τ} → q {b = b′} [ γ ,s t ] ≡ t 
  ▹βt′  : {γ : Sub b Δ (Γ ▹ τ)}{ φ : Sub false Γ (Δ ▹ σ) } 
        → q {b = false} [ φ ∘ π₁ ] [ γ ] ≡ q {b = false} [ φ ] [ π₁ ∘ γ ]
  lam[] : {t : Code (Γ ▹ τ) σ} {γ : Sub true Δ Γ}
        → (lam false t)[ γ ] ≡ (lam true t)[ γ ]

pattern lam-code x = lam false x
pattern lam-quot x = lam true x

pattern v₀ {b} = q  {b}
pattern v₁ {b} = v₀ {b} [ π₁ ]
pattern v₂ {b} = v₁ {b} [ π₁ ]
pattern v₃ {b} = v₂ {b} [ π₁ ]

pattern _[_]₀ x y = _[_] {b′ = false}  x y
pattern _[_]₁ x y = _[_] {b′ = true}   x y


module Tests where

    -- id
    test-id : Code Γ (τ ⇒ τ)
    test-id = lam-code v₀

    -- k
    test-k : Code Γ (τ ⇒ σ ⇒ τ)
    test-k = lam-code (lam-code (v₁ {false}))

    test-id$ : (x : Code Γ τ) → (test-id $ x) [ id {true} ] ≡ x [ id ]
    test-id$ x = 
      (lam-code q $ x) [ id ] ≡⟨ $[] ⟩
      lam-code q [ id ] $ (x [ id ]) ≡⟨ cong (_$ (x [ id ])) lam[] ⟩
      lam-quot q [ id ] $ (x [ id ]) ≡⟨ ⇒βt ⟩
      q [ id ,s (x [ id ]) ] ≡⟨ ▹βt ⟩
      x [ id ]
      ∎

    test-k$ : (x : Code Γ τ) (y : Code Γ σ) → (test-k $ x $ y) [ id {true} ] ≡ x [ id ]
    test-k$ x y = 
      (lam-code (lam-code (q [ π₁ ])) $ x $ y) [ id ] ≡⟨ $[] ⟩
      (lam-code (lam-code (q [ π₁ ])) $ x) [ id ] $ (y [ id ]) ≡⟨ cong (_$ (y [ id ])) $[] ⟩
      lam-code (lam-code (q [ π₁ ])) [ id ] $ (x [ id ]) $ (y [ id ]) 
        ≡⟨ cong (λ t → t $ x [ id ] $ y [ id ]) lam[] ⟩
      lam-quot (lam-code (q [ π₁ ])) [ id ] $ (x [ id ]) $ (y [ id ]) 
        ≡⟨ cong (_$ (y [ id ])) ⇒βt ⟩
      lam-code (q [ π₁ ]) [ id ,s (x [ id ]) ] $ (y [ id ]) 
        ≡⟨ cong (_$ (y [ id ])) lam[] ⟩
      lam-quot (q [ π₁ ]) [ id ,s (x [ id ]) ] $ (y [ id ]) ≡⟨ ⇒βt ⟩
      q [ π₁ ] [ id ,s (x [ id ]) ,s (y [ id ]) ] 
        ≡⟨ cong (λ t → q {false} [ t ] [ id {true} ,s (x [ id ]) ,s (y [ id ]) ]) (sym idl) ⟩
      q [ id ∘ π₁ ] [ id ,s (x [ id ]) ,s (y [ id ]) ]
        ≡⟨ ▹βt′ ⟩
      q [ id ] [ π₁ ∘ (id ,s (x [ id ]) ,s (y [ id ])) ]
        ≡⟨ cong (λ t → q {false} [ id ]₀ [ t ]₁) ▹βs ⟩
      q [ id ] [ id ,s (x [ id ]) ] 
        ≡⟨ cong (_[ id ,s (x [ id ]) ]₁) [id]  ⟩
      q [ id ,s (x [ id ]) ] ≡⟨ ▹βt ⟩
      x [ id ]
      ∎


