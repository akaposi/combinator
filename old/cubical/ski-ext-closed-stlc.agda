{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)

open import ski-ext-closed
open import ski-ext-closed-sub
open import ski-ext-closed-sub-wk
open import ski-ext-closed-sub-eta
import stlc as L

pattern v₀ = L.q
pattern v₁ = v₀ L.[ L.π₁ ]
pattern v₂ = v₁ L.[ L.π₁ ]
pattern v₃ = v₂ L.[ L.π₁ ]

ski-lam : SKV Γ τ → L.Tm Γ τ
ski-lam q = L.q
ski-lam (wk x) = ski-lam x L.[ L.π₁ ]
ski-lam K = L.lam (L.lam v₁)
ski-lam S = L.lam (L.lam (L.lam (v₂ L.$ v₀ L.$ (v₁ L.$ v₀))))
ski-lam (x $ x₁) = ski-lam x L.$ ski-lam x₁
ski-lam (Kβ {x = x}{y} i) = L.Tests.test-k (ski-lam x) (ski-lam y) i
ski-lam (Sβ i) = ?
ski-lam (wk$ {x = x}{y} i) = L.sub-$ L.π₁ (ski-lam x) (ski-lam y) i
ski-lam (wkK i) = ?
ski-lam (wkS i) = ?
ski-lam (lamKβ i) = ?
ski-lam (lamSβ i) = ?
ski-lam (lamwk$ i) = ?
ski-lam (lamext i) = ?
ski-lam (skvSet x x₁ x₂ y i i₁) = ?


lam-ski : L.Tm Γ τ → SKV Γ τ
sub-ski : L.Sub Γ Δ → Sub Γ Δ

sub-ski L.id = id
sub-ski (s L.∘ s₁) = sub-ski s ∘ sub-ski s₁
sub-ski (s L.,s x) = sub-ski s , lam-ski x
sub-ski L.π₁ = p
sub-ski (L.ass i) = ?
sub-ski (L.idl {γ = γ} i) = idl {s = sub-ski γ} i
sub-ski (L.idr {γ = γ} i) = idr {s = sub-ski γ} i
sub-ski (L.▹βs {γ = γ}{t} i) = p∘ {s = sub-ski γ}{x = lam-ski t} i
sub-ski (L.▹η {γ = γ} i) = ? -- carefully wrap p∘


lam[p]$q : (f : SKV (Γ ▹ τ) σ) → lam (f [ p ]) $ q ≡ f
lam[p]$q f = cong (λ t → lam t $ q) (sym [p]) ∙ Kβ


lam-ski v₀ = q
lam-ski (L.lam x) = lam (lam-ski x)
lam-ski (L.app x) = wk (lam-ski x) $ q
lam-ski (x L.[ x₁ ]) = lam-ski x [ sub-ski x₁ ]
lam-ski (L.[∘] {γ = γ}{φ}{t} i) = [∘] {t = lam-ski t}{s = sub-ski γ}{p = sub-ski φ} i
lam-ski (L.[id] {t = t} i) = [id] {x = lam-ski t} i
lam-ski (L.lamβ {t = t} i) = wk$q (lam-ski t) i
lam-ski (L.lamη {t = t} i) = lamext$x {x = lam-ski t} i
lam-ski (L.▹βt {t = t} i) = lam-ski t
lam-ski (L.lam[] i) = ?

ski-lam∘lam-ski : ∀ {x : L.Tm Γ τ} → ski-lam (lam-ski x) ≡ x
ski-lam∘lam-ski {x = v₀} = refl
ski-lam∘lam-ski {x = L.lam x} = ?
ski-lam∘lam-ski {x = L.app x} = cong (λ t → (t L.[ L.π₁ ]) L.$ v₀) (ski-lam∘lam-ski {x = x}) ∙ ?
ski-lam∘lam-ski {x = x L.[ x₁ ]} = ?
ski-lam∘lam-ski {x = L.[∘] i} = ?
ski-lam∘lam-ski {x = L.[id] i} = ?
ski-lam∘lam-ski {x = L.lamβ i} = ?
ski-lam∘lam-ski {x = L.lamη i} = ?
ski-lam∘lam-ski {x = L.▹βt i} = ?
ski-lam∘lam-ski {x = L.lam[] i} = ?

lam-ski∘ski-lam : ∀ {x : SKV Γ τ} → lam-ski (ski-lam x) ≡ x
lam-ski∘ski-lam {x = q} = refl
lam-ski∘ski-lam {x = wk x} = ?
lam-ski∘ski-lam {x = K} = ?
lam-ski∘ski-lam {x = S} = ?
lam-ski∘ski-lam {x = x $ x₁} = ?
lam-ski∘ski-lam {x = Kβ i} = ?
lam-ski∘ski-lam {x = Sβ i} = ?
lam-ski∘ski-lam {x = wk$ i} = ?
lam-ski∘ski-lam {x = wkK i} = ?
lam-ski∘ski-lam {x = wkS i} = ?
lam-ski∘ski-lam {x = lamKβ i} = ?
lam-ski∘ski-lam {x = lamSβ i} = ?
lam-ski∘ski-lam {x = lamwk$ i} = ?
lam-ski∘ski-lam {x = lamext i} = ?
lam-ski∘ski-lam {x = skvSet x x₁ x₂ y i i₁} = ?
