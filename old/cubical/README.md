
Syntax (we always have the 3 axioms)
  Common                (Ty,Con,⇒*)
  C                     η on/off   C,Cη
  C-var                 η on/off
  C-wk                  η on/off
  C-wk-ext              (η always on)
  L                     η on/off
Conversion
  C=C-var          C.Tm A = C-var.Tm ∙ A                   Tamas
  C-var=C-wk       C-var.Tm Γ A = C-wk.Tm Γ A              Tamas
  C-wk=L           C-wk.Tm Γ A = L.Tm Γ A                  Artem
  L=L              L.Tm Γ A = L.Tm ∙ (Γ ⇒* A)              Tamas,Ambrus
  C-wk-η=C-wk-ext  C-wk-η.Tm Γ A = C-wk-ext.Tm Γ A         done
C=L                C.Tm (Γ ⇒* A) = L.Tm Γ A
Other/Old


story for the paper:
  proof pearl (an old problem, but in a new gown)
  quotiented (algebraic) syntax
  equations between syntaxes are the same as isomorphism (H.O.T.T. style universal algebra, application of "structural identity principle")
    unlike for dependent types, there is no coherence problem here because types don't include equations
  sweet spot halfway between Hindley-Seldin and a categorical reconstruction
  old belief that lambda calculus is not algebraic

  papers to cite (and we should fully understand them!):
  - Selinger: The lambda calculus is algebraic. https://www.mscs.dal.ca/~selinger/papers/combinatory.pdf
    - this says "extensional models do not form an algebraic variety"
    - Selinger is wrong because if you algebraise lambda calculus properly, then closed
      terms do not form a subalgebra
      - he cites Plotkin 1974 who shows that closed (untyped) lambda terms are not extensional
      - it is completely clear that closed (typed) lambda terms are not extensional, i.e. for
        f,g:Tm ∙ (A ⇒ B) we have

        (∀Δ.(a:Tm Δ A) → f[ε]$a = g[ε]$a) → f = g

        but we don't have

        ((a : Tm ∙ A) → f$a = g$a) → f = g

        e.g. if A=Nat, f = lam q, g = lam (q + 0), we have the premise but not the conclusion
  - Hyland: Classical lambda calculus in modern dress. https://arxiv.org/pdf/1211.5762.pdf

# Systems that we consider

* **weak sCwF**
  We have this in `cbv-stlc.agda` (not sure we need it).

* **STLC**
  This is sCwF with with lam,_$_,β,η which is defined in `stlc.agda`.

* **STCL₀**
  Combinatory calculus without variables.  This is **not defined**,
  but we can easily derive this from `ski.agda` if we need to.
  
* **STCL₁**
  Combinatory calculus with variables, defined in `ski.agda`.

* **STCL₂**
  Combinatory calculus with variables and single weakening.
  This is defined in `ski-ext` (but we also postulate extensionality
  and show how to obtain lam).

  In `ski-ext-closed*.agda` files we postulate the same system
  where extensionality is given by 4 closed equations,
  and we can derive lam and ext.

* **STCL₂'**
  Combinatory calculus w/ variables and all weakenings.
  This is **not defined**, but I am not sure we need this.

* **STCL₃**
  Proper sCwF with $,K,S that includes defines substitution as
  an operator.  This is **not defined** yet, but we can easily
  derive this from `stlc.agda` and `ski-ext.agda`.

# Translations between the systems

* STCL₀ ↔︎ STCL₁
  Combinators with variables in empty context are the same
  as combinators without variables.  Do we need this?

* STCL₁ ↔︎ STCL₂
  In STCL₁ weakening is defined externally, so we
  can translate one into the other (modulo extensionality).
  (all other constructors are the same)

* STCL₂ ↔︎ STCL₃
  In STCL₂ substitution is defined externally, so we
  can translate one into the other.

* STCL₂+ext ↔︎ STLC
  Combinators with extensionality define by closed equations
  correspond to STLC.  This is quite interesting, I think.

  I started putting the proof into `ski-ext-closed-stlc.agda`.



  
  


