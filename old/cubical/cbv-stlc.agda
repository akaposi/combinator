{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Bool

open import stlc as L using  (Ty; Con; ε; _▹_; nat; _⇒_)

infixl 10 _$_
infixl 5 _,s_
infixl 5 _∈_


variable
  τ σ   : Ty
  Γ Δ : Con
  b : Bool

data _∈_ : Ty → Con → Set where
  here : τ ∈ Γ ▹ τ
  there : τ ∈ Γ → τ ∈ Γ ▹ σ

data Tm : Bool → Con → Ty → Type
data Sub : Con → Con → Type

PreTerm = Tm false
Term = Tm true

data Sub where
  id    : Sub Γ Γ
  _,s_  : Sub Γ Δ → Term Γ τ → Sub Γ (Δ ▹ τ)

data Tm where
  var   : τ ∈ Γ → PreTerm Γ τ
  lam   : (b : Bool) → PreTerm (Γ ▹ τ) σ  → Tm b Γ (τ ⇒ σ)
  _$_   : Tm b Γ (τ ⇒ σ) → Tm b Γ τ → Tm b Γ σ

  -- Substitution always "returns" Terms
  _[_]  : Tm b Γ τ → Sub Δ Γ → Term Δ τ

  ⇒βt   : {t : PreTerm (Γ ▹ τ) σ} {γ : Sub Δ Γ}
          {a : Term Δ τ} → (lam true t)[ γ ] $ a ≡ t [ γ ,s a ]
  $[]   : {t : Tm b Γ (τ ⇒ σ)} {a : Tm b Γ τ} {γ : Sub Δ Γ}
        → (t $ a)[ γ ] ≡ t [ γ ] $ a [ γ ]

  -- Variables under substitution and admitting that lam is a normalform.
  ▹βvz  : {γ : Sub Γ Δ}{t : Term Γ τ} → (var here) [ γ ,s t ] ≡ t 
  ▹βvs  : {v : τ ∈ Δ}{γ : Sub Γ Δ}{t : Term Γ σ} 
        → (var (there v)) [ γ ,s t ] ≡ (var v) [ γ ] 
  lam[] : {t : PreTerm (Γ ▹ τ) σ} {γ : Sub Δ Γ}
        → (lam false t)[ γ ] ≡ (lam true t)[ γ ]

pattern pre-lam  x = lam false x
pattern term-lam x = lam true x

pattern v₀′ = here 
pattern v₁′ = there v₀′
pattern v₂′ = there v₁′

pattern v₀ = var v₀′
pattern v₁ = var v₁′
pattern v₂ = var v₂′


module Tests where
    -- id
    test-id : PreTerm Γ (τ ⇒ τ)
    test-id = pre-lam v₀ 

    -- k
    test-k : PreTerm Γ (τ ⇒ σ ⇒ τ)
    test-k = pre-lam (pre-lam v₁)

    test-id$ : (x : PreTerm Γ τ) → (test-id $ x) [ id ] ≡ x [ id ]
    test-id$ x = 
      (pre-lam (var v₀′) $ x) [ id ] ≡⟨ $[] ⟩
      (pre-lam (var v₀′) [ id ]) $ (x [ id ]) ≡⟨ cong (_$ (x [ id ])) lam[] ⟩
      (term-lam (var v₀′) [ id ]) $ (x [ id ]) ≡⟨ ⇒βt ⟩
      var v₀′ [ id ,s (x [ id ]) ] ≡⟨ ▹βvz ⟩
      (x [ id ])
      ∎

    test-k$ : (x : PreTerm Γ τ) (y : PreTerm Γ σ) → (test-k $ x $ y) [ id ] ≡ x [ id ]
    test-k$ x y = 
      (pre-lam (pre-lam (var v₁′)) $ x $ y) [ id ] ≡⟨ $[] ⟩
      (pre-lam (pre-lam (var v₁′)) $ x) [ id ] $ y [ id ] ≡⟨ cong (_$ (y [ id ])) $[] ⟩
      pre-lam (pre-lam (var v₁′)) [ id ] $ (x [ id ]) $ (y [ id ]) 
        ≡⟨ cong (λ t → t $ x [ id ] $ y [ id ]) lam[] ⟩
      term-lam (pre-lam (var v₁′)) [ id ] $ x [ id ] $ y [ id ] 
        ≡⟨ cong (_$ (y [ id ])) ⇒βt ⟩ 
      pre-lam (var v₁′) [ id ,s (x [ id ]) ] $ y [ id ]
        ≡⟨ cong (_$ (y [ id ])) lam[] ⟩
      term-lam (var v₁′) [ id ,s (x [ id ]) ] $ y [ id ]
        ≡⟨ ⇒βt ⟩
      var v₁′ [ id ,s (x [ id ]) ,s (y [ id ]) ] 
        ≡⟨ ▹βvs ⟩
      var v₀′ [ id ,s (x [ id ]) ] 
        ≡⟨ ▹βvz ⟩
      x [ id ]
      ∎


⟦_⟧t : Ty → Set
⟦ nat ⟧t = ℕ
⟦ τ ⇒ σ ⟧t = ⟦ τ ⟧t → ⟦ σ ⟧t

data Env : Con → Set where
  ε   : Env ε
  _▹_ : Env Γ → ⟦ τ ⟧t → Env (Γ ▹ τ)

lookup : Env Γ → τ ∈ Γ → ⟦ τ ⟧t
lookup (ρ ▹ x) v₀′ = x
lookup (ρ ▹ _) (there x) = lookup ρ x




⟦_⟧ : Term Γ τ → Env Γ → ⟦ τ ⟧t
norm : PreTerm Γ τ → Env Γ → ⟦ τ ⟧t

env-sub : Env Γ → Sub Γ Δ → Env Δ
env-sub ρ id = ρ
env-sub ρ (s ,s x) = env-sub ρ s ▹ (⟦ x ⟧ ρ)

⟦ lam .true t ⟧ ρ x = norm t (ρ ▹ x)
⟦ f $ x ⟧ ρ = ⟦ f ⟧ ρ (⟦ x ⟧ ρ)
⟦ _[_] {b = false} t s ⟧ ρ = norm t (env-sub ρ s)
⟦ _[_] {b = true}  t s ⟧ ρ = ⟦ t ⟧ (env-sub ρ s)

⟦ ⇒βt {t = t}{γ}{a} i ⟧ ρ = norm t (env-sub ρ γ ▹ ⟦ a ⟧ ρ)
⟦ $[] {b = false}{t = t}{a}{γ} i ⟧ ρ = norm t (env-sub ρ γ) (norm a (env-sub ρ γ))
⟦ $[] {b = true}{t = t}{a}{γ} i ⟧ ρ = ⟦ t ⟧ (env-sub ρ γ) (⟦ a ⟧ (env-sub ρ γ))
⟦ ▹βvz {t = t} i ⟧ ρ = ⟦ t ⟧ ρ
⟦ ▹βvs {v = v} {γ = γ} i ⟧ ρ = lookup (env-sub ρ γ) v
⟦ lam[] {t = t}{γ} i ⟧ ρ x = norm t (env-sub ρ γ ▹ x)


norm (var x) ρ = lookup ρ x
norm (lam .false t) ρ x = norm t (ρ ▹ x)
norm (f $ x) ρ = norm f ρ (norm x ρ)


