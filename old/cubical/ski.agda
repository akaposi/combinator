{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)

import stlc as L

-- This is what stlc.org calls STCL₁ (combinators with variables).
-- In this file the data structure is called SKV.


infixr 10 _⇒_
infixl 10 _$_

Con = L.Con
Ty = L.Ty
_▹_ = L._▹_
_⇒_ = L._⇒_


variable
  τ σ δ   : Ty
  Γ Δ Ξ Ψ : Con

data _∈_ : Ty → Con → Type where
  here  : τ ∈ (Γ ▹ τ)
  there : τ ∈ Γ → τ ∈ (Γ ▹ σ) 


data SKV : Con → Ty → Type where
  var : τ ∈ Γ → SKV Γ τ
  K : SKV Γ (τ ⇒ σ ⇒ τ)
  S : SKV Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_ : SKV Γ (τ ⇒ σ) → SKV Γ τ → SKV Γ σ
  Kβ : {x : SKV Γ τ}{y : SKV Γ σ} → K $ x $ y ≡ x
  Sβ : {x : SKV Γ (τ ⇒ σ ⇒ δ)}{y : SKV Γ (τ ⇒ σ)}{z : SKV Γ τ}
     → S $ x $ y $ z ≡ x $ z $ (y $ z)

wk : SKV Γ σ → SKV (Γ ▹ τ) σ
wk (var x) = var (there x)
wk K = K
wk S = S
wk (e $ e₁) = wk e $ wk e₁
wk (Kβ {x = x}{y} i) = Kβ {x = wk x}{wk y} i
wk (Sβ {x = x}{y}{z} i) = Sβ {x = wk x}{wk y}{wk z} i


-- This we cannot yet show, as we'd need to obtain
-- extensional(-isch) equalities for Kβ and Sβ.  See comments
-- in the last two cases.
-- wk : SKV (Γ ▹ τ) σ → SKV Γ (τ ⇒ σ)
-- wk {τ = τ} (var here) = S $ K $ K { σ = τ }
-- wk (var (there x)) = K $ (var x)
-- wk K = K $ K
-- wk S = K $ S
-- wk (x $ x₁) = S $ wk x $ wk x₁
-- wk (Kβ i) = ?  -- S (S (KK) x) y == x
-- wk (Sβ i) = ?  -- S (S (S (K S) x) y) z == S(Sxz)(Syz)


-- XXX why isn't this terminating???
skv-l : SKV Γ τ → L.Tm Γ τ
skv-l (var here) = L.q
skv-l (var (there x)) = skv-l (var x) L.[ L.π₁ ]
skv-l K = L.lam (L.lam (L.v₁))
skv-l S = L.lam (L.lam (L.lam (L.v₂ L.$ L.v₀ L.$ (L.v₁ L.$ L.v₀))))
skv-l (e $ e₁) = skv-l e L.$ skv-l e₁
skv-l (Kβ {x = x}{y} i) = cong skv-l (Kβ {x = x} {y}) i
skv-l (Sβ {x = x}{y}{z} i) = cong skv-l (Sβ {x = x}{y}{z}) i


