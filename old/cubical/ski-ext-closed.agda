{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
import Cubical.Foundations.Function as F
open import Cubical.Data.Nat
open import Cubical.Data.Unit
open import Cubical.Data.Sigma hiding (Sub)

import stlc as L

-- This is what stlc.org calls STCL₁ (combinators with variables).
-- In this file the data structure is called SKV.


infixr 10 _⇒_
infixl 10 _$_

Con = L.Con
Ty = L.Ty
infixl 10 _▹_
_▹_ = L._▹_
_⇒_ = L._⇒_
ε = L.ε


variable
  τ σ δ ι : Ty
  Γ Δ Ξ Ψ : Con


data SKV : Con → Ty → Type where
  q : SKV (Γ ▹ τ) τ
  wk : SKV Γ σ → SKV (Γ ▹ τ) σ

  K : SKV Γ (τ ⇒ σ ⇒ τ)
  S : SKV Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_ : SKV Γ (τ ⇒ σ) → SKV Γ τ → SKV Γ σ
  Kβ : {x : SKV Γ τ}{y : SKV Γ σ} → K $ x $ y ≡ x
  Sβ : {x : SKV Γ (τ ⇒ σ ⇒ δ)}{y : SKV Γ (τ ⇒ σ)}{z : SKV Γ τ}
     → S $ x $ y $ z ≡ x $ z $ (y $ z)

  wk$ : {x : SKV Γ (τ ⇒ σ)}{y : SKV Γ τ} → wk {τ = δ} (x $ y) ≡ wk x $ wk y
  wkK : wk {Γ = Γ}{τ = δ} (K {τ = τ}{σ = σ}) ≡ K
  wkS : ∀ {γ} → wk {Γ = Γ}{τ = γ} (S {τ = τ}{σ = σ}{δ = δ}) ≡ S

  -- Previously extensionality was defined as follows:
  -- ext : {x y : SKV Γ (τ ⇒ σ)} → wk x $ q ≡ wk y $ q → x ≡ y
  --
  -- instead of ext, we need:
  -- 1. lamKβ : lam (K $ x $ y) ≡ lam x
  -- unfold lam
  -- 2. lamKβ : S $ (S $ (K $ K) $ lam x) $ lam y ≡ lam x
  -- remove lam
  -- 3. lamKβ : S $ (S $ (K $ K) $ u) $ v ≡ u
  -- make it closed
  -- 4. lamKβ : lam (lam (S $ (S $ (K $ K) $ wk vz) $ vz)) ≡ K
  lamKβ : _≡_ {A = SKV ε ((ι ⇒ τ) ⇒ (ι ⇒ σ) ⇒ (ι ⇒ τ))} 
              (S $ (K $ S) $ (S $ (K $ K)))
              K

  lamSβ : _≡_ {A = SKV ε ((ι ⇒ τ ⇒ σ ⇒ δ) ⇒ (ι ⇒ τ ⇒ σ) ⇒ (ι ⇒ τ) ⇒ (ι ⇒ δ))}
              (S $ (K $(S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S))))
              (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))

  lamwk$ : _≡_ {A = SKV ε ((τ ⇒ σ) ⇒ τ ⇒ (ι ⇒ σ))}
               (S $ (K $ K))
               (S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K))

  -- Extensionality: λ v → x v ≡ x
  -- [x] .S(Kx)I = [x] .x;
  lamext : _≡_ {A = SKV ε ((τ ⇒ σ) ⇒ τ ⇒ σ)}
           (S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K {σ = τ})))
           (S $ K $ K {σ = τ})

  skvSet : isSet (SKV Γ τ)



lift-ctx : SKV ε τ → SKV Γ τ 
lift-ctx {Γ = L.ε} t = t
lift-ctx {Γ = Γ L.▹ x} t = wk (lift-ctx {Γ = Γ} t)

lift-ctx-≡ : {t u : SKV ε τ} 
           → t ≡ u → lift-ctx {Γ = Γ} t ≡ lift-ctx u
lift-ctx-≡ eq = cong lift-ctx eq


lift-ctx-$ : ∀ {t : SKV _ (τ ⇒ σ)}{u} 
           → (lift-ctx {Γ = Γ} (t $ u)) ≡ lift-ctx t $ lift-ctx u
lift-ctx-$ {Γ = L.ε} {t} {u} = refl
lift-ctx-$ {Γ = Γ L.▹ x} {t} {u} = cong wk lift-ctx-$ ∙ wk$

lift-ctx-S : lift-ctx {Γ = Γ} (S {τ = τ}{σ}{δ}) ≡ S
lift-ctx-S {Γ = L.ε} = refl
lift-ctx-S {Γ = Γ L.▹ x} = cong wk lift-ctx-S ∙ wkS

lift-ctx-K : lift-ctx {Γ = Γ} (K {τ = τ}{σ}) ≡ K
lift-ctx-K {Γ = L.ε} = refl
lift-ctx-K {Γ = Γ L.▹ x} = cong wk lift-ctx-K ∙ wkK


lift-$₁ : ∀ {t p : SKV Γ (τ ⇒ σ)}{a₁} → t ≡ p → t $ a₁ ≡ p $ a₁
lift-$₁ {a₁ = a₁} eq = cong (_$ a₁) eq

lift-$₂ : ∀ {t p : SKV Γ (τ ⇒ σ ⇒ δ)}{a₁ a₂} 
        → t ≡ p → t $ a₁ $ a₂ ≡ p $ a₁ $ a₂
lift-$₂ {a₁ = a₁}{a₂} eq = lift-$₁ (lift-$₁ eq)


lift-$₃ : ∀ {γ}{t p : SKV Γ (τ ⇒ σ ⇒ δ ⇒ γ)}{a₁ a₂ a₃} 
        → t ≡ p → t $ a₁ $ a₂ $ a₃ ≡ p $ a₁ $ a₂ $ a₃
lift-$₃ eq = lift-$₁ (lift-$₂ eq)

lift-$₄ : ∀ {γ α}{t p : SKV Γ (τ ⇒ σ ⇒ δ ⇒ γ ⇒ α)}{a₁ a₂ a₃ a₄} 
        → t ≡ p → t $ a₁ $ a₂ $ a₃ $ a₄ ≡ p $ a₁ $ a₂ $ a₃ $ a₄
lift-$₄ eq = lift-$₂ (lift-$₂ eq)

lamKβ-≡ : ∀ {x : SKV Γ (ι ⇒ τ)}{y : SKV Γ (ι ⇒ σ)}
        → S $ (S $ (K $ K) $ x) $ y ≡ x
lamKβ-≡ {x = x}{y} =
        sym (
          lift-$₂ lift-ctx-$
          ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
          ∙ lift-$₁ Sβ
          ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₂ Kβ
          ∙ lift-$₂ lift-ctx-S
          ∙ cong (λ t → S $ t $ y) 
                 (cong (_$ x) 
                       (lift-ctx-$ 
                        ∙ cong₂ _$_ lift-ctx-S 
                                    (lift-ctx-$ 
                                     ∙ cong₂ _$_ 
                                             lift-ctx-K
                                             lift-ctx-K)))
        )
        ∙ cong (λ t → t $ x $ y) (lift-ctx-≡ lamKβ)
        ∙ lift-$₂ lift-ctx-K
        ∙ Kβ


lamSβ-≡ : ∀ {x : SKV Γ (ι ⇒ τ ⇒ σ ⇒ δ)}{y : SKV Γ (ι ⇒ τ ⇒ σ)}{z : SKV Γ (ι ⇒ τ)}
        → S $ (S $ (S $ (K $ S) $ x) $ y) $ z
          ≡ S $ (S $ x $ z) $ (S $ y $ z)
lamSβ-≡ {x = x}{y}{z} =
        sym (
          lift-$₃ lift-ctx-$
          ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
          ∙ lift-$₂ Sβ
          ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₃ Kβ
          ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
          ∙ lift-$₁ Sβ
          ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₂ Kβ
          ∙ lift-$₂ lift-ctx-S
          ∙ cong (λ t → S $ t $ z)
                 (
                    lift-$₂ lift-ctx-$
                    ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
                    ∙ lift-$₁ Sβ
                    ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
                    ∙ lift-$₂ Kβ
                    ∙ lift-$₂ lift-ctx-S
                    ∙ cong (λ t → S $ t $ y) 
                           (
                             lift-$₁ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
                             ∙ cong (λ t → S $ t $ x) 
                                    (lift-ctx-$ ∙ cong₂ _$_ lift-ctx-K lift-ctx-S)
                           )
                 )
        ) 
        ∙ cong (λ t → t $ x $ y $ z) (lift-ctx-≡ lamSβ)
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-$ ∙ lift-$₂ lift-ctx-S)
        ∙ lift-$₃ Sβ
        ∙ lift-$₃ (lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K))
        ∙ lift-$₄ Kβ
        ∙ lift-$₄ lift-ctx-S
        ∙ lift-$₁ Sβ
        ∙ lift-$₄ lift-ctx-$
        ∙ lift-$₃ (lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S))
        ∙ lift-$₃ Sβ
        ∙ lift-$₃ (lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K))
        ∙ lift-$₄ Kβ
        ∙ lift-$₄ lift-ctx-K
        ∙ lift-$₂ Kβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ lift-ctx-S
        ∙ Sβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₁ Sβ
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₂ Kβ
        ∙ lift-$₂ lift-ctx-S
        ∙ cong₂ (λ x y → S $ x $ y)
                (lift-$₂ lift-ctx-S)
                (lift-$₃ (lift-ctx-$ 
                          ∙ lift-$₁ lift-ctx-K)
                          ∙ lift-$₂ Kβ
                          ∙ lift-$₂ lift-ctx-S)


lamwk$-≡ : ∀ {x : SKV Γ (τ ⇒ σ)}{y : SKV Γ τ}
         → K {σ = ι} $ (x $ y) ≡ S $ (K $ x) $ (K $ y)
lamwk$-≡ {ι = ι}{x = x}{y} =
        sym (
          lift-$₂ lift-ctx-$
          ∙ lift-$₃ lift-ctx-S
          ∙ Sβ
          ∙ lift-$₂ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
          ∙ lift-$₁ Kβ
          ∙ lift-$₁ lift-ctx-K
        )
        ∙ cong (λ t → t $ x $ y) (lift-ctx-≡ (lamwk$ {ι = ι}))
        ∙ lift-$₂ lift-ctx-$
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₁ Sβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ lift-ctx-S
        ∙ Sβ
        ∙ lift-$₃ lift-ctx-$
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₂ Sβ
        ∙ lift-$₄ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₃ Kβ
        ∙ lift-$₃ lift-ctx-K
        ∙ lift-$₁ Kβ
        ∙ lift-$₂ lift-ctx-$
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-S)
        ∙ lift-$₁ Sβ
        ∙ lift-$₃ (lift-ctx-$ ∙ lift-$₁ lift-ctx-K)
        ∙ lift-$₂ Kβ
        ∙ lift-$₂ lift-ctx-S
        ∙ cong₂ (λ x y → S $ x $ y)
                (
                  lift-$₁ lift-ctx-K
                )
                (
                  lift-$₂ lift-ctx-$
                  ∙ lift-$₃ lift-ctx-K
                  ∙ lift-$₁ Kβ
                  ∙ lift-$₁ lift-ctx-K
                )


lam : SKV (Γ ▹ τ) σ → SKV Γ (τ ⇒ σ)
lam {τ = τ} q = S $ K $ K {σ = τ}
lam (wk x) = K $ x
lam K = K $ K
lam S = K $ S
lam (x $ x₁) = S $ lam x $ lam x₁
lam {Γ = Γ} (Kβ {x = x}{y} i) = lamKβ-≡ {x = lam x}{lam y} i
lam (Sβ {x = x}{y}{z} i) = lamSβ-≡ {x = lam x}{lam y}{lam z} i
lam (wk$ {x = x}{y} i) = lamwk$-≡ {x = x}{y} i
lam (wkK i) = K $ K
lam (wkS i) = K $ S
lam (skvSet a b c d i j) = skvSet (lam a) (lam b) (cong lam c) (cong lam d) i j








