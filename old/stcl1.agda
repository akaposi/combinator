{-# OPTIONS --prop --rewriting #-}

module stcl1 where

open import Agda.Primitive

infix 4 _≡_
infix  3 _∎
infixr 2 _≡⟨_⟩_
infixr 5 _⇒_ _⇒*_
infixl 5 _,Σ_
infixl 6 _▹_
infixl 5 _++_

data _≡_ {i}{A : Set i}(x : A) : A → Prop i where
  refl : x ≡ x
postulate
  subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A}(e : a ≡ a') → P a → P a'
  substβ : ∀{i j}{A : Set i}{P : A → Set j}{a : A}{u : P a} → subst P refl u ≡ u
{-# BUILTIN REWRITE _≡_ #-}
{-# REWRITE substβ #-}
cong : ∀{i j}{A : Set i}{B : Set j}{a₀ a₁ : A} → (f : A → B) →  a₀ ≡ a₁ → f a₀ ≡ f a₁
cong f refl = refl
cong2 : ∀{i j k}{A : Set i}{B : Set j}{C : Set k}{a₀ a₁ : A}{b₀ b₁ : B} → (f : A → B → C) →  a₀ ≡ a₁ → b₀ ≡ b₁  → f a₀ b₀ ≡ f a₁ b₁
cong2 f refl refl = refl
trans : ∀{i}{A : Set i}{a a' a'' : A}(a= : a ≡ a')(a=' : a' ≡ a'') → a ≡ a''
trans refl refl = refl
sym : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a') → a' ≡ a
sym refl = refl
_≡⟨_⟩_ : ∀{ℓ}{A : Set ℓ}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = trans x≡y y≡z
_∎ : ∀{ℓ}{A : Set ℓ}(x : A) → x ≡ x
x ∎ = refl {x = x}
record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σ public
record ⊤ : Set where
data ⊥ : Set where
ite⊥ : ∀{i}{A : Set i} → ⊥ → A
ite⊥ ()
record ↑ {i}(A : Prop i) : Set i where
  constructor mk
  field
    un : A
open ↑

data Ty : Set where
  ι : Ty
  _⇒_ : Ty → Ty → Ty
variable
  A B C D W X Y Z : Ty

data Con : Set where
  ◆ : Con
  _▹_ : Con → Ty → Con
variable
  Γ Γ₀ Γ₁ Δ : Con

_⇒*_ : Con → Ty → Ty
◆       ⇒* A = A
(Γ ▹ C) ⇒* A = Γ ⇒* C ⇒ A

_++_ : Con → Con → Con
Γ ++ ◆ = Γ
Γ ++ Δ ▹ A = (Γ ++ Δ) ▹ A

++⇒* : ∀ Δ → (Γ ++ Δ) ⇒* A ≡ Γ ⇒* Δ ⇒* A
++⇒* ◆       = refl
++⇒* (Δ ▹ C) = ++⇒* Δ

module ₀ where

  postulate
    Tm : Ty → Set
    _$_ : Tm (A ⇒ B) → Tm A → Tm B
    K   : Tm (A ⇒ B ⇒ A)
    S   : Tm ((C ⇒ A ⇒ B) ⇒ (C ⇒ A) ⇒ C ⇒ B)
  infixl 6 _$_
  variable
    a b c d f g : Tm A
  postulate
    Kβ    : K $ a $ b ≡ a
    Sβ    : S $ f $ g $ c ≡ f $ c $ (g $ c)
  {-# REWRITE Kβ Sβ #-}

  B' : Tm ((A ⇒ B) ⇒ (C ⇒ A) ⇒ C ⇒ B)
  B' = S $ (K $ S) $ K
  C' : Tm ((A ⇒ (B ⇒ C))⇒(B ⇒ A ⇒ C))
  C' = S $ (S $ (K $ (S $ (K $ S) $ K)) $ S) $ (K $ K)

  --B' $ (B' $ S{C ⇒ B ⇒ A}) $ (S{A ⇒ C ⇒ (B ⇒ A) ⇒ A} $ (S $ (K $ S)))
  postulate
    lamKβ : ∀{C B A} → B' $ S{C}{B}{A} $ (S $ (K $ K)) ≡ K
    --lamSβ : ∀{C B A} → B' $ (B' $ S) $ (B' $ S $ (S $ (K $ S))) ≡ C' $ (B' $ (B' $ S) $ (B' $ (B' $ S) $ S)) $ S
    lamSβ : ∀{C B A} → S $ (K $ (S $ (K $ S{C}{B}{A}))) $ (S $ (K $ S) $ (S $ (K $ S{B}{A}))) ≡ S $ (S $ (K $ (S $ (K $ (S $ (K $ S) $ K)) $ S)) $ (S $ (K $ (S $ (K $ S)))$ S)) $ (K $ S)
    
  -- lamKβ' : ∀{C B A}{f : Tm (C ⇒ A)}{g : Tm (C ⇒ B)} → S {C}{B}{A} $ (S $ (K $ K) $ f) $ g ≡ f
  lamKβ' : S $ (S $ (K $ K) $ f) $ g ≡ f
  lamKβ' {f = f}{g = g} = cong (λ z → z $ f $ g) lamKβ --cong (λ z → z $ f $ g) lamKβ
  lamSβ' : S $ (S $ (S $ (K $ S) $ f) $ g) $ a ≡ S $ (S $ f $ a) $ (S $ g $ a) -- lam (S $ f $ g $ a) = lam (f $ a $ (g $ a))
  lamSβ' {f = f}{g = g}{a = a} = {!cong (λ z → z $ ?) lamSβ!}

  I : Tm (A ⇒ A)
  I {A = A} = S $ K $ K {B = A}

  K*' : ∀ Γ → Tm (A ⇒ B) → Tm (A ⇒ Γ ⇒* B)
  K*' ◆       t = t
  K*' (Γ ▹ C) t = K*' Γ (S $ (K $ K) $ t)

  K* : ∀ Γ → Tm (A ⇒ Γ ⇒* A)
  K* Γ = K*' Γ I

  var' : ∀ Γ₀ Γ₁ → Tm (A ⇒ B) → Tm (Γ₀ ⇒* A ⇒ Γ₁ ⇒* B)
  var' Γ₀ ◆        t = K* Γ₀ $ t
  var' Γ₀ (Γ₁ ▹ C) t = var' Γ₀ Γ₁ (S $ (K $ K) $ t)

  var : Tm (Γ₀ ⇒* A ⇒ Γ₁ ⇒* A)
  var {Γ₀ = Γ₀}{Γ₁ = Γ₁} = var' Γ₀ Γ₁ I

  S*' : ∀ Γ → Tm ((W ⇒ X) ⇒ Y ⇒ Z) → Tm ((Γ ⇒* W ⇒ X) ⇒ (Γ ⇒* Y) ⇒ Γ ⇒* Z)
  S*' ◆       t = t
  S*' (Γ ▹ A) t = S*' Γ (B' $ S $ (B' $ t))

  S* : Tm ((Γ ⇒* A ⇒ B) ⇒ (Γ ⇒* A) ⇒ Γ ⇒* B)
  S* {Γ = Γ} = S*' Γ I

  -- K*β
  -- S*β
  -- lamK*β
  -- lamS*β

record Model₀ : Set₁ where
  field
    Tm    : Ty → Set
    _$_   : Tm (A ⇒ B) → Tm A → Tm B
    K     : Tm (A ⇒ B ⇒ A)
    S     : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    Kβ    : {a : Tm A}{b : Tm B} → K $ a $ b ≡ a
    Sβ    : {a : Tm A}{b : Tm (A ⇒ B)}{c : Tm (A ⇒ B ⇒ C)} → S $ c $ b $ a ≡ c $ a $ (b $ a)
  infixl 6 _$_
  postulate
    ⟦_⟧₀ : ₀.Tm A → Tm A
    ⟦$⟧₀ : ⟦ ₀.b ₀.$ ₀.a ⟧₀ ≡ ⟦ ₀.b ⟧₀ $ ⟦ ₀.a ⟧₀
    ⟦K⟧₀ : ⟦ ₀.K {A = A}{B = B} ⟧₀ ≡ K
    ⟦S⟧₀ : ⟦ ₀.S {C = C}{A = A}{B = B} ⟧₀ ≡ S
  {-# REWRITE ⟦$⟧₀ ⟦K⟧₀ ⟦S⟧₀ #-}

record DepModel₀ : Set₁ where
  field
    Tm  : (A : Ty) → ₀.Tm A → Prop
    _$_ : Tm (A ⇒ B) ₀.b → Tm A ₀.a → Tm B (₀.b ₀.$ ₀.a)
    K   : Tm (A ⇒ B ⇒ A) ₀.K
    S   : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) ₀.S
  infixl 6 _$_
  postulate
    ⟦_⟧₀ : (a : ₀.Tm A) → Tm A a

module _ where
  open ₀
  cong$₀ : a ≡ b → c ≡ d → a $ c ≡ b $ d
  cong$₀ = cong2 _$_

module ₁ where
  
  postulate
    Var : Con → Ty → Set
    vz : Var (Γ ▹ A) A
    vs : Var Γ A → Var (Γ ▹ B) A

  postulate
    Tm : Con → Ty → Set
    var : Var Γ A → Tm Γ A
    _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    K   : Tm Γ (A ⇒ B ⇒ A) 
    S   : Tm Γ ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)

  infixl 6 _$_
  variable
    a b c d : Tm Γ A
    
  postulate
    Kβ : K $ a $ b ≡ a
    Sβ : S $ c $ b $ a ≡ c $ a $ (b $ a)
  {-# REWRITE Kβ Sβ #-}

module _ where
  open ₁
  cong$₁ : a ≡ b → c ≡ d → a $ c ≡ b $ d
  cong$₁ = cong2 _$_

  congvar₁ : {a a₀ : Var Γ A} → a₀ ≡ a → var a₀  ≡ var a
  congvar₁ = cong ₁.var

record Model₁ : Set₁ where
  field
    Tm : Con → Ty → Set
    Var : Con → Ty → Set
    vz : Var (Γ ▹ A) A
    vs : Var Γ A → Var (Γ ▹ B) A
    var : Var Γ A → Tm Γ A
    _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    K   : Tm Γ (A ⇒ B ⇒ A) 
    S   : Tm Γ ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    Kβ  : {a : Tm Γ A}{b : Tm Γ B} → K $ a $ b ≡ a
    Sβ  : {a : Tm Γ A}{b : Tm Γ (A ⇒ B)}{c : Tm Γ (A ⇒ B ⇒ C)} → S $ c $ b $ a ≡ c $ a $ (b $ a)
  infixl 6 _$_
  postulate
    ⟦_⟧₁ : ₁.Tm Γ A → Tm Γ A
    ⟦$⟧₁ : ⟦ ₁.b ₁.$ ₁.a ⟧₁ ≡ ⟦ ₁.b ⟧₁ $ ⟦ ₁.a ⟧₁
    ⟦K⟧₁ : ⟦ ₁.K{Γ} {A = A}{B = B} ⟧₁ ≡ K
    ⟦S⟧₁ : ⟦ ₁.S{Γ} {A = A}{B = B}{C = C} ⟧₁ ≡ S
    {-# REWRITE ⟦$⟧₁ ⟦K⟧₁ ⟦S⟧₁ #-}

record DepModel₁ : Set₁ where
  field
    Tm  : (Γ : Con)(A : Ty) → ₁.Tm Γ A → Prop
    _$_ : Tm Γ (A ⇒ B) ₁.b → Tm Γ A ₁.a → Tm Γ B (₁.b ₁.$ ₁.a)
    K   : Tm Γ (A ⇒ B ⇒ A) ₁.K
    S   : Tm Γ ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) ₁.S
  infixl 6 _$_
  postulate
    ⟦_⟧₁ : (a : ₁.Tm Γ A) → Tm Γ A a

M : Model₁
M = record
  { Tm  = λ Γ A → ₀.Tm (Γ ⇒* A)
  ; Var = λ Γ A → Σ Con λ Γ₀ → Σ Con λ Γ₁ → ↑ (Γ ≡ Γ₀ ▹ A ++ Γ₁)
  ; vz  = λ {Γ}{A} → Γ ,Σ (◆ ,Σ mk refl)
  ; vs  = λ { {Γ}{A}{B} (Γ₀ ,Σ (Γ₁ ,Σ e)) → Γ₀ ,Σ (Γ₁ ▹ B ,Σ mk (cong (_▹ B) (un e))) }
  ; var = λ { {Γ}{A} (Γ₀ ,Σ (Γ₁ ,Σ e)) → subst ₀.Tm (trans (sym (++⇒* Γ₁)) (cong (λ Γ → Γ ⇒* A) (sym (un e)))) (₀.var {Γ₀}{A}{Γ₁})  } -- (cong (λ Γ → Γ ⇒* A) (sym (un e)))
  ; _$_ = λ {Γ}{A}{B} t u → ₀.S* {Γ}{A}{B} ₀.$ t ₀.$ u
  ; K   = λ {Γ} → ₀.K* Γ ₀.$ ₀.K
  ; S   = λ {Γ} → ₀.K* Γ ₀.$ ₀.S
  ; Kβ  = {!!}
  {-
  S*' Γ I $ (S*' Γ I $ (K*' Γ I $ K) $ a) $ b ≡ a
————————————
  -}
  ; Sβ  = {!!}
  }


{-


fTm : ₀.Tm A → ₁.Tm ◆ A
fTm = M.⟦_⟧₀
  where
    M : Model₀
    M = record
      { Tm  = ₁.Tm ◆ -- fTm : ₀.Tm A → ₁.Tm ◆ A
      ; _$_ = ₁._$_  -- fTm (x ₀.$ x₁) = fTm x ₁.$ fTm x₁
      ; K   = ₁.K    -- fTm ₀.K = ₁.K
      ; S   = ₁.S    -- fTm ₀.S = ₁.S
      ; Kβ  = refl   -- fTmKβ : {a : ₀.Tm A}{b : ₀.Tm B} → ₁.K ₁.$ fTm a ₁.$ fTm b ≡ fTm a
      ; Sβ  = refl } -- fTmSβ : {c : ₀.Tm (A ⇒ B ⇒ C)}{b : ₀.Tm (A ⇒ B)}{a : ₀.Tm A} → ₁.S ₁.$ fTm c ₁.$ fTm b ₁.$ fTm a ≡ fTm (c ₀.$ a ₀.$ (b ₀.$ a))
    module M = Model₀ M

gTm : ₁.Tm ◆ A → ₀.Tm A
gTm a₁ = M.⟦ a₁ ⟧₁ refl
    where
      M : Model₁
      M = record
        { Tm = λ Γ A → Γ ≡ ◆ → ₀.Tm A
        ; Var = λ Γ A → Γ ≡ ◆ → ⊥
        ; _$_ = λ f₀ a₀ e → f₀ e ₀.$ a₀ e
        ; K = λ _ → ₀.K
        ; S = λ _ → ₀.S
        ; vz = λ ()
        ; vs = λ _ ()
        ; var = λ x e → ite⊥ (x e) -- z
        ; q = λ ()
        ; wk = λ _ _ ()
        ; Kβ = refl
        ; Sβ = refl
        }
      module M = Model₁ M  
gfTm : (a : ₀.Tm A) → gTm (fTm a) ≡ a
gfTm = M.⟦_⟧₀
  where
    M : DepModel₀
    M = record
      { Tm  = λ A a → gTm (fTm a) ≡ a
      ; _$_ = cong$₀
      ; K   = refl
      ; S   = refl }
    module M = DepModel₀ M

{-
fgTm : (a : ₁.Tm ◆ A) → fTm (gTm a) ≡ a
fgTm = M.⟦ ⟧₁
  where
    M : DepModel₁
    M = record
      { Tm  = λ Γ A _ → Γ ≡ ◆ → {!!}
      ; _$_ = {!!}
      ; K   = {!!}
      ; S   = λ x → {!!} }
    module M = DepModel₁ M



-- roundtrips



fgTm : (x : ₁.Tm ◆ A) → fTm (gTm x) ≡ x
fgTm (x ₁.$ x₁) = cong$₁ (fgTm x) (fgTm x₁)
fgTm ₁.K = refl
fgTm ₁.S = refl

-- fgTmKβ trivial
-- fgTmSβ trivial

module _ where
  open ₁

  wk₁ : Tm Γ A → Tm (Γ ▹ B) A
  wk₁ (var x) = var (vs x)
  wk₁ (x $ x₁) = wk₁ x $ wk₁ x₁
  wk₁ K = K
  wk₁ S = S
  
  wk₁Kβ : K $ wk₁ {B = B} a $ wk₁ b ≡ wk₁ a
  wk₁Kβ = refl
  wk₁Sβ : S $ wk₁ {B = B} c $ wk₁ b $ wk₁ a ≡ wk₁ c $ wk₁ a $ (wk₁ b $ wk₁ a)
  wk₁Sβ = refl

module ₂ where
  
  data Tm : Con → Ty → Set where
    _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    K   : Tm Γ (A ⇒ B ⇒ A) 
    S   : Tm Γ ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    q   : Tm (Γ ▹ A) A
    wk  : (B : Ty) → Tm Γ A → Tm (Γ ▹ B) A
  infixl 6 _$_
  variable
    a b c d : Tm Γ A

  
  postulate
    Kβ : K $ a $ b ≡ a
    Sβ : S $ c $ b $ a ≡ c $ a $ (b $ a)
    wk$ : {a : Tm Γ A}{f : Tm Γ (A ⇒ B)} → wk C (f $ a) ≡ wk C f $ wk C a
    wkK : wk C (K{Γ}{A}{B}) ≡ K
    wkS : wk D (S{Γ}{A}{B}{C}) ≡ S
    {-# REWRITE Kβ Sβ wk$ wkK wkS #-}

  I : Tm Γ (A ⇒ A)
  I {A = A} = S $ K $ K {B = A}

module _ where
  open ₂
  cong$₂ : a ≡ b → c ≡ d → a $ c ≡ b $ d
  cong$₂ x x₂ = cong2 _$_ x x₂

  congwk₂ : a ≡ b →  wk A a  ≡ wk _ b
  congwk₂ x = cong (wk _) x

module _ where
  open ₂
  postulate
    ext : {u : Tm Γ (A ⇒ B)}{v : Tm Γ (A ⇒ B)} → wk A u $ q ≡ wk A v $ q → u ≡ v

  lam : Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  lam (x $ x₁) = S $ lam x $ lam x₁
  lam K = K $ K
  lam S = K $ S
  lam (q) = I
  lam (wk _ x) = K $ x
  
  -- lamKβ : {b : Tm (Γ ▹ A) B}{c : Tm (Γ ▹ A) C} → lam (K $ b $ c) ≡ lam b -- NEM JO, HA VAN REWRITE RULE
  lamKβ : {b : Tm (Γ ▹ A) B}{c : Tm (Γ ▹ A) C} → S $ (S $ (K $ K) $ lam b) $ lam c ≡ lam b
  lamKβ {Γ = Γ}{A = A}{B = B}{C = C}{b = b}{c = c} = ext refl

  lamSβ : S $ (S $ (S $ (K $ S) $ lam a) $ lam b) $ lam c ≡ lam(a $ c $ (b $ c))
  lamSβ = ext refl

  lamwk$ : K $ (c $ a) ≡ lam(wk A c $ wk A a)
  lamwk$ = ext refl
  
  lamwkK :  K {Γ}{B = A} $ K ≡ lam (K {A = B}{C})
  lamwkK = refl

  lamwkS : K {Γ}{B = A} $ S ≡ lam {A = A} (S {A = B}{C}{D})
  lamwkS = refl

  lamext : {u v : Tm (Γ ▹ C) (A ⇒ B)} → lam (wk A u $ q) ≡ lam (wk A v $ q) → lam u ≡ lam v
  lamext e = cong lam (ext (cong (λ z → wk _ z $ q) e))
-}
  {-
   ext (
    wk A (S $ (S $ (K $ K) $ lam b) $ lam c) $ q
                                                    ≡⟨ cong (_$ q) wk$ ⟩
    wk A (S $ (S $ (K $ K) $ lam b)) $ wk A (lam c) $ q
                                                    ≡⟨ cong (λ z → z $ wk A (lam c) $ q) wk$ ⟩
    wk A S $ wk A (S $ (K $ K) $ lam b) $ wk A (lam c) $ q
                                                    ≡⟨ cong (λ z → z $ wk A (S $ (K $ K) $ lam b) $ wk A (lam c) $ q) wkS ⟩
    S $ wk A (S $ (K $ K) $ lam b) $ wk A (lam c) $ q
                                                    ≡⟨ Sβ ⟩
    wk A (S $ (K $ K) $ lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ q $ (wk A (lam c) $ q)) wk$ ⟩
    wk A (S $ (K $ K)) $ wk A (lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ wk A (lam b) $ q $ (wk A (lam c) $ q)) wk$ ⟩
    wk A S $ wk A (K $ K) $ wk A (lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ wk A (K $ K) $ wk A (lam b) $ q $ (wk A (lam c) $ q)) wkS ⟩
    S $ wk A (K $ K) $ wk A (lam b) $ q $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (_$ (wk A (lam c) $ q)) Sβ ⟩
    wk A (K $ K) $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) wk$ ⟩
    wk A K $ wk A K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ wk A K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) wkK ⟩
    K $ wk A K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → K $ z  $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) wkK ⟩
    K $ K $ q $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ cong (λ z → z $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)) Kβ ⟩
    K $ (wk A (lam b) $ q) $ (wk A (lam c) $ q)
                                                    ≡⟨ Kβ ⟩
    wk A (lam b) $ q
                                                    ∎)
  -}
  
  -- lamwk...a
  -- lamext 
  -- ₁ → ₂
{-
jVar : ₁.Var Γ A → ₂.Tm Γ A
jVar ₁.vz = ₂.q
jVar (₁.vs x) = ₂.wk _ (jVar x)

jTm :  ₁.Tm Γ A → ₂.Tm Γ A
jTm (x ₁.$ x₁) = jTm x ₂.$ jTm x₁
jTm ₁.K = ₂.K
jTm ₁.S = ₂.S
jTm (₁.var x) = jVar x

jTmKβ : {a : ₁.Tm Γ A}{b : ₁.Tm Γ B} → ₂.K ₂.$ jTm a ₂.$ jTm b ≡ jTm a
jTmKβ = refl

jTmSβ : {c : ₁.Tm  Γ (A ⇒ B ⇒ C)}{b : ₁.Tm Γ (A ⇒ B)}{a : ₁.Tm Γ A} → ₂.S ₂.$ jTm c ₂.$ jTm b ₂.$ jTm a ≡ jTm (c ₁.$ a ₁.$ (b ₁.$ a))
jTmSβ = refl

-- ₂ → ₁

hTm :  ₂.Tm Γ A → ₁.Tm Γ A
hTm (x ₂.$ x₁) = hTm x ₁.$ hTm x₁
hTm ₂.K = ₁.K
hTm ₂.S = ₁.S
hTm ₂.q = ₁.var ₁.vz
hTm (₂.wk B x) = wk₁ (hTm x)  

hTmKβ : {a : ₂.Tm Γ A}{b : ₂.Tm Γ A} → ₁.K ₁.$ hTm a ₁.$ hTm b ≡ hTm a
hTmKβ = refl

hTmSβ : {c : ₂.Tm  Γ (A ⇒ B ⇒ C)}{b : ₂.Tm Γ (A ⇒ B)}{a : ₂.Tm Γ A} → ₁.S ₁.$ hTm c ₁.$ hTm b ₁.$ hTm a ≡ hTm (c ₂.$ a ₂.$ (b ₂.$ a))
hTmSβ = refl

hTmwk$ : {a : ₂.Tm Γ A}{f : ₂.Tm Γ (A ⇒ B)} → wk₁ (hTm f) ₁.$ wk₁ (hTm a) ≡ hTm (₂.wk C f ₂.$ ₂.wk C a)
hTmwk$ = refl

hTmwkK : ₂.wk C (₂.K{Γ}{A}{B}) ≡ ₂.K
hTmwkK = ₂.wkK
hTmwkS : ₂.wk D (₂.S{Γ}{A}{B}{C}) ≡ ₂.S
hTmwkS = ₂.wkS

-- rountrips

-- hjVar
hjVar : (x : ₁.Var Γ A) → hTm (jVar x) ≡ ₁.var x
hjVar ₁.vz = refl
hjVar (₁.vs x) = cong wk₁ (hjVar x)

hjTm : (x : ₁.Tm Γ A) → hTm (jTm x) ≡ x
hjTm (₁.var x) = hjVar x 
hjTm (x ₁.$ x₁) = cong$₁ (hjTm x) (hjTm x₁)
hjTm ₁.K = refl
hjTm ₁.S = refl

-- hjTmKβ, hjTmSβ trivial

jwk : (x : ₁.Tm Γ A) → jTm (wk₁ x) ≡ ₂.wk B (jTm x)
jwk (₁.var x) = refl
jwk (x ₁.$ x₁) = cong$₂ (jwk x) (jwk x₁)
jwk ₁.K = sym ₂.wkK
jwk ₁.S = sym ₂.wkS

jhTm : (x : ₂.Tm Γ A) → jTm (hTm x) ≡ x
jhTm (x ₂.$ x₁) = cong$₂ (jhTm x) (jhTm x₁)
jhTm ₂.K = refl
jhTm ₂.S = refl
jhTm ₂.q = refl
jhTm (₂.wk B x) = trans (jwk (hTm x)) (cong (₂.wk _) (jhTm x))

-- jhTm Kβ,Sβ,wk$,wkK,wkS trivial

-}
-}
