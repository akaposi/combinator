{-# OPTIONS --type-in-type --rewriting #-}

open import Agda.Builtin.Unit
open import Agda.Builtin.Equality
open import Agda.Builtin.Nat renaming (Nat to ℕ)

module combTTdeep where

transport : ∀{i j}{A : Set i}{x y : A}(P : A → Set j) → x ≡ y → P x → P y
transport _ refl a = a
sym : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
sym refl = refl
trans : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
trans refl refl = refl
ap : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
ap f refl = refl
ap2 : ∀{i j k}{A : Set i}{B : Set j}{C : Set k}(f : A → B → C){a a' : A} → a ≡ a' → {b b' : B} → b ≡ b' → f a b ≡ f a' b'
ap2 f refl refl = refl

{-# BUILTIN REWRITE _≡_ #-}

infixr 5 _⇒_
infixl 6 _$_ _∙_

postulate
  Ty          : Set
  Tm          : Ty → Set
  _⇒_         : Ty → Ty → Ty
  _$_         : ∀{A B} → Tm (A ⇒ B) → Tm A → Tm B
  K           : ∀{A B} → Tm (A ⇒ B ⇒ A)
  Kβ          : ∀{A B}{u : Tm A}{v : Tm B} → K $ u $ v ≡ u
  S           : ∀{A B C} → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  Sβ          : ∀{A B C}{u : Tm (A ⇒ B ⇒ C)}{v : Tm (A ⇒ B)}{w : Tm A} → S $ u $ v $ w ≡ u $ w $ (v $ w)
  U           : Ty
  Ty∅         : Ty ≡ Tm U
  {-# REWRITE Kβ Sβ Ty∅ #-}
  Π           : (A : Ty) → Tm ((A ⇒ U) ⇒ U)
  _∙_         : ∀{A B} → Tm (Π A $ B) → (u : Tm A) → Tm (B $ u)
  ⇒∅          : ∀{A B} → A ⇒ B ≡ Π A $ (K $ B)
  -- {-# REWRITE ⇒∅ #-} -- this makes Agda loop
  $∅          : ∀{A B}{t : Tm (A ⇒ B)}{u : Tm A} → t $ u ≡ (transport Tm ⇒∅ t ∙ u)
  -- {-# REWRITE $∅ #-}
  ar : Tm (U ⇒ U ⇒ U)
  ⇒∅' : ∀{A B} → ar $ A $ B ≡ A ⇒ B -- the opposite direction makes Agda loop
  {-# REWRITE ⇒∅' #-}
  
  B' : ∀{A B C} → Tm ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  B'β : ∀{A B C u v w} → B' {A}{B}{C} $ u $ v $ w ≡ u $ (v $ w)
  C' : ∀{A B C} → Tm ((A ⇒ B ⇒ C) ⇒ B ⇒ A ⇒ C)
  C'β : ∀{A B C u v w} → C' {A}{B}{C} $ u $ v $ w ≡ (u $ w) $ v
  {-# REWRITE B'β C'β #-}
  {- B' : ∀{A B C} → Tm ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  B' = S $ (K $ S) $ K
  B'β : ∀{A B C u v w} → B' {A}{B}{C} $ u $ v $ w ≡ u $ (v $ w)
  B'β = refl
  C' : ∀{A B C} → Tm ((A ⇒ B ⇒ C) ⇒ B ⇒ A ⇒ C)
  C' = S $ (S $ (K $ B') $ S) $ (K $ K)
  C'β : ∀{A B C u v w} → C' {A}{B}{C} $ u $ v $ w ≡ (u $ w) $ v
  C'β = refl
   -}
postulate
  Kdep  : {A : Ty}{B : Tm (A ⇒ U)} → Tm (Π A $ (C' $ (B' $ ar $ B) $ A))
  Kdepβ : {A : Ty}{B : Tm (A ⇒ U)}{u : Tm A}{v : Tm (B $ u)} → Kdep {A}{B} ∙ u $ v ≡ u
  {-# REWRITE Kdepβ #-}
  anEquality₀ : ∀{A B} → C' {A} $ (B' $ ar $ (K $ B)) $ A ≡ K $ (B ⇒ A)
  -- to typecheck this, it is enough to have: (∀a.B$a=B'$a)→ΠA$B=ΠA$B'
  K∅ : ∀{A B} → K {A}{B} ≡ transport Tm (trans (ap (Π A $_) (anEquality₀ {A}{B})) (sym (⇒∅ {A}{B ⇒ A}))) (Kdep {A}{K $ B})
  Sdep₀  : ∀{A}{B : Tm (A ⇒ U)}{C} → Tm ((Π A $ (C' $ (B' $ ar $ B) $ C)) ⇒ (Π A $ B) ⇒ A ⇒ C)
  Sdep₀β : {A : Ty}{B : Tm (A ⇒ U)}{C : Ty}{u : Tm (Π A $ (C' $ (B' $ ar $ B) $ C))}{v : Tm (Π A $ B)}{w : Tm A} →
           Sdep₀ $ u $ v $ w ≡ (u ∙ w) $ (v ∙ w)
  anEquality₁ : ∀{A}{B : Ty}{C} → C' {A} $ (B' $ ar $ (K $ B)) $ C ≡ K $ (B ⇒ C)
  anEquality₀∅ : ∀{A B} → anEquality₀ {A}{B} ≡ anEquality₁ {A}{B}{A}
  S∅ : ∀{A B C : Ty} → S {A}{B}{C} ≡
    transport Tm (trans (ap (λ x → Π A $ x ⇒ Π A $ (K $ B) ⇒ A ⇒ C) (anEquality₁ {A}{B}{C})) (ap2 (λ x y → x ⇒ y ⇒ A ⇒ C) (sym (⇒∅ {A}{B ⇒ C})) (sym (⇒∅ {A}{B}))))
      (Sdep₀ {A}{K $ B}{C})
  -- {-# REWRITE Sdep₀β #-}
  π   : Tm (Π U $ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U))
  Π∅  : {A : Ty}{B : Tm (A ⇒ U)} → Π A $ B ≡ π ∙ A $ B
  ar∅ : {A B : Ty} → ar $ A $ B ≡ π ∙ A $ (K $ B)
  -- {-# REWRITE Π∅ #-}
  Bdep : {A B : Ty}{C : Tm (B ⇒ U)} → Tm ((Π B $ C) ⇒ (Π  (A ⇒ B) $ (B' $ Π A $ (B' $ C))))
  Bdepβ : {A B : Ty}{C : Tm (B ⇒ U)}{u : Tm (Π B $ C)}{v : Tm (A ⇒ B)}{w : Tm A} → Bdep $ u ∙ v ∙ w ≡ u ∙ (v $ w)
  -- {-# REWRITE Bdepβ #-}
  --                                                         C' $ (B' $ ar $ (K $ B)) $ C  ≡ K $ (B ⇒ C)
  anotherEquality : ∀{A}{B : Tm (A ⇒ U)} → (C' $ (B' $ ar $ (C' $ (B' $ ar $ B      ) $ U)) $ U) ≡ (B' $ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U) $ B)
  Sdep : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))} →
         Tm ((Π A $ (Sdep₀ $ transport Tm (ap (Π A $_) (sym anotherEquality)) (Bdep $ π ∙ B) $ C)) ⇒ (Π (Π A $ B) $ (B' $ (π ∙ A) $ (Sdep₀ $ C))))
  -- TODO: Sdepβ
  -- TODO: Bdep∅
  -- TODO: Sdep₀∅ (ehhez kell kdep, esetleg meg egyebek is)
  {-
  {-
                    -- Tm (Π (A ⇒ U) $ (B' $ Π A $ (B' $ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U))))
  Bdep$π  : {A : Ty} → Tm (Π (A ⇒ U) $ (B' $ Π A $ (C' $ (B' $ C' $ (B' $ (B' $ ar) $ (C' $ (B' $ C' $ (B' $ ar)) $ U))) $ U)))
  Bdep$πβ : {A : Ty}{B : Tm (A ⇒ U)}{a : Tm A} → Bdep$π ∙ B ∙ a ≡ π ∙ (B $ a)
  {-# REWRITE Bdep$πβ #-}
  -}
  {- deriving the type of Bdep$π:

  Goal: Tm (Π A $ (C' $ (B' $ ar $ (C' $ (B' $ ar $ B) $ U)) $ U))
  Have: Tm (Π A $ (B' $ (C' $ (B' $ ar $ (C' $ ar $ U)) $ U) $ B))   <- with the ordinary Bdep combinator

  so we need an F s.t. F $ B = (C' $ (B' $ ar $ (C' $ (B' $ ar $ B) $ U)) $ U)
  F =
  λB.C' $ (B' $ ar $ (C' $ (B' $ ar $ B) $ U)) $ U =
  C' $ (λB.C' $ (B' $ ar $ (C' $ (B' $ ar $ B) $ U))) $ U =
  C' $ (B' $ C' $ (λ B . B' $ ar $ (C' $ (B' $ ar $ B) $ U))) $ U =
  C' $ (B' $ C' $ (B' $ (B' $ ar) $ (λB.C' $ (B' $ ar $ B) $ U))) $ U =
  C' $ (B' $ C' $ (B' $ (B' $ ar) $ (C' $ (λB.C' $ (B' $ ar $ B)) $ U))) $ U =
  C' $ (B' $ C' $ (B' $ (B' $ ar) $ (C' $ (B' $ C' $ (B' $ ar)) $ U))) $ U
  -}
  {-
  Sdep : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))} →
    Tm ((Π A $ (Sdep₀ {A}{C' {A}{U}{U} $ (B' {A}{U}{U ⇒ U} $ ar $ B) $ U}{U} $ (Bdep$π ∙ B) $ C)) ⇒
        (Π (Π A $ B) $ (B' {Π A $ B}{A ⇒ U}{U} $ (_∙_ π A) $ (Sdep₀ {A}{B}{U} $ C))))
  -}
  -- Sdepβ : {A : Ty}{B : Tm (A ⇒ U)}{C : Tm (Π A $ (C' $ (B' $ ar $ B) $ U))}{f : Tm (Π A $ (Sdep₀ $ (Bdep$π ∙ B) $ C))}{g : Tm (Π A $ B)}{a : Tm A} →
  --   Sdep $ f ∙ g ∙ a ≡ (f ∙ a) ∙ (g ∙ a)
-}
