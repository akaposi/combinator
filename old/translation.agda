open import Relation.Nullary
open import Relation.Binary.PropositionalEquality hiding ([_])
open import Data.Product renaming (_,_ to _,,_)
open import Data.Nat

infixr 10 _⇒_
infixl 10 _$_
infixl 5 _,_

data Ty : Set where
  nat : Ty
  _⇒_ : Ty → Ty → Ty

variable
  τ σ δ : Ty

data Ctx : Set where
  ε   : Ctx
  _,_ : Ctx → Ty → Ctx

variable Γ : Ctx

data _∈_ : Ty → Ctx → Set where
  here  : τ ∈ (Γ , τ)
  there : τ ∈ Γ → τ ∈ (Γ , σ) 

data L : Ctx → Ty → Set where
  var : τ ∈ Γ → L Γ τ
  app : L Γ (τ ⇒ σ) → L Γ τ → L Γ σ
  lam : L (Γ , τ) σ → L Γ (τ ⇒ σ)

data SK : Ty → Set where
  K : SK (τ ⇒ σ ⇒ τ)
  S : SK ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_ : SK (τ ⇒ σ) → SK τ → SK σ

SK⇒L : SK τ → L ε τ
SK⇒L K = lam (lam (var (there here)))
SK⇒L S = lam (lam (lam (app (app (var (there (there here))) (var here)) (app (var (there here)) (var here)))))
SK⇒L (e $ e₁) = app (SK⇒L e) (SK⇒L e₁)

data SKV : Ctx → Ty → Set where
  var : τ ∈ Γ → SKV Γ τ
  K : SKV Γ (τ ⇒ σ ⇒ τ)
  S : SKV Γ ((τ ⇒ σ ⇒ δ) ⇒ (τ ⇒ σ) ⇒ τ ⇒ δ)
  _$_ : SKV Γ (τ ⇒ σ) → SKV Γ τ → SKV Γ σ


skv-transf-stupid : SKV (Γ , τ) σ → SKV Γ (τ ⇒ σ)
skv-transf-stupid {τ = τ} (var here) = S $ K $ K {σ = τ}
skv-transf-stupid (var (there x)) = K $ var x
skv-transf-stupid K = K $ K
skv-transf-stupid S = K $ S
skv-transf-stupid (e $ e₁) = S $ skv-transf-stupid e $ skv-transf-stupid e₁

l-skv-stupid : L Γ τ → SKV Γ τ
l-skv-stupid (var x) = var x
l-skv-stupid (app e e₁) = l-skv-stupid e $ l-skv-stupid e₁
l-skv-stupid (lam e) = skv-transf-stupid (l-skv-stupid e)


_/_ : (Γ : Ctx) → τ ∈ Γ → Ctx
(Γ , x) / here = Γ
(Γ , x) / there v = (Γ / v) , x

wkv : (v : σ ∈ Γ) → τ ∈ (Γ / v) → τ ∈ Γ
wkv here w = there w
wkv (there v) here = here
wkv (there v) (there w) = there (wkv v w)

wk : (v : τ ∈ Γ) → L (Γ / v) σ → L Γ σ
wk v (var x) = (var (wkv v x))
wk v (app e e₁) = app (wk v e) (wk v e₁)
wk v (lam e) = lam (wk (there v) e)

free : L (Γ , τ) σ → Set
free {Γ = Γ}{σ = σ} e = Σ (L Γ σ) λ e′ → e ≡ wk here e′


freevar : (v : τ ∈ Γ) → (w : σ ∈ Γ) → Set
freevar {Γ = Γ}{σ = σ} v w = Σ (σ ∈ (Γ / v)) λ w′ → w ≡ wkv v w′

isfreevar : (v : τ ∈ Γ) → (w : σ ∈ Γ) → Dec (freevar v w)
isfreevar here here = no λ { (_ ,, ()) }
isfreevar here (there w) = yes (w ,, refl)
isfreevar (there v) here = no λ { (_ ,, ()) }
isfreevar (there v) (there w) with isfreevar v w
... | yes (p ,, refl) = yes (there p ,, refl)
... | no p = no λ { (_ ,, ()) }

-- Free variable in a lambda term
--l-free : (v : τ ∈ Γ) → L Γ σ → Set
--l-free {Γ = Γ}{σ = σ} v e = Σ (L (Γ / v) σ) λ e′ → e ≡ wk v e′
--
--l-isfree : (v : τ ∈ Γ) → (e : L Γ σ) → Dec (l-free v e)
--l-isfree v (var x) with isfreevar v x
--... | yes (p ,, refl) = yes (var p ,, refl)
--... | no p = no λ { (_ ,, ()) }
--l-isfree v (app e e₁) with l-isfree v e | l-isfree v e₁
--... | yes (p ,, refl) | yes (w ,, refl) = yes (app p w ,, refl)
--... | no p | yes q = no λ { (_ ,, ()) }
--... | yes p | no q = no λ { (_ ,, ()) }
--... | no p | no q = no λ { (_ ,, ()) }
--l-isfree v (lam e) with l-isfree (there v) e
--... | yes (p ,, refl) = yes (lam p ,, refl)
--... | no p = no λ { (lam fst ,, refl) → p (fst ,, refl) }


skv-wk : (v : τ ∈ Γ) → SKV (Γ / v) σ → SKV Γ σ
skv-wk v (var x) = var (wkv v x)
skv-wk v K = K
skv-wk v S = S
skv-wk v (e $ e₁) = skv-wk v e $ skv-wk v e₁

skv-free : (v : τ ∈ Γ) → SKV Γ σ → Set
skv-free {Γ = Γ}{σ = σ} v e = Σ (SKV (Γ / v) σ) λ e′ → e ≡ skv-wk v e′

skv-isfree : (v : τ ∈ Γ) → (e : SKV Γ σ) → Dec (skv-free v e)
skv-isfree v (var x) with isfreevar v x
... | yes (p ,, refl) = yes (var p ,, refl)
... | no _ = no λ { (_ ,, ()) }
skv-isfree v K = yes (K ,, refl)
skv-isfree v S = yes (S ,, refl)
skv-isfree v (e $ e₁) with skv-isfree v e | skv-isfree v e₁
... | yes (p ,, refl) | yes (q ,, refl) = yes (p $ q ,, refl)
... | _ | _ = no λ { (_ ,, ()) }


skv-transf : SKV (Γ , τ) σ → SKV Γ (τ ⇒ σ)
skv-transf {τ = τ} (var here) = S $ K $ K {σ = τ}
skv-transf (var (there x)) = K $ var x
skv-transf K = K $ K
skv-transf S = K $ S
skv-transf (e $ e₁) with skv-isfree here e | skv-isfree here e₁
skv-transf (e $ e₁)| yes (p ,, refl) | yes (q ,, refl) = K $ (p $ q)
skv-transf (e $ (var here)) | yes (p ,, refl) | no _ = p
skv-transf (e $ e₁) | _ | _ = S $ skv-transf e $ skv-transf e₁


l-trans : L Γ τ → SKV Γ τ
l-trans (var x) = var x
l-trans (app e e₁) = l-trans e $ l-trans e₁
l-trans (lam e) = skv-transf (l-trans e)

skv-sk : SKV ε τ → SK τ
skv-sk K = K
skv-sk S = S
skv-sk (e $ e₁) = skv-sk e $ skv-sk e₁

l-sk : L ε τ → SK τ
l-sk e = skv-sk (l-trans e)

l-sk-stupid : L ε τ → SK τ
l-sk-stupid e = skv-sk (l-skv-stupid e)

-- Interpretation

SS : ∀ {A B C : Set} → (A → B → C) → (A → B) → (A → C)
SS f g x = f x (g x)

KK : ∀ {A B : Set} → (A → B → A)
KK x y = x

[_] : Ty → Set
[ nat ] = ℕ
[ τ ⇒ σ ] = [ τ ] → [ σ ]

eval : SK τ → [ τ ]
eval K = KK
eval S = SS
eval (e $ e₁) = eval e (eval e₁)


ex : L ε ((τ ⇒ τ ⇒ σ) ⇒ τ ⇒ σ)
ex = lam (lam (app (app (var (there here)) (var here)) (var here)))

ex-stupid-l = eval (l-sk-stupid ex)
ex-l = eval (l-sk ex)

_ : ex-stupid-l ≡ ex-l
_ = refl



EX : ∀ {A B : Set} → (A → A → B) → A → B
EX f x = f x x

EXcomb : ∀ {A B : Set} → (A → A → B) → A → B
EXcomb {A} = SS SS (KK (SS KK (KK {B = A})))

