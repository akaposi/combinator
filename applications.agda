{-# OPTIONS --rewriting #-}

module _ where
open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}



-- Example 3
-- =========
--
-- Encoding dependent pair example using 
-- the Münchausen technique.

module DependentPair where
    open import Data.Nat
    open import Data.Fin
    open import Data.Bool

    -- We know that A × B ≅ (b:Bool) → if b then A else B
    -- and we can encode this in Agda straight away.
    pair : (b : Bool) → if b then ℕ else ℕ
    pair = λ { true → 1; false → 2 }

    -- Dependent version of the same principle looks like:
    -- Σ A B ≅ f : (b:Bool) → if b then A else B (f true)
    -- (I am inventing the syntax).  This definition
    -- obviously doesn't work because of circularity.
    -- However, we can workaround the problem as follows:

    postulate
      a : ℕ
    dpair : (b : Bool) → if b then ℕ else Fin a
    postulate
      q : a ≡ dpair true
      {-# REWRITE q #-}

    dpair true = 5
    dpair false = # 3

