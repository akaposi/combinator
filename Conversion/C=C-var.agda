{-# OPTIONS --cubical --safe #-}
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence

open import Syntax.Ty

module Conversion.C=C-var (T : TY) where

open import Syntax.Common T
open import Syntax.C T as C 
open import Syntax.C-var T as C-var

-- 
f : C.Tm τ → C-var.Tm ε τ

f (x $ x₁)            = (f x) $ (f x₁) 
f K                   = K
f S                   = S
f (η{δ = δ} i)        = η{δ = δ} i
f (Kβ{x = x}{y} i)    = Kβ{x = f x}{f y} i
f (Sβ{x = x}{y}{z} i) = Sβ{x = f x}{f y}{f z} i
f (lamKβ i)           = lamKβ i
f (lamSβ i)           = lamSβ i
f (lamwk$ i)          = lamwk$ i
f (TmSet a b c d i j) = TmSet (f a) (f b) (cong f c) (cong f d) i j

g : C-var.Tm ε τ → C.Tm τ
g (x $ x₁)            = (g x) $ (g x₁)
g K                   = K
g S                   = S
g (η{δ = δ} i)        = η{δ = δ} i
g (lamKβ i)           = lamKβ i
g (lamSβ i)           = lamSβ i
g (lamwk$ i)          = lamwk$ i
g (Kβ{x = x}{y} i)    = Kβ{x = g x}{g y} i
g (Sβ{x = x}{y}{z} i) = Sβ{x = g x}{g y}{g z} i
g (TmSet a b c d i j) = TmSet (g a) (g b) (cong g c) (cong g d) i j

gf : (x : C.Tm τ) → g (f x) ≡ x
gf (x $ x₁) i            = gf x i $ gf x₁ i
gf K i                   = K
gf S i                   = S
gf (η i)                 = refl
gf (lamKβ i)             = refl
gf (lamSβ i)             = refl
gf (lamwk$ i)            = refl
gf (Kβ {x = a} {b} i) j  = isSet→isSet' TmSet (λ j → K $ gf a j $ gf b j) (gf a) Kβ Kβ i j
gf (Sβ{x = c}{b}{a} i) j =
  isSet→isSet' TmSet (λ j → S $ gf c j $ gf b j $ gf a j) (λ j → gf c
  j $ gf a j $ (gf b j $ gf a j)) Sβ Sβ i j
gf (TmSet a b c d i j) k =
  isGroupoid→isGroupoid' (isSet→isGroupoid TmSet) (λ j k → gf (c j) k)
  (λ j k → gf (d j) k) (λ i k → gf a k) (λ i k → gf b k) (λ i j →
  TmSet (g (f a)) (g (f b)) (λ i₁ → g (f (c i₁))) (λ i₁ → g (f (d
  i₁))) i j) (λ i j → TmSet a b c d i j) i j k

fg : (x : C-var.Tm ε τ) → f (g x) ≡ x
fg (x $ x₁) i             = fg x i $ fg x₁ i
fg K i                    = K
fg S i                    = S
fg (η i)                  = refl
fg (lamKβ i)              = refl
fg (lamSβ i)              = refl
fg (lamwk$ i)             = refl
fg (Kβ{x = a}{b} i) j     =
  isSet→isSet' TmSet (λ j → K $ fg a j $ fg b j) (fg a) Kβ Kβ i j
fg (Sβ {x = c}{b}{a} i) j =
  isSet→isSet' TmSet (λ j → S $ fg c j $ fg b j $ fg a j) (λ j → fg c
  j $ fg a j $ (fg b j $ fg a j)) Sβ Sβ i j
fg (TmSet a b c d i j) k  =
  isGroupoid→isGroupoid' (isSet→isGroupoid TmSet) (λ j k → fg (c j) k)
  (λ j k → fg (d j) k) (λ j k → fg a k) (λ i k → fg b k) (λ i j →
  TmSet (f (g a)) (f (g b)) (λ i₁ → f (g (c i₁))) (λ i₁ → f (g (d
  i₁))) i j) (λ i j → TmSet a b c d i j) i j k

eq : C.Tm τ ≡ C-var.Tm ε τ
eq = ua (isoToEquiv (iso f g fg gf))
