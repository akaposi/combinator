{-# OPTIONS --cubical --safe #-}

open import Cubical.Foundations.HLevels 
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Univalence
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Data.Sigma hiding (Sub)
open import Cubical.Data.Unit

open import Syntax.Ty

module Conversion.C-wk=L (T : TY) where

open import Syntax.Common T
open import Syntax.L T as L
open import Syntax.C-wk T as C-wk
open import Syntax.L.Induction T
open import Syntax.C-wk.Induction T

-- f : C-wk ≅ L : g

fTm : C-wk.Tm Γ τ → L.Tm Γ τ
fTm q = L.q
fTm (wk x) = fTm x L.[ L.π₁ ]
fTm K = L.lam (L.lam v₁)
fTm S = L.lam (L.lam (L.lam (v₂ L.$ v₀ L.$ (v₁ L.$ v₀))))
fTm (x C-wk.$ x₁) = fTm x L.$ fTm x₁
fTm (Kβ {x = x}{y} i) = L.LKβ (fTm x) (fTm y) i
fTm (Sβ {x = x}{y}{z} i) = L.LSβ (fTm x) (fTm y) (fTm z) i
fTm (wk$ {x = x}{y} i) = L.sub-$ L.π₁ (fTm x) (fTm y) i
fTm (wkK i) = L.k[π₁] i
fTm (wkS i) = L.s[π₁] i
fTm (lamKβ i) = L.lamKβ i
fTm (lamSβ i) =  L.lamSβ i
fTm (lamwk$ i) = L.lamwk$ i
fTm (η {τ = τ}{δ = δ} i) = L.comb-lamη {δ = δ} i
fTm (TmSet a b c d i j) = TmSet (fTm a) (fTm b) (λ i → fTm (c i)) (λ i → fTm (d i))  i j

fSub : C-wk.Sub Γ Δ → L.Sub Γ Δ
fSub {Δ = ε} s = εs
fSub {Δ = Δ ▹ τ} (γ , t) = fSub γ ,s fTm t

f-wks : {γ : C-wk.Sub Γ Δ} → fSub (wks {τ = τ} γ) ≡ fSub γ L.∘ L.π₁
f-wks {Δ = ε} {γ = γ} = refl
f-wks {Δ = Δ ▹ x} {γ = γ , t} = cong₂ L._,s_ f-wks refl ∙ sym L.,s∘

f-id : fSub (C-wk.id {Γ = Γ}) ≡ L.id
f-id {Γ = ε} = refl
f-id {Γ = Γ ▹ τ} = cong (L._,s q) (f-wks ∙ cong (L._∘ L.π₁) f-id) ∙ cong₂ L._,s_ L.idl refl ∙ L.π,q

f-p : fSub (p {Γ = Γ}{τ = τ}) ≡ L.π₁
f-p = f-wks ∙ (cong (L._∘ L.π₁) f-id) ∙ L.idl

f-[] : ∀{Δ}{t : C-wk.Tm Γ τ}{ϕ : C-wk.Sub Δ Γ} → fTm (t C-wk.[ ϕ ]) ≡ (fTm t) L.[ fSub ϕ ]
f-[] {Δ = Δ}{t = t}{ϕ} = ind (record
  { Tmᴰ = λ Γ τ t → {ϕ : C-wk.Sub Δ Γ} → fTm (t C-wk.[ ϕ ]) ≡ (fTm t) L.[ fSub ϕ ]
  ; TmᴰProp = λ {Γ}{τ}{t} → isPropImplicitΠ λ ϕ → L.TmSet _ _
  ; qᴰ = λ { {Γ}{τ}{ψ , t} → sym (▹βt {γ = fSub ψ}{fTm t}) }
  ; wkᴰ = λ { {Γ}{σ}{τ}{t} e {ψ , u} → e {ψ} ∙ cong (λ z → fTm t L.[ z ]) (sym ▹βs) ∙ L.[∘] {γ = π₁}{(fSub ψ ,s fTm u)} }
  ; Kᴰ = λ {Γ}{τ}{σ}{ψ} → cong L.lam (cong L.lam ((sym ▹βt ∙ cong (v₀ L.[_]) (sym ,s∘ ∙ sym ▹βs)) ∙ L.[∘]) ∙ sym L.lam[]) ∙ sym L.lam[]
  ; Sᴰ = λ {Γ}{τ}{σ}{δ}{ϕ} → cong L.lam (cong L.lam (cong L.lam (cong₂ L._$_ (cong₂ L._$_ (sym v₂-ign) (sym v₀-ign) ∙ sym (sub-$ _ _ _)) (cong₂ L._$_ (sym v₁-ign) (sym v₀-ign) ∙ sym (sub-$ _ _ _)) ∙ sym (sub-$ _ _ _)) ∙ sym L.lam[]) ∙ sym L.lam[]) ∙ sym L.lam[]
  ; _$ᴰ_ = λ {Γ}{τ}{σ}{t} e {u} e' {ψ} → cong₂ L._$_ (e {ψ}) (e' {ψ}) ∙ sym (sub-$ _ _ _)
  })
  t {ϕ}

f-∘ : {γ : C-wk.Sub Ξ Δ} {δ : C-wk.Sub Γ Ξ} → fSub (γ C-wk.∘ δ) ≡ fSub γ L.∘ fSub δ
f-∘ {Δ = ε}     {γ = γ} {δ} = εs-unique
f-∘ {Δ = Δ ▹ τ} {γ = γ , t} {δ} = cong₂ _,s_ (f-∘ {Δ = Δ}{γ = γ}{δ}) (f-[] {t = t}{δ}) ∙ sym ,s∘

gSub : L.Sub Γ Δ → C-wk.Sub Γ Δ
gTm : L.Tm Γ τ → C-wk.Tm Γ τ

gSub L.id = C-wk.id
gSub (s L.∘ s₁) = gSub s C-wk.∘ gSub s₁
gSub (s L.,s x) = gSub s , gTm x
gSub L.π₁ = p
gSub (L.ass {γ = γ}{φ}{ψ} i) = C-wk.∘-assoc {s = gSub γ}{gSub φ}{gSub ψ} i
gSub (L.idl {γ = γ} i) = C-wk.idl {s = gSub γ} i
gSub (L.idr {γ = γ} i) = C-wk.idr {s = gSub γ} i
gSub (L.▹βs {γ = γ}{t} i) = p∘ {s = gSub γ}{x = gTm t} i
gSub (L.▹η {γ = γ} i) = C-wk.p-snd {γ = gSub γ} i
gSub (L.SubSet a b c d i j) = C-wk.SubSet (gSub a) (gSub b) (λ i → gSub (c i)) (λ i → gSub (d i))  i j

gTm v₀ = q
gTm (L.lam x) = C-wk.lam (gTm x)
gTm (L.app x) = C-wk.wk (gTm x) C-wk.$ q
gTm (x L.[ x₁ ]) = gTm x C-wk.[ gSub x₁ ]
gTm (L.[∘] {γ = γ}{φ}{t} i) = C-wk.[∘] {t = gTm t}{s = gSub γ}{p = gSub φ} i
gTm (L.[id] {t = t} i) = C-wk.[id] {x = gTm t} i
gTm (L.lamβ {t = t} i) = wk$q (gTm t) i
gTm (L.lamη {τ = τ}{t = t} i) = η$x {δ = τ}{x = gTm t} i
gTm (L.app-sub γ x i) = cong (C-wk._$ q) (C-wk.wk[] {x = gTm x}{gSub γ} ∙ cong (gTm x C-wk.[_]) C-wk.∘p) i
gTm (L.▹βt {t = t} i) = gTm t
gTm (L.lam[] {t = t}{γ} i) = C-wk.lam[] {s = gSub γ}{x = gTm t} i
gTm (L.TmSet a b c d i j) = C-wk.TmSet (gTm a) (gTm b) (λ i → gTm (c i)) (λ i → gTm (d i))  i j

gTm∘fTm : (t : C-wk.Tm Γ τ) → gTm (fTm t) ≡ t
gTm∘fTm t = ind (record
  { Tmᴰ = λ Γ τ x → gTm (fTm x) ≡ x
  ; TmᴰProp = C-wk.TmSet _ _
  ; qᴰ = refl
  ; wkᴰ = λ {Γ}{σ}{τ}{x} e → cong (C-wk._[ p ]) e ∙ sym (cong C-wk.wk (sym C-wk.[id]) ∙ C-wk.wk[] {x = x})
  ; Kᴰ = C-wk.K-eq
  ; Sᴰ = C-wk.S-eq
  ; _$ᴰ_ = λ e e' → cong₂ C-wk._$_ (C-wk.[id] ∙ e) e'
  })
  t

f-lam : ∀ {t : C-wk.Tm (Γ ▹ τ) σ} → fTm (C-wk.lam t) ≡ L.lam (fTm t)
f-lam {t = t} = ind▹ (record
  { Tmᴰ = λ Γ τ σ t → fTm (C-wk.lam t) ≡ L.lam (fTm t)
  ; TmᴰProp = TmSet _ _
  ; qᴰ = L.skk≡i
  ; wkᴰ = lam-$ _ _ ∙ L.lam[] ∙ cong L.lam (sym L.[∘] ∙ cong (v₀ L.[_]) (▹βs ∙ ,s∘) ∙ ▹βt)
  ; Kᴰ = L.KK
  ; Sᴰ = L.KS
  ; _$ᴰ_ = λ {_}{_}{_}{_}{t} e {u} e' → cong₂ (λ x y → L.LS L.$ x L.$ y) e e' ∙ L.S$lamx$lamy
  }) t

fTm∘gTm : (t : L.Tm Γ τ) → fTm (gTm t) ≡ t
fTm∘gTm t = indTm (record
  { Tmᴰ = λ Γ τ t → fTm (gTm t) ≡ t
  ; TmᴰProp = L.TmSet _ _
  ; Subᴰ = λ Γ Δ ψ → fSub (gSub ψ) ≡ ψ
  ; SubᴰProp = L.SubSet _ _
  ; idᴰ = f-id
  ; _∘ᴰ_ = λ e e' → f-∘ ∙ cong₂ L._∘_ e e'
  ; _,sᴰ_ = λ e e' → cong₂ L._,s_ e e'
  ; π₁ᴰ = f-wks ∙ (cong (L._∘ L.π₁) f-id) ∙ L.idl
  ; qᴰ = refl
  ; lamᴰ = λ {_}{_}{_}{t} e → f-lam {t = gTm t} ∙ cong L.lam e
  ; appᴰ = λ e → cong (λ t → (t L.[ L.π₁ ]) L.$ v₀) e ∙ x[π]q
  ; _[_]ᴰ = λ {_}{_}{_}{t} e {ϕ} e' → f-[] {t = gTm t}{gSub ϕ} ∙ cong₂ (L._[_]) e e'
  })
  t

eq : C-wk.Tm Γ τ ≡ L.Tm Γ τ
eq = ua (isoToEquiv (iso fTm gTm fTm∘gTm gTm∘fTm))

-- an application: extensionality (this is also proven in C-wk.Eta)
{-
ext' : ∀{Γ A B}{t t' : C-wk.Tm Γ (A ⇒ B)} → wk t C-wk.$ q ≡ wk t' C-wk.$ q → t ≡ t'
ext' {t = t}{t'} e =
  t
                   ≡⟨ sym (gTm∘fTm t) ⟩ -- 
  gTm (fTm t)
                   ≡⟨ sym (cong gTm (L.lamη {t = fTm t})) ⟩
  gTm (L.lam (L.app (fTm t)))
                   ≡⟨ {!!} ⟩
  gTm (L.lam (L.app (fTm t) L.[ (π₁ L.∘ π₁ ,s L.q) L.∘ (L.id ,s L.q) ]))
                   ≡⟨ {!cong gTm (cong L.lam (sym (L.[∘] {γ = (π₁ L.∘ π₁ ,s L.q)}{φ = (L.id ,s L.q)}{t = L.app (fTm t)})))!} ⟩
  gTm (L.lam (L.app (fTm t) L.[ π₁ L.∘ π₁ ,s L.q ] L.[ L.id ,s L.q ]))
                   ≡⟨ cong gTm (cong L.lam (sym (cong (L._[ L.id L.,s Tm.q ]) (app-sub π₁ (fTm t))))) ⟩
  gTm (L.lam (L.app (fTm t L.[ π₁ ]) L.[ L.id ,s L.q ]))
                   ≡⟨ cong gTm (cong L.lam (cong fTm e)) ⟩
  gTm (L.lam (L.app (fTm t' L.[ π₁ ]) L.[ L.id ,s L.q ]))
                   ≡⟨ {!!} ⟩
  gTm (L.lam (L.app (fTm t')))
                   ≡⟨ cong gTm (L.lamη {t = fTm t'}) ⟩
  gTm (fTm t')
                   ≡⟨ gTm∘fTm t' ⟩
  t'
                   ∎
-}
