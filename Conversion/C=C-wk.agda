{-# OPTIONS --cubical --safe #-}
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence

open import Syntax.Ty

module Conversion.C=C-wk (T : TY) where

open import Syntax.Common T
open import Syntax.C T as C 
open import Syntax.C-wk T as C-wk

-- 
f : C.Tm τ → C-wk.Tm ε τ
f K = K
f S = S
f (t $ u) = f t $ f u
f (Kβ {x = u}{y = v} i) = Kβ {x = f u}{y = f v} i
f (Sβ {x = u}{y = v}{z = w} i) = Sβ {x = f u}{y = f v}{z = f w} i
f (lamKβ i) = lamKβ i
f (lamSβ i) = lamSβ i
f (lamwk$ i) = lamwk$ i
f (η {δ = δ} i) = η {δ = δ} i
f (TmSet a b c d i j) = TmSet (f a) (f b) (cong f c) (cong f d) i j

g : C-wk.Tm ε τ → C.Tm τ
g (t $ u)            = (g t) $ (g u)
g K                   = K
g S                   = S
g (η {δ = δ} i)       = η {δ = δ} i
g (lamKβ i)           = lamKβ i
g (lamSβ i)           = lamSβ i
g (lamwk$ i)          = lamwk$ i
g (Kβ{x = u}{v} i)    = Kβ{x = g u}{g v} i
g (Sβ{x = u}{v}{w} i) = Sβ{x = g u}{g v}{g w} i
g (TmSet a b c d i j) = TmSet (g a) (g b) (cong g c) (cong g d) i j

gf : (t : C.Tm τ) → g (f t) ≡ t
gf K = refl
gf S = refl
gf (t $ u) i = gf t i $ gf u i
gf (Kβ {x = u}{v} i) j = isSet→isSet' TmSet (λ j → K $ gf u j $ gf v j) (gf u) Kβ Kβ i j
gf (Sβ{x = c}{b}{a} i) j = isSet→isSet' TmSet (λ j → S $ gf c j $ gf b j $ gf a j) (λ j → gf c j $ gf a j $ (gf b j $ gf a j)) Sβ Sβ i j
gf (lamKβ i) = refl
gf (lamSβ i) = refl
gf (lamwk$ i) = refl
gf (η i) = refl
gf (TmSet a b c d i j) k = isGroupoid→isGroupoid' (isSet→isGroupoid TmSet) (λ j k → gf (c j) k) (λ j k → gf (d j) k) (λ i k → gf a k) (λ i k → gf b k) (λ i j → TmSet (g (f a)) (g (f b)) (λ i₁ → g (f (c i₁))) (λ i₁ → g (f (d i₁))) i j) (λ i j → TmSet a b c d i j) i j k

open import Syntax.C-wk.Induction

fg : (t : C-wk.Tm ε τ) → f (g t) ≡ t
fg K = refl
fg S = refl
fg (t $ u) i = fg t i $ fg u i
fg (Kβ {x = u}{v} i) j = isSet→isSet' TmSet (λ j → K $ fg u j $ fg v j) (fg u) Kβ Kβ i j
fg (Sβ{x = c}{b}{a} i) j = isSet→isSet' TmSet (λ j → S $ fg c j $ fg b j $ fg a j) (λ j → fg c j $ fg a j $ (fg b j $ fg a j)) Sβ Sβ i j
fg (lamKβ i) = refl
fg (lamSβ i) = refl
fg (lamwk$ i) = refl
fg (η i) = refl
fg (TmSet a b c d i j) k = isGroupoid→isGroupoid' (isSet→isGroupoid TmSet) (λ j k → fg (c j) k) (λ j k → fg (d j) k) (λ i k → fg a k) (λ i k → fg b k) (λ i j → TmSet (f (g a)) (f (g b)) (λ i₁ → f (g (c i₁))) (λ i₁ → f (g (d i₁))) i j) (λ i j → TmSet a b c d i j) i j k

eq : C.Tm τ ≡ C-wk.Tm ε τ
eq = ua (isoToEquiv (iso f g fg gf))
