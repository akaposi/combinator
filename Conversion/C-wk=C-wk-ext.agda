{-# OPTIONS --cubical --safe #-}

open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence

open import Syntax.Ty

module Conversion.C-wk=C-wk-ext (T : TY) where

open import Syntax.Common T
open import Syntax.C-wk     T as C-wk
open import Syntax.C-wk-ext T as C-wk-ext

f : C-wk.Tm Γ τ → C-wk-ext.Tm Γ τ
f q = q
f (wk t) = wk (f t)
f K = K
f S = S
f (t $ u) = f t $ f u
f (Kβ {x = x}{y = y} i) = Kβ {x = f x}{y = f y} i
f (Sβ {x = x}{y = y}{z = z} i) = Sβ {x = f x}{y = f y}{z = f z} i
f (wk$ {x = x}{y = y} i) = wk$ {x = f x}{y = f y} i
f (wkK i) = wkK i
f (wkS i) = wkS i
f (lamKβ i) = C-wk-ext.lamKβ i
f (lamSβ i) = C-wk-ext.lamSβ i
f (lamwk$ i) = C-wk-ext.lamwk$ i
f (η {δ = δ} i) = C-wk-ext.η {δ = δ} i
f (TmSet t t₁ x y i i₁) = TmSet (f t) (f t₁) (cong f x) (cong f y) i i₁

g : C-wk-ext.Tm Γ τ → C-wk.Tm Γ τ
g q = q
g (wk t) = wk (g t)
g K = K
g S = S
g (t $ u) = g t $ g u
g (Kβ {x = x}{y = y} i) = Kβ {x = g x}{y = g y} i
g (Sβ {x = x}{y = y}{z = z} i) = Sβ {x = g x}{y = g y}{z = g z} i
g (wk$ {x = x}{y = y} i) = wk$ {x = g x}{y = g y} i
g (wkK i) = wkK i
g (wkS i) = wkS i
g (ext x i) = C-wk.ext (cong g x) i
g (TmSet t t₁ x y i i₁) = TmSet (g t) (g t₁) (cong g x) (cong g y) i i₁

import Syntax.C-wk.Induction as C-wk

gf : (t : C-wk.Tm Γ τ) → g (f t) ≡ t
gf = C-wk.ind T (record
  { Tmᴰ = λ Γ τ t → g (f t) ≡ t
  ; TmᴰProp = TmSet _ _
  ; qᴰ = refl
  ; wkᴰ = cong wk
  ; Kᴰ = refl
  ; Sᴰ = refl
  ; _$ᴰ_ = λ e e' → cong₂ _$_ e e'
  })

import Syntax.C-wk-ext.Induction as C-wk-ext

fg : (t : C-wk-ext.Tm Γ τ) → f (g t) ≡ t
fg = C-wk-ext.ind T (record
  { Tmᴰ = λ _ _ t → f (g t) ≡ t
  ; TmᴰProp = TmSet _ _
  ; qᴰ = refl
  ; wkᴰ = cong wk
  ; Kᴰ = refl
  ; Sᴰ = refl
  ; _$ᴰ_ = λ e e' → cong₂ _$_ e e'
  })

eq : C-wk.Tm Γ τ ≡ C-wk-ext.Tm Γ τ
eq = ua (isoToEquiv (iso f g fg gf))
