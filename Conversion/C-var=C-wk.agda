{-# OPTIONS --cubical --safe #-}

open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.Univalence

open import Syntax.Ty

module Conversion.C-var=C-wk (T : TY) where

open import Syntax.Common T
open import Syntax.C-var T as C-var
open import Syntax.C-wk T as C-wk


fVar : C-var.Var Γ τ → C-wk.Tm Γ τ
fVar vz = q
fVar (vs x) = C-wk.wk (fVar x)

f : ∀{Γ}{τ} → C-var.Tm Γ τ → C-wk.Tm Γ τ
f (x $ x₁)            = f x $ f x₁
f K                   = K
f S                   = S
f (η{δ = δ} i)        = η-≡{δ = δ} i
f (Kβ{x = a}{b} i)    = Kβ{x = f a}{f b} i
f (Sβ{x = c}{b}{a} i) = Sβ{x = f c}{f b}{f a} i
f (var x)             = fVar x
f (TmSet a b c d i j) = TmSet (f a) (f b) (cong f c) (cong f d) i j 
f (lamKβ i)           = lamKβ'-≡ i
f (lamSβ i)           = lamSβ'-≡ i
f (lamwk$ i)          = lamwk$'-≡ i
                         
fwk : ∀{Γ}{τ}{σ}(x : C-var.Tm Γ τ) → f {Γ ▹ σ}{τ} (C-var.wk {τ = σ} x) ≡ C-wk.wk (f x)
fwk (var x) = refl
fwk K = sym wkK
fwk S = sym wkS
fwk (x $ x₁) = cong₂ C-wk._$_ (fwk x) (fwk x₁) ∙ sym C-wk.wk$
fwk {_}{_}{σ₁}(Kβ {Γ}{τ}{σ}{x = x}{y} i) j  =
  isSet→isSet' TmSet
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkK) (fwk x) ∙ sym C-wk.wk$) (fwk y) ∙ sym C-wk.wk$)
  (fwk x)
  (λ i → f {Γ ▹ σ₁} {τ} (C-var.wk {Γ} {τ} {σ₁} (Kβ {Γ} {τ} {σ} {x} {y} i)))
  (λ i → C-wk.wk {Γ} {τ} {σ₁} (f {Γ} {τ} (Kβ {Γ} {τ} {σ} {x} {y} i)))
  i j
fwk{_}{_}{σ₁} (Sβ{Γ}{τ}{σ}{δ}{x = x}{y}{z} i) j =
  isSet→isSet' TmSet
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (fwk x) ∙ sym C-wk.wk$) (fwk y) ∙ sym C-wk.wk$) (fwk z) ∙ sym C-wk.wk$)
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (fwk x) (fwk z) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (fwk y) (fwk z) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
  (λ i → f (C-var.wk{Γ}{δ}{σ₁} (Sβ {Γ}{τ}{σ}{δ}{x}{y}{z} i))) 
  (λ i → C-wk.wk (f (Sβ{Γ}{τ}{σ}{δ}{x}{y}{z} i)))
  i j
fwk (lamKβ i) j =
  isSet→isSet' TmSet
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
  (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
  (fwk K)
  (λ i → f (C-var.wk (lamKβ i)))
  (λ i → C-wk.wk (f (lamKβ i)))
  i j
fwk (lamSβ i) j  = 
  isSet→isSet' TmSet
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
 (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
 (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
 ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
 (λ i → f (C-var.wk (lamSβ i)))
 (λ i →  C-wk.wk (f (lamSβ i)))
 i j

fwk (lamwk$ i) j = 
  isSet→isSet' TmSet
  (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_   (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (sym wkK) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
  (λ i → f (C-var.wk (lamwk$ i)))
  (λ i → C-wk.wk (f (lamwk$ i)))
  i j
fwk (η{δ = δ} i) j = 
  isSet→isSet' TmSet
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (cong₂ C-wk._$_ (sym wkK) (sym wkS) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) (cong₂ C-wk._$_ (sym wkK) (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (sym wkK) ∙ sym C-wk.wk$) (sym wkK) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$) ∙ sym C-wk.wk$)
  (cong₂ C-wk._$_ (cong₂ C-wk._$_ (sym wkS) (sym wkK) ∙ sym C-wk.wk$) (sym wkK) ∙ sym C-wk.wk$)
  (λ i → f (C-var.wk (η{δ = δ} i)))
  (λ i → C-wk.wk (f (η{δ = δ} i))) i j
fwk (TmSet a b c d i j) k  = 
  isGroupoid→isGroupoid' (isSet→isGroupoid TmSet) (λ j → fwk (c j)) (λ j → fwk (d j)) (λ _ → fwk a) (λ _ →  fwk b) (λ i j → f (C-var.wk (TmSet a b c d i j))) (λ i j →  C-wk.wk (f (TmSet a b c d i j))) i j k
--------------------------------------

g : C-wk.Tm Γ τ → C-var.Tm Γ τ
g (x $ x₁)               = g x $ g x₁
g K                      = K
g S                      = S
g (η{δ = δ} i)           = η{δ = δ} i 
g q                      = var vz
g (wk x)                 = C-var.wk (g x)
g (Kβ{x = a}{b} i)       = Kβ{x = g a}{g b} i
g (Sβ{x = c}{b}{a} i)    = Sβ{x = g c}{g b}{g a} i
g (wk${x = b}{a} i)      = C-var.wk (g b $ g a)
g (wkK i)                = C-var.wk (g K)
g (wkS i)                = C-var.wk (g S)
g (lamKβ i)              = lamKβ i
g (lamSβ i)              = lamSβ i
g (lamwk$ i)             = lamwk$ i
g (TmSet a b c d i j)    = TmSet (g a) (g b) (cong g c) (cong g d) i j

gfVar : (x : Var Γ τ) → g (fVar x) ≡ var x
gfVar vz     = refl
gfVar (vs x) = cong C-var.wk (gfVar x)

--------------------------------------

gf : (x : C-var.Tm Γ τ) → g (f x) ≡ x
gf (x $ x₁) i             = gf x i $ gf x₁ i
gf K i                    = K
gf S i                    = S
gf (var x)                = gfVar x
gf (Kβ {x = a}{b} i) j    =
  isSet→isSet' TmSet (λ j → K $ gf a j $ gf b j) (gf a) Kβ Kβ i j
gf  (Sβ{x = c}{b}{a} i) j =
  isSet→isSet' TmSet (λ j → S $ gf c j $ gf b j $ gf a j) (λ j → gf c j $ gf a j $ (gf b j $ gf a j)) Sβ Sβ i j
gf (lamKβ i) j            =
  isSet→isSet' TmSet (λ j → S $ (K $ S) $ (S $ (K $ K))) (λ j → K) (λ i → g (f (lamKβ i))) lamKβ i j
gf (η{Γ}{τ}{σ}{δ} i) j  =
  isSet→isSet' TmSet (λ j → S $ (S $ (K $ S) $ K) $ (K $ (S $ K $ K{Γ} {τ} {δ}))) (λ j → S $ K $ K{Γ} {τ ⇒ σ} {δ}) (λ i → g (f (η{δ = δ} i))) η i j
gf (lamSβ i) j            =
  isSet→isSet' TmSet (λ j → S $ (K $ (S $ (K $ S))) $ (S $ (K $ S) $ (S $ (K $ S)))) (λ j → S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ (S $ (K $ (S $ (K $ S))) $ S)))) $ (K $ S))
  (λ i → g (f (lamSβ i))) lamSβ i j
gf (lamwk$ i) j           =
  isSet→isSet' TmSet (λ j → S $ (K $ K)) (λ j → S $ (S $ (K $ S) $ (S $ (K $ K) $ (S $ (K $ S) $ K))) $ (K $ K)) (λ i → g (f (lamwk$ i))) lamwk$ i j
gf (TmSet a b c d i j) k  = 
  isGroupoid→isGroupoid' (isSet→isGroupoid TmSet) (λ j k → gf (c j) k) (λ j k → gf (d j) k) (λ i k → gf a k) (λ i k → gf b k) (λ i j → TmSet (g (f a)) (g (f b)) (λ i₁ → g (f (c i₁)))
         (λ i₁ → g (f (d i₁))) i j) (λ i j → TmSet a b c d i j) i j k

--------------------------------------

open import Syntax.C-wk.Induction T

fg : ∀{Γ}{τ} → (t : C-wk.Tm Γ τ) → f (g t) ≡ t
fg {Γ}{τ} = ind (record
  { Tmᴰ = λ Γ τ t → f (g t) ≡ t
  ; TmᴰProp = TmSet _ _
  ; qᴰ = refl
  ; wkᴰ = λ {_}{_}{_}{t} e → _ ≡⟨ fwk (g t) ⟩ cong C-wk.wk e
  ; Kᴰ = refl
  ; Sᴰ = refl
  ; _$ᴰ_ = λ e e' → cong₂ _$_ e e'
  })

eq : C-var.Tm Γ τ ≡ C-wk.Tm Γ τ
eq = ua (isoToEquiv (iso f g fg gf))
