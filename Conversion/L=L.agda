{-# OPTIONS --cubical --safe #-}

open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Univalence
open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

module Conversion.L=L (T : TY) where

open import Syntax.Common T
open import Syntax.L T as L

f : Tm ε (Γ ⇒* τ) → Tm Γ τ
f {ε} t = t
f {Γ ▹ τ} t = app (f t)

g : Tm Γ τ → Tm ε (Γ ⇒* τ)
g {ε} t = t
g {Γ ▹ τ} t = g (lam t)

fg : (x : Tm Γ τ) → f (g x) ≡ x
fg {ε} x = refl
fg {Γ ▹ τ} x =  _ ≡⟨ cong app (fg (lam x)) ⟩ lamβ

gf : (x : Tm ε (Γ ⇒* τ)) → g{Γ} (f x) ≡ x
gf {ε} x = refl
gf {Γ ▹ τ} x =  _ ≡⟨ cong{y = f{Γ} x} g lamη ⟩ gf {Γ} x

eq : L.Tm ε (Γ ⇒* τ) ≡ L.Tm Γ τ
eq {Γ} = ua (isoToEquiv (iso f g (fg {Γ}) (gf {Γ})))
