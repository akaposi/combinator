{-# OPTIONS --cubical --safe #-}

open import Cubical.Foundations.Prelude hiding (Sub)

open import Syntax.Ty

module Conversion.C=L (T : TY) where

open import Syntax.Common T
import Syntax.C     T as C
import Syntax.C-wk  T as C-wk
import Syntax.L     T as L

import Conversion.C=C-wk     T as C=C-wk
import Conversion.C-wk=L     T as C-wk=L
import Conversion.L=L        T as L=L

eq : C.Tm (Γ ⇒* τ) ≡ L.Tm Γ τ
eq {Γ}{τ} =
  C.Tm (Γ ⇒* τ)
                        ≡⟨ C=C-wk.eq ⟩
  C-wk.Tm ε (Γ ⇒* τ)
                        ≡⟨ C-wk=L.eq ⟩
  L.Tm ε (Γ ⇒* τ)
                        ≡⟨ L=L.eq ⟩
  L.Tm Γ τ
                        ∎

f : C.Tm (Γ ⇒* τ) → L.Tm Γ τ
f t = L=L.f (C-wk=L.fTm (C=C-wk.f t))

g : L.Tm Γ τ → C.Tm (Γ ⇒* τ)
g t = C=C-wk.g (C-wk=L.gTm (L=L.g t))
